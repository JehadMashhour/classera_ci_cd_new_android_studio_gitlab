package com.classera.announcements

import android.view.ViewGroup
import com.classera.announcements.databinding.RowAnnouncementsBinding
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter


/**
 * Created by Rawan Al-Theeb on 1/1/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AnnouncementsAdapter(private val viewModel: AnnouncementsViewModel) :
    BasePagingAdapter<AnnouncementsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowAnnouncementsBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getAnnouncementCount()
    }

    inner class ViewHolder(binding: RowAnnouncementsBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowAnnouncementsBinding> {
                announcemetItem = viewModel.getAnnouncement(position)?.announcement
            }
        }
    }
}
