package com.classera.announcements

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.announcements.AnnouncementsRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 1/1/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AnnouncementsViewModelFactory @Inject constructor(
    private val announcementsRepository: AnnouncementsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AnnouncementsViewModel(announcementsRepository) as T
    }
}
