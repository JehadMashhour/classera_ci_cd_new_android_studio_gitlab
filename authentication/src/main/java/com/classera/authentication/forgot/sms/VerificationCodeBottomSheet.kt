package com.classera.authentication.forgot.sms

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import com.classera.authentication.R
import com.classera.core.fragments.BaseBottomSheetValidationDialogFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.observe
import com.classera.data.network.errorhandling.Resource
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class VerificationCodeBottomSheet : BaseBottomSheetValidationDialogFragment(), Validator.ValidationListener {

    @Inject
    lateinit var viewModel: VerificationCodeViewModel

    private var buttonVerify: Button? = null
    private var progressBar: ProgressBar? = null

    @NotEmpty(message = "validation_bottom_sheet_verification_code_code")
    private var editTextVerificationCode: EditText? = null

    override val layoutId: Int = R.layout.bottom_sheet_verification_code

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        buttonVerify = view?.findViewById(R.id.button_bottom_sheet_verification_code_verify)
        editTextVerificationCode = view?.findViewById(R.id.edit_text_bottom_sheet_verification_code_code)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_submit)

    }

    private fun initListeners() {
        buttonVerify?.setOnClickListener {
            validator.validate()
        }
    }

    override fun onValidationSucceeded() {
        activity?.hideKeyboard()
        val code = editTextVerificationCode?.text?.toString()
        val username = getUserName(arguments)
        viewModel.checkVerificationCode(code, username).observe(this, this::handleResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            buttonVerify?.text = null
            buttonVerify?.isEnabled = false
        } else {
            progressBar?.visibility = View.GONE
            buttonVerify?.setText(R.string.button_bottom_sheet_forgot_password)
            buttonVerify?.isEnabled = true
        }
    }

    private fun handleSuccessResource() {
        behavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun handleErrorResource(resource: Resource.Error) {
        val message = context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    companion object {

        private const val USERNAME_ARGUMENT = "username"

        fun show(supportFragmentManager: FragmentManager?, username: String?) {
            val sheet = VerificationCodeBottomSheet()
            sheet.arguments = bundleOf(USERNAME_ARGUMENT to username)
            sheet.show(supportFragmentManager!!, "")
        }

        private fun getUserName(bundle: Bundle?): String? {
            return bundle?.getString(USERNAME_ARGUMENT)
        }
    }
}
