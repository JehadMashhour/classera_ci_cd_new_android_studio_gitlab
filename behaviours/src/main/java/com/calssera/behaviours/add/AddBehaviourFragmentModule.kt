package com.calssera.behaviours.add

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.calssera.behaviours.BehavioursViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class AddBehaviourFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: BehavioursViewModelFactory
        ): AddBehaviourViewModel {
            return ViewModelProvider(fragment, factory)[AddBehaviourViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AddBehaviourFragment): Fragment
}
