@file:Suppress("DEPRECATION")

package com.calssera.behaviours.add

import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.DatePicker
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.calssera.behaviours.R
import com.calssera.behaviours.add.AddBehaviourViewModel.Companion.BEHAVIOR_TYPE_NEGATIVE
import com.calssera.behaviours.add.AddBehaviourViewModel.Companion.BEHAVIOR_TYPE_POSITIVE
import com.classera.core.Screen
import com.classera.core.fragments.BaseValidationFragment
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onItemClicked
import com.classera.data.models.BaseWrapper
import com.classera.data.models.user.User
import com.classera.data.network.errorhandling.Resource
import com.google.android.material.textfield.TextInputLayout
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@Screen("Add Behaviour")
class AddBehaviourFragment : BaseValidationFragment(), DatePickerDialog.OnDateSetListener {

    private var file: MediaFile? = null

    @Inject
    lateinit var viewModel: AddBehaviourViewModel

    private var progressDialog: ProgressDialog? = null
    private val args: AddBehaviourFragmentArgs by navArgs()

    private val currentDayCalendar = Calendar.getInstance()
    private var year: Int = currentDayCalendar.get(Calendar.YEAR)
    private var month: Int = currentDayCalendar.get(Calendar.MONTH)
    private var dayOfMonth: Int = currentDayCalendar.get(Calendar.DAY_OF_MONTH)

    @NotEmpty(message = "validation_fragment_add_behaviour_student")
    private var textViewStudent: AppCompatAutoCompleteTextView? = null

    @NotEmpty(message = "validation_fragment_add_behaviour_course")
    private var textViewCourse: AppCompatAutoCompleteTextView? = null

    @NotEmpty(message = "validation_fragment_add_behaviour_behaviour")
    private var textViewBehaviors: AppCompatAutoCompleteTextView? = null

    private var textViewBehaviorsActions: AppCompatAutoCompleteTextView? = null

    @NotEmpty(message = "validation_fragment_add_behaviour_group")
    private var textViewBehaviorGroup: AppCompatAutoCompleteTextView? = null

    @NotEmpty(message = "validation_fragment_add_behaviour_date")
    private var editTextDate: EditText? = null

    private var editTextDetails: EditText? = null

    private var textInputLayoutStudents: TextInputLayout? = null
    private var textInputLayoutCourses: TextInputLayout? = null
    private var textInputLayoutBehaviourGroup: TextInputLayout? = null
    private var textViewInputBehaviorsActions: TextInputLayout? = null
    private var textViewTopTitle: AppCompatTextView? = null
    private var radioGroupBehaviorTypes: RadioGroup? = null
    private var imageViewAttachment: ImageView? = null
    private var textViewAttachmentName: TextView? = null
    private var progressBar: ProgressBar? = null
    private var checkboxShowToGuardian: CheckBox? = null
    private var checkboxShowToStudent: CheckBox? = null
    private var buttonSubmit: Button? = null

    override val layoutId: Int = R.layout.fragment_add_behaviour

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.setBehaviour(args.behaviour)
        initProgressDialog()
        findViews()
        initListeners()
        handleEditModeViews()
        getCourses()
        getBehaviorGroup()
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(requireContext())
        progressDialog?.setMessage(getString(R.string.please_wait))
    }

    private fun findViews() {
        textViewStudent =
            view?.findViewById(R.id.auto_complete_text_view_fragment_add_behaviour_student)
        textInputLayoutStudents =
            view?.findViewById(R.id.text_input_layout__view_fragment_add_behaviour_student)
        textViewStudent?.setOnClickListener {
            textViewStudent?.hideKeyboard()
        }

        textViewCourse =
            view?.findViewById(R.id.auto_complete_text_view_fragment_add_behaviour_course)
        textInputLayoutCourses =
            view?.findViewById(R.id.text_input_layout_view_fragment_add_behaviour_course)
        textViewCourse?.setOnClickListener {
            textViewCourse?.hideKeyboard()
        }

        textViewBehaviorGroup =
            view?.findViewById(R.id.auto_complete_text_view_fragment_add_behaviour_group)
        textInputLayoutBehaviourGroup =
            view?.findViewById(R.id.text_input_layout_view_fragment_add_behaviour_group)
        textViewBehaviorGroup?.setOnClickListener {
            textViewBehaviorGroup?.hideKeyboard()
        }


        textViewBehaviors =
            view?.findViewById(R.id.auto_complete_text_view_fragment_add_behaviour_behaviors)
        textViewBehaviors?.setOnClickListener {
            textViewBehaviors?.hideKeyboard()
        }

        textViewBehaviorsActions =
            view?.findViewById(R.id.auto_complete_text_view_fragment_add_behaviour_actions)
        textViewBehaviorsActions?.setOnClickListener {
            textViewBehaviorsActions?.hideKeyboard()
        }

        textViewInputBehaviorsActions =
            view?.findViewById(R.id.text_input_text_view_fragment_add_behaviour_actions)
        textViewInputBehaviorsActions?.setOnClickListener {
            textViewInputBehaviorsActions?.hideKeyboard()
        }

        textViewTopTitle = view?.findViewById(R.id.text_view_fragment_add_behaviour_top_title)
        radioGroupBehaviorTypes = view?.findViewById(R.id.radio_group_fragment_add_behavior_types)
        editTextDate = view?.findViewById(R.id.edit_text_fragment_add_behavior)
        editTextDetails = view?.findViewById(R.id.edit_text_fragment_add_behavior_details)
        imageViewAttachment = view?.findViewById(R.id.image_view_fragment_add_behaviour_attachment)
        textViewAttachmentName = view?.findViewById(R.id.text_view_fragment_add_behaviour_file_name)
        buttonSubmit = view?.findViewById(R.id.button_fragment_add_behaviour_submit)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_add_behaviour)
        checkboxShowToGuardian =
            view?.findViewById(R.id.checkbox_fragment_add_behavior_show_to_guardian)
        checkboxShowToStudent =
            view?.findViewById(R.id.checkbox_fragment_add_behavior_show_to_student)
    }

    private fun handleEditModeViews() {
        if (viewModel.isEditMode()) {
            radioGroupBehaviorTypes?.visibility = View.GONE
            textInputLayoutCourses?.visibility = View.GONE
            textInputLayoutBehaviourGroup?.visibility = View.GONE
            textInputLayoutStudents?.visibility = View.GONE
        }
    }

    private fun fillViewsInEditMode() {
        if (args.behaviour != null) {
            buttonSubmit?.text = getString(R.string.title_menu_behavior_teacher_actions_edit)
            textViewTopTitle?.text =
                getString(R.string.title_menu_behavior_teacher_actions_edit_behaviour)
            textViewCourse?.setText(viewModel.getBehaviour()?.courseTitle, false)
            textViewBehaviorGroup?.setText(viewModel.getBehaviour()?.behaviorGroupTitle, false)
            textViewStudent?.setText(viewModel.getBehaviour()?.studentName, false)
            textViewBehaviors?.setText(viewModel.getBehaviour()?.behaviorTitle, false)
            textViewBehaviorsActions?.setText(viewModel.getBehaviour()?.actionTitle, false)
            editTextDate?.setText(viewModel.getBehaviour()?.date)
            editTextDetails?.setText(viewModel.getBehaviour()?.details)
            checkboxShowToGuardian?.isChecked = viewModel.getBehaviour()?.showToGuardian == true
            checkboxShowToStudent?.isChecked = viewModel.getBehaviour()?.showToStudent == true

            getStudentsByCourse(viewModel.getSelectedCoursePosition())
            getBehaviorsAndActions(viewModel.getSelectedBehaviourGroupPosition())
        }
    }

    private fun initListeners() {
        radioGroupBehaviorTypes?.setOnCheckedChangeListener { _, _ ->
            getBehaviorGroup()
            textViewBehaviorGroup?.text = null
        }
        editTextDate?.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                year,
                month,
                dayOfMonth
            ).show()
        }
        imageViewAttachment?.setOnClickListener {
            val intent = Intent(requireContext(), FilePickerActivity::class.java)
            intent.putExtra(
                FilePickerActivity.CONFIGS, Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(true)
                    .setShowImages(true)
                    .enableImageCapture(false)
                    .setSingleChoiceMode(true)
                    .setSkipZeroSizeFiles(true)
                    .build()

            )
            startActivityForResult(intent, FILE_REQUEST_CODE)
        }
        buttonSubmit?.setOnClickListener { validator.validate(true) }

        textViewStudent?.onItemClicked(viewModel::onStudentSelected)
        textViewCourse?.onItemClicked(viewModel::onCourseSelected)
        textViewBehaviors?.onItemClicked(viewModel::onBehaviourSelected)
        textViewBehaviorsActions?.onItemClicked(viewModel::onActionSelected)
        textViewBehaviorGroup?.onItemClicked(viewModel::onGroupSelected)
    }

    private fun getBehaviorGroup() {
        val behaviorType: String = if (viewModel.isEditMode()) {
            viewModel.getBehaviour()?.behaviorGroupType.orEmpty()
        } else {
            getBehaviorGroupType()
        }
        viewModel.getBehaviorGroup(behaviorType).observe(this, ::handleBehaviorGroupResource)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        this.year = year
        this.month = month
        this.dayOfMonth = dayOfMonth
        currentDayCalendar.set(Calendar.YEAR, year)
        currentDayCalendar.set(Calendar.MONTH, month)
        currentDayCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        val myFormat = "yyyy-MM-dd"
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        editTextDate?.setText(sdf.format(currentDayCalendar.time))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FILE_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val files = data.getParcelableArrayListExtra<MediaFile>(FilePickerActivity.MEDIA_FILES)
            file = files?.firstOrNull()
            textViewAttachmentName?.text = file?.name
        }
    }

    private fun getCourses() {
        viewModel.getCourses().observe(this, ::handleCoursesResource)
    }

    private fun getBehaviorGroupType() = when (this.radioGroupBehaviorTypes?.checkedRadioButtonId) {
        R.id.radio_button_fragment_add_behavior_positive_type -> {
            BEHAVIOR_TYPE_POSITIVE
        }
        R.id.radio_button_fragment_add_behavior_negative_type -> {
            BEHAVIOR_TYPE_NEGATIVE
        }
        else -> {
            BEHAVIOR_TYPE_POSITIVE
        }
    }

    private fun handleCoursesResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCoursesLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCoursesSuccessResource()
            }
            is Resource.Error -> {
                handleCoursesErrorResource(resource)
            }
        }
    }

    private fun handleBehaviorGroupResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleBehaviorGroupLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleBehaviorGroupSuccessResource()
            }
            is Resource.Error -> {
                handleBehaviorGroupErrorResource(resource)
            }
        }
    }

    private fun handleBehaviorGroupLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.dismiss()
        }
    }

    private fun handleCoursesLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.dismiss()
        }
    }

    private fun handleCoursesSuccessResource() {
        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            viewModel.getCourseTitles()
        )
        textViewCourse?.setAdapter(adapter)
        textViewCourse?.setOnItemClickListener { _, _, position, _ ->
            textViewStudent?.text = null
            getStudentsByCourse(position)
        }
        fillViewsInEditMode()
    }

    private fun handleBehaviorGroupSuccessResource() {
        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            viewModel.getBehaviorGroupTitles()
        )
        textViewBehaviorGroup?.setAdapter(adapter)
        textViewBehaviorGroup?.setOnItemClickListener { _, _, position, _ ->
            getBehaviorsAndActions(position)
            textViewBehaviorsActions?.text = null
            textViewBehaviors?.text = null
        }
    }

    private fun getStudentsByCourse(position: Int) {
        viewModel.getStudentsByCourse(position).observe(this, ::handleStudentsResource)
    }

    private fun getBehaviorsAndActions(position: Int) {
        viewModel.getBehaviors(position).observe(this, ::handleBehaviorActionsResource)
    }

    private fun handleStudentsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleStudentsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleStudentsSuccessResource(resource as Resource.Success<BaseWrapper<List<User>>>)
            }
            is Resource.Error -> {
                handleStudentsErrorResource(resource)
            }
        }
    }

    private fun handleBehaviorActionsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleBehaviorActionsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleBehaviorActionsSuccessResource()
            }
            is Resource.Error -> {
                handleBehaviorActionsErrorResource(resource)
            }
        }
    }

    private fun handleBehaviorActionsLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.dismiss()
        }
    }

    private fun handleStudentsLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.dismiss()
        }
    }

    private fun handleStudentsSuccessResource(resource: Resource.Success<BaseWrapper<List<User>>>) {
        val students = resource.data?.data
        val adapter = StudentsAdapter(students ?: listOf())
        textViewStudent?.setAdapter(adapter)
    }

    private fun handleBehaviorActionsSuccessResource() {

        if(viewModel.getBehaviorActionsTitles().isEmpty()){
            textViewBehaviorsActions?.visibility = View.GONE
            textViewInputBehaviorsActions?.visibility = View.GONE
        }

        val behaviorsAdapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            viewModel.getBehaviorsTitles()
        )
        val behaviorActionsAdapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            viewModel.getBehaviorActionsTitles()
        )
        textViewBehaviors?.setAdapter(behaviorsAdapter)
        if (viewModel.isEditMode()) {
            viewModel.onBehaviourSelected(viewModel.getSelectedBehaviourPosition())
        }

        if (viewModel.actionsFound()) {
            textViewBehaviorsActions?.visibility = View.VISIBLE
            textViewInputBehaviorsActions?.visibility = View.VISIBLE
            textViewBehaviorsActions?.setAdapter(behaviorActionsAdapter)
            textViewBehaviorsActions?.visibility = View.VISIBLE
            textViewInputBehaviorsActions?.visibility = View.VISIBLE
            if (viewModel.isEditMode()) {
                viewModel.onActionSelected(viewModel.getSelectedActionPosition())
            }
        } else {
            textViewBehaviorsActions?.visibility = View.GONE
            textViewInputBehaviorsActions?.visibility = View.GONE
        }
    }

    private fun handleStudentsErrorResource(resource: Resource.Error) {
        resource.error.message?.let {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }

    private fun handleCoursesErrorResource(resource: Resource.Error) {
        resource.error.message?.let {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }

    private fun handleBehaviorGroupErrorResource(resource: Resource.Error) {
        resource.error.message?.let {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }

    private fun handleBehaviorActionsErrorResource(resource: Resource.Error) {
        textViewBehaviorsActions?.visibility = View.GONE
        textViewInputBehaviorsActions?.visibility = View.GONE
        resource.error.message?.let {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }

    override fun onValidationSucceeded() {
        val showToGuardian = if (checkboxShowToGuardian?.isChecked == true) "1" else "0"
        val showToStudent = if (checkboxShowToStudent?.isChecked == true) "1" else "0"
        val path = file?.path ?: ""
        val mimeType = file?.mimeType ?: ""

        val data: MutableMap<String, String?> = mutableMapOf(
            "date" to editTextDate?.text?.toString(),
            "semester_id" to viewModel.getSemesterId(),
            "show_to_student" to showToStudent,
            "show_to_guardian" to showToGuardian,
            "details" to editTextDetails?.text?.toString()
        )

        if (!viewModel.isEditMode()) {
            if (viewModel.actionsFound()) {
                data["action_id"] = viewModel.getSelectedActionId()
            }
            data["student_id"] = viewModel.getSelectedStudentId()
            data["lecture_id"] = viewModel.getSelectedLectureId()
            data["behavior_id"] = viewModel.getSelectedBehaviourId()
            viewModel.addBehaviour(data, path, mimeType).observe(
                this,
                ::handleAddBehaviourResource
            )
        } else {
            if (viewModel.actionsFound()) {
                data["action_id"] = viewModel.getBehaviour()?.actionId
            }
            data["id"] = viewModel.getBehaviour()?.id
            data["student_id"] = viewModel.getBehaviour()?.studentId
            data["lecture_id"] = viewModel.getBehaviour()?.lectureId
            data["behavior_id"] = viewModel.getBehaviour()?.behaviorId

            viewModel.editBehaviour(data, path, mimeType)
                .observe(this, ::handleAddBehaviourResource)
        }
    }

    private fun handleAddBehaviourResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleAddBehaviourLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleAddBehaviourSuccessResource()
            }
            is Resource.Error -> {
                handleAddBehaviourErrorResource(resource)
            }
        }
    }

    private fun handleAddBehaviourLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            buttonSubmit?.text = null
            buttonSubmit?.isEnabled = false
            progressBar?.visibility = View.VISIBLE
        } else {
            buttonSubmit?.text = if (viewModel.isEditMode()) {
                getString(R.string.title_menu_behavior_teacher_actions_edit)
            } else {
                getString(R.string.button_fragment_add_behaviour)
            }
            buttonSubmit?.isEnabled = true
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleAddBehaviourSuccessResource() {
        findNavController().popBackStack()
    }

    private fun handleAddBehaviourErrorResource(resource: Resource.Error) {
        resource.error.message?.let {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }

    private companion object {

        private const val FILE_REQUEST_CODE = 20
    }
}
