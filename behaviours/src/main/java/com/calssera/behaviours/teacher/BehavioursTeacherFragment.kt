package com.calssera.behaviours.teacher

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.calssera.behaviours.BehavioursAdapter
import com.calssera.behaviours.BehavioursFragment
import com.calssera.behaviours.BehavioursViewModel
import com.calssera.behaviours.R
import com.classera.data.models.user.UserRole
import com.classera.data.prefs.Prefs
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

class BehavioursTeacherFragment : BehavioursFragment() {

    @Inject
    lateinit var viewModel: BehavioursTeacherViewModel

    @Inject
    lateinit var adapter: BehavioursTeacherAdapter

    @Inject
    lateinit var prefs: Prefs

    private var floatingActionButtonAdd: FloatingActionButton? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
        initViewModelListeners()

        floatingActionButtonAdd?.visibility = View.VISIBLE
    }

    private fun findViews() {
        floatingActionButtonAdd =
            view?.findViewById(R.id.floating_action_button_fragment_behavior_add)
    }

    private fun initListeners() {
        floatingActionButtonAdd?.setOnClickListener {
            findNavController().navigate(BehavioursTeacherFragmentDirections.addBehaviorDirection())
        }
    }

    override fun getBehaviorViewModel(): BehavioursViewModel {
        return viewModel
    }

    override fun getBehaviorAdapter(): BehavioursAdapter<*> {
        return adapter
    }

    override fun handleAdapterListeners() {
        adapter.setOnItemClickListener { view, position ->
            when (view.id) {
                R.id.image_view_row_behavior_more -> {
                    handleMoreClicked(view, position)
                }
                else -> {
                    val behavioursResponse = viewModel.getBehaviour(position)
                    behavioursResponse?.let {
                        findNavController().navigate(
                            BehavioursTeacherFragmentDirections.behaviorsDetailsDirection(
                                behavioursResponse.behaviorTitle,
                                behavioursResponse
                            ).apply {
                                role = prefs.userRole ?: UserRole.TEACHER
                            }
                        )
                    }
                }
            }
        }
    }

    private fun handleMoreClicked(view: View, position: Int) {
        val menu = PopupMenu(requireContext(), view)
        menu.menuInflater.inflate(R.menu.menu_behavior_teacher_actions, menu.menu)
        menu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.item_menu_behaviour_teacher_actions_delete -> {
                    handleDeleteItemClicked(position)
                }
                R.id.item_menu_behaviour_teacher_actions_edit -> {
                    findNavController().navigate(
                        BehavioursTeacherFragmentDirections.addBehaviorDirection(
                        ).apply {
                            behaviour = viewModel.getBehaviour(position)
                        }
                    )
                }
            }
            return@setOnMenuItemClickListener true
        }
        menu.show()
    }

    private fun handleDeleteItemClicked(position: Int) {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.title_delete_behavior_dialog)
            .setMessage(
                getString(
                    R.string.message_delete_behavior_dialog,
                    viewModel.getBehaviour(position)?.behaviorTitle
                )
            )
            .setPositiveButton(R.string.button_positive_delete_behavior_dialog) { _, _ ->
                viewModel.deleteBehaviour(
                    position
                )
            }
            .setNegativeButton(R.string.button_negative_delete_behavior_dialog, null)
            .show()
    }

    private fun initViewModelListeners() {
        viewModel.notifyItemRemovedLiveData.observe(this) { adapter.notifyItemRemoved(it) }
    }

}
