package com.calssera.behaviours.teacher

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.calssera.behaviours.BehavioursViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class BehavioursTeacherFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: BehavioursViewModelFactory
        ): BehavioursTeacherViewModel {
            return ViewModelProvider(fragment, factory)[BehavioursTeacherViewModel::class.java]
        }


        @Provides
        @JvmStatic
        fun provideBehavioursAdapter(viewModel: BehavioursTeacherViewModel): BehavioursTeacherAdapter {
            return BehavioursTeacherAdapter(viewModel)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: BehavioursTeacherFragment): Fragment
}
