package com.classera.chat.groupusers

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BasePaginationWrapper
import com.classera.data.models.BaseWrapper
import com.classera.data.models.chat.Role
import com.classera.data.models.chat.ThreadUser
import com.classera.data.models.filter.Filterable
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.network.errorhandling.tryThirdPartyResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.chat.ChatRepository
import kotlinx.coroutines.Dispatchers

/**
 * Created on 25/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatGroupUsersViewModel(
    private val chatRepository: ChatRepository,
    private val prefs: Prefs,
    chatGroupUsersFragmentArgs: ChatGroupUsersFragmentArgs
) : BaseViewModel() {

    private val users = mutableListOf<ThreadUser>()
    var selectedEditUsers = listOf<String>()
    private var filterRoles: List<Role?>? = null

    init {
        chatGroupUsersFragmentArgs.selectedEditUsers?.let { selectedEditUsersArg ->
            selectedEditUsers = selectedEditUsersArg.toList()
        }

        Log.d("testThreadID", "    selectedEditUsers     $selectedEditUsers")
    }

    fun getUsers(
        limit: String, pagination: String? = null,
        searchName: CharSequence? = "", roleID: String? = ""
    ): LiveData<Resource> {
        return getUsers(limit, pagination, searchName, roleID, true)
    }

    fun refreshUsers(
        limit: String, pagination: String? = null,
        searchName: CharSequence? = "", roleID: String? = ""
    ) = getUsers(
        limit, pagination,
        searchName, roleID, false
    )

    private fun getUsers(
        limit: String, pagination: String? = null,
        searchName: CharSequence? = "", roleID: String? = "", showProgress: Boolean
    ) = liveData(Dispatchers.IO) {

        if (users.isNotEmpty()) {
            emit(Resource.Success(users))
            return@liveData
        }

        if (showProgress) {
            emit(Resource.Loading(show = true))
        }

        val resource = tryThirdPartyResource {
            chatRepository.getGroupThreads(
                limit,
                pagination,
                searchName,
                roleID
            )
        }

        if (pagination == null) {
            users.clear()
        }

        users.addAll(
            (resource.element<BasePaginationWrapper<List<ThreadUser>>>()?.data) ?: mutableListOf()
        )

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getUserCount(): Int {
        return getFilteredUsers().size
    }

    fun getUser(position: Int): ThreadUser? {
        return getFilteredUsers()[position]
    }

    fun toggleFilter(position: Int) {
        val selectable = getFilteredUsers()[position]
        selectable.selected = selectable.selected.not() == true
    }

    fun getSelectedUsers(): Array<ThreadUser> {
        return getFilteredUsers().filter { it.selected }.toTypedArray()
    }

    private fun getFilteredUsers(): Array<ThreadUser> {
        return users.filter { !selectedEditUsers.contains(it.userId) }.toTypedArray()
    }

    fun getRoles() = liveData(Dispatchers.IO) {
        val resource = tryResource { chatRepository.getRoles() }
        filterRoles = resource.element<BaseWrapper<List<Role>>>()?.data ?: mutableListOf<Role>()

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getFilterRoles(): Array<out Filterable?>? {
        return filterRoles?.toTypedArray()
    }

    fun getUserId(): String? {
        return prefs.userId
    }
}
