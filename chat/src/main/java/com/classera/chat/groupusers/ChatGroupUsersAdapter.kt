package com.classera.chat.groupusers

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.classera.chat.databinding.RowThreadGroupUserBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder

/**
 * Created on 25/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatGroupUsersAdapter(private val viewModel: ChatGroupUsersViewModel) :
    BaseAdapter<ChatGroupUsersAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowThreadGroupUserBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getUserCount()
    }

    inner class ViewHolder(binding: RowThreadGroupUserBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowThreadGroupUserBinding> {
                threadGroupUser = viewModel.getUser(position)
            }

            itemView.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    viewModel.toggleFilter(clickedPosition)
                    notifyDataSetChanged()
                }
            }
        }
    }

}
