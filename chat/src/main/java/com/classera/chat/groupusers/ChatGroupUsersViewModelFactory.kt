package com.classera.chat.groupusers

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.chat.ChatRepository
import javax.inject.Inject

/**
 * Created on 25/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatGroupUsersViewModelFactory @Inject constructor(
    private val chatRepository: ChatRepository,
    private val prefs: Prefs,
    private val chatGroupUsersFragmentArgs: ChatGroupUsersFragmentArgs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ChatGroupUsersViewModel(
            chatRepository,
            prefs,
            chatGroupUsersFragmentArgs
        ) as T
    }
}
