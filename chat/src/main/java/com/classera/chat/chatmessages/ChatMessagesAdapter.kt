package com.classera.chat.chatmessages

import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageButton
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.classera.chat.BR
import com.classera.chat.R
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.data.models.chat.Message
import com.classera.data.models.chat.MessageMediaType
import com.classera.data.models.chat.MessageTypeEnum

/**
 * Created by Odai Nazzal on 1/11/2020, Modified by Lana Manaseer & Mohammad Hamdan
 * Classera
 *
 * o.nazzal@classera.com
 */
class ChatMessagesAdapter(
    private val viewModel: ChatMessagesViewModel
) : BasePagingAdapter<ChatMessagesAdapter.ViewHolder>() {

    init {
        disableAnimations()
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(inflater!!, viewType, parent, false)
        return ViewHolder(binding)
    }

    @Suppress("ReturnCount")
    override fun getItemViewType(position: Int): Int {
        if (viewModel.getMessageCount() == 0) {
            return super.getItemViewType(position)
        }
        val message = viewModel.getMessage(position)
        if (message?.body?.getMediaType() != null && message.body?.getMediaType() != MessageMediaType.UNKNOWN) {
            return if (viewModel.isSender(message.fromId)) {
                message.body?.getMediaType()?.senderLayout!!
            } else {
                message.body?.getMediaType()?.receiverLayout!!
            }
        }

        return if (message?.type?.containsTwoDirections == true) {
            getTwoDirectionsViewType(message)
        } else {
            message?.type?.firstLayout!!
        }
    }

    private fun getTwoDirectionsViewType(message: Message): Int {
        return if (!viewModel.isSender(message.fromId)) {
            message.type?.secondLayout!!
        } else {
            message.type?.firstLayout!!
        }
    }

    override fun getItemsCount(): Int {
        return viewModel.getMessageCount()
    }

    inner class ViewHolder(private val binding: ViewDataBinding) : BaseBindingViewHolder(binding) {

        private var mediaView: View? = null
        private var removeUploadAttachment: AppCompatImageButton? = null
        private var retryUploadAttachment: AppCompatButton? = null
        private var relativeLayoutPlay: RelativeLayout? = null

        init {
            mediaView = itemView.findViewById(R.id.view_media)
            removeUploadAttachment = itemView.findViewById(R.id.remove_upload_attachment)
            retryUploadAttachment = itemView.findViewById(R.id.retry_upload_attachment)
            relativeLayoutPlay = itemView.findViewById(R.id.relative_layout_row_message_audio_play)
        }

        override fun bind(position: Int) {
            binding.setVariable(BR.message, viewModel.getMessage(position))
            binding.executePendingBindings()

            mediaView?.setOnClickListener {
                handleItemClicked(adapterPosition)
            }

            removeUploadAttachment?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    getOnItemClickListener()?.invoke(it, clickedPosition)
                }
            }

            retryUploadAttachment?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    getOnItemClickListener()?.invoke(it, clickedPosition)
                }
            }

            relativeLayoutPlay?.setOnClickListener { view ->
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    getOnItemClickListener()?.invoke(view, clickedPosition)
                }
            }
        }

        private fun handleItemClicked(position: Int) {
            if (position == RecyclerView.NO_POSITION) {
                return
            }
            if (viewModel.getMessage(position)?.type != MessageTypeEnum.TEXT) {
                val intent = Intent(Intent.ACTION_VIEW)
                val message = viewModel.getMessage(position)
                val mimeType =
                    MimeTypeMap.getSingleton().getMimeTypeFromExtension(message?.body?.getFileExtension())
                intent.setDataAndType(Uri.parse(message?.body?.url), mimeType)
                if (intent.resolveActivity(context!!.packageManager) != null) {
                    context?.startActivity(intent)
                } else {
                    Toast.makeText(
                        context,
                        context?.getString(R.string.toast_chat_messages_adapter_no_activity_found),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }
}
