package com.classera.chat.chatmessages

import android.Manifest.permission.RECORD_AUDIO
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Bundle
import android.util.SparseArray
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import com.classera.chat.R
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.chat.Message
import com.classera.data.network.errorhandling.Resource
import com.esafirm.imagepicker.features.ImagePicker
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import com.varunjohn1990.audio_record_view.AudioRecordView
import com.varunjohn1990.audio_record_view.AudioRecordView.RecordingListener
import java.io.File
import javax.inject.Inject

/**
 * Created on 26/02/2020.
 * Classera
 *
 * @author Lana Manaseer, Modified by Mohammad Hamdan
 */
@Screen("Chat Messages")
@Suppress("MagicNumber", "TooManyFunctions")
class ChatMessagesFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: ChatMessagesViewModel

    private var startRecordTime = 0L
    private var mediaPlayer: MediaPlayer? = null
    private var recordFile: File? = null
    private var audioRecordView: AudioRecordView? = null
    private var recyclerView: RecyclerView? = null
    private var adapter: ChatMessagesAdapter? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var blockedString: String? = null
    private var mediaRecorder: MediaRecorder? = null
    private var message: Message? = null
    private var updateProgressRunnable: Runnable? = Runnable {
        val currentPosition = mediaPlayer?.currentPosition ?: 0
        message?.maxProgress = mediaPlayer?.duration ?: 0
        message?.playingProgress = currentPosition

        val seconds = (currentPosition / 1000) % 60
        val minutes = (currentPosition / (1000 * 60) % 60)
        val currentTime = "%d:%02d".format(minutes, seconds)

        message?.currentTime = currentTime
        startUpdateProgress()
    }
    private val sparseArray: SparseArray<String> = SparseArray()

    override val layoutId: Int = R.layout.fragment_chat_messages

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(viewModel)
        audioRecordView = AudioRecordView()
        findViews()
        checkBlockedMeUser()
        initViewModelListeners()
        initListeners()
        getListMessages()
        initSparseArray()

        changeTypeMessageText()
    }

    private fun findViews() {
        val view: FrameLayout? = view?.findViewById(R.id.chatLayoutMain)
        view?.removeAllViews()
        audioRecordView?.initView(view)
        val containerView = audioRecordView?.setContainerView(R.layout.chat_messages_container)

        recyclerView = containerView?.findViewById(R.id.recycler_view_fragment_chat_messages)
        progressBar = containerView?.findViewById(R.id.progress_bar_fragment_chat_messages)
        errorView = containerView?.findViewById(R.id.error_view_fragment_chat_messages)

        audioRecordView?.setAudioRecordButtonImage(R.drawable.ic_mic_circle)
        audioRecordView?.setSendButtonImage(R.drawable.ic_send_circle)
        recyclerView?.itemAnimator = DefaultItemAnimator()
        audioRecordView?.removeAttachmentOptionAnimation(false)
    }

    private fun checkBlockedMeUser() {
        viewModel.resetIsBlocked()
        if (viewModel.isGroup == false) {
            viewModel.checkBlockedUser().observe(this) {
                if (viewModel.userBlockMeStatus.value == true) {
                    audioRecordView?.messageView?.visibility = View.INVISIBLE
                } else {
                    audioRecordView?.messageView?.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun initViewModelListeners() {
        viewModel.newMessageLiveData.observe(this) {
            if (adapter?.getItemsCount() == 1) {
                adapter?.notifyDataSetChanged()
                return@observe
            }
            adapter?.notifyItemInserted(0)
            recyclerView?.scrollToPosition(0)
        }
    }

    private fun initListeners() {
        adapter?.setOnLoadMoreListener { getListMessages() }
        audioRecordView?.recordingListener = object : RecordingListener {
            override fun onRecordingStarted() {
                startRecordTime = System.currentTimeMillis()
                if (requestRecordAudioPermission()) {
                    startRecording()
                }
            }

            override fun onRecordingLocked() {
                // No impl
            }

            override fun onRecordingCompleted() {
                stopRecording()
                if ((System.currentTimeMillis() - startRecordTime) < MIN_RECORDING_TIME) {
                    Toast.makeText(requireContext(), R.string.record_is_too_short, Toast.LENGTH_LONG).show()
                    return
                }
                if (recordFile != null) {
                    startUploadAttachment(recordFile!!)
                }
            }

            override fun onRecordingCanceled() {
                stopRecording()
            }
        }

        audioRecordView?.showEmojiIcon(false)
        audioRecordView?.cameraView?.setOnClickListener {
            disableAttachmentViewsTemp()
            ImagePicker.create(this)
                .showCamera(true)
                .single()
                .start()
        }

        audioRecordView?.sendView?.setOnClickListener {
            val message = audioRecordView?.messageView?.text.toString().trim()
            audioRecordView?.messageView?.setText("")
            viewModel.addNewMessage(message)
        }

        audioRecordView?.attachmentView?.setOnClickListener {
            disableAttachmentViewsTemp()
            val intent = Intent(requireContext(), FilePickerActivity::class.java)
            intent.putExtra(
                FilePickerActivity.CONFIGS, Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(true)
                    .setShowImages(true)
                    .enableImageCapture(false)
                    .setSingleChoiceMode(true)
                    .setSkipZeroSizeFiles(true)
                    .build()
            )
            startActivityForResult(intent, FILE_REQUEST_CODE)
        }
    }

    private fun requestRecordAudioPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(context!!, RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            ActivityCompat.requestPermissions(activity!!, arrayOf(RECORD_AUDIO), RC_AUDIO_RECORD_PERMISSION)
            false
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == RC_AUDIO_RECORD_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startRecording()
        }
    }

    private fun startRecording() {
        recordFile = File.createTempFile("audio", "tmp.mp3", context?.cacheDir)
        recordFile?.deleteOnExit()

        mediaRecorder = MediaRecorder()
        mediaRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
        mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
        mediaRecorder?.setAudioSamplingRate(AUDIO_SAMPLING_RATE)
        mediaRecorder?.setAudioEncodingBitRate(AUDIO_ENCODING_BIT_RATE)
        mediaRecorder?.setOutputFile(recordFile?.absolutePath)
        mediaRecorder?.prepare()
        mediaRecorder?.start()
    }

    private fun stopRecording() {
        try {
            mediaRecorder?.stop()
            mediaRecorder?.release()
        } catch (e: Exception) {
            // No impl
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val images = ImagePicker.getImages(data)
            val firstImagePath = images.firstOrNull()?.path
            val firstImageFile = File(firstImagePath ?: "")
            startUploadAttachment(firstImageFile)
            disableAttachmentViews()
        } else if (requestCode == FILE_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val files = data.getParcelableArrayListExtra<MediaFile>(FilePickerActivity.MEDIA_FILES)
            val file = File(files?.firstOrNull()?.path ?: "")
            startUploadAttachment(file)
            disableAttachmentViews()
        }
    }

    private fun disableAttachmentViews() {
        audioRecordView?.cameraView?.isEnabled = false
        audioRecordView?.attachmentView?.isEnabled = false
    }

    private fun disableAttachmentViewsTemp() {
        audioRecordView?.cameraView?.isEnabled = false
        audioRecordView?.cameraView?.postDelayed({
            audioRecordView?.cameraView?.isEnabled = true
        }, 1000)
        audioRecordView?.attachmentView?.isEnabled = false
        audioRecordView?.attachmentView?.postDelayed({
            audioRecordView?.attachmentView?.isEnabled = true
        }, 1000)
    }

    private fun enableAttachmentViews() {
        audioRecordView?.cameraView?.isEnabled = true
        audioRecordView?.attachmentView?.isEnabled = true
    }

    private fun startUploadAttachment(firstImageFile: File) {
        viewModel.uploadAttachment(firstImageFile).observe(this) {
            handleUploadAttachmentResource(it)
        }
        if (adapter?.getItemsCount() == 1) {
            adapter?.notifyDataSetChanged()
            return
        }
        adapter?.notifyItemInserted(0)
        recyclerView?.scrollToPosition(0)
    }

    private fun handleUploadAttachmentResource(resource: Resource) {
        if (resource.isSuccess()) {
            enableAttachmentViews()
        } else if (resource is Resource.Error) {
            enableAttachmentViews()
            viewModel.removeBrokenAttachment(
                sparseArray.get(resource.error.code!!) ?: getString(resource.error.resourceMessage)
            )
        }
    }

    private fun getListMessages() {
        viewModel.getListMessages().observe(this, ::handleResource)
    }

    private fun initSparseArray() {
        sparseArray.put(413, getString(R.string.exceed_size))
        sparseArray.put(500, getString(R.string.something_went_wrong))
        sparseArray.put(502, getString(R.string.something_went_wrong))
        sparseArray.put(503, getString(R.string.something_went_wrong))
        sparseArray.put(504, getString(R.string.something_went_wrong))
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            recyclerView?.visibility = View.VISIBLE
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleSuccessResource() {
        initAdapter()
    }

    private fun initAdapter() {
        if (adapter == null) {
            adapter = ChatMessagesAdapter(viewModel)
            initAdapterListeners()
            recyclerView?.adapter = adapter
        } else {
            adapter?.notifyDataSetChanged()
        }
    }

    private fun initAdapterListeners() {
        adapter?.setOnItemClickListener(::handleItemClickedListener)
    }

    private fun handleItemClickedListener(view: View, position: Int) {
        when (view.id) {
            R.id.relative_layout_row_message_audio_play -> {
                handlePlayAudioClickListener(position)
            }
            R.id.remove_upload_attachment -> {
                handleRemoveUploadAttachment(position)
            }
            R.id.retry_upload_attachment -> {
                handleRetryUploadAttachment(position)
            }
        }
    }

    private fun handlePlayAudioClickListener(position: Int) {
        val tempMessage = viewModel.getMessage(position)
        checkIfThereIsAnotherMessagePlayingAudio(tempMessage)
        if (checkIfClickedOnMessageAlreadyPlayingAudio()) {
            initMessagePlayingState()
            initMediaPlayer()
            handlePreparedListener()
            handleCompletionListener()
        }
    }

    private fun handleRemoveUploadAttachment(position: Int) {
        viewModel.removeMessage(position)
        adapter?.notifyItemRemoved(position)
    }

    private fun handleRetryUploadAttachment(position: Int) {
        viewModel.retryUpload(position).observe(this) {
            handleUploadAttachmentResource(it)
        }
    }

    private fun checkIfThereIsAnotherMessagePlayingAudio(tempMessage: Message?) {
        if (tempMessage?.messageId != message?.messageId && message != null) {
            mediaPlayer?.stop()
            message?.showProgress = false
            message?.icon = R.drawable.ic_play
            startUpdateProgress()
            message?.isPlaying = false
        }
        message = tempMessage
        message?.icon = R.drawable.ic_pause
    }

    private fun checkIfClickedOnMessageAlreadyPlayingAudio(): Boolean {
        if (message?.isPlaying == true) {
            message?.icon = R.drawable.ic_play
            mediaPlayer?.pause()
            stopUpdateProgress()
            message?.isPlaying = false
            message?.showProgress = false
            return false
        }
        return true
    }

    private fun initMessagePlayingState() {
        message?.isPlaying = true
        message?.showProgress = true
    }

    private fun initMediaPlayer() {
        mediaPlayer = MediaPlayer()
        mediaPlayer?.setDataSource(message?.body?.url)
        mediaPlayer?.prepareAsync()
    }

    private fun handlePreparedListener() {
        mediaPlayer?.setOnPreparedListener { mediaPlayer ->
            message?.showProgress = false
            startUpdateProgress()
            val maxProgress = mediaPlayer?.duration ?: 0
            mediaPlayer?.seekTo(message?.playingProgress ?: 0)
            message?.maxProgress = maxProgress
            mediaPlayer?.start()
        }
    }

    private fun handleCompletionListener() {
        mediaPlayer?.setOnCompletionListener {
            message?.maxProgress = 0
            message?.playingProgress = 0
            message?.currentTime = message?.duration
            message?.icon = R.drawable.ic_play
            message?.isPlaying = false
            stopUpdateProgress()
        }
    }

    private fun stopUpdateProgress() {
        recyclerView?.removeCallbacks(updateProgressRunnable)
    }

    private fun startUpdateProgress() {
        recyclerView?.postDelayed(updateProgressRunnable, 100)
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (viewModel.isGroup == true) {
            inflater.inflate(R.menu.menu_fragment_chat_group_message, menu)
        } else {
            inflater.inflate(R.menu.menu_fragment_chat_message, menu)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_fragment_chat_message_group_users -> {
                navigateToEditChatGroupInfo()
            }
            R.id.item_menu_fragment_chat_message_block_user -> {
                showToggleBlockUserDialog()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun navigateToEditChatGroupInfo() {
        findNavController().navigate(
            ChatMessagesFragmentDirections.chatGroupInfoDirection(
                arguments?.get("title")?.toString(),
                null,
                viewModel.threadId.toString(),
                arguments?.get("title")?.toString(),
                viewModel.getCreatorID()
            )
        )
    }

    private fun showToggleBlockUserDialog() {
        blockedString = if (viewModel.isBlockedUser()) {
            getString(R.string.message_chat_unblock_word)
        } else {
            getString(R.string.message_chat_block_word)
        }
        AlertDialog.Builder(requireContext())
            .setTitle(getString(R.string.title_menu_chat_blocked_users))
            .setMessage(getString(R.string.message_chat_blocked_users_dialog, blockedString))
            .setPositiveButton(android.R.string.yes) { _, _ ->
                toggleUserBlockStatus()
                if (viewModel.isBlockedUser()) {
                    Toast.makeText(context, getString(R.string.message_chat_unblock_success), Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(context, getString(R.string.message_chat_block_success), Toast.LENGTH_LONG).show()
                }
            }
            .show()
    }

    private fun toggleUserBlockStatus() {
        viewModel.toggleUserBlockStatus().observe(this) {
            if (viewModel.isBlockedUser()) {
                hideBottomViews()
            } else {
                showBottomViews()
            }
        }
    }

    private fun hideBottomViews() {
        val layoutMessageField = audioRecordView?.javaClass?.getDeclaredField("layoutMessage")
        layoutMessageField?.isAccessible = true
        val layoutMessage = layoutMessageField?.get(audioRecordView) as? View?
        layoutMessage?.visibility = View.GONE

        val imageViewAudioField = audioRecordView?.javaClass?.getDeclaredField("imageViewAudio")
        imageViewAudioField?.isAccessible = true
        val imageViewAudio = imageViewAudioField?.get(audioRecordView) as? View?
        imageViewAudio?.visibility = View.GONE
    }

    private fun showBottomViews() {
        val layoutMessageField = audioRecordView?.javaClass?.getDeclaredField("layoutMessage")
        layoutMessageField?.isAccessible = true
        val layoutMessage = layoutMessageField?.get(audioRecordView) as? View?
        layoutMessage?.visibility = View.VISIBLE

        val imageViewAudioField = audioRecordView?.javaClass?.getDeclaredField("imageViewAudio")
        imageViewAudioField?.isAccessible = true
        val imageViewAudio = imageViewAudioField?.get(audioRecordView) as? View?
        imageViewAudio?.visibility = View.VISIBLE
    }

    private fun changeTypeMessageText() {
        val layoutMessageField = audioRecordView?.javaClass?.getDeclaredField("editTextMessage")
        layoutMessageField?.isAccessible = true
        val layoutTypeMessage = layoutMessageField?.get(audioRecordView) as? EditText?
        layoutTypeMessage?.hint = getString(R.string.type_a_message)
    }

    override fun onDestroyView() {
        recordFile = null
        audioRecordView = null
        recyclerView = null
        adapter = null
        progressBar = null
        errorView = null
        blockedString = null
        mediaRecorder = null
        message = null
        updateProgressRunnable = null
        viewModel.reset()
        lifecycle.removeObserver(viewModel)
        try {
            mediaPlayer?.stop()
            mediaPlayer?.release()
            mediaPlayer = null
        } catch (ignored: Exception) {
            // No impl
        }
        super.onDestroyView()
    }

    private companion object {

        private const val MIN_RECORDING_TIME = 1000
        private const val RC_AUDIO_RECORD_PERMISSION = 101
        private const val AUDIO_SAMPLING_RATE = 48000
        private const val AUDIO_ENCODING_BIT_RATE = 48000
        private const val FILE_REQUEST_CODE = 20
    }
}
