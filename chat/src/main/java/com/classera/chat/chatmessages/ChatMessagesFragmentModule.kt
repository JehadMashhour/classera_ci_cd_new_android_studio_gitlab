package com.classera.chat.chatmessages

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created on 26/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
@Module
abstract class ChatMessagesFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ChatMessagesViewModelFactory
        ): ChatMessagesViewModel {
            return ViewModelProvider(fragment, factory)[ChatMessagesViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideChatMessagesFragmentArgs(fragment: Fragment): ChatMessagesFragmentArgs {
            val args by fragment.navArgs<ChatMessagesFragmentArgs>()
            return args
        }
    }

    @Binds
    abstract fun bindFragment(fragmentChat: ChatMessagesFragment): Fragment
}
