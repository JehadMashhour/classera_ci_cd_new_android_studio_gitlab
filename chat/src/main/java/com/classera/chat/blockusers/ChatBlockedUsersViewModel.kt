package com.classera.chat.blockusers

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.user.User
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.chat.ChatBlockedUsersRepository
import kotlinx.coroutines.Dispatchers

/**
 * Created on 20/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatBlockedUsersViewModel(
    private val chatRepository: ChatBlockedUsersRepository
) : BaseViewModel() {

    private val blockedUsers = mutableListOf<User>()

    fun getBlockedUsers() = liveData(Dispatchers.IO) {

        emit(Resource.Loading(show = true))

        val resource = tryResource {
            chatRepository.getBlockedUsers()
        }

        blockedUsers.clear()
        blockedUsers.addAll(
            resource.element<BaseWrapper<List<User>>>()?.data ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getBlockedUsersCount(): Int {
        return blockedUsers.size
    }

    fun getBlockedUser(position: Int): User? {
        return blockedUsers[position]
    }

    fun toggleUserBlockStatus(userBlockID: String?) = liveData(Dispatchers.IO) {

        val resource = tryResource {
            chatRepository.toggleUserBlockStatus(userBlockID)
        }

        if (resource.isSuccess()) {
            val blockedUser = blockedUsers.filter {
                it.id == userBlockID
            }.getOrNull(0)
            blockedUsers.remove(blockedUser)
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

}
