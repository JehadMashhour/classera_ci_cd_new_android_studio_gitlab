package com.classera.chat.users

import android.view.ViewGroup
import com.classera.chat.ChatViewModel
import com.classera.chat.databinding.RowThreadBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder

/**
 * Created on 18/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatUsersAdapter(private val viewModel: ChatUsersViewModel) : BaseAdapter<ChatUsersAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowThreadBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getUserCount()
    }

    inner class ViewHolder(binding: RowThreadBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowThreadBinding> {
                thread = viewModel.getUser(position)
            }
        }
    }
}
