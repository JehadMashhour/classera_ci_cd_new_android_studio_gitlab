package com.classera.chat.users

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BasePaginationWrapper
import com.classera.data.models.BaseWrapper
import com.classera.data.models.chat.Role
import com.classera.data.models.chat.Thread
import com.classera.data.models.filter.Filterable
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.network.errorhandling.tryThirdPartyResource
import com.classera.data.repositories.chat.ChatRepository
import kotlinx.coroutines.Dispatchers

/**
 * Created on 18/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatUsersViewModel(
    private val chatRepository: ChatRepository
) : BaseViewModel() {

    private val users = mutableListOf<Thread>()
    private var filterRoles: List<Role?>? = null

    fun getUsers(
        limit: String, pagination: String? = null,
        searchName: CharSequence? = "", roleID: String? = ""
    ): LiveData<Resource> {
        return if (true){
            getUsers(limit, pagination, searchName, roleID, true)
        }else{
            getUsers(limit, pagination, searchName, roleID, true)
        }
    }

    fun refreshUsers(
        limit: String, pagination: String? = null,
        searchName: CharSequence? = "", roleID: String? = ""
    ) = getUsers(
        limit, pagination,
        searchName, roleID, false
    )

    private fun getUsers(
        limit: String, pagination: String? = null,
        searchName: CharSequence? = "", roleID: String? = "", showProgress: Boolean
    ) = liveData(Dispatchers.IO) {
        if (showProgress) {
            emit(Resource.Loading(show = true))
        }

        val resource = tryThirdPartyResource {
            chatRepository.getFilteredThreadsWithoutGroups(
                limit,
                pagination,
                searchName,
                roleID
            )
        }

        if (pagination == null) {
            users.clear()
        }

        users.addAll(
            resource.element<BasePaginationWrapper<List<Thread>>>()?.data ?: mutableListOf()
        )

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getUserCount(): Int {
        return users.size
    }

    fun getUser(position: Int): Thread? {
        return users[position]
    }

    fun getRoles() = liveData(Dispatchers.IO) {

        val resource = tryResource {
            chatRepository.getRoles()
        }

        filterRoles = resource.element<BaseWrapper<List<Role>>>()?.data ?: listOf<Role>()

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getFilterRoles(): Array<out Filterable?>? {
        return filterRoles?.toTypedArray()
    }

}
