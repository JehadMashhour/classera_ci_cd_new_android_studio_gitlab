package com.classera.chat.users

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.calendar.CalendarRepository
import com.classera.data.repositories.chat.ChatRepository
import javax.inject.Inject

/**
 * Created on 18/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatUsersViewModelFactory @Inject constructor(
    private val chatRepository: ChatRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ChatUsersViewModel(chatRepository) as T
    }
}
