package com.classera.switchsemester

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class SwitchSemesterFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: SwitchSemesterViewModelFactory
        ): SwitchSemesterViewModel{
            return ViewModelProvider(fragment, factory)[SwitchSemesterViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: SwitchSemesterFragment): Fragment
}
