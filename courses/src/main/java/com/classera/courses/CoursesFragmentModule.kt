package com.classera.courses

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class CoursesFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: CoursesViewModelFactory
        ): CoursesViewModel {
            return ViewModelProvider(fragment, factory)[CoursesViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: CoursesFragment): Fragment
}
