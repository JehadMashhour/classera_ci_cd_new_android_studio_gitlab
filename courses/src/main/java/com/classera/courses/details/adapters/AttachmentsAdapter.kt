package com.classera.courses.details.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.utils.android.getBackgroundColorBasedOn
import com.classera.courses.details.CourseDetailsViewModel
import com.classera.digitallibrary.R
import com.classera.digitallibrary.databinding.RowDigitalLibraryBinding

/**
 * Project: Classera
 * Created: Jan 09, 2020
 *
 * @author Mohamed Hamdan
 */
class AttachmentsAdapter(
    type: String,
    private val viewModel: CourseDetailsViewModel
) : CourseDetailsAdapter<AttachmentsAdapter.ViewHolder>(type) {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowDigitalLibraryBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getAttachmentsCountBy(type)
    }

    inner class ViewHolder(binding: RowDigitalLibraryBinding) : BaseBindingViewHolder(binding) {

        private var linearLayoutLike: View? = null
        private var linearLayoutRate: View? = null

        init {
            linearLayoutLike = itemView.findViewById(R.id.linear_layout_row_digital_library_like)
            linearLayoutRate = itemView.findViewById(R.id.linear_layout_row_digital_library_rate)
        }

        override fun bind(position: Int) {
            linearLayoutLike?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            linearLayoutRate?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            bind<RowDigitalLibraryBinding> {
                library = viewModel.getAttachmentBy(type, position)
                library?.backgroundColor = getBackgroundColorBasedOn(position)
            }
        }
    }
}
