package com.classera.courses.details

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.courses.CoursesRepository
import com.classera.data.repositories.vcr.VcrRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class CourseDetailsFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: CourseDetailsViewModelFactory
        ): CourseDetailsViewModel {
            return ViewModelProvider(fragment, factory)[CourseDetailsViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideViewModelFactory(
            fragment: Fragment,
            coursesRepository: CoursesRepository,
            attachmentRepository: AttachmentRepository,
            prefs: Prefs,
            vcrRepository: VcrRepository
        ): CourseDetailsViewModelFactory {
            val arguments by fragment.navArgs<CourseDetailsFragmentArgs>()
            return CourseDetailsViewModelFactory(
                arguments,
                coursesRepository,
                attachmentRepository,
                prefs,
                vcrRepository
            )
        }
    }

    @Binds
    abstract fun bindFragment(fragment: CourseDetailsFragment): Fragment
}
