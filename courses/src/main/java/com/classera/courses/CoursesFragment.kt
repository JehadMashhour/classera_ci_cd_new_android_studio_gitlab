package com.classera.courses

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.courses.Course
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import java.util.*
import javax.inject.Inject

/**
 * Created by Odai Nazzal
 * Classera
 * o.nazzal@classera.com
 */
@Screen("Courses")
class CoursesFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: CoursesViewModel

    private val navArguments: CoursesFragmentArgs by navArgs()

    private var progressBar: ProgressBar? = null
    private var adapter: CoursesAdapter? = null
    private var errorView: ErrorView? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    override val layoutId: Int = R.layout.fragment_courses

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_courses)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_courses)
        errorView = view?.findViewById(R.id.error_view_fragment_courses)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_courses)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            refreshCourses()
        }

        getCourses()
    }

    private fun getCourses() {
        viewModel.getCourses().observe(this, this::handleResource)
    }

    private fun refreshCourses() {
        viewModel.getCourses().observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
            initAdapterListeners()
        } else {
            adapter?.notifyDataSetChanged()
        }
    }

    private fun initAdapter() {
        adapter = CoursesAdapter(viewModel)
        recyclerView?.adapter = adapter
    }

    private fun initAdapterListeners() {
        adapter?.setOnItemClickListener { _, position ->
            navigateToCourseDetails(viewModel.getCourse(position))
        }

    }

    private fun navigateToCourseDetails(course: Course?) {
        when (UserRole.valueOf(navArguments.type.toUpperCase(Locale.ENGLISH))) {
            UserRole.STUDENT -> {
                handleStudentDirection(course)
            }
            UserRole.GUARDIAN -> {
                handleStudentDirection(course)
            }
            else -> {
                handleTeacherDirection(course)
            }
        }
    }

    private fun handleStudentDirection(course: Course?) {
        val courseId = course?.courseId
        val teacherId = course?.teacherUserId
        val title = course?.courseTitle ?: getString(R.string.title_course_bar)
        findNavController().navigate(CoursesFragmentDirections.courseDetailsDirection(courseId, teacherId, title))
    }

    private fun handleTeacherDirection(course: Course?) {
        findNavController().navigate(CoursesFragmentDirections.courseActionDirection().apply {
            this.course = course
        })
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getCourses() }
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }
}
