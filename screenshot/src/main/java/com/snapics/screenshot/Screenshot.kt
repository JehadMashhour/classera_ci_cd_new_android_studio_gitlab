package com.snapics.screenshot

import android.view.View
import androidx.fragment.app.Fragment

/**
 * Project: Classera
 * Created: Jan 04, 2020
 *
 * @author Mohamed Hamdan
 */
interface Screenshot {

    fun take(fragment: Fragment, view: View)
}
