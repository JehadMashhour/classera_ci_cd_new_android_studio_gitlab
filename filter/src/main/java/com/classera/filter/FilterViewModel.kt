package com.classera.filter

import com.classera.core.BaseViewModel
import com.classera.data.models.filter.Filterable

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class FilterViewModel(val filters: Array<out Filterable?>?) : BaseViewModel() {

    fun getItemsCount(): Int {
        return filters?.size ?: 0
    }

    fun getFilterable(position: Int): Filterable? {
        return filters?.get(position)
    }

    fun selectFilter(position: Int) {
        filters?.filter { it?.selected == true }?.forEach { it?.selected = false }
        filters?.get(position)?.selected = true
    }

    var isFirstFilterItemAll: Boolean = true
}
