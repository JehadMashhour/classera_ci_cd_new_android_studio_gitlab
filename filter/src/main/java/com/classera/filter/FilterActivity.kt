package com.classera.filter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.activities.BaseToolbarActivity
import com.classera.data.models.filter.Filterable
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class FilterActivity : BaseToolbarActivity() {

    @Inject
    lateinit var viewModel: FilterViewModel

    private var recyclerViewFilter: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        findViews()
        initAdapter()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_activity_filter, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_activity_filter_done -> {
                val data = Intent()
                data.putExtra(
                    EXTRA_SELECTED_FILTER,
                    viewModel.filters?.firstOrNull { it?.selected == true })
                setResult(Activity.RESULT_OK, data)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun findViews() {
        recyclerViewFilter = findViewById(R.id.recycler_view_activity_filter)
    }

    private fun initAdapter() {
        viewModel.isFirstFilterItemAll = isFirstFilterItemAll ?: true

        recyclerViewFilter?.adapter = FilterAdapter(viewModel) {
            val data = Intent()
            data.putExtra(EXTRA_ALL_CLICKED, "")
            setResult(Activity.RESULT_OK, data)
            finish()
        }
    }

    companion object {

        private var isFirstFilterItemAll: Boolean? = true
        private const val REQUEST_CODE = 100
        private const val EXTRA_SELECTED_FILTER = "selectedFilter"
        private const val EXTRA_FILTERABLE = "filterable"
        private const val EXTRA_ALL_CLICKED = "all"

        fun start(
            fragment: Fragment, filters: Array<out Filterable?>?,
            selectedFilter: Filterable? = null, isFirstFilterItemAll: Boolean? = true
        ) {
            this.isFirstFilterItemAll = isFirstFilterItemAll
            val intent = Intent(fragment.requireContext(), FilterActivity::class.java)
            intent.putExtra(EXTRA_FILTERABLE, filters)
            intent.putExtra(EXTRA_SELECTED_FILTER, selectedFilter)
            fragment.startActivityForResult(intent, REQUEST_CODE)
        }

        @Suppress("UNCHECKED_CAST")
        internal fun getFilters(activity: AppCompatActivity): Array<Filterable?>? {
            return activity.intent.getParcelableArrayExtra(EXTRA_FILTERABLE)
                ?.map { it as? Filterable? }?.toTypedArray()
        }

        @Suppress("UNCHECKED_CAST")
        internal fun getSelectedFilter(activity: AppCompatActivity): Filterable? {
            return activity.intent.getParcelableExtra(EXTRA_SELECTED_FILTER)
        }

        fun isDone(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
            return requestCode == REQUEST_CODE
                    && resultCode == Activity.RESULT_OK
                    && data?.extras?.containsKey(EXTRA_SELECTED_FILTER) == true
                    && data.extras?.getParcelable<Parcelable>(EXTRA_SELECTED_FILTER) != null
        }

        fun isAll(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
            return requestCode == REQUEST_CODE
                    && resultCode == Activity.RESULT_OK
                    && data?.extras?.containsKey(EXTRA_ALL_CLICKED) == true
        }

        fun getSelectedFilter(data: Intent?): Filterable? {
            return data?.getParcelableExtra(EXTRA_SELECTED_FILTER)
        }
    }
}
