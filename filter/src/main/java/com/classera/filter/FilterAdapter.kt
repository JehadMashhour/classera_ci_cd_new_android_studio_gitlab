package com.classera.filter

import android.view.View
import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BaseViewHolder
import com.classera.filter.databinding.RowFilterBinding

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class FilterAdapter(
    private val viewModel: FilterViewModel,
    private val onAllClickListener: () -> Unit
) : BaseAdapter<BaseViewHolder>() {

    init {
        disableAnimations()
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        ALL_VIEW_TYPE -> {
            if (viewModel.isFirstFilterItemAll) {
                val view = inflater?.inflate(R.layout.row_filter_all, parent, false)
                AllViewHolder(view)
            } else {
                val view = inflater?.inflate(R.layout.row_filter_general, parent, false)
                AllViewHolder(view)
            }
        }
        else -> {
            val binding = RowFilterBinding.inflate(inflater!!, parent, false)
            FilterViewHolder(binding)
        }
    }

    override fun bind(holder: BaseViewHolder, position: Int) {
        super.bind(holder, position - 1)
    }

    override fun getItemsCount(): Int {
        return if (viewModel.getItemsCount() > 0) viewModel.getItemsCount() + 1 else viewModel.getItemsCount()
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0 && viewModel.getItemsCount() > 0) {
            return ALL_VIEW_TYPE
        }
        return super.getItemViewType(position)
    }

    private inner class AllViewHolder(view: View?) : BaseViewHolder(view) {

        override fun bind(position: Int) {
            itemView.setOnClickListener {
                onAllClickListener()
            }
        }
    }

    private inner class FilterViewHolder(binding: RowFilterBinding) :
        BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowFilterBinding> { filterable = viewModel.getFilterable(position) }
            itemView.setOnClickListener {
                viewModel.selectFilter(position)
                notifyDataSetChanged()
            }
        }
    }

    private companion object {

        private const val ALL_VIEW_TYPE = 10
    }
}
