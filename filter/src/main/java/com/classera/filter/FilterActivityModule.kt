package com.classera.filter

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.classera.data.models.filter.Filterable
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class FilterActivityModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: FilterViewModelFactory
        ): FilterViewModel {
            return ViewModelProvider(activity, factory)[FilterViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideArguments(activity: AppCompatActivity): Array<out Filterable?>? {
            val filters = FilterActivity.getFilters(activity)
            filters?.firstOrNull { it?.filterId == FilterActivity.getSelectedFilter(activity)?.filterId }?.selected =
                true
            return filters
        }
    }

    @Binds
    abstract fun bindActivity(activity: FilterActivity): AppCompatActivity
}
