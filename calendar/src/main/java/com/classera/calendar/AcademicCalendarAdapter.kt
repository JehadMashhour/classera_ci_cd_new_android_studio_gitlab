package com.classera.calendar

import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseViewHolder

/**
 * Created by Odai Nazzal on 12/28/2019.
 * Classera
 * o.nazzal@classera.com
 */
abstract class AcademicCalendarAdapter<V : BaseViewHolder> : BaseAdapter<V> ()
