package com.classera.calendar.add

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Switch
import android.widget.TextView
import android.widget.TimePicker
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.classera.calendar.R
import com.classera.calendar.databinding.FragmentAddEventBinding
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseValidationFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.selection.Selectable
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.classera.data.toDate
import com.classera.data.toString
import com.classera.selection.MultiSelectionActivity
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import java.util.*
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 1/27/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Screen("Add Event")
class AddEventFragment : BaseValidationFragment(), TimePickerDialog.OnTimeSetListener,
    DatePickerDialog.OnDateSetListener {

    @Inject
    lateinit var viewModel: AddEventViewModel

    @Inject
    lateinit var prefs: Prefs

    private val args: AddEventFragmentArgs by navArgs()

    private var currentDayCalendar = Calendar.getInstance()
    private var year: Int = currentDayCalendar.get(Calendar.YEAR)
    private var month: Int = currentDayCalendar.get(Calendar.MONTH)
    private var dayOfMonth: Int = currentDayCalendar.get(Calendar.DAY_OF_MONTH)
    private var hourOfDay: Int = currentDayCalendar.get(Calendar.HOUR_OF_DAY)
    private var minute: Int = currentDayCalendar.get(Calendar.MINUTE)

    @NotEmpty(message = "validation_fragment_add_calender_event_start_time")
    private var editTextStartTime: EditText? = null

    @NotEmpty(message = "validation_fragment_add_calender_event_start_date")
    private var editTextStartDate: EditText? = null

    @NotEmpty(message = "validation_fragment_add_calender_event_end_time")
    private var editTextEndTime: EditText? = null

    @NotEmpty(message = "validation_fragment_add_calender_event_end_date")
    private var editTextEndDate: EditText? = null

    @NotEmpty(message = "validation_fragment_add_calender_event_lectures")
    private var editTextLectures: EditText? = null

    @NotEmpty(message = "validation_fragment_add_calender_event_title")
    private var editTextTitle: EditText? = null

    private var textInputLectures: TextInputLayout? = null

    private var textViewMainTitle: TextView? = null
    private var editTextTime: EditText? = null
    private var editTextDate: EditText? = null
    private var editTextDescription: EditText? = null

    private var buttonCreate: Button? = null
    private var switchAllDay: Switch? = null
    private var switchToAll: Switch? = null
    private var toAll: String? = null
    private var progressBarSave: ProgressBar? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var linearLayoutParent: LinearLayout? = null

    override val layoutId: Int = R.layout.fragment_add_event

    override fun isBindingEnabled() = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        removeLecturesFromAdminSide()
        bindData()
        initCreateListener()
        initStartingDateTimeListener()
        initEndingDateTimeListener()
        initLecturesListener()
        initToAllListener()
        initLecturesListener()
        initErrorViewListener()
        handleEditCase()
    }

    private fun removeLecturesFromAdminSide() {
        if (prefs.userRole == UserRole.ADMIN) {
            progressBar?.visibility = View.GONE
            textInputLectures?.visibility = View.GONE
        } else {
            getLectures()
        }
    }

    private fun findViews() {
        editTextStartTime = view?.findViewById(R.id.edit_text_fragment_add_event_start_time)
        editTextStartDate = view?.findViewById(R.id.edit_text_fragment_add_event_start_date)
        editTextEndTime = view?.findViewById(R.id.edit_text_fragment_add_event_end_time)
        editTextEndDate = view?.findViewById(R.id.edit_text_fragment_add_event_end_date)
        editTextLectures = view?.findViewById(R.id.edit_text_fragment_add_event_lectures)
        textViewMainTitle = view?.findViewById(R.id.text_view_fragment_add_event_main_title)
        buttonCreate = view?.findViewById(R.id.button_fragment_add_event_create)
        editTextTitle = view?.findViewById(R.id.edit_text_fragment_add_event_title)
        editTextDescription = view?.findViewById(R.id.edit_text_fragment_add_event_description)
        switchAllDay = view?.findViewById(R.id.switch_fragment_add_event_all_day)
        switchToAll = view?.findViewById(R.id.switch_fragment_add_event_send_to_all)
        progressBarSave = view?.findViewById(R.id.progress_bar_fragment_add_event_save)
        linearLayoutParent = view?.findViewById(R.id.linear_layout_fragment_add_event_parent)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_add_event)
        errorView = view?.findViewById(R.id.error_view_fragment_add_event)
        textInputLectures = view?.findViewById(R.id.text_input_fragment_add_event_lectures)
    }

    private fun bindData() {
        bind<FragmentAddEventBinding> {
            this?.event = args.event
        }
    }

    private fun initStartingDateTimeListener() {
        editTextStartTime?.setOnClickListener {
            TimePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                hourOfDay,
                minute,
                false
            ).show()
            editTextTime = editTextStartTime
        }
        editTextStartDate?.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                year,
                month,
                dayOfMonth
            ).show()
            editTextDate = editTextStartDate
        }
    }

    private fun initEndingDateTimeListener() {
        editTextEndTime?.setOnClickListener {
            TimePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                hourOfDay,
                minute,
                false
            ).show()
            editTextTime = editTextEndTime
        }
        editTextEndDate?.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                year,
                month,
                dayOfMonth
            ).show()
            editTextDate = editTextEndDate
        }
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        this.hourOfDay = hourOfDay
        this.minute = minute
        val stringTime =
            getString(R.string.text_fragment_add_calender_event_time_format, hourOfDay, minute)
        val formattedTime = stringTime.toDate("hh:mm")?.toString("HH:mm a")
        editTextTime?.setText(formattedTime)
        removeError(editTextTime?.parent?.parent as TextInputLayout)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        this.year = year
        this.month = month
        this.dayOfMonth = dayOfMonth
        val humanMonth = month + 1
        editTextDate?.setText(
            getString(
                R.string.text_fragment_add_calender_event_date_format,
                year,
                humanMonth,
                dayOfMonth
            )
        )
        removeError(editTextDate?.parent?.parent as TextInputLayout)
    }

    private fun initLecturesListener() {
        editTextLectures?.setOnClickListener {
            MultiSelectionActivity.start(this, viewModel.lectures)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (MultiSelectionActivity.isDone(requestCode, resultCode, data)) {
            addLectureChips(MultiSelectionActivity.getSelectedFilterFromIntent(data))
        }
    }

    private fun addLectureChips(lectures: Array<out Selectable>?) {
        viewModel.setSelectedLectures(lectures)
        editTextLectures?.setText(lectures?.joinToString(separator = " | ") { it.title ?: "" })
        removeError(editTextLectures?.parent?.parent as TextInputLayout)
    }

    private fun initErrorViewListener() {
        errorView?.setOnRetryClickListener {
            getLectures()
        }
    }

    private fun getLectures() {
        viewModel.getLectures().observe(this, ::handleLecturesResource)
    }

    private fun handleLecturesResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLecturesLoadingResource(resource)
            }
            is Resource.Error -> {
                handleLecturesErrorResource(resource)
            }
        }
    }

    private fun handleLecturesLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            linearLayoutParent?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            linearLayoutParent?.visibility = View.VISIBLE
        }
    }

    private fun handleLecturesErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    private fun initToAllListener() {
        if (switchToAll?.isChecked!!) {
            this.toAll = TO_ALL_FALSE
        } else {
            this.toAll = TO_ALL_TRUE
        }
    }

    private fun initCreateListener() {
        buttonCreate?.setOnClickListener {
            validator.validate()
        }
    }

    override fun onValidationSucceeded() {
        viewModel.onSaveClicked(
            editTextTitle?.text?.toString(),
            editTextStartDate?.text?.toString(),
            editTextStartTime?.text?.toString(),
            editTextEndDate?.text?.toString(),
            editTextEndTime?.text?.toString(),
            editTextDescription?.text.toString(),
            getAllDayValue()
        ).observe(this, ::handleCreateResource)
    }

    private fun getAllDayValue() =
        if (switchAllDay?.isChecked == true) ALL_DAY_TRUE else ALL_DAY_FALSE

    private fun handleCreateResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCreateLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCreateSuccessResource()
            }
            is Resource.Error -> {
                handleCreateErrorResource(resource)
            }
        }
    }

    private fun handleCreateLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBarSave?.visibility = View.VISIBLE
            buttonCreate?.text = ""
            buttonCreate?.isEnabled = false
        } else {
            progressBarSave?.visibility = View.GONE
            buttonCreate?.setText(R.string.button_fragment_add_event_save)
            buttonCreate?.isEnabled = true
        }
    }

    private fun handleCreateSuccessResource() {
        findNavController().popBackStack()
    }

    private fun handleCreateErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleEditCase() {
        if (args.event != null) {
            textInputLectures?.visibility = View.GONE
        }
    }

    private companion object {

        private const val ALL_DAY_TRUE = "1"
        private const val ALL_DAY_FALSE = "0"

        private const val TO_ALL_TRUE = "1"
        private const val TO_ALL_FALSE = "0"
    }
}
