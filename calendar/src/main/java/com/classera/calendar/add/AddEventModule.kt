package com.classera.calendar.add

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.classera.calendar.AcademicCalendarViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 1/27/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class AddEventModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(fragment: Fragment, factory: AddEventViewModelFactory): AddEventViewModel {
            return ViewModelProvider(fragment, factory)[AddEventViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideAddEventFragmentArgs(fragment: Fragment): AddEventFragmentArgs {
            val args by fragment.navArgs<AddEventFragmentArgs>()
            return args
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AddEventFragment): Fragment
}
