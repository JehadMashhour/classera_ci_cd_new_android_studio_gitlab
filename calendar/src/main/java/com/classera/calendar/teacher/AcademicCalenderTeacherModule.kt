package com.classera.calendar.teacher

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.calendar.AcademicCalendarViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 1/23/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class AcademicCalenderTeacherModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AcademicCalendarViewModelFactory
        ): AcademicCalenderTeacherViewModel {
            return ViewModelProvider(fragment, factory)[AcademicCalenderTeacherViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideAcademicCalenderTeacherAdapter(viewModel: AcademicCalenderTeacherViewModel):
                AcademicCalenderTeacherAdapter {
            return AcademicCalenderTeacherAdapter(viewModel)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AcademicCalenderTeacherFragment): Fragment
}
