package com.classera.classvisits

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.data.network.errorhandling.Resource
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

@Screen("Class Visits")
class ClassVisitsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: ClassVisitsViewModel

    private var progressBar: ProgressBar? = null
    private var adapter: ClassVisitsAdapter? = null
    private var errorView: ErrorView? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var floatingActionButtonAdd: FloatingActionButton? = null


    override val layoutId: Int = R.layout.fragment_class_visits

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_class_visits)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_class_visits)
        errorView = view?.findViewById(R.id.error_view_fragment_class_visits)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_class_visits)
        floatingActionButtonAdd = view?.findViewById(R.id.floating_action_button_fragment_class_visits)
    }

    private fun initListeners() {
        floatingActionButtonAdd?.setOnClickListener {
            val direction = ClassVisitsFragmentDirections.addClassVisitDirection()
            findNavController().navigate(direction)
        }

        swipeRefreshLayout?.setOnRefreshListener {
            refreshClassVisits()
        }

        viewModel.notifyAdapterItemLiveData.observe(this) { adapter?.notifyItemChanged(it) }

        viewModel.toastLiveData.observe(this) { message ->
            val stringMessage = if (message is Int) getString(message) else message as String
            Toast.makeText(context, stringMessage, Toast.LENGTH_LONG).show()
        }
        getClassVisits()
    }

    private fun getClassVisits() {
        viewModel.getClassVisits()
            .observe(this, this::handleResource)
    }

    private fun refreshClassVisits() {
        viewModel.refreshClassVisits()
            .observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.notifyDataSetChanged()
        }
    }

    private fun initAdapter() {
        adapter = ClassVisitsAdapter(viewModel)
        recyclerView?.adapter = adapter
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getClassVisits() }
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }
}
