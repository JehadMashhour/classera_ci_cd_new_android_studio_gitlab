package com.classera.classvisits.add

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.classvisits.ClassVisitsRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 2/13/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AddClassVisitViewModelFactory @Inject constructor(
    private val classVisitsRepository: ClassVisitsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddClassVisitViewModel(classVisitsRepository) as T
    }
}
