package com.classera.utils.views.fragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.models.selection.Selectable
import javax.inject.Inject


class ListViewModelFactory @Inject constructor(
    private val selectableArray: Array<out Selectable?>?
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ListViewModel(selectableArray) as T
    }
}
