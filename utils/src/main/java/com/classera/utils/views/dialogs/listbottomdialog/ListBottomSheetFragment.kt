package com.classera.utils.views.dialogs.listbottomdialog

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.fragments.BaseBottomSheetDialogBindingFragment

import com.classera.core.utils.android.observe
import com.classera.data.models.selection.Selectable
import com.classera.utils.R
import com.classera.utils.databinding.FragmentListBottomSheetBinding


import javax.inject.Inject


class ListBottomSheetFragment : BaseBottomSheetDialogBindingFragment() {

    override val layoutId: Int = R.layout.fragment_list_bottom_sheet

    @Inject
    lateinit var listBottomSheetViewModel: ListBottomSheetViewModel

    @Inject
    lateinit var listBottomSheetAdapter: ListBottomSheetAdapter


    private val args by navArgs<ListBottomSheetFragmentArgs>()

    private lateinit var titleTextView: AppCompatTextView
    private lateinit var recyclerView: RecyclerView


    override fun enableDependencyInjection(): Boolean {
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        bindData()
        setupRecyclerView()
        initToggleListener()
    }

    private fun findViews() {
        requireView().run {
            titleTextView = findViewById(R.id.fragment_list_bottom_sheet_txt_view_title)
            recyclerView = findViewById(R.id.fragment_list_bottom_recycler_view)
        }

    }

    private fun bindData() {
        bind<FragmentListBottomSheetBinding>
        {
            this?.title = args.title
        }
    }

    private fun setupRecyclerView() {
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        recyclerView.adapter = listBottomSheetAdapter
        listBottomSheetAdapter.setOnItemClickListener { view, position ->
            handleItemClicked(view, position)
        }
        Handler(Looper.getMainLooper()).postDelayed({
            args.selectedId?.let { it ->
                listBottomSheetViewModel.getSelectedItemPosition(it)
                    ?.let { recyclerView.smoothScrollToPosition(it + 1) }
            }
        }, DELAY)


    }

    private fun initToggleListener() {
        listBottomSheetViewModel.changedPosition.observe(this) {
            listBottomSheetAdapter.notifyItemChanged(it)
        }
    }


    private fun handleItemClicked(view: View, position: Int) {
        setFragmentResult(
            args.key,
            bundleOf(DATA to listBottomSheetViewModel.getSelectable(position))
        )
        findNavController().navigateUp()
    }


    companion object {
        const val DEFAULT_REQUEST_KEY = "default_request_key"
        const val DELAY = 500L
        const val DATA = "data"

        inline fun <T : Selectable> show(
            fragment: Fragment,
            requestKey: String = DEFAULT_REQUEST_KEY,
            title: String?,
            selectableList: Array<Selectable>,
            selectedId: String? = null,
            crossinline listener: ((requestKey: String, t: T) -> Unit)
        ) {
            fragment.setFragmentResultListener(requestKey) { key, bundle ->
                listener(key, bundle.get(DATA) as T)
            }

            fragment.findNavController().navigate(
                R.id.listBottomSheetFragmentDirection, Bundle().apply {
                    putString("key", requestKey)
                    putString("title", title)
                    putParcelableArray("selectableList", selectableList)
                    putString("selectedId", selectedId)
                }
            )
        }

    }
}
