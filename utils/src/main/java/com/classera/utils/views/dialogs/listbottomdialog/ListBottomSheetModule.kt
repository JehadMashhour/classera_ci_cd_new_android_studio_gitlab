package com.classera.utils.views.dialogs.listbottomdialog

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.classera.data.models.selection.Selectable
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class ListBottomSheetModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factoryBottomSheet: ListBottomSheetViewModelFactory
        ): ListBottomSheetViewModel {
            return ViewModelProvider(
                fragment,
                factoryBottomSheet
            )[ListBottomSheetViewModel::class.java]
        }


        @Provides
        @JvmStatic
        fun provideSelectableList(fragment: Fragment): Array<out Selectable?> {
            val args by fragment.navArgs<ListBottomSheetFragmentArgs>()
            val selectables = args.selectableList

            selectables.map {
                it.apply {
                    selected = false
                }
            }.filter {
                it.id == args.selectedId && !args.selectedId.isNullOrEmpty()
            }.map {
                it.apply {
                    it.selected = true
                }
            }
            return selectables
        }


        @Provides
        @JvmStatic
        fun provideListBottomSheetAdapter(listBottomSheetViewModel: ListBottomSheetViewModel)
                : ListBottomSheetAdapter {
            return ListBottomSheetAdapter(listBottomSheetViewModel)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: ListBottomSheetFragment): Fragment

}
