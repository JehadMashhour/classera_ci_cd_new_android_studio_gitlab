package com.classera.utils.views.fragments

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.utils.databinding.RowListBinding

class ListAdapter(private val listViewModel: ListViewModel) :
    BaseAdapter<ListAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowListBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return listViewModel.getItemsCount()
    }

    inner class ViewHolder(binding: RowListBinding) :
        BaseBindingViewHolder(binding) {

        init {
            setItemViewClickListener()
        }

        private fun setItemViewClickListener() {
            itemView.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    listViewModel.toggleClickedSelectable(clickedPosition)
                    notifyDataSetChanged()
                }
            }
        }

        override fun bind(position: Int) {
            bind<RowListBinding> {
                this.selectable = listViewModel.getSelectable(position)
            }
        }

    }


}
