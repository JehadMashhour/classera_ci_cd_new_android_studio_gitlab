package com.classera.utils.views.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.classera.core.BaseViewModel
import com.classera.data.models.selection.Selectable


class ListViewModel(private val selectableArray: Array<out Selectable?>?) :
    BaseViewModel() {

    var lastSelectedPosition: Int? = null

    private val _changedPosition = MutableLiveData<Int>()
    val changedPosition: LiveData<Int> = _changedPosition


    fun getItemsCount(): Int {
        return selectableArray?.size ?: 0
    }

    fun getSelectable(position: Int): Selectable? {
        return selectableArray?.get(position)
    }

    fun getSelectable(selectedId: String?): Selectable? {
        return selectableArray?.find {
            it?.id == selectedId
        }
    }

    fun getSelectedItemPosition(selectedId: String?): Int?{
        return selectableArray?.indexOfFirst { it?.id == selectedId }
    }

    private fun toggleSelected(position: Int) {
        val selectable = selectableArray?.get(position)
        selectable?.apply {
            selected = selected.not()
        }
        _changedPosition.postValue(position)
    }

    fun toggleClickedSelectable(position: Int?) {
        if (lastSelectedPosition != position) {
            lastSelectedPosition?.let {
                toggleSelected(it)
            }

            position?.let {
                toggleSelected(it)
            }
            lastSelectedPosition = position
        }

    }

}
