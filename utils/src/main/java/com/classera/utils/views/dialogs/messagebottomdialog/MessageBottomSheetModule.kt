package com.classera.utils.views.dialogs.messagebottomdialog

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class MessageBottomSheetModule {



    @Binds
    abstract fun bindFragment(fragment: MessageBottomSheetFragment): Fragment
}
