package com.classera.utils.views.dialogs.alertdialog

/**
 * Project: Classera
 * Created: 7/14/2021
 *
 * @author Jehad Abdalqader
 */
interface AlertDialogHandler {
    fun onLeftButtonClicked()
    fun onRightButtonClicked()
}
