package com.classera.notification

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.notification.NotificationMessage
import com.classera.data.models.notification.NotificationWrapper
import com.classera.data.network.CNS_DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryThirdPartyResource
import com.classera.data.repositories.notification.NotificationRepository
import com.classera.data.repositories.settings.SettingsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NotificationListViewModel(
    private val notificationRepository: NotificationRepository,
    private val settingsRepository: SettingsRepository
) : BaseViewModel() {

    private var notificationList: MutableList<NotificationMessage?>? = mutableListOf()

    fun getUserNotifications(
        pageNumber: Int
    ): LiveData<Resource> {
        return getNotifications(pageNumber, pageNumber == CNS_DEFAULT_PAGE)
    }

    fun refreshUserNotifications() =
        getNotifications(CNS_DEFAULT_PAGE, false)

    private fun getNotifications(
        pageNumber: Int,
        showProgress: Boolean
    ) =
        liveData(Dispatchers.IO) {
            if (showProgress) {
                emit(Resource.Loading(show = true))
            }
            val resource = tryThirdPartyResource {
                notificationRepository.getNotificationList(pageNumber)
            }
            if (pageNumber == CNS_DEFAULT_PAGE) {
                notificationList?.clear()
            }
            notificationList?.addAll(
                resource.element<NotificationWrapper>()?.notifications ?: mutableListOf()
            )
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getNotificationCount(): Int {
        return notificationList?.size ?: 0
    }

    fun getNotificationItem(position: Int): NotificationMessage? {
        return notificationList?.get(position)
    }

    fun updateNotificationStatus(uuid: String) {
        viewModelScope.launch(Dispatchers.IO) { settingsRepository.updateNotificationStatus(uuid) }
    }
}
