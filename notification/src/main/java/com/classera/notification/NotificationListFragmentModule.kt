package com.classera.notification

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.notigation.AnnouncementNavigationProcessor
import com.classera.notigation.AssignmentNavigationProcessor
import com.classera.notigation.AttachmentNavigationProcessor
import com.classera.notigation.DiscussionCommentNavigationProcessor
import com.classera.notigation.MailNavigationProcessor
import com.classera.notigation.NavigationHandlerImpl
import com.classera.notigation.TicketNavigationProcessor
import com.classera.notigation.VCRNavigationProcessor
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module

abstract class NotificationListFragmentModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: NotificationListViewModelFactory
        ): NotificationListViewModel {
            return ViewModelProvider(fragment, factory)[NotificationListViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideNavigationHandler(): com.classera.notigation.NavigationHandler {
            return NavigationHandlerImpl(
                setOf(
                    MailNavigationProcessor(),
                    AssignmentNavigationProcessor(),
                    VCRNavigationProcessor(),
                    AttachmentNavigationProcessor(),
                    TicketNavigationProcessor(),
                    DiscussionCommentNavigationProcessor(),
                    AnnouncementNavigationProcessor()
                )
            )
        }
    }

    @Binds
    abstract fun bindActivity(activity: NotificationListFragment): Fragment
}
