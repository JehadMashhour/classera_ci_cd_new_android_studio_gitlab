package com.classera.notigation

import androidx.navigation.NavController

class VCRNavigationProcessor : NavigationProcessor {

    override fun matches(event: String): Boolean {
        return event.equals("vcr",true) ||
                event.equals("Smart Classroom Comment",false)
    }

    override fun executeWithNavController(notificationBody: String?, navController: NavController) {
        navController.navigate(R.id.item_menu_activity_main_navigation_view_virtual_class_room)
    }
}
