package com.classera.notigation

import androidx.navigation.NavController
import com.classera.data.models.notification.TicketNotificationBody
import com.google.gson.Gson

class TicketNavigationProcessor : NavigationProcessor {

    override fun matches(event: String): Boolean {
        return event.equals("ticket", true) ||
               event.equals("internal ticket", true)
    }

    override fun executeWithNavController(notificationBody: String?, navController: NavController) {
        val ticketNotificationBody =
            Gson().fromJson(notificationBody, TicketNotificationBody::class.java)
        navController.navigate(R.id.item_menu_activity_main_navigation_view_support)
        //Todo remove the comment bellow when the Api fix it.
//        navController.navigate(
//            R.id.item_menu_activity_main_navigation_view_support_details,
//            Bundle().apply {
//                putString("title", ticketNotificationBody.title)
//                putString("ticketId", ticketNotificationBody.id)
//            })
    }
}
