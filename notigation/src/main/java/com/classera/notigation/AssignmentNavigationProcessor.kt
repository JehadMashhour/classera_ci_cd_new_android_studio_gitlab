package com.classera.notigation

import android.os.Bundle
import androidx.navigation.NavController
import com.classera.data.models.notification.AssignmentNotificationBody
import com.google.gson.Gson

class AssignmentNavigationProcessor : NavigationProcessor {

    private var assignmentType: String? = null

    override fun matches(event: String): Boolean {
        assignmentType = event
        return assignmentType.equals("homework", true) ||
               assignmentType.equals("exam", true)
    }

    override fun executeWithNavController(notificationBody: String?, navController: NavController) {
        val notificationAssignmentBody =
            Gson().fromJson(notificationBody, AssignmentNotificationBody::class.java)
        navController.navigate(R.id.item_menu_activity_main_navigation_view_assignments,
            Bundle().apply {
                putString(ASSIGNMENT_FILTER_KEY, assignmentType?.capitalize())
            }
        )
        //Todo remove the comment bellow when the Api fix it.
//        navController.navigate(
//            R.id.item_menu_activity_main_navigation_view_assignment_details,
//            Bundle().apply {
//                putString("title", notificationAssignmentBody.title)
//                putParcelable(
//                    "assignment",
//                    Assignment(
//                        id = notificationAssignmentBody.assignmentId,
//                        courseId = notificationAssignmentBody.courseId,
//                        type = assignmentType?.capitalize(),
//                        status = AssignmentStatus.LAUNCH
//                    )
//                )
//            })
    }

    private companion object {

        private const val ASSIGNMENT_FILTER_KEY = "selectedFilter"
    }
}
