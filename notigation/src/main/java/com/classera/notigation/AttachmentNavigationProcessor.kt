package com.classera.notigation

import android.os.Bundle
import androidx.navigation.NavController
import com.classera.data.models.notification.AttachmentNotificationBody
import com.google.gson.Gson

class AttachmentNavigationProcessor : NavigationProcessor {

    override fun matches(event: String): Boolean {
        return event.equals("attachment",true)
    }

    override fun executeWithNavController(notificationBody: String?, navController: NavController) {
        val attachmentNotificationBody =
            Gson().fromJson(notificationBody, AttachmentNotificationBody::class.java)
        navController.navigate(R.id.activity_navigation_fragment_main_attachment_details,
            Bundle().apply {
            putString("title", attachmentNotificationBody.title)
            putString("id", attachmentNotificationBody.attachmentId)
        })
    }
}
