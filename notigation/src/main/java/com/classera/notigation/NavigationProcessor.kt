package com.classera.notigation

import androidx.navigation.NavController

interface NavigationProcessor {

    fun matches(event: String): Boolean

    fun execute(event: String){
        //No Impl
    }

    fun executeWithNavController(notificationBody: String?, navController: NavController){
        //No Impl
    }
}
