package com.classera.notigation

import android.os.Bundle
import androidx.navigation.NavController
import com.classera.data.models.announcements.Announcement
import com.classera.data.models.announcements.AnnouncementWrapper
import com.classera.data.models.notification.AnnouncementNotificationBody
import com.google.gson.Gson

class AnnouncementNavigationProcessor : NavigationProcessor {

    override fun matches(event: String): Boolean {
        return event.equals("announcement",true)
    }

    override fun executeWithNavController(notificationBody: String?, navController: NavController) {
        val announcementNotificationBody =
            Gson().fromJson(notificationBody, AnnouncementNotificationBody::class.java)
        navController.navigate(R.id.item_menu_activity_main_navigation_view_announcements)
        navController.navigate(R.id.announcement_details, Bundle().apply {
            putString("announcementFragmentLabel", announcementNotificationBody.title)
            putParcelable(
                "announcement",
                AnnouncementWrapper(announcement = Announcement(id = announcementNotificationBody.announcementId))
            )
        })
    }
}
