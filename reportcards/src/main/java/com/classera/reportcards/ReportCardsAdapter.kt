package com.classera.reportcards

import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.reportcards.databinding.RowReportCardsBinding

class ReportCardsAdapter(private val viewModel: ReportCardsViewModel) :
    BasePagingAdapter<ReportCardsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowReportCardsBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getReportCardsCount()
    }

    inner class ViewHolder(binding: RowReportCardsBinding) : BaseBindingViewHolder(binding) {

        private var viewDownload: View? = null
        private var viewReportCard: AppCompatImageView? = null

        init {
            viewDownload = itemView.findViewById(R.id.view_row_report_cards_download)
            viewReportCard = itemView.findViewById(R.id.image_view_row_report_cards_view)
        }

        override fun bind(position: Int) {
            viewDownload?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    getOnItemClickListener()?.invoke(it, clickedPosition)
                }
            }
            viewReportCard?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    getOnItemClickListener()?.invoke(it, clickedPosition)
                }
            }
            bind<RowReportCardsBinding> {
                reportCardItem = viewModel.getReportCard(position)
            }
        }
    }
}
