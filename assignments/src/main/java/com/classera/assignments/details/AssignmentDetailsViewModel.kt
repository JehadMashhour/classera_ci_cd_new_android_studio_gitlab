package com.classera.assignments.details

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assignments.Assignment
import com.classera.data.models.assignments.AssignmentStatus
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.assignments.AssignmentsRepository

/**
 * Project: Classera
 * Created: Feb 16, 2020
 *
 * @author Mohamed Hamdan
 */
class AssignmentDetailsViewModel(
    private val assignmentsRepository: AssignmentsRepository,
    private var assignment: Assignment?
) : BaseViewModel() {

    fun getAssignmentDetails() = liveData {
        emit(Resource.Loading(show = true))

        val resource = tryResource { assignmentsRepository.getAssignmentDetails(assignment) }
        val localAssignment = resource.element<BaseWrapper<Assignment>>()?.data

        if (localAssignment != null) {
            assignment = localAssignment.copy(
                id = assignment?.id,
                status = assignment?.status,
                courseId = assignment?.courseId,
                type = localAssignment.type ?: assignment?.type
            )
        }

        resource.element<BaseWrapper<Assignment>>()?.data = assignment
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun canShowAnswers() = assignment?.status == AssignmentStatus.SUBMISSION_GRADE && assignment?.viewAnswers == true

    fun getAssignment(): Assignment? {
        return assignment
    }

    fun getDraftQuestions() = liveData {
        emit(Resource.Loading(show = true))
        val resource = if(assignment?.submissionId != null) {
            tryResource { assignmentsRepository.getDraft(assignment?.id,assignment?.submissionId ) }
        }else {
            tryResource { assignmentsRepository.getDraft(assignment?.id, "") }
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }
}
