package com.classera.assignments.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.models.assignments.Assignment
import com.classera.data.repositories.assignments.AssignmentsRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Feb 16, 2020
 *
 * @author Mohamed Hamdan
 */
class AssignmentDetailsModelFactory @Inject constructor(
    private val assignmentsRepository: AssignmentsRepository,
    private val assignment: Assignment?
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AssignmentDetailsViewModel(assignmentsRepository, assignment) as T
    }
}
