package com.classera.assignments.mcq


import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.classera.assignments.R
import com.classera.core.activities.BaseValidationActivity
import com.classera.core.custom.views.ErrorView
import com.classera.core.utils.android.observe
import com.classera.data.network.errorhandling.Resource
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import javax.inject.Inject

/**
 * Created on 18/05/2020.
 * Classera
 *
 * @author Saeed Halawani
 */
class MultipleQuestionActivity : BaseValidationActivity() {

    @Inject
    lateinit var viewModel: MultipleQuestionViewModel

    @NotEmpty(message = "Please add question title")
    private var editTextQuestionTitle: EditText? = null

    @NotEmpty(message = "Please add question mark")
    private var editTextQuestionMark: EditText? = null

    private var autoCompleteTextViewDifficulties: AutoCompleteTextView? = null
    private var buttonSubmit: Button? = null
    private var progressBarSubmit: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var buttonAddAnswers: Button? = null
    private var chipGroupAnswers: ChipGroup? = null
    private var editTextAnswers: EditText? = null

    private var difficulties: Array<String>? = null
    private var selectedDifficultiesPosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multiple_choice_question)

        findViews()
        initDifficulties()
        initListeners()
    }


    override fun onValidationSucceeded() {

        var answerOne = (chipGroupAnswers?.getChildAt(ANSWER_ONE_INDEX) as Chip).text.toString()
        var answerTwo = (chipGroupAnswers?.getChildAt(ANSWER_TWO_INDEX) as Chip).text.toString()
        var answerThree = (chipGroupAnswers?.getChildAt(ANSWER_THREE_INDEX) as Chip).text.toString()
        var answerFour = (chipGroupAnswers?.getChildAt(ANSWER_FOUR_INDEX) as Chip).text.toString()
        var answerFive = (chipGroupAnswers?.getChildAt(ANSWER_FIVE_INDEX) as Chip).text.toString()
        var answerSix = (chipGroupAnswers?.getChildAt(ANSWER_SIX_INDEX) as Chip).text.toString()


        viewModel.submitAddMultipleChoiceQuestion(
            "", QUESTION_TYPE,
            editTextQuestionTitle?.text.toString(), editTextQuestionMark?.text.toString(),
            selectedDifficultiesPosition.toString(), "", "", "",
            "", ORDER, "", answerOne, answerTwo, answerThree, answerFour, answerFive, answerSix
        ).observe(this@MultipleQuestionActivity, ::handleResource)

    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBarSubmit?.visibility = View.VISIBLE
            buttonSubmit?.isEnabled = false
        } else {
            progressBarSubmit?.visibility = View.GONE
            buttonSubmit?.isEnabled = true
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        Toast.makeText(this, resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleSuccessResource() {
        Toast.makeText(
            this@MultipleQuestionActivity,
            getString(R.string.question_added_successfully), Toast.LENGTH_LONG
        )
            .show()
        this@MultipleQuestionActivity.finish()
    }

    private fun initDifficulties() {
        difficulties = resources.getStringArray(R.array.difficulty_entries)
        val adapter = ArrayAdapter(
            this, R.layout.dropdown_menu_popup_item, difficulties ?: arrayOf()
        )
        autoCompleteTextViewDifficulties?.setAdapter(adapter)

        autoCompleteTextViewDifficulties?.setOnItemClickListener { _, _, position, _ ->
            selectedDifficultiesPosition = position
        }
    }

    private fun findViews() {
        editTextQuestionTitle = findViewById(R.id.edit_text_add_multiple_choice_question_title)
        editTextQuestionMark = findViewById(R.id.edit_text_add_multiple_choice_question_mark)
        autoCompleteTextViewDifficulties =
            findViewById(R.id.auto_complete_text_view_add_multiple_choice_question_difficulty)
        progressBarSubmit = findViewById(R.id.progress_bar_fragment_add_multiple_choice_question)
        progressBarSubmit?.visibility = View.INVISIBLE
        errorView = findViewById(R.id.error_view_fragment_add_multiple_choice_question)
        buttonSubmit = findViewById(R.id.button_fragment_create_multiple_question_submit)
        chipGroupAnswers = findViewById(R.id.chip_group_create_multiple_choice_question_tags)
        buttonAddAnswers = findViewById(R.id.button_fragment_create_multiple_question_add_answers)
        editTextAnswers = findViewById(R.id.edit_text_fragment_create_multiple_question_answers)
    }

    private fun initListeners() {
        buttonAddAnswers?.setOnClickListener {
            if (chipGroupAnswers?.childCount != MAXIMUM_NUMBER_OF_ANSWERS) {
                val chip = Chip(this)
                chip.isCloseIconVisible = true
                chip.isCheckable = true
                chip.id = View.generateViewId()


                chip.setOnCloseIconClickListener {
                    chipGroupAnswers?.removeView(chip)
                    buttonAddAnswers?.isEnabled = true
                }

                chip.text = editTextAnswers?.text
                chipGroupAnswers?.addView(chip)
                editTextAnswers?.setText("")
            } else {
                buttonAddAnswers?.isEnabled = false
            }
        }

        buttonSubmit?.setOnClickListener { validator.validate() }
    }

    private companion object {
        private const val QUESTION_TYPE = "mcq"
        private const val ORDER = "1"
        private const val ANSWER_ONE_INDEX = 0
        private const val ANSWER_TWO_INDEX = 1
        private const val ANSWER_THREE_INDEX = 2
        private const val ANSWER_FOUR_INDEX = 3
        private const val ANSWER_FIVE_INDEX = 4
        private const val ANSWER_SIX_INDEX = 5
        private const val MAXIMUM_NUMBER_OF_ANSWERS = 6
    }
}
