package com.classera.assignments.mcq

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.repositories.assignments.AssignmentsRepository

/**
 * Created on 18/05/2020.
 * Classera
 *
 * @author Saeed Halawani
 */
@Suppress("LongParameterList")
class MultipleQuestionViewModel(
    private val assignmentsRepository: AssignmentsRepository
) : BaseViewModel() {

    fun submitAddMultipleChoiceQuestion(
        assignmentId: String?, type: String?, text: String?,
        mark: String?, difficulty: String?, correctAnswer: String?,
        correctAnswerResponse: String?, wrongAnswerResponse: String?,
        questionHint: String?, order: String?, questionId: String?, answerOne: String?,
        answerTwo: String?, answerThree: String?, answerFour: String?, answerFive: String?,
        answerSix: String?
    ) = liveData {
        emit(Resource.Loading(show = true))
        val resource = addMultipleChoiceQuestion(
            assignmentId, type, text,
            mark, difficulty, correctAnswer,
            correctAnswerResponse, wrongAnswerResponse,
            questionHint, order, questionId, answerOne,
            answerTwo, answerThree, answerFour, answerFive,
            answerSix
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private suspend fun addMultipleChoiceQuestion(
        assignmentId: String?, type: String?, text: String?,
        mark: String?, difficulty: String?, correctAnswer: String?,
        correctAnswerResponse: String?, wrongAnswerResponse: String?,
        questionHint: String?, order: String?, questionId: String?, answerOne: String?,
        answerTwo: String?, answerThree: String?, answerFour: String?, answerFive: String?,
        answerSix: String?
    ): Resource = tryNoContentResource {
        assignmentsRepository.addMultipleChoiceQuestion(
            assignmentId, type, text, mark, difficulty, correctAnswer, correctAnswerResponse,
            wrongAnswerResponse, questionHint, order, questionId,
            answerOne, answerTwo, answerThree, answerFour, answerFive, answerSix
        )
    }

}
