package com.classera.assignments.mcq

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created on 18/05/2020.
 * Classera
 *
 * @author Saeed Halawani
 */
@Module
abstract class MultipleQuestionModule{

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: MultipleQuestionModelFactory
        ): MultipleQuestionViewModel {
            return ViewModelProvider(activity, factory)[MultipleQuestionViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: MultipleQuestionActivity): AppCompatActivity
}
