package com.classera.assignments.add

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.assignments.AssignmentsRepository
import com.classera.data.repositories.attendance.AttendanceRepository
import com.classera.data.repositories.courses.CoursesRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: May 18, 2020
 *
 * @author Kahled Mohammad
 */
class AddAssignmentViewModelFactory @Inject constructor(
    private val coursesRepository: CoursesRepository,
    private val assignmentsRepository: AssignmentsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddAssignmentViewModel(coursesRepository, assignmentsRepository) as T
    }
}
