package com.classera.assignments.add

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.DatePicker
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.ScrollView
import android.widget.TimePicker
import android.widget.Toast
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.view.isVisible
import androidx.navigation.fragment.navArgs
import com.classera.assignments.R
import com.classera.assignments.databinding.FragmentAddAssignmentBinding
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assignments.AssignmentDetails
import com.classera.data.models.assignments.AssignmentTypeEnum
import com.classera.data.models.assignments.MoreOption
import com.classera.data.models.assignments.PublishToEnum
import com.classera.data.models.selection.Selectable
import com.classera.data.network.errorhandling.Resource
import com.classera.selection.MultiSelectionActivity
import com.google.android.material.button.MaterialButton
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.util.*
import javax.inject.Inject

/**
 * Project: Classera
 * Created: May 18, 2020
 *
 * @author Kahled Mohammad
 */
@Suppress(
    "MagicNumber",
    "LongMethod",
    "TooManyFunctions",
    "ReturnCount",
    "ComplexMethod",
    "LargeClass"
)
class AddAssignmentFragment : BaseBindingFragment(), DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {

    override val layoutId: Int = R.layout.fragment_add_assignment

    private var currentDayCalendar = Calendar.getInstance()
    private var year: Int = currentDayCalendar.get(Calendar.YEAR)
    private var month: Int = currentDayCalendar.get(Calendar.MONTH)
    private var dayOfMonth: Int = currentDayCalendar.get(Calendar.DAY_OF_MONTH)
    private var hourOfDay: Int = currentDayCalendar.get(Calendar.HOUR_OF_DAY)
    private var minute: Int = currentDayCalendar.get(Calendar.MINUTE)

    private val args by navArgs<AddAssignmentFragmentArgs>()

    private var editTextPublishDate: TextInputEditText? = null
    private var editTextDuoDate: TextInputEditText? = null
    private var autoCompleteSelectCourse: AutoCompleteTextView? = null
    private var checkBoxPublishLater: MaterialCheckBox? = null
    private var textInputLayoutPublishLater: TextInputLayout? = null
    private var textInputLayoutDuoDate: TextInputLayout? = null
    private var scrollView: ScrollView? = null
    private var progressBar: ProgressBar? = null
    private var editTextDateTime: EditText? = null
    private var autoCompleteAssignmentType: AppCompatAutoCompleteTextView? = null
    private var autoCompleteCourse: AppCompatAutoCompleteTextView? = null
    private var autoCompletePublishTo: AppCompatAutoCompleteTextView? = null
    private var autoCompletePublishToEntries: AppCompatAutoCompleteTextView? = null
    private var textInputPublishToEntries: TextInputLayout? = null
    private var checkBoxOtherOptions: MaterialCheckBox? = null
    private var linearLayoutOtherOptionsContainer: LinearLayoutCompat? = null
    private var checkBoxAllowMultipleSubmission: MaterialCheckBox? = null
    private var textInputNumberOfSubmission: TextInputLayout? = null
    private var editTextNumberOfSubmission: TextInputEditText? = null
    private var checkBoxSetTotalMark: MaterialCheckBox? = null
    private var textInputSetTotalMark: TextInputLayout? = null
    private var editTextSetTotalMark: TextInputEditText? = null
    private var autoCompletePreparations: AppCompatAutoCompleteTextView? = null
    private var errorView: ErrorView? = null
    private var textInputLayoutAssignmentDuration: TextInputLayout? = null
    private var checkBoxQuestionDuration: MaterialCheckBox? = null
    private var buttonCreateAssignment: MaterialButton? = null
    private var editTextAssignmentTitle: TextInputEditText? = null
    private var editTextAssignmentDuration: TextInputEditText? = null
    private var editTextPassingPercentage: TextInputEditText? = null
    private var autoCompleteMoreOptions: AppCompatAutoCompleteTextView? = null

    private var selectedAssignmentTypePosition: Int? = null
    private var selectedPublishToPosition: Int? = null
    private var selectedEntries: MutableList<Selectable>? = null
    private var selectedMoreOptions: MutableList<Selectable>? = null

    @Inject
    lateinit var viewModel: AddAssignmentViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initAssignmentInfoListener()
        initAssignmentAudience()
        initOtherOptionsListener()
        initAssignmentTimingListener()
        initAssignmentTypeAdapter()
        initErrorViewListener()
        initPublishToAdapter()
        initAssignmentTypeSelection()
        handelModes()
    }

    private fun findViews() {
        scrollView = view?.findViewById(R.id.scroll_view_add_assignment)
        progressBar = view?.findViewById(R.id.progress_bar_add_assignment)
        errorView = view?.findViewById(R.id.error_view_add_assignment)
        buttonCreateAssignment = view?.findViewById(R.id.button_fragment_add_assignment_create)
        editTextPublishDate =
            view?.findViewById(R.id.edit_text_fragment_add_assignment_publish_date)
        editTextDuoDate = view?.findViewById(R.id.label_fragment_add_assignment_duo_date)
        checkBoxPublishLater =
            view?.findViewById(R.id.check_box_fragment_add_assignment_publish_later)
        textInputLayoutPublishLater =
            view?.findViewById(R.id.text_input_layout_fragment_assignment_publish_date)
        textInputLayoutDuoDate =
            view?.findViewById(R.id.text_input_layout_fragment_assignment_duo_date)
        autoCompleteSelectCourse =
            view?.findViewById(R.id.auto_complete_text_fragment_add_assignment_course)
        autoCompleteAssignmentType = view?.findViewById(R.id.edit_text_fragment_add_assignment_type)
        autoCompleteCourse =
            view?.findViewById(R.id.auto_complete_text_fragment_add_assignment_course)
        autoCompletePublishTo =
            view?.findViewById(R.id.auto_complete_text_fragment_add_assignment_publish_to)
        autoCompletePublishToEntries =
            view?.findViewById(R.id.auto_complete_text_fragment_add_assignment_publish_to_entries)
        textInputPublishToEntries =
            view?.findViewById(R.id.text_input_fragment_add_assignment_publish_to_entries)
        checkBoxOtherOptions =
            view?.findViewById(R.id.check_box_fragment_add_assignment_other_option)
        linearLayoutOtherOptionsContainer =
            view?.findViewById(R.id.linear_layout_fragment_add_assignment_other_options_container)
        checkBoxAllowMultipleSubmission =
            view?.findViewById(R.id.check_box_fragment_add_assignment_allow_multiple_submission)
        textInputNumberOfSubmission =
            view?.findViewById(R.id.text_input_layout_fragment_add_assignment_number_of_submissions)
        editTextNumberOfSubmission =
            view?.findViewById(R.id.edit_text_fragment_add_assignment_number_of_submissions)
        checkBoxSetTotalMark =
            view?.findViewById(R.id.check_box_fragment_add_assignment_set_total_mark)
        textInputSetTotalMark =
            view?.findViewById(R.id.input_text_layout_fragment_add_assignment_total_mark)
        editTextSetTotalMark =
            view?.findViewById(R.id.edit_text_fragment_add_assignment_total_mark)
        autoCompletePreparations =
            view?.findViewById(R.id.auto_complete_text_fragment_add_assignment_preparation)
        textInputLayoutAssignmentDuration =
            view?.findViewById(R.id.text_input_layout_fragment_add_assignment_assignment_duration)
        checkBoxQuestionDuration =
            view?.findViewById(R.id.check_box_fragment_add_assignment_question_duration)
        editTextAssignmentTitle =
            view?.findViewById(R.id.edit_text_fragment_add_assignment_title)
        editTextAssignmentDuration =
            view?.findViewById(R.id.edit_text_fragment_add_assignment_assignment_duration)
        editTextPassingPercentage =
            view?.findViewById(R.id.edit_text_fragment_add_assignment_passing_percentage)
        autoCompleteMoreOptions =
            view?.findViewById(R.id.auto_complete_text_fragment_add_assignment_more_options)
    }

    private fun handelModes() {
        if (args.isEditMode || args.isViewDetails) {
            if (args.isViewDetails) {
                buttonCreateAssignment?.isVisible = false
                disableAllViews()
            }
            viewModel.getTeacherAssignmentDetails(args.assignment?.id)
                .observe(this, ::handleTeacherAssignmentDetailsResource)
        } else {
            handelCoursesObserver()
        }
    }

    private fun initAssignmentTypeSelection() {
        handelAssignmentTypeSelectedItem(args.selectedType)
        autoCompleteAssignmentType?.setText(
            resources.getStringArray(R.array.fragment_add_assignment_assignment_type)[args.selectedType],
            false
        )
    }

    private fun initAssignmentTimingListener() {
        editTextPublishDate?.setOnClickListener {
            TimePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                hourOfDay,
                minute,
                true
            ).show()


            DatePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                year,
                month,
                dayOfMonth
            ).show()

            editTextDateTime = editTextPublishDate
        }

        editTextDuoDate?.setOnClickListener {
            TimePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                hourOfDay,
                minute,
                true
            ).show()

            DatePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                year,
                month,
                dayOfMonth
            ).show()

            editTextDateTime = editTextDuoDate
        }
    }

    private fun handelAutoCompletePublishToEntriesHint(position: Int) {
        textInputPublishToEntries?.hint = if (position == PublishToEnum.SECTIONS.ordinal) {
            getString(R.string.hint_fragment_add_assignment_select_sections)
        } else {
            getString(R.string.hint_fragment_add_assignment_select_students)
        }
    }

    private fun initAssignmentInfoListener() {
        autoCompleteCourse?.setOnItemClickListener { _, _, position, _ ->
            viewModel.setSelectedCourse(position)
            viewModel.getPublishUser().observe(this, ::handlePublishUserResource)
            viewModel.getPreparation().observe(this, ::handlePreparationResource)
        }

        autoCompleteAssignmentType?.setOnItemClickListener { _, _, position, _ ->
            handelAssignmentTypeSelectedItem(position)
        }
    }

    private fun initAssignmentAudience() {
        autoCompletePublishTo?.setOnItemClickListener { _, _, position, _ ->
            selectedPublishToPosition = position
            viewModel.setSelectedPublishType(position)
            handelAutoCompletePublishToEntriesHint(position)
        }

        autoCompletePublishToEntries?.setOnClickListener {
            MultiSelectionActivity.start(
                this,
                viewModel.getPublishEntries(),
                selectedFilter = selectedEntries?.toTypedArray(),
                key = PUBLISH_TO_ENTRIES_REQUEST_CODE
            )
        }

        checkBoxPublishLater?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                textInputLayoutPublishLater?.visibility = View.GONE
                textInputLayoutDuoDate?.visibility = View.GONE
            } else {
                textInputLayoutPublishLater?.visibility = View.VISIBLE
                textInputLayoutDuoDate?.visibility = View.VISIBLE
            }
        }

    }

    private fun initOtherOptionsListener() {
        checkBoxOtherOptions?.setOnCheckedChangeListener { _, isChecked ->
            linearLayoutOtherOptionsContainer?.visibility = if (isChecked) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        checkBoxAllowMultipleSubmission?.setOnCheckedChangeListener { _, isChecked ->
            textInputNumberOfSubmission?.visibility = if (isChecked) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        checkBoxSetTotalMark?.setOnCheckedChangeListener { _, isChecked ->
            textInputSetTotalMark?.visibility = if (isChecked) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        checkBoxQuestionDuration?.setOnCheckedChangeListener { _, isChecked ->
            textInputLayoutAssignmentDuration?.hint = if (isChecked) {
                getString(R.string.label_fragment_add_assignment_question_duration)
            } else {
                getString(R.string.hint_fragment_add_assignment_assignment_duration)
            }
        }

        autoCompletePreparations?.setOnItemClickListener { _, _, position, _ ->
            viewModel.setSelectedPreparation(position)
        }

        autoCompleteMoreOptions?.setOnClickListener {
            MultiSelectionActivity.start(
                this,
                getSelectableMoreOptions(),
                key = MORE_OPTIONS_REQUEST_CODE
            )
        }

        buttonCreateAssignment?.setOnClickListener {
            if (!isValid()) {
                return@setOnClickListener
            }
            val assignmentPayload = AddAssignmentPayload(
                assignmentId = viewModel.assignmentDetails?.assignment?.id,
                courseId = viewModel.selectedCourse?.courseId,
                typePosition = selectedAssignmentTypePosition,
                title = editTextAssignmentTitle?.text.toString(),
                assignmentDuration = editTextAssignmentDuration?.text.toString(),
                questionDuration = checkBoxQuestionDuration?.isChecked,
                publishDate = editTextPublishDate?.text.toString(),
                duoDate = editTextDuoDate?.text.toString(),
                publishLater = checkBoxPublishLater?.isChecked ?: false,
                selectedPublishToPosition = selectedPublishToPosition,
                selectedEntries = selectedEntries,
                passingPercentage = editTextPassingPercentage?.text.toString(),
                preparationId = viewModel.selectedPreparation?.id,
                allowMultipleSubmission = checkBoxAllowMultipleSubmission?.isChecked,
                numberOfSubmission = editTextNumberOfSubmission?.text.toString(),
                distributeMarkOnQuestion = checkBoxSetTotalMark?.isChecked,
                totalMark = editTextSetTotalMark?.text.toString(),
                selectedMoreOptions = selectedMoreOptions
            )
            if (!args.isEditMode) {
                viewModel.addAssignment(assignmentPayload)
                    .observe(this, ::handleAddAssignmentResource)
            } else {
                viewModel.editTeacherAssignment(assignmentPayload)
                    .observe(this, ::handleAddAssignmentResource)
            }
        }
    }

    private fun isValid(): Boolean {
        if (viewModel.selectedCourse == null) {
            Toast.makeText(
                requireContext(),
                R.string.helper_text_fragment_add_assignment_please_select_course,
                Toast.LENGTH_SHORT
            ).show()
            return false
        }

        if (editTextAssignmentTitle?.text.isNullOrEmpty()) {
            Toast.makeText(
                requireContext(),
                R.string.error_fragment_add_assignemnt_empty_title,
                Toast.LENGTH_SHORT
            ).show()
            return false
        }

        if (selectedPublishToPosition != PublishToEnum.SECTIONS.ordinal
            && selectedPublishToPosition != PublishToEnum.STUDENTS.ordinal
        ) {
            Toast.makeText(
                requireContext(),
                R.string.error_fragment_add_assignment_empty_publish_to,
                Toast.LENGTH_SHORT
            ).show()

            return false
        }

        if (selectedEntries.isNullOrEmpty()) {
            if (selectedPublishToPosition == PublishToEnum.SECTIONS.ordinal) {
                Toast.makeText(
                    requireContext(),
                    R.string.error_fragment_add_assignment_empty_section,
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    requireContext(),
                    R.string.error_fragment_add_assignment_empty_student,
                    Toast.LENGTH_SHORT
                ).show()
            }

            return false
        }

        if (checkBoxAllowMultipleSubmission?.isChecked == true && editTextNumberOfSubmission?.text.isNullOrEmpty()) {
            Toast.makeText(
                requireContext(),
                R.string.error_fragment_add_assignment_empty_number_of_submission,
                Toast.LENGTH_SHORT
            ).show()

            return false
        }

        if (checkBoxSetTotalMark?.isChecked == true && editTextSetTotalMark?.text.isNullOrEmpty()) {
            Toast.makeText(
                requireContext(),
                R.string.error_fragment_add_assignment_empty_total_mark,
                Toast.LENGTH_SHORT
            ).show()
            return false
        }

        return true
    }

    private fun handelAssignmentTypeSelectedItem(position: Int) {
        viewModel.setSelectedPublishType(position)
        selectedAssignmentTypePosition = position
        if (position == AssignmentTypeEnum.EXAM.ordinal) {
            textInputLayoutAssignmentDuration?.visibility = View.VISIBLE
            checkBoxQuestionDuration?.visibility = View.VISIBLE
        } else {
            textInputLayoutAssignmentDuration?.visibility = View.GONE
            checkBoxQuestionDuration?.visibility = View.GONE
        }
    }

    private fun initCourseAdapter() {
        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            viewModel.getCourseTitles()
        )
        autoCompleteCourse?.setAdapter(adapter)
    }

    private fun initAssignmentTypeAdapter() {
        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            resources.getStringArray(R.array.fragment_add_assignment_assignment_type)
        )
        autoCompleteAssignmentType?.setAdapter(adapter)
    }

    private fun initPublishToAdapter() {
        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            resources.getStringArray(R.array.fragment_assignment_publish_to)
        )
        autoCompletePublishTo?.setAdapter(adapter)
    }

    private fun initPreparationAdapter() {
        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            viewModel.getPreparationTitles()
        )
        autoCompletePreparations?.setAdapter(adapter)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleTeacherAssignmentDetailsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleTeacherAssignmentDetailsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleTeacherAssignmentDetailsSuccessResource(
                    resource as Resource.Success<BaseWrapper<AssignmentDetails>>
                )
            }
            is Resource.Error -> {
                handleTeacherAssignmentDetailsErrorResource(resource)
            }
        }
    }

    private fun handleTeacherAssignmentDetailsLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            buttonCreateAssignment?.isEnabled = false
            View.VISIBLE
        } else {
            buttonCreateAssignment?.isEnabled = true
            View.GONE
        }
    }

    private fun handleTeacherAssignmentDetailsErrorResource(resource: Resource.Error) {
        buttonCreateAssignment?.isEnabled = true
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleTeacherAssignmentDetailsSuccessResource(
        resource: Resource.Success<BaseWrapper<AssignmentDetails>>
    ) {
        scrollView?.visibility = View.VISIBLE
        selectedMoreOptions = getSelectableMoreOptions().toMutableList()
        bind<FragmentAddAssignmentBinding> {
            this?.assignmentDetails = resource.data?.data
        }
        initCourseAdapter()
        initPreparationAdapter()
        initPublishToAdapter()
        handelEditSelectedPublishType()
        buttonCreateAssignment?.setText(R.string.label_fragment_add_assignment_edit)
        checkBoxOtherOptions?.isChecked = viewModel.hasOtherOptions()
        checkBoxAllowMultipleSubmission?.isChecked =
            viewModel.assignmentDetails?.assignment?.allowMultipleSubmissions == true
        checkBoxQuestionDuration?.isChecked =
            viewModel.assignmentDetails?.assignment?.questionTime == "1"
        editTextNumberOfSubmission?.setText(viewModel.assignmentDetails?.assignment?.maxSubmissionsAllowed)
        autoCompletePreparations?.setText(viewModel.selectedPreparation?.title, false)
        autoCompleteMoreOptions?.setText(
            selectedMoreOptions?.map { it.title }?.joinToString(", "), false
        )
        with(viewModel.assignmentDetails?.assignment?.mark) {
            if (this != null && this != "0") {
                checkBoxSetTotalMark?.isChecked = true
                editTextSetTotalMark?.setText(this)
            }
        }
    }

    private fun handelEditSelectedPublishType() {
        autoCompleteCourse?.setText(viewModel.selectedCourse?.courseTitle, false)
        viewModel.getSelectedPublishTypeIndex()?.let {
            selectedPublishToPosition = it
            handelAutoCompletePublishToEntriesHint(it)
            autoCompletePublishTo?.setText(
                resources.getStringArray(R.array.fragment_assignment_publish_to)[it],
                false
            )
        }
        selectedEntries = viewModel.getSelectedEntries()
        autoCompletePublishToEntries?.setText(
            selectedEntries?.map { it.title }?.joinToString(", "), false
        )
    }

    private fun handelCoursesObserver() {
        viewModel.getCourses().observe(this, ::handleCoursesResource)
    }

    private fun handleAddAssignmentResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleAddAssignmentLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleAddAssignmentSuccessResource()
            }
            is Resource.Error -> {
                handleAddAssignmentErrorResource(resource)
            }
        }
    }

    private fun handleAddAssignmentLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            buttonCreateAssignment?.isEnabled = false
            View.VISIBLE
        } else {
            buttonCreateAssignment?.isEnabled = true
            View.GONE
        }
    }

    private fun handleAddAssignmentErrorResource(resource: Resource.Error) {
        buttonCreateAssignment?.isEnabled = true
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleAddAssignmentSuccessResource() {
        if (!args.isEditMode) {
            context?.let {
                Toast.makeText(
                    it,
                    getString(R.string.message_assignment_created_successfully),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        activity?.onBackPressed()
    }

    private fun handleCoursesResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCoursesLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCoursesSuccessResource()
            }
            is Resource.Error -> {
                handleCoursesErrorResource(resource)
            }
        }
    }

    private fun handleCoursesLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun handleCoursesErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleCoursesSuccessResource() {
        scrollView?.visibility = View.VISIBLE
        initCourseAdapter()
    }

    private fun handlePublishUserResource(resource: Resource) {
        when (resource) {
            is Resource.Error -> {
                handlePublishUserErrorResource(resource)
            }
        }
    }

    private fun handlePublishUserErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handlePreparationResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                initPreparationAdapter()
            }
            is Resource.Error -> {
                handlePreparationErrorResource(resource)
            }
        }
    }

    private fun handlePreparationErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when {
            MultiSelectionActivity.isDone(
                requestCode,
                resultCode,
                data,
                PUBLISH_TO_ENTRIES_REQUEST_CODE
            ) -> {
                selectedEntries =
                    MultiSelectionActivity.getSelectedFilterFromIntent(data)
                        ?.toMutableList() ?: mutableListOf()

                autoCompletePublishToEntries?.setText(
                    selectedEntries?.map { it.title }?.joinToString(", "), false
                )
            }
            MultiSelectionActivity.isDone(
                requestCode,
                resultCode,
                data,
                MORE_OPTIONS_REQUEST_CODE
            ) -> {
                selectedMoreOptions =
                    MultiSelectionActivity.getSelectedFilterFromIntent(data)
                        ?.toMutableList() ?: mutableListOf()

                autoCompleteMoreOptions?.setText(
                    selectedMoreOptions?.map { it.title }?.joinToString(", "), false
                )
            }
        }
    }

    private fun initErrorViewListener() {
        errorView?.setOnRetryClickListener {
            handelCoursesObserver()
        }
    }

    private fun getSelectableMoreOptions(): Array<Selectable> {
        val moreOptionList = mutableListOf<Selectable>()
        resources.getStringArray(R.array.fragment_assignment_more_options)
            .forEachIndexed { index, option ->
                moreOptionList.add(MoreOption(id = index.toString(), title = option))
            }
        viewModel.assignmentDetails?.assignment?.let {
            moreOptionList[0].selected = it.showResponses ?: false
            moreOptionList[1].selected = it.showHints ?: false
            moreOptionList[2].selected = it.viewAnswers ?: false
            moreOptionList[3].selected = it.viewAnswersAfterCutOff == true
        }
        return moreOptionList.toTypedArray()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        this.year = year
        this.month = month
        this.dayOfMonth = dayOfMonth
        val humanMonth = month + 1
        editTextDateTime?.setText(
            getString(
                R.string.label_fragment_add_assignment_date_format,
                year,
                humanMonth,
                dayOfMonth
            )
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        this.hourOfDay = hourOfDay
        this.minute = minute
        val time = getString(R.string.label_fragment_add_assignment_time_format, hourOfDay, minute)
        editTextDateTime?.setText(editTextDateTime?.text.toString() + " " + time + ":00")
    }

    private companion object {

        private const val PUBLISH_TO_ENTRIES_REQUEST_CODE = 856
        private const val MORE_OPTIONS_REQUEST_CODE = 857
    }
}

