package com.classera.assignments.solve.types.adapters

import android.view.ViewGroup
import com.classera.assignments.databinding.RowAnswerCheckBoxBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.data.models.assignments.SelectableChoice

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
class MultipleSelectAdapter(
    private val choices: List<SelectableChoice>?
) : BaseAdapter<MultipleSelectAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowAnswerCheckBoxBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return choices?.size ?: 0
    }

    fun setSelectedPosition(position: Int) {
        choices?.get(position)?.toggle()
    }

    inner class ViewHolder(binding: RowAnswerCheckBoxBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowAnswerCheckBoxBinding> {
                this.choice = choices?.get(position)
                this.position = position
            }
        }
    }
}
