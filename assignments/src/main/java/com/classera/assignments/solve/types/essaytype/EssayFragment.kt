package com.classera.assignments.solve.types.essaytype

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.annotation.Keep
import com.classera.assignments.R
import com.classera.assignments.solve.types.BaseQuestionTypeFragment
import com.classera.core.Screen
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onDebounceTextChange
import com.classera.core.utils.android.removeNull
import dagger.android.support.AndroidSupportInjection

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
@Screen("Essay question")
@Keep
class EssayFragment : BaseQuestionTypeFragment() {

    override var layoutResource: Int = R.layout.fragment_essay
    private var editTextAnswer: EditText? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        setDraftAnswer()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private fun findViews() {
        editTextAnswer = view?.findViewById(R.id.edit_text_fragment_essay_answer)
        editTextAnswer?.onDebounceTextChange {
            val data = this.getData().removeNull()
            viewModel.saveAsDraft(data).observe(viewLifecycleOwner) {}
        }
    }

    private fun setDraftAnswer() {

        question?.let { question ->
            val answer = questionAnswer?.firstOrNull { it.questionId.toString() == question.id }
            question.draftAnswer = answer?.answer
            question.studentAnswer = answer?.answer
            editTextAnswer?.setText(answer?.answer)
        }
    }

    override fun getData(): Map<String, Any?> {
        return mapOf(
            "assignment_id" to assignment?.id,
            "question_id" to question?.id,
            "submission_id" to settings?.submissionId,
            "question_number" to saveQuestionNumber,
            "answer" to editTextAnswer?.text?.toString()
        )
    }

    override fun getSubmitData(mapToBeFilled: MutableMap<String, Any?>) {
        if (editTextAnswer?.text?.toString()?.isNotBlank() == true) {
            mapToBeFilled["answers[$submitQuestionNumber][question_id]"] = question?.id
            mapToBeFilled["answers[$submitQuestionNumber][text]"] = editTextAnswer?.text?.toString()
        }
    }
}
