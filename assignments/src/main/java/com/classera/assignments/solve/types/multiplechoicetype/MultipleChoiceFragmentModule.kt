package com.classera.assignments.solve.types.multiplechoicetype

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module


/**
 * Created by Rawan Al-Theeb on 6/17/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class MultipleChoiceFragmentModule {

    @Binds
    abstract fun bindFragment(multipleChoiceFragment: MultipleChoiceFragment) : Fragment
}
