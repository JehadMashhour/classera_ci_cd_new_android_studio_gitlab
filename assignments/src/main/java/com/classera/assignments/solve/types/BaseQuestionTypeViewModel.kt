package com.classera.assignments.solve.types

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.repositories.assignments.AssignmentsRepository

/**
 * Project: Classera
 * Created: June 15, 2020
 *
 * @author ٌRawan Altheeb
 */
class BaseQuestionTypeViewModel(
    private val assignmentsRepository: AssignmentsRepository
) : BaseViewModel() {

    fun saveAsDraft(data: Map<String, Any?>?) = liveData {
        emit(Resource.Loading(show = true))
        val resource: Resource = tryNoContentResource {
            assignmentsRepository.saveAsDraft(data)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }
}
