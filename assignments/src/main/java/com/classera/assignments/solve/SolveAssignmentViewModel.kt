package com.classera.assignments.solve

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assignments.Assignment
import com.classera.data.models.assignments.AssignmentSettings
import com.classera.data.models.assignments.Question
import com.classera.data.models.assignments.QuestionDraft
import com.classera.data.models.assignments.QuestionType
import com.classera.data.models.assignments.QuestionsWrapper
import com.classera.data.models.assignments.UserLastQuestion
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.assignments.AssignmentsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

/**
 * Project: Classera
 * Created: Feb 18, 2020
 *
 * @author Mohamed Hamdan
 */
class SolveAssignmentViewModel(
    private val assignmentsRepository: AssignmentsRepository,
    private val assignment: Assignment?
) : BaseViewModel(), LifecycleObserver {

    private var assignmentSettings: AssignmentSettings? = null
    private val questions: MutableList<Question> = mutableListOf()
    val draftQuestions: MutableList<QuestionDraft> = mutableListOf()
    private var userLastQuestion: UserLastQuestion? = null
    val missedQuestionErrorLiveData = MutableLiveData<String>()

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        viewModelScope.launch(Dispatchers.IO) {
            userLastQuestion = assignmentsRepository.getLastQuestion(assignment?.id.toString()).first()
        }
    }

    fun getQuestions() = liveData {
        emit(Resource.Loading(show = true))

        val wrapper = if (assignment?.questions?.isNotEmpty() == true) {
            val wrapper =
                BaseWrapper(data = QuestionsWrapper(assignmentQuestions = assignment.questions))
            wrapper
        } else {
            val resource = tryResource { assignmentsRepository.getQuestions(assignment) }
            if (resource is Resource.Error) {
                missedQuestionErrorLiveData.value = resource.error.message
                return@liveData
            }
            val wrapper = resource.element<BaseWrapper<QuestionsWrapper>>()
            wrapper
        }
        assignmentSettings = wrapper?.data?.assignmentSettings

        questions.addAll(wrapper?.data?.assignmentQuestions ?: listOf())

        val resource = if (assignmentSettings?.submissionId != null) {
            tryResource { assignmentsRepository.getDraft(assignment?.id, assignmentSettings?.submissionId) }
        } else {
            tryResource { assignmentsRepository.getDraft(assignment?.id, "") }
        }

        val wrapper2 = resource.element<BaseWrapper<List<QuestionDraft>>>()
        draftQuestions.addAll(wrapper2?.data ?: listOf())

        emit(Resource.Success(wrapper))
        emit(Resource.Loading(show = false))
    }

    fun getQuestionCount(): Int {
        return if (questions.size >= 1) {
            questions.size
        } else {
            1
        }
    }

    fun getQuestionType(position: Int): QuestionType {
        return questions[position].type
    }

    fun getQuestionCounter(position: Int): String {
        return "${position + 1}/${assignmentSettings?.questionsNumber}"
    }

    fun getQuestion(position: Int): Question {
        return questions[position]
    }

    fun getSettings(): AssignmentSettings? {
        return assignmentSettings
    }

    fun getAssignment(): Assignment? {
        return assignment
    }

    fun saveAnswers(data: Map<String, Any?>?) = liveData {
        emit(Resource.Loading(show = true))
        val resource: Resource = tryNoContentResource {
            assignmentsRepository.saveAnswers(data)
        }

        if (assignmentSettings?.canNavigateBetweenQuestions() == false) {
            tryNoContentResource { assignmentsRepository.saveAnswers(data) }
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun submitAnswers(allQuestionsData: List<Map<String, Any?>?>) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val data = getData(allQuestionsData)
        val resource = tryNoContentResource { assignmentsRepository.submitAnswers(data) }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private fun getData(allQuestionsData: List<Map<String, Any?>?>): Map<String, Any?> {
        val result = mutableMapOf<String, Any?>()
        allQuestionsData.forEach { data -> data?.forEach { result[it.key] = it.value } }
        return result
    }

    fun canShowResponse(): Boolean {
        return assignmentSettings?.showResponses == true
    }

    fun getResponse(position: Int, isCorrectAnswer: Boolean): String? {
        return if (isCorrectAnswer) {
            questions[position].correctAnswerResponse
        } else {
            questions[position].wrongAnswerResponse
        }
    }

    fun canNavigateBetweenQuestions(): Boolean {
        return assignmentSettings == null || assignmentSettings?.canNavigateBetweenQuestions() == true
    }

    fun canSaveAnswers(): Boolean {
        return (assignmentSettings != null &&
                (assignmentSettings!!.questionTime != 0 || assignmentSettings!!.showResponses != false))
    }

    fun getQuestionNumber(position: Int): Int {
        var questionNumber = 0
        questions.forEachIndexed { index, question ->
            if (index == position) {
                return questionNumber
            }
            questionNumber += question.subQuestions?.getAllQuestions()?.size ?: 1
        }
        return questionNumber
    }

    fun resetQuestionTimeLeft() {
        assignmentSettings?.questionTimeLeft = assignmentSettings?.questionTime?.toLong()
    }

    fun canShowHint(): Boolean {
        return assignmentSettings?.showHints == true
    }

    fun getHint(position: Int): String? {
        return questions[position].questionHint
    }

    fun getLastQuestion(): UserLastQuestion?{
        return userLastQuestion
    }

    fun insertLastQuestion(userLastQuestion: UserLastQuestion) {
        viewModelScope.launch {
            assignmentsRepository.insertLastQuestion(userLastQuestion)
        }
    }

    fun deleteLastQuestion() {
        viewModelScope.launch {
            assignmentsRepository.deleteLastQuestion(assignment?.id.toString())
        }
    }

}
