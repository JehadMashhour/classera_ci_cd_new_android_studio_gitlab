@file:Suppress("DEPRECATION")

package com.classera.assignments.solve

import android.app.AlertDialog
import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.classera.assignments.R
import com.classera.assignments.databinding.FragmentSloveAssignmentBinding
import com.classera.assignments.solve.response.AnswerResponseBottomSheetFragment
import com.classera.assignments.solve.types.BaseQuestionTypeFragment
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onPageChanged
import com.classera.core.utils.android.removeNull
import com.classera.core.utils.android.setCurrentItemWithoutListener
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assignments.QuestionsWrapper
import com.classera.data.models.assignments.UserLastQuestion
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Feb 18, 2020
 *
 * @author Mohamed Hamdan
 */
@Screen("Solve Assignment")
class SolveAssignmentFragment : BaseBindingFragment() {

    @Inject
    lateinit var viewModel: SolveAssignmentViewModel

    @Inject
    lateinit var prefs: Prefs

    private val args: SolveAssignmentFragmentArgs by navArgs()

    private var progressDialog: ProgressDialog? = null

    private var questionsAdapter: QuestionsAdapter? = null
    private var questionTabsAdapter: QuestionTabsAdapter? = null

    private var clickedPosition = -1
    private var position = 0
    private var isFirst = true
    private var currentPage = 0
    private var isNext = true
    private var isLastQuestion = false
    private var resetTimer = true
    private var submitClicked = false

    private var progressBar: ProgressBar? = null
    private var linearLayoutContent: LinearLayout? = null
    private var errorView: ErrorView? = null
    private var viewPager: ViewPager2? = null
    private var recyclerViewQuestions: RecyclerView? = null
    private var textViewQuestionNumber: TextView? = null
    private var textViewQuestionsCounter: TextView? = null
    private var buttonNext: Button? = null
    private var buttonPrevious: Button? = null
    private var buttonSubmit: Button? = null
    private var textViewShowHint: TextView? = null
    private var timeLeftLayout: LinearLayout? = null

    private var userLastQuestion: UserLastQuestion? = null

    override val layoutId: Int = R.layout.fragment_slove_assignment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.lifecycleOwner = this
        initProgressDialog()
        findViews()
        userLastQuestion = viewModel.getLastQuestion()
        initAdapters()
        initAdapterListeners()
        initViewListeners()
        getQuestions()
        handelMissedQuestionObserver()
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(requireContext())
        progressDialog?.setMessage(getString(R.string.please_wait))
        progressDialog?.setCancelable(false)
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_solve_assignment)
        linearLayoutContent = view?.findViewById(R.id.linear_layout_fragment_solve_assignment_content)
        errorView = view?.findViewById(R.id.error_view_fragment_solve_assignment)
        viewPager = view?.findViewById(R.id.view_pager_fragment_solve_assignment_questions)
        textViewQuestionNumber = view?.findViewById(R.id.text_view_fragment_solve_assignment_question_number)
        textViewQuestionsCounter = view?.findViewById(R.id.text_view_fragment_solve_assignment_questions_counter)
        recyclerViewQuestions = view?.findViewById(R.id.recycler_view_fragment_solve_assignment_questions)
        buttonNext = view?.findViewById(R.id.button_fragment_solve_assignments_next)
        buttonSubmit = view?.findViewById(R.id.button_fragment_solve_assignments_submit)
        buttonPrevious = view?.findViewById(R.id.button_fragment_solve_assignments_previous)
        textViewShowHint = view?.findViewById(R.id.text_view_fragment_solve_assignment_show_hint)
        timeLeftLayout = view?.findViewById(R.id.linear_layout_fragment_solve_assignment_time_left)

        if(prefs.userRole == UserRole.GUARDIAN)
            buttonSubmit?.visibility = View.INVISIBLE

        if(args.assignment?.duration == ZERO)
            timeLeftLayout?.visibility = View.INVISIBLE

    }

    private fun handelMissedQuestionObserver() {
        viewModel.missedQuestionErrorLiveData.observe(this) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            viewModel.deleteLastQuestion()
            findNavController().popBackStack()
        }
    }

    private fun initAdapters() {
        questionsAdapter = QuestionsAdapter(viewModel)
    }

    private fun initAdapterListeners() {
        questionsAdapter?.setOnItemClickListener { _, position ->
            if (viewModel.canNavigateBetweenQuestions()) {
                clickedPosition = position
                isNext = (viewPager?.currentItem ?: 0) < position
                saveAnswers(viewPager?.currentItem ?: 0)
                handleQuestionNumber(position)
                handleQuestionNumbersState(position)
            }

            handleNavigationButtonsEnabledState(position)
        }

        viewPager?.isUserInputEnabled = false
        viewPager?.onPageChanged { position ->
            handleOnPageChanged(position)
        }
    }

    private fun handleOnPageChanged(position: Int) {
        if(userLastQuestion != null){
            handleQuestionNumber(userLastQuestion!!.questionNumber.toInt())
            handleQuestionNumbersState(userLastQuestion!!.questionNumber.toInt())
            handleNavigationButtonsEnabledState(userLastQuestion!!.questionNumber.toInt())
            viewPager?.currentItem = userLastQuestion!!.questionNumber.toInt()
        }else {
            handleQuestionNumber(position)
            handleQuestionNumbersState(position)
            handleNavigationButtonsEnabledState(position)
        }

        isNext = currentPage < position
        currentPage = position

        if (isFirst) {
            isFirst = false
            return
        }
    }

    private fun handleNavigationButtonsEnabledState(position: Int) {
        buttonNext?.isEnabled = position < (viewModel.getQuestionCount() - 1)
        buttonPrevious?.isEnabled = position > 0 && viewModel.canNavigateBetweenQuestions()
        buttonSubmit?.isEnabled =
            viewModel.canNavigateBetweenQuestions() || position == (viewModel.getQuestionCount() - 1)
    }

    private fun handleQuestionNumber(position: Int) {
        val question = position + 1
        textViewQuestionNumber?.text = getString(R.string.label_fragment_solve_assignment_question_number, question)
        textViewQuestionsCounter?.text = viewModel.getQuestionCounter(position)
    }

    private fun handleQuestionNumbersState(position: Int) {
        questionsAdapter?.setSelectedPosition(position)
        recyclerViewQuestions?.smoothScrollToPosition(position)
    }

    private fun initViewListeners() {
        buttonNext?.setOnClickListener {
            isNext = true
            saveAnswers(viewPager?.currentItem ?: 0)
            handleNavigationButtonsEnabledState(position+1)
        }

        buttonPrevious?.setOnClickListener {
            isNext = false
            saveAnswers(viewPager?.currentItem ?: 0)
            handleNavigationButtonsEnabledState(position-1)
        }

        buttonSubmit?.setOnClickListener {
            if (viewModel.canSaveAnswers()) {
                submitClicked = true
                saveAnswers(viewPager?.currentItem ?: 0)
            } else {
                val allQuestionsData = getAllQuestionsData()
                submitAnswers(allQuestionsData)
            }
        }

        textViewShowHint?.setOnClickListener{
            viewPager?.currentItem?.let { it1 -> handleShowHint(it1) }
        }
    }

    private fun getAllQuestionsData(): List<Map<String, Any?>?> {
        val fragments = mutableListOf<BaseQuestionTypeFragment?>()
        val fragmentCount = questionTabsAdapter?.itemCount ?: 0
        for (fragmentIndex in 0 until fragmentCount) {
            val fragment = questionTabsAdapter?.getFragment(fragmentIndex)
            fragments.add(fragment)
        }
        return fragments.map { it?.getSubmitData()?.removeNull() }
    }

    private fun saveAnswers(position: Int) {
        resetTimer = position + 1 != viewModel.getQuestionCount()

        this.position = position
        val currentFragment = questionTabsAdapter?.getFragment(position)
        val data = currentFragment?.getData()?.removeNull()
        if (viewModel.canSaveAnswers()) {
            if (resetTimer) {
                viewModel.resetQuestionTimeLeft()
            }
            viewModel.saveAnswers(data).observe(this, ::handleSaveAnswersResource)
        } else {
            val selectedItem = if (isNext) (viewPager?.currentItem ?: 0) + 1 else (viewPager?.currentItem ?: 0) - 1
            if (selectedItem != -1) {
                viewPager?.setCurrentItemWithoutListener(selectedItem)
                questionsAdapter?.setSelectedPosition(selectedItem)
                handleQuestionNumber(selectedItem)
                handleQuestionNumbersState(selectedItem)
            }
        }
    }

    private fun handleSaveAnswersResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSaveAnswersLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSaveAnswersSuccessResource()
            }
            is Resource.Error -> {
                handleSaveAnswersErrorResource(resource)
            }
        }
    }

    private fun handleSaveAnswersLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.dismiss()
        }
    }

    private fun handleSaveAnswersSuccessResource() {
        if (submitClicked || position + 1 == viewModel.getQuestionCount()) {
            val allQuestionsData = getAllQuestionsData()
            submitAnswers(allQuestionsData)
        } else {
            handleShowResponse(position)
            val position = when {
                clickedPosition != -1 -> clickedPosition
                isNext -> this.position + 1
                else -> this.position - 1
            }

            viewModel.insertLastQuestion(
                UserLastQuestion(
                    questionId = viewModel.getQuestion(position).id.toString(),
                    examId = viewModel.getAssignment()?.id.toString(),
                    questionNumber = viewModel.getQuestionNumber(position).toString(),
                    userId = ""
                )
            )

            viewPager?.setCurrentItemWithoutListener(position)
            handleQuestionNumber(position)
            handleQuestionNumbersState(position)
            handleNavigationButtonsEnabledState(position)
            clickedPosition = -1

        }
    }

    private fun handleSaveAnswersErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.resourceMessage, Toast.LENGTH_LONG).show()
    }

    private fun handleShowResponse(position: Int) {
        val fragment = questionTabsAdapter?.getFragment(position)
        if (viewModel.canShowResponse()) {
            val response = viewModel.getResponse(position, fragment?.isCorrectAnswer() == true)
            AnswerResponseBottomSheetFragment.show(childFragmentManager, response)
        }
    }

    private fun submitAnswers(allQuestionsData: List<Map<String, Any?>?>) {
        if(isLastQuestion){
            viewModel.submitAnswers(allQuestionsData).observe(this, ::handleSubmitAnswersResource)
        }else {
            AlertDialog.Builder(requireContext())
                .setTitle(getString(R.string.title_submit_answers_dialog))
                .setMessage(getString(R.string.message_submit_answers_dialog))
                .setPositiveButton(android.R.string.yes) { _, _ ->
                    viewModel.submitAnswers(allQuestionsData).observe(this, ::handleSubmitAnswersResource)
                }
                .setNegativeButton(android.R.string.no, null)
                .show()
        }
    }

    private fun handleSubmitAnswersResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSubmitAnswersLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSubmitAnswersSuccessResource()
            }
            is Resource.Error -> {
                handleSubmitAnswersErrorResource(resource)
            }
        }
    }

    private fun handleSubmitAnswersLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.dismiss()
        }
    }

    private fun handleSubmitAnswersSuccessResource() {
        viewModel.deleteLastQuestion()
        Toast.makeText(requireContext(), resources.getString(R.string.toast_exam_submitted), Toast.LENGTH_LONG).show()
        findNavController().popBackStack()
    }

    private fun handleSubmitAnswersErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.resourceMessage, Toast.LENGTH_LONG).show()
    }

    private fun getQuestions() {
        viewModel.getQuestions().observe(this, ::handleResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<QuestionsWrapper>>)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            linearLayoutContent?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            linearLayoutContent?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource(resource: Resource.Success<BaseWrapper<QuestionsWrapper>>) {
        bind<FragmentSloveAssignmentBinding> {
            this?.settings = resource.data?.data?.assignmentSettings

        }

        if (resource.data?.data?.assignmentSettings?.timeLeft ?: 0 <= ZERO) {
            resource.data?.data?.assignmentSettings?.questionTimeLeftTimeLiveData?.observe(this) {
                if (it == TIME_LEFT_ZERO) {
                    isNext = true
                    viewModel.resetQuestionTimeLeft()
                    saveAnswers(viewPager?.currentItem ?: 0)
                    isLastQuestion = position + 1 == viewModel.getQuestionCount()
                    if (isLastQuestion)
                        resource.data?.data?.assignmentSettings?.questionTimeLeftTimeLiveData?.removeObservers(this)
                }
            }
        } else {
            resource.data?.data?.assignmentSettings?.timeLeftTimeLiveData?.observe(this) {
                if (it == TIME_LEFT_ZERO) {
                    resource.data?.data?.assignmentSettings?.timeLeftTimeLiveData?.removeObservers(this)
                    val allQuestionsData = getAllQuestionsData()
                    Toast.makeText(requireContext(), resources.getString(R.string.toast_time_left), Toast.LENGTH_LONG)
                        .show()
                    viewModel.submitAnswers(allQuestionsData).observe(this, ::handleSubmitAnswersResource)
                }
            }
        }
        questionTabsAdapter = QuestionTabsAdapter(this, viewModel)
        viewPager?.offscreenPageLimit = viewModel.getQuestionCount()
        viewPager?.adapter = questionTabsAdapter
        recyclerViewQuestions?.adapter = questionsAdapter
    }


    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener {
            getQuestions()
        }
    }

    private fun handleShowHint(position: Int) {
        if (viewModel.canShowHint()) {
            val response = viewModel.getHint(position)
            AnswerResponseBottomSheetFragment.show(childFragmentManager, response)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        progressDialog?.dismiss()
        progressDialog = null
        progressBar = null
        linearLayoutContent = null
        errorView = null
        viewPager?.adapter = null
        viewPager = null
        recyclerViewQuestions?.adapter = null
        recyclerViewQuestions = null
        textViewQuestionNumber = null
        textViewQuestionsCounter = null
        buttonNext = null
        buttonSubmit = null
        textViewShowHint = null
        timeLeftLayout = null
    }

    private companion object {

        private const val TIME_LEFT_ZERO = "00.00.01"
        private const val ZERO = 0L

    }
}
