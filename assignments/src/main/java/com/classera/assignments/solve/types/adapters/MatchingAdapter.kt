package com.classera.assignments.solve.types.adapters

import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.recyclerview.widget.RecyclerView
import com.classera.assignments.R
import com.classera.assignments.databinding.RowMatchingBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.utils.android.onTextChanged
import com.classera.data.models.assignments.SubQuestions

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
class MatchingAdapter(private val questions: SubQuestions?) : BaseAdapter<MatchingAdapter.ViewHolder>() {

    private var onItemSelectedListener: ((test: String, position: Int) -> Unit)? = null

    override fun getViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        return ViewHolder(RowMatchingBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return questions?.questions?.size ?: 0
    }

    fun setOnItemSelectedListener(onItemSelectedListener: (test: String, position: Int) -> Unit) {
        this.onItemSelectedListener = onItemSelectedListener
    }

    inner class ViewHolder(binding: RowMatchingBinding) : BaseBindingViewHolder(binding) {

        private var autoCompleteTextViewAnswers: AutoCompleteTextView? = null

        init {
            autoCompleteTextViewAnswers = itemView.findViewById(R.id.auto_complete_text_view_row_matching_answers)
            initAdapter()
        }

        private fun initAdapter() {
            val adapter = ArrayAdapter(context!!, R.layout.dropdown_menu_popup_item, (questions?.choices ?: listOf()))
            autoCompleteTextViewAnswers?.onTextChanged {
                val currentPosition = adapterPosition
                if (currentPosition != RecyclerView.NO_POSITION) {
                    val text = autoCompleteTextViewAnswers?.text?.toString() ?: ""
                    onItemSelectedListener?.invoke(text, currentPosition)
                }
            }
            autoCompleteTextViewAnswers?.setAdapter(adapter)
        }

        override fun bind(position: Int) {

            bind<RowMatchingBinding> {
                this.question = questions?.questions?.get(position)
            }
        }
    }
}
