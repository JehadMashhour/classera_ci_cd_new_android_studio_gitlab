package com.classera.assignments.solve.types.adapters

import android.view.ViewGroup
import com.classera.assignments.databinding.RowAnswerRadioButtonBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.data.models.assignments.SelectableChoice

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
class MultipleChoicesAdapter(
    private val choices: List<SelectableChoice>?
) : BaseAdapter<MultipleChoicesAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowAnswerRadioButtonBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return choices?.size ?: 0
    }

    fun setSelectedPosition(position: Int) {
        choices?.forEachIndexed { index, selectableChoice -> if (position != index) selectableChoice.selected = false }
        choices?.get(position)?.toggle()
    }

    inner class ViewHolder(binding: RowAnswerRadioButtonBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowAnswerRadioButtonBinding> {
                this.choice = choices?.get(position)
                this.position = position
            }
        }
    }
}
