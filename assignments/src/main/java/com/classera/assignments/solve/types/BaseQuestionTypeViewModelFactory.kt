package com.classera.assignments.solve.types

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.assignments.AssignmentsRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: June 15, 2020
 *
 * @author ٌRawan Altheeb
 */

class BaseQuestionTypeViewModelFactory @Inject constructor(
    private val assignmentsRepository: AssignmentsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return BaseQuestionTypeViewModel(
            assignmentsRepository
        ) as T
    }
}
