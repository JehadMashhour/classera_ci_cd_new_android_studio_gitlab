package com.classera.assignments.solve

import android.view.ViewGroup
import com.classera.assignments.databinding.RowExamBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
class QuestionsAdapter(
    private val viewModel: SolveAssignmentViewModel
) : BaseAdapter<QuestionsAdapter.ViewHolder>() {

    private var selectedPosition: Int = 0

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowExamBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getQuestionCount()
    }

    fun setSelectedPosition(position: Int) {
        this.selectedPosition = position
        notifyDataSetChanged()
    }

    inner class ViewHolder(binding: RowExamBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowExamBinding> {
                this.question = viewModel.getQuestion(position)
                this.selectedPosition = this@QuestionsAdapter.selectedPosition
                this.position = position
            }
        }
    }
}
