package com.classera.assignments.create.essay

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.repositories.assignments.AssignmentsRepository

@Suppress("LongParameterList")
class EssayQuestionViewModel(

    private val assignmentsRepository: AssignmentsRepository
) : BaseViewModel() {

    fun onSaveClicked(
        assignmentId: String?,
        type: String?,
        text: String?,
        mark: String?,
        difficulty: String?,
        correctAnswer: String?
        ) = liveData {
        emit(Resource.Loading(show = true))
        val resource = createEssayQuestion(assignmentId, type, text,mark, difficulty, correctAnswer)
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private suspend fun createEssayQuestion(
        assignmentId: String?,
        type: String?,
        text: String?,
        mark: String?,
        difficulty: String?,
        correctAnswer: String?
    ): Resource =
        tryNoContentResource {
            assignmentsRepository.createEssayQuestion(
                assignmentId = assignmentId,
                type = type,
                text = text,
                mark = mark,
                difficulty = difficulty,
                correctAnswer = correctAnswer
            )
        }

}
