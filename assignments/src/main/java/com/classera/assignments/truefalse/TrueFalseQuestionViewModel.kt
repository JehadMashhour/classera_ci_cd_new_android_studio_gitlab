package com.classera.assignments.truefalse

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.repositories.assignments.AssignmentsRepository

/**
 * Created on 18/03/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
@Suppress("LongParameterList")
class TrueFalseQuestionViewModel(
    private val assignmentsRepository: AssignmentsRepository
) : BaseViewModel() {

    fun addToTrueFalseQuestion(
        assignmentId: String?, type: String?, text: String?,
        mark: String?, difficulty: String?, correctAnswer: Int?
    ) = liveData {
        emit(Resource.Loading(show = true))
        val resource = addTrueFalseQuestion(assignmentId, type, text, mark, difficulty, correctAnswer)
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private suspend fun addTrueFalseQuestion(
        assignmentId: String?, type: String?, text: String?,
        mark: String?, difficulty: String?, correctAnswer: Int?
    ): Resource = tryNoContentResource {
        assignmentsRepository.addTrueFalseQuestion(assignmentId, type, text, mark, difficulty, correctAnswer)
    }
}
