package com.classera.assignments.truefalse

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.assignments.AssignmentsRepository
import javax.inject.Inject

/**
 * Created on 18/03/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class TrueFalseQuestionModelFactory @Inject constructor(
    private val assignmentsRepository: AssignmentsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TrueFalseQuestionViewModel(assignmentsRepository) as T
    }
}
