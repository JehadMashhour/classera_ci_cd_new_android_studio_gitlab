package com.classera.assignments.truefalse

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RadioButton
import android.widget.Spinner
import android.widget.Toast
import com.classera.assignments.R
import com.classera.core.activities.BaseValidationActivity
import com.classera.core.custom.views.ErrorView
import com.classera.core.utils.android.observe
import com.classera.data.network.errorhandling.Resource
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import javax.inject.Inject

/**
 * Created on 18/03/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class TrueFalseQuestionActivity : BaseValidationActivity() {

    @Inject
    lateinit var viewModel: TrueFalseQuestionViewModel

    @NotEmpty(message = "Please add question title")
    private var questionTitle: EditText? = null

    @NotEmpty(message = "Please add mark")
    private var questionMark: EditText? = null

    private var trueAnswer: RadioButton? = null
    private var falseAnswer: RadioButton? = null
    private var difficulties: Spinner? = null
    private var addQuestionButton: Button? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_true_false_question)

        findViews()
        initListeners()
    }

    private fun findViews() {
        questionTitle = findViewById(R.id.edit_text_add_question_title)
        questionMark = findViewById(R.id.edit_text_add_question_mark)
        difficulties = findViewById(R.id.spinner_difficulty_fragment_true_false_question)
        progressBar = findViewById(R.id.progress_bar_fragment_true_false_question)
        errorView = findViewById(R.id.error_view_fragment_true_false_question)
        addQuestionButton = findViewById(R.id.add_question_button_fragment_true_false_question)
        trueAnswer = findViewById(R.id.true_answer_fragment_true_false_question)
        falseAnswer = findViewById(R.id.false_answer_fragment_true_false_question)
    }

    private fun initListeners() {
        addQuestionButton?.setOnClickListener {
            validator.validate()
        }
    }

    override fun onValidationSucceeded() {
        val trueAnswer = if (trueAnswer?.isChecked == true) 1 else 0
        val falseAnswer = if (falseAnswer?.isChecked == true) 1 else 0

        if (questionMark?.text?.toString().equals("0")) {
            Toast.makeText(this, getString(R.string.mark_entered_check), Toast.LENGTH_LONG).show()
            return
        }

        if (trueAnswer == 0 && falseAnswer == 0) {
            Toast.makeText(this, getString(R.string.ask_to_choose_answer), Toast.LENGTH_LONG).show()
            return
        }
        val questionAnswer = if (trueAnswer == 1) 1 else 2

        viewModel.addToTrueFalseQuestion(
            "000",
            TRUE_FALSE_TYPE,
            questionTitle?.text.toString(),
            questionMark?.text.toString(),
            difficulties?.selectedItemPosition.toString(),
            questionAnswer
        ).observe(this, ::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            addQuestionButton?.isEnabled = false
        } else {
            progressBar?.visibility = View.GONE
            addQuestionButton?.isEnabled = true
        }
    }

    private fun handleSuccessResource() {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.question_added_successfully))
            .setPositiveButton(android.R.string.yes) { _, _ ->
                this.finish()
            }
            .show()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        Toast.makeText(this, resource.error.message, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        questionMark = null
        questionTitle = null
        trueAnswer = null
        falseAnswer = null
        difficulties = null
        addQuestionButton = null
        progressBar = null
        errorView = null
        super.onDestroy()
    }

    companion object {
        const val TRUE_FALSE_TYPE = "taf"
    }
}
