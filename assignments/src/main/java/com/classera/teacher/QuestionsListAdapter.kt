package com.classera.teacher

import android.view.ViewGroup
import com.classera.assignments.databinding.RowQuestionsListBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder


/**
 * Created by Rawan Al-Theeb on 3/20/2020.
 * Classera
 * r.altheeb@classera.com
 */
class QuestionsListAdapter(private val viewModel: QuestionsListViewModel) : BaseAdapter<BaseBindingViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowQuestionsListBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getQuestionsCount()
    }

    inner class ViewHolder(binding: RowQuestionsListBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowQuestionsListBinding> { question = viewModel.getQuestion(position) }
        }
    }
}

