package com.classera.schools

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class SchoolsFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: SchoolsViewModelFactory
        ): SchoolsViewModel{
            return ViewModelProvider(fragment, factory)[SchoolsViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: SchoolsFragment): Fragment
}

