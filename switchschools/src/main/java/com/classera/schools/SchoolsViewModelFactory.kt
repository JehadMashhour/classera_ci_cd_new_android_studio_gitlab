package com.classera.schools

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.switchschools.SwitchSchoolsRepository
import javax.inject.Inject

class SchoolsViewModelFactory @Inject constructor(
    private val switchSchoolsRepository: SwitchSchoolsRepository,
    private val prefs: Prefs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SchoolsViewModel(switchSchoolsRepository, prefs) as T
    }
}

