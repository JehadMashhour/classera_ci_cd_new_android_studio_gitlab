@file:Suppress("DEPRECATION")

package com.classera.schools.sub

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.Activities
import com.classera.core.fragments.BaseFragment
import com.classera.core.intentTo
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.network.errorhandling.Resource
import com.classera.schools.R
import javax.inject.Inject

class SubSchoolsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: SubSchoolsViewModel

    private var progressDialog: ProgressDialog? = null

    private var recyclerView: RecyclerView? = null
    private var adapter: SubSchoolsAdapter? = null
    private var searchView: SearchView? = null

    override val layoutId: Int = R.layout.fragment_sub_schools

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        handleAdapter()
        initProgressDialog()
    }

    private fun handleAdapter() {
        if(viewModel.getSchoolCount() == 0) {
            val manager = GridLayoutManager(
                activity, 1, GridLayoutManager.HORIZONTAL,
                false
            )
            recyclerView?.layoutManager = manager
        }else{
            val manager = GridLayoutManager(
                activity, 2, GridLayoutManager.VERTICAL,
                false
            )
            recyclerView?.layoutManager = manager
        }

        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(requireContext())
        progressDialog?.setMessage(getString(R.string.please_wait))
    }

    private fun findViews() {
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_switch_schools)
    }

    private fun initAdapter() {
        adapter = SubSchoolsAdapter(viewModel)
        adapter?.setOnItemClickListener { view, position ->
            if (viewModel.getSchool(position).subSchools?.size != 0) {
                val school = viewModel.getSchool(position)
                val schools = school.subSchools?.toTypedArray()
                val title = school.schoolName
                schools?.let {
                    findNavController().navigate(
                        SubSchoolsFragmentDirections.actionSubSchoolsFragment(title, schools)
                    )
                }
            } else {
                changeSchool(position)
            }
        }
        recyclerView?.adapter = adapter
    }

    private fun changeSchool(position: Int) {
        val schoolName = viewModel.getSchool(position).schoolName
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.title_schools_fragment_change_school_confirmation)
            .setMessage(
                getString(
                    R.string.message_schools_fragment_change_school_confirmation,
                    schoolName
                )
            )
            .setPositiveButton(android.R.string.yes) { _, _ ->
                viewModel.changeSelectedSchool(position).observe(this, ::handleAuthResponse)
            }
            .setNegativeButton(android.R.string.no, null)
            .show()
    }

    private fun handleAuthResponse(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleAuthLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleAuthSuccessResource()
            }
            is Resource.Error -> {
                handleAuthErrorResource(resource)
            }
        }
    }

    private fun handleAuthLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.hide()
        }
    }

    private fun handleAuthSuccessResource() {
        val intent = intentTo(Activities.Splash)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun handleAuthErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_switch_schools, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_switch_schools_search)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange {
            viewModel.currentSearchValue = it.toString().trim()
            viewModel.getFilteredSchoolList()
            handleAdapter()
        }

        searchView?.setOnCloseListener {
            handleAdapter()
            return@setOnCloseListener false
        }
    }

    override fun onDestroyView() {
        recyclerView = null
        adapter = null
        super.onDestroyView()
    }
}

