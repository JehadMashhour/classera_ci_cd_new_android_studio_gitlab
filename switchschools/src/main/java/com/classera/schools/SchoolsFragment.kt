@file:Suppress("DEPRECATION")

package com.classera.schools

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Activities
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.intentTo
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject

@Screen("Switch schools")
class SchoolsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: SchoolsViewModel

    private var progressDialog: ProgressDialog? = null

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var adapter: SchoolsAdapter? = null
    private var errorView: ErrorView? = null
    private var searchView: SearchView? = null

    override val layoutId: Int = R.layout.fragment_schools

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
        initProgressDialog()
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(requireContext())
        progressDialog?.setMessage(getString(R.string.please_wait))
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_switch_schools)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_switch_schools)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_switch_schools)
        errorView = view?.findViewById(R.id.error_view_fragment_switch_schools)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            adapter?.resetPaging()
            getSchoolsList()
        }
        getSchoolsList()
    }

    private fun getSchoolsList() {
        viewModel.getSchoolsList().observe(this, this::handleResource)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_switch_schools, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_switch_schools_search)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange {
            viewModel.currentSearchValue = it.toString().trim()
            viewModel.getFilteredSchoolList()
            handleSuccessResource()
        }

        searchView?.setOnCloseListener {
            getSchoolsList()
            return@setOnCloseListener false
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if(viewModel.getSchoolsCount() == 0) {
            val manager = GridLayoutManager(
                activity, 1, GridLayoutManager.HORIZONTAL,
                false
            )
            recyclerView?.layoutManager = manager
        }else{
            val manager = GridLayoutManager(
                activity, 2, GridLayoutManager.VERTICAL,
                false
            )
            recyclerView?.layoutManager = manager
        }

        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = SchoolsAdapter(viewModel)
        adapter?.setOnItemClickListener { _, position ->
            if (viewModel.getSchool(position).subSchools?.size != 0) {
                val school = viewModel.getSchool(position)
                val schools = school.subSchools?.toTypedArray()
                val title = school.schoolName
                schools?.let {
                    findNavController().navigate(
                        SchoolsFragmentDirections.actionSubSchoolsFragment(
                            title,
                            schools
                        )
                    )
                }
            } else {
                changeSchool(position)
            }
        }
        recyclerView?.adapter = adapter
    }

    private fun changeSchool(position: Int) {
        val schoolName = viewModel.getSchool(position).schoolName
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.title_schools_fragment_change_school_confirmation)
            .setMessage(
                getString(
                    R.string.message_schools_fragment_change_school_confirmation,
                    schoolName
                )
            )
            .setPositiveButton(android.R.string.yes) { _, _ ->
                viewModel.changeSelectedSchool(position).observe(this, ::handleAuthResponse)
            }
            .setNegativeButton(android.R.string.no, null)
            .show()
    }

    private fun handleAuthResponse(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleAuthLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleAuthSuccessResource()
            }
            is Resource.Error -> {
                handleAuthErrorResource(resource)
            }
        }
    }

    private fun handleAuthLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.hide()
        }
    }

    private fun handleAuthSuccessResource() {
        val intent = intentTo(Activities.Splash)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun handleAuthErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getSchoolsList() }
        adapter?.finishLoading()
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }
}

