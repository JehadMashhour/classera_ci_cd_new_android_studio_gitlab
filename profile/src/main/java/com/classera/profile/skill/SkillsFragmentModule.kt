package com.classera.profile.skill

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.profile.ProfileViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Module
abstract class SkillsFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ProfileViewModelFactory
        ): SkillsViewModel {
            return ViewModelProvider(fragment, factory)[SkillsViewModel::class.java]
        }

    }

    @Binds
    abstract fun bindActivity(activity: SkillsFragment): Fragment

}
