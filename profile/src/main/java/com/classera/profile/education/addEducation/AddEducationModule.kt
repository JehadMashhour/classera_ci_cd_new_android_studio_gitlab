package com.classera.profile.education.addEducation

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.profile.ProfileViewModelFactory
import com.classera.profile.personal.addcity.AddCountryCityBottomSheet
import com.classera.profile.personal.addcity.AddCountryCityViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Module
abstract class AddEducationModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ProfileViewModelFactory
        ): AddEducationViewModel {
            return ViewModelProvider(fragment, factory)[AddEducationViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: AddEducationBottomSheet): Fragment

}
