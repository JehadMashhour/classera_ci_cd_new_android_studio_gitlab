package com.classera.profile.education

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.custom.callbacks.SwipeToDeleteCallback
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import androidx.lifecycle.observe
import com.classera.data.network.errorhandling.Resource
import com.classera.profile.R
import com.classera.profile.education.addEducation.AddEducationBottomSheet
import com.classera.profile.education.addEducation.AddEducationBottomSheet.Companion.MODE_CREATE
import com.classera.profile.education.addEducation.AddEducationBottomSheet.Companion.MODE_MODIFY
import com.github.florent37.expansionpanel.ExpansionHeader
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class EducationFragment : BaseFragment(), EducationFragmentRefresh {

    @Inject
    lateinit var viewModel: EducationViewModel

    private var educationExpansionHeader: ExpansionHeader? = null
    private var educationRecylerview: RecyclerView? = null
    private var addEducation: AppCompatImageView? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var content: LinearLayout? = null

    private var educationAdapter: EducationAdapter? = null
    private var swipeToDeleteEducation: SwipeToDeleteCallback? = null

    override val layoutId: Int = R.layout.fragment_education

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        getData()
        initListeners()
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleSuccessResource() {
        content?.visibility = View.VISIBLE
        initAdapters()
        initSwipeToDeleteListener()
    }

    private fun initSwipeToDeleteListener() {
        if (viewModel.getEducationsCount() != 0) {
            val itemTouchHelperEducation = ItemTouchHelper(swipeToDeleteEducation!!)
            itemTouchHelperEducation.attachToRecyclerView(educationRecylerview)
        }

        swipeToDeleteEducation?.getSwiped()?.observe(this) {
            if (it) {
                swipeToDeleteEducation?.updateSwipe(false)
                showConfirmToDeleteDialog(swipeToDeleteEducation?.position!!)
            }
        }
    }

    private fun showConfirmToDeleteDialog(position: Int) {
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(getString(R.string.delete))
                .setMessage(R.string.confirm_delete)
                .setPositiveButton(R.string.discard) { _, _ ->
                    viewModel.delete(position).observe(this, this::handleDeleteEducationResource)
                }
                .setNegativeButton(android.R.string.cancel) { _, _ ->
                    educationAdapter?.notifyItemChanged(position)
                }
                .setCancelable(false)
                .show()
        }
    }

    private fun handleDeleteEducationResource(resource: Resource) {
        if (resource is Resource.Error) {
            Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
            educationAdapter?.notifyItemChanged(swipeToDeleteEducation?.position!!)
        } else {
            educationAdapter?.removeItem(swipeToDeleteEducation?.position!!)
        }
    }

    private fun initAdapters() {
        educationAdapter = EducationAdapter(viewModel)
        educationRecylerview?.adapter = educationAdapter

        educationAdapter?.setOnItemClickListener { _, position ->
            val education = viewModel.getEducation(position)
            AddEducationBottomSheet(this, MODE_MODIFY, education, "edit").show(fragmentManager!!, "")
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            errorView?.visibility = View.GONE
            content?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        progressBar?.visibility = View.GONE
        errorView?.visibility = View.VISIBLE
        content?.visibility = View.GONE

        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getData() }
    }

    private fun initListeners() {
        addEducation?.setOnClickListener {
            AddEducationBottomSheet(this, MODE_CREATE, null, "save").show(fragmentManager!!, "")
        }
    }

    private fun findViews() {
        educationExpansionHeader = view?.findViewById(R.id.profile_education_expansion_header_education)
        educationRecylerview = view?.findViewById(R.id.profile_education_rv_education)
        addEducation = view?.findViewById(R.id.profile_education_add_education)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_education)
        errorView = view?.findViewById(R.id.error_view_fragment_education)
        content = view?.findViewById(R.id.profile_education_content)

        swipeToDeleteEducation = SwipeToDeleteCallback(
            0,
            ItemTouchHelper.LEFT,
            context!!,
            educationRecylerview
        )
    }

    override fun refresh() {
        getData()
    }

    private fun getData() {
        viewModel.getEducation().observe(this, this::handleResource)
    }

    override fun onDestroy() {
        educationExpansionHeader = null
        educationRecylerview = null
        addEducation = null
        progressBar = null
        errorView = null
        content = null
        super.onDestroy()
    }
}
