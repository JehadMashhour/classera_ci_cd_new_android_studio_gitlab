package com.classera.profile.education.addEducation

import android.icu.util.Calendar
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.observe
import com.classera.core.fragments.BaseBottomSheetValidationDialogFragment
import com.classera.data.models.BaseWrapper
import com.classera.data.models.profile.CityResponse
import com.classera.data.models.profile.CountryResponse
import com.classera.data.models.profile.Education
import com.classera.data.network.errorhandling.Resource
import com.classera.profile.R
import com.classera.profile.education.EducationFragmentRefresh
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.whiteelephant.monthpicker.MonthPickerDialog
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class AddEducationBottomSheet(
    private val fragment: EducationFragmentRefresh,
    private val mode: Int,
    private val education: Education?,
    private val type: String
) : BaseBottomSheetValidationDialogFragment() {

    @Inject
    lateinit var viewModel: AddEducationViewModel

    private var buttonSubmit: Button? = null
    private var progressBar: ProgressBar? = null
    private var closeImageView: AppCompatImageView? = null
    private var countryLayout: TextInputLayout? = null
    private var cityLayout: TextInputLayout? = null
    private var textViewTitle: AppCompatTextView? = null
    private var textInputLayout: TextInputLayout? = null


    @NotEmpty(message = "validation_bottom_sheet_add_education_from_date")
    private var fromDate: EditText? = null

    @NotEmpty(message = "validation_bottom_sheet_add_education_to_date")
    private var toDate: EditText? = null

    @NotEmpty(message = "validation_bottom_sheet_add_education_field")
    private var fieldEditText: EditText? = null

    @NotEmpty(message = "validation_bottom_sheet_add_education_description")
    private var descriptionEditText: EditText? = null

    @NotEmpty(message = "validation_bottom_sheet_add_education_school")
    private var schoolEditText: EditText? = null

    @NotEmpty(message = "validation_bottom_sheet_add_education_degree")
    private var degreeEditText: EditText? = null

    @NotEmpty(message = "validation_bottom_sheet_add_education_country")
    private var countryMenu: AppCompatAutoCompleteTextView? = null

    @NotEmpty(message = "validation_bottom_sheet_add_education_city")
    private var cityMenu: AppCompatAutoCompleteTextView? = null

    override val layoutId: Int = R.layout.bottom_sheet_education

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        initListener()
        prepareBottomSheet()

    }

    private fun prepareBottomSheet() {
        textInputLayout?.hint = getString(R.string.field_of_study)
        viewModel.getCountries().observe(this, this::handleCountryResource)
        if (mode == MODE_MODIFY) {
            textViewTitle?.text = getString(R.string.edit_education)
            fieldEditText?.text = SpannableStringBuilder(education?.fieldOfStudy ?: "")
            descriptionEditText?.text = SpannableStringBuilder(education?.description ?: "")
            schoolEditText?.text = SpannableStringBuilder(education?.school ?: "")
            degreeEditText?.text = SpannableStringBuilder(education?.degree ?: "")
            fromDate?.text = SpannableStringBuilder(education?.fromYear ?: "")
            toDate?.text = SpannableStringBuilder(education?.toYear ?: "")

        } else {
            textViewTitle?.text = getString(R.string.add_education)
        }
    }

    private fun handleCountryResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCountryLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCountrySuccessResource(resource)
            }
        }
    }

    private fun handleCountryLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            buttonSubmit?.isEnabled = false
            countryLayout?.visibility = View.INVISIBLE
            cityLayout?.visibility = View.INVISIBLE
        } else {
            buttonSubmit?.setText(R.string.submit)
            buttonSubmit?.isEnabled = true
            countryLayout?.visibility = View.VISIBLE
        }
    }

    private fun handleCountrySuccessResource(resource: Resource) {
        val countryNames = resource.element<BaseWrapper<CountryResponse>>()?.data?.countries?.map {
            it.nameEng
        }
        val adapter = createMenuAdapter(countryNames)
        countryMenu?.text = SpannableStringBuilder(education?.countryName ?: "")
        countryMenu?.setAdapter(adapter)
    }

    private fun initListener() {
        buttonSubmit?.setOnClickListener {
            validator.validate()
        }

        closeImageView?.setOnClickListener {
            behavior.state = BottomSheetBehavior.STATE_HIDDEN
        }

        countryMenu?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(country: Editable?) {
                getCities(country?.toString())
            }

            @Suppress("EmptyFunctionBlock")
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            @Suppress("EmptyFunctionBlock")
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        fromDate?.setOnClickListener {
            createYearPickerBuilder(fromDate).show()
        }

        toDate?.setOnClickListener {
            createYearPickerBuilder(toDate).show()
        }
    }

    private fun createYearPickerBuilder(target: EditText?): MonthPickerDialog {
        val builder = MonthPickerDialog.Builder(
            context,
            MonthPickerDialog.OnDateSetListener { _, selectedYear ->
                target?.setText(
                    selectedYear.toString()
                )
            }, Calendar.YEAR, Calendar.MONTH
        )

        return builder.setMinYear(YEAR_PICKER_STARTING_YEAR)
            .setActivatedYear(YEAR_PICKER_STARTING_YEAR)
            .showYearOnly()
            .build()
    }

    private fun getCities(countryName: String?) {
        viewModel.getCities(countryName ?: "").observe(this, this::handleCityResource)
    }

    private fun handleCityResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCityLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCitySuccessResource(resource)
            }
        }
    }

    private fun handleCityLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            buttonSubmit?.isEnabled = false
            cityLayout?.visibility = View.INVISIBLE
        } else {
            buttonSubmit?.isEnabled = true
            cityLayout?.visibility = View.VISIBLE
        }
    }

    private fun handleCitySuccessResource(resource: Resource) {
        val cityNames = resource.element<BaseWrapper<CityResponse>>()?.data?.cities?.map {
            it.nameEng
        }
        val adapter = createMenuAdapter(cityNames)
        for ((index, value) in cityNames!!.withIndex()) {
            println("$index: $value")
            if (value == education?.cityName) {
                cityMenu?.text = SpannableStringBuilder(education?.cityName ?: "")
                cityMenu?.setAdapter(adapter)
                return
            }
        }
        cityMenu?.text = SpannableStringBuilder("")
        cityMenu?.setAdapter(adapter)
        return
    }

    private fun createMenuAdapter(list: List<String?>?): ArrayAdapter<String> {
        return ArrayAdapter(
            context!!,
            R.layout.profile_drop_down_menu_item,
            Array(list?.size!!) { list[it]!! }
        )
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            buttonSubmit?.isEnabled = false
            buttonSubmit?.text = ""
        } else {
            progressBar?.visibility = View.VISIBLE
            buttonSubmit?.isEnabled = true
            buttonSubmit?.text = getString(R.string.submit)
        }
    }

    private fun handleSuccessResource() {
        if (type == "save") {
            Toast.makeText(context, getString(R.string.saved), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, getString(R.string.edited), Toast.LENGTH_SHORT).show()
        }
        fragment.refresh()
        behavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun handleErrorResource(resource: Resource.Error) {
        Toast.makeText(context, resource.error.message, Toast.LENGTH_SHORT).show()
    }

    override fun onValidationSucceeded() {
        viewModel.createEducation(
            education?.id,
            mode,
            countryMenu?.text?.toString()!!,
            cityMenu?.text?.toString()!!,
            schoolEditText?.text?.toString()!!,
            fromDate?.text?.toString()!!,
            toDate?.text?.toString()!!,
            degreeEditText?.text?.toString()!!,
            fieldEditText?.text?.toString()!!,
            descriptionEditText?.text?.toString()!!
        ).observe(this, this::handleResource)
    }

    private fun findViews() {
        buttonSubmit = view?.findViewById(R.id.button_bottom_sheet_education_submit)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_submit)
        closeImageView = view?.findViewById(R.id.image_view_bottom_sheet_close)
        countryMenu = view?.findViewById(R.id.edit_text_bottom_sheet_country)
        cityMenu = view?.findViewById(R.id.edit_text_bottom_sheet_city)
        countryLayout = view?.findViewById(R.id.text_input_layout_bottom_sheet_country)
        cityLayout = view?.findViewById(R.id.text_input_layout_bottom_sheet_city)
        fieldEditText = view?.findViewById(R.id.edit_text_bottom_sheet_field_of_study)
        descriptionEditText = view?.findViewById(R.id.edit_text_bottom_sheet_description)
        schoolEditText = view?.findViewById(R.id.edit_text_bottom_sheet_school)
        degreeEditText = view?.findViewById(R.id.edit_text_bottom_sheet_degree)
        fromDate = view?.findViewById(R.id.edit_text_bottom_sheet_from_date)
        toDate = view?.findViewById(R.id.edit_text_bottom_sheet_to_date)
        textViewTitle = view?.findViewById(R.id.text_view_bottom_sheet_education_title)
        textInputLayout = view?.findViewById(R.id.text_input_layout_bottom_sheet_field_of_study)
    }

    override fun onDestroyView() {
        buttonSubmit = null
        progressBar = null
        closeImageView = null
        countryMenu = null
        cityMenu = null
        countryLayout = null
        cityLayout = null
        fieldEditText = null
        descriptionEditText = null
        schoolEditText = null
        degreeEditText = null
        fromDate = null
        toDate = null
        super.onDestroyView()
    }

    companion object {
        const val YEAR_PICKER_STARTING_YEAR = 1990
        const val MODE_CREATE = 0
        const val MODE_MODIFY = 1
    }
}


