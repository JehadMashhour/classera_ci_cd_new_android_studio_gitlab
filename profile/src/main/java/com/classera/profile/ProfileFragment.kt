package com.classera.profile


import android.os.Bundle
import android.view.View
import com.classera.core.Screen
import com.classera.core.custom.views.CustomViewPager
import com.classera.core.fragments.BaseFragment
import com.google.android.material.tabs.TabLayout

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Screen("Profile")
class ProfileFragment : BaseFragment(){

    private var viewpager: CustomViewPager? = null
    private var tabLayout: TabLayout? = null
    private var viewpagerAdapter: ProfileViewPagerAdapter? = null

    override val layoutId: Int = R.layout.fragment_profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        prepareViews()
    }

    private fun findViews() {
        viewpager = view?.findViewById(R.id.profile_viewpager)
        tabLayout = view?.findViewById(R.id.profile_tab_layout)
    }

    private fun prepareViews() {
        // disable swiping between pages
        viewpager?.isEnabled = false
        viewpagerAdapter = ProfileViewPagerAdapter(childFragmentManager,requireContext())
        viewpager!!.adapter = viewpagerAdapter
        tabLayout!!.setupWithViewPager(viewpager)
    }

    override fun onDestroyView() {
        viewpager = null
        tabLayout = null
        viewpagerAdapter = null
        super.onDestroyView()
    }


}
