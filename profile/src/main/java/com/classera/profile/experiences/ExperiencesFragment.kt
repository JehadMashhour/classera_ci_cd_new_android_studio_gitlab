package com.classera.profile.experiences

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.lifecycle.observe
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.custom.callbacks.SwipeToDeleteCallback
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.data.network.errorhandling.Resource
import com.classera.profile.R
import com.classera.profile.experiences.addExperiences.AddExperienceBottomSheet
import com.github.florent37.expansionpanel.ExpansionHeader
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 1/27/2021.
 * Classera
 * r.altheeb@classera.com
 */
class ExperiencesFragment : BaseFragment(), ExperiencesFragmentRefresh {

    @Inject
    lateinit var viewModel: ExperiencesViewModel

    private var experiencesExpansionHeader: ExpansionHeader? = null
    private var experiencesRecyclerview: RecyclerView? = null
    private var addExperiences: AppCompatImageView? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var content: LinearLayout? = null
    private var experiencesAdapter: ExperiencesAdapter? = null
    private var swipeToDeleteExperiences: SwipeToDeleteCallback? = null
    private var dialog: AlertDialog? = null

    override val layoutId: Int = R.layout.fragment_experiences

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        getData()
        initListeners()
        observeSwipeToDeleteListener()
    }

    private fun findViews() {
        experiencesExpansionHeader =
            view?.findViewById(R.id.profile_experiences_expansion_header_experiences)
        experiencesRecyclerview =
            view?.findViewById(R.id.profile_experiences_recycler_view_education)
        addExperiences = view?.findViewById(R.id.profile_experiences_add_experiences)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_experiences)
        errorView = view?.findViewById(R.id.error_view_fragment_experiences)
        content = view?.findViewById(R.id.profile_experiences_content)

        swipeToDeleteExperiences = SwipeToDeleteCallback(
            0,
            ItemTouchHelper.LEFT,
            context!!,
            experiencesRecyclerview
        )
    }

    private fun getData() {
        viewModel.getExperiences().observe(this, this::handleResource)
    }

    private fun initListeners() {
        addExperiences?.setOnClickListener {
            AddExperienceBottomSheet(
                this,
                AddExperienceBottomSheet.MODE_CREATE,
                null)
                .show(fragmentManager!!, "")
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            errorView?.visibility = View.GONE
            content?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleSuccessResource() {
        content?.visibility = View.VISIBLE
        initAdapters()
        initSwipeToDeleteListener()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        progressBar?.visibility = View.GONE
        errorView?.visibility = View.VISIBLE
        content?.visibility = View.GONE

        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getData() }
    }

    private fun initAdapters() {
        experiencesAdapter = ExperiencesAdapter(viewModel)
        experiencesRecyclerview?.adapter = experiencesAdapter

        experiencesAdapter?.setOnItemClickListener { _, position ->
            val experience = viewModel.getExperience(position)
            AddExperienceBottomSheet(
                this,
                AddExperienceBottomSheet.MODE_MODIFY,
                experience
            ).show(fragmentManager!!, "")
        }
    }

    private fun observeSwipeToDeleteListener() {
        swipeToDeleteExperiences?.getSwiped()?.observe(this) {
            if (it) {
                swipeToDeleteExperiences?.updateSwipe(false)
                showConfirmToDeleteDialog(swipeToDeleteExperiences?.position!!)
            }
        }
    }

    private fun initSwipeToDeleteListener() {
        if (viewModel.getExperiencesCount() != 0) {
            val itemTouchHelperExperiences = ItemTouchHelper(swipeToDeleteExperiences!!)
            itemTouchHelperExperiences.attachToRecyclerView(experiencesRecyclerview)
        }
    }

    private fun showConfirmToDeleteDialog(position: Int) {
        dialog = null
        dialog = AlertDialog.Builder(requireContext())
            .setTitle(getString(R.string.delete))
            .setMessage(R.string.confirm_delete)
            .setPositiveButton(R.string.discard) { _, _ ->
                viewModel.deleteExperience(position)
                    .observe(this, this::handleDeleteExperienceResource)
                dialog?.dismiss()
            }
            .setNegativeButton(android.R.string.cancel) { _, _ ->
                experiencesAdapter?.notifyItemChanged(position)
                dialog?.dismiss()
            }
            .setCancelable(false)
            .show()

    }

    private fun handleDeleteExperienceResource(resource: Resource) {
        dismissDialog()
        if (resource is Resource.Error) {
            Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
            experiencesAdapter?.notifyItemChanged(swipeToDeleteExperiences?.position!!)
        } else {
            experiencesAdapter?.removeItem(swipeToDeleteExperiences?.position!!)
        }
    }

    override fun refresh() {
        getData()
    }

    private fun dismissDialog() {
        dialog?.dismiss()
    }

    override fun onResume() {
        dismissDialog()
        super.onResume()
    }

    override fun onDestroy() {
        experiencesExpansionHeader = null
        experiencesRecyclerview = null
        addExperiences = null
        progressBar = null
        errorView = null
        content = null
        super.onDestroy()
    }

}
