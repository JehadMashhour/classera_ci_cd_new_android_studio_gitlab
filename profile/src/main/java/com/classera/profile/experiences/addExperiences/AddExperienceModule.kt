package com.classera.profile.experiences.addExperiences

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.profile.ProfileViewModelFactory
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 1/27/2021.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class AddExperienceModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel (
            fragment : Fragment,
            factory: ProfileViewModelFactory
        ) : AddExperienceViewModel {
            return ViewModelProvider(fragment, factory)[AddExperienceViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AddExperienceBottomSheet): Fragment
}
