package com.classera.profile.experiences

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.profile.ProfileViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 1/27/2021.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class ExperiencesFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ProfileViewModelFactory
        ): ExperiencesViewModel {
            return ViewModelProvider(fragment, factory)[ExperiencesViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: ExperiencesFragment): Fragment
}
