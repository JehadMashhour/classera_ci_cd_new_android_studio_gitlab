package com.classera.profile

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
interface AddCountryCity {

    fun countryCitySubmitted(
        countryId: String,
        cityId: String,
        cityName: String
    )

}
