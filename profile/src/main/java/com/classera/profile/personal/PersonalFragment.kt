package com.classera.profile.personal

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.observe
import com.classera.core.Activities
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.intentTo
import com.classera.core.utils.android.getStringOrElse
import com.classera.data.glide.GlideApp
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.classera.profile.AddCountryCity
import com.classera.profile.R
import com.classera.profile.databinding.FragmentPersonalBinding
import com.classera.profile.personal.addcity.AddCountryCityBottomSheet
import com.google.android.material.textfield.TextInputLayout
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import javax.inject.Inject


/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class PersonalFragment : BaseBindingFragment(), AddCountryCity {

    @Inject
    lateinit var viewModel: PersonalViewModel

    @Inject
    lateinit var prefs: Prefs

    private var editTextEmail: EditText? = null
    private var editTextBio: EditText? = null
    private var editTextAddress: EditText? = null
    private var editTextPhone: EditText? = null
    private var editTextCity: EditText? = null
    private var textLayoutCity: TextInputLayout? = null
    private var publicProfileButton: AppCompatButton? = null
    private var submit: AppCompatButton? = null
    private var countryId: String = ""
    private var cityId: String = ""
    private var cityName: String = ""
    private var scoreTextView: AppCompatTextView? = null
    private var imageViewUserProfileImage: AppCompatImageView? = null

    private var userProfileImagePick: MediaFile? = null
    private var progressBar: ProgressBar? = null

    override val layoutId: Int = R.layout.fragment_personal

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        initListener()
        initAllowedListener()

        viewModel.getUser().observe(this) { user ->
            bind<FragmentPersonalBinding> {
                this?.user = user
            }
        }


        handleParentCases()
        handleAdminCase()
    }

    private fun initAllowedListener() {
        if (prefs.studentsChangePhoneNumber == NOT_ALLOWED) {
            editTextPhone?.isEnabled = false
        } else {
            if (prefs.language == "en") {
                editTextPhone?.setCompoundDrawablesWithIntrinsicBounds(
                    null, null,
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_edit
                    ), null
                )
            } else {
                editTextPhone?.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_edit
                    ), null, null, null
                )
            }
        }

        if (prefs.studentsChangeEmail == NOT_ALLOWED) {
            editTextEmail?.isEnabled = false
        } else {
            if (prefs.language == "eng") {
                editTextEmail?.setCompoundDrawablesWithIntrinsicBounds(
                    null, null, ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_edit
                    ), null
                )
            } else {
                editTextEmail?.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_edit
                    ), null, null, null
                )
            }

        }

    }

    private fun initListener() {
        editTextCity?.setOnClickListener {
            AddCountryCityBottomSheet(this).show(fragmentManager!!, "")
        }

        textLayoutCity?.setOnClickListener {
            AddCountryCityBottomSheet(this).show(fragmentManager!!, "")
        }

        submit?.setOnClickListener {
            if (validatePhone() and validateOthers()) {
                viewModel.updateProfile(
                    countryId,
                    editTextAddress?.text.toString(),
                    cityId,
                    editTextBio?.text.toString(),
                    editTextEmail?.text.toString(),
                    editTextPhone?.text.toString()
                ).observe(this, this::handleResource)
            } else {
                Toast.makeText(
                    context,
                    getString(R.string.err_personal_validation),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        publicProfileButton?.setOnClickListener {
            val intent = intentTo(Activities.PublicProfile)
            intent.putExtra("userId", viewModel.getUserId()) // Pref
            startActivity(intent)
        }

        imageViewUserProfileImage?.setOnClickListener {
            val intent = Intent(requireContext(), FilePickerActivity::class.java)
            intent.putExtra(
                FilePickerActivity.CONFIGS, Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(false)
                    .setShowImages(true)
                    .setShowAudios(false)
                    .setShowVideos(false)
                    .enableImageCapture(true)
                    .setSingleChoiceMode(true)
                    .setSkipZeroSizeFiles(true)
                    .build()
            )
            startActivityForResult(intent, ATTACH_USER_IMAGE_REQUEST_CODE)
        }
    }

    private fun validateOthers(): Boolean {
        return !(editTextAddress?.text.toString().isEmpty() ||
                editTextBio?.text.toString().isEmpty())
    }

    private fun validatePhone(): Boolean {
        return if (editTextPhone?.text.toString().length >= MIN_PHONE_NUMBER_DIGITS) {
            true
        } else {
            Toast.makeText(context, getString(R.string.err_phone_number), Toast.LENGTH_SHORT).show()
            false
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        submit?.isEnabled = !resource.show
    }

    private fun handleSuccessResource() {
        Toast.makeText(
            context,
            getString(R.string.personal_edited_successfully),
            Toast.LENGTH_SHORT
        ).show()
        viewModel.getRemoteUser()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        val message =
            context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    private fun findViews() {
        editTextAddress = view?.findViewById(R.id.personal_fragment_edit_text_address)
        editTextEmail = view?.findViewById(R.id.personal_fragment_edit_text_email)
        editTextBio = view?.findViewById(R.id.personal_fragment_edit_text_about_me)
        editTextPhone = view?.findViewById(R.id.personal_fragment_edit_text_phone)
        editTextCity = view?.findViewById(R.id.personal_fragment_edit_text_city)
        textLayoutCity = view?.findViewById(R.id.text_input_layout_bottom_sheet_city)
        submit = view?.findViewById(R.id.button_profile_submit)
        publicProfileButton = view?.findViewById(R.id.public_profile_button)
        scoreTextView = view?.findViewById(R.id.personal_fragment_score)
        imageViewUserProfileImage = view?.findViewById(R.id.personal_fragment_image)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_profile_user_image)
    }

    override fun countryCitySubmitted(countryId: String, cityId: String, cityName: String) {
        this.countryId = countryId
        this.cityId = cityId
        this.cityName = cityName

        editTextCity?.text = SpannableStringBuilder(cityName)
    }

    override fun onDestroyView() {
        editTextAddress = null
        editTextEmail = null
        editTextBio = null
        editTextPhone = null
        editTextCity = null
        textLayoutCity = null
        submit = null
        super.onDestroyView()
    }

    private fun handleParentCases() {
        if (prefs.userRole == UserRole.GUARDIAN) {
            scoreTextView?.visibility = View.GONE
        }
    }

    private fun handleAdminCase() {
        if (prefs.userRole == UserRole.ADMIN) {
            scoreTextView?.visibility = View.GONE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ATTACH_USER_IMAGE_REQUEST_CODE &&
            resultCode == Activity.RESULT_OK && data != null
        ) {
            val images = data.getParcelableArrayListExtra<MediaFile>(
                FilePickerActivity.MEDIA_FILES
            )
            userProfileImagePick = images?.firstOrNull()
            if(images?.isNotEmpty() == true)
                requestChangeUserProfilePicture(userProfileImagePick)
        }
    }

    private fun requestChangeUserProfilePicture(userProfileImage: MediaFile?) {
        val userProfileImagePath = userProfileImagePick?.path ?: ""
        val userProfileImageType = userProfileImagePick?.mimeType ?: ""
        viewModel.changeUserProfilePicture(userProfileImagePath, userProfileImageType)
            .observe(this, ::handleChangeProfileImageResource)
    }

    private fun handleChangeProfileImageResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleChangeProfileImageLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleChangeProfileImageSuccessResource()
            }
            is Resource.Error -> {
                handleChangeProfileImageErrorResource(resource)
            }
        }
    }

    private fun handleChangeProfileImageLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleChangeProfileImageSuccessResource() {
        imageViewUserProfileImage?.let {
            GlideApp.with(this).load(userProfileImagePick?.path)
                .circleCrop().into(it)
        }
        sendNewUserImageBrodCast()
    }

    private fun sendNewUserImageBrodCast() {
        val intent = Intent("USER_IMAGE_CHANGED")
        intent.putExtra("newUserProfileImage", userProfileImagePick?.path)
        context?.sendBroadcast(intent)
    }

    private fun handleChangeProfileImageErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    companion object {

        private const val NOT_ALLOWED = "0"
        private const val MIN_PHONE_NUMBER_DIGITS = 7
        private const val ATTACH_USER_IMAGE_REQUEST_CODE = 101
    }

}
