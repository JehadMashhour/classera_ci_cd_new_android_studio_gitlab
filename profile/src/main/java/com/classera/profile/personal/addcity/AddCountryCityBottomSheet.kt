package com.classera.profile.personal.addcity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.appcompat.widget.AppCompatImageView
import com.classera.core.fragments.BaseBottomSheetValidationDialogFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.data.models.BaseWrapper
import com.classera.data.models.profile.CityResponse
import com.classera.data.models.profile.CountryResponse
import com.classera.data.network.errorhandling.Resource
import com.classera.profile.AddCountryCity
import com.classera.profile.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class AddCountryCityBottomSheet(
    private var fragment: AddCountryCity
) : BaseBottomSheetValidationDialogFragment() {

    @Inject
    lateinit var viewModel: AddCountryCityViewModel

    private var buttonSubmit: Button? = null
    private var progressBar: ProgressBar? = null
    private var closeImageView: AppCompatImageView? = null
    private var countryLayout: TextInputLayout? = null
    private var cityLayout: TextInputLayout? = null

    @NotEmpty (message = "validation_bottom_sheet_add_education_country")
    private var countryMenu: AppCompatAutoCompleteTextView? = null

    @NotEmpty (message = "validation_bottom_sheet_add_education_city")
    private var cityMenu: AppCompatAutoCompleteTextView? = null

    override val layoutId: Int = R.layout.bottom_sheet_countries_cities

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        initListener()

        viewModel.getCountries().observe(this, this::handleCountryResource)
    }

    private fun handleCountryResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCountryLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCountrySuccessResource(resource)
            }
            is Resource.Error -> {
                handleCountryErrorResource(resource)
            }
        }
    }

    private fun handleCountryLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            buttonSubmit?.isEnabled = false
            countryLayout?.visibility = View.INVISIBLE
            cityLayout?.visibility = View.INVISIBLE
        } else {
            progressBar?.visibility = View.GONE
            buttonSubmit?.setText(R.string.submit)
            buttonSubmit?.isEnabled = true
            countryLayout?.visibility = View.VISIBLE
        }
    }

    private fun handleCountrySuccessResource(resource: Resource) {
        val countryNames
                = resource.element<BaseWrapper<CountryResponse>>()?.data?.countries?.map { it.nameEng }
        val adapter = createMenuAdapter(countryNames)
        countryMenu?.setAdapter(adapter)
    }

    private fun handleCountryErrorResource(resource: Resource.Error) {
        val message = context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

        behavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun initListener() {
        buttonSubmit?.setOnClickListener {
            validator.validate()
        }

        closeImageView?.setOnClickListener {
            behavior.state = BottomSheetBehavior.STATE_HIDDEN
        }

        countryMenu?.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(country: Editable?) {
                getCities(country?.toString())
            }

            @Suppress("EmptyFunctionBlock")
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            @Suppress("EmptyFunctionBlock")
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
    }

    private fun getCities(countryName: String?) {
        viewModel.getCities(countryName?:"Unknown").observe(this, this::handleCityResource)
    }

    private fun handleCityResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCityLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCitySuccessResource(resource)
            }
            is Resource.Error -> {
                handleCityErrorResource(resource)
            }
        }
    }

    private fun handleCityLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            buttonSubmit?.isEnabled = false
            cityLayout?.visibility = View.INVISIBLE
        } else {
            buttonSubmit?.isEnabled = true
            cityLayout?.visibility = View.VISIBLE
        }
    }

    private fun handleCitySuccessResource(resource: Resource) {
        val cityNames
                = resource.element<BaseWrapper<CityResponse>>()?.data?.cities?.map { it.nameEng }
        val adapter = createMenuAdapter(cityNames)
        cityMenu?.setAdapter(adapter)
    }

    private fun handleCityErrorResource(resource: Resource.Error) {
        val message = context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

        behavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun createMenuAdapter(list: List<String?>?) : ArrayAdapter<String>{
        return ArrayAdapter(
            context!!,
            R.layout.profile_drop_down_menu_item,
            Array(list?.size!!) { list[it]!! }
        )
    }

    private fun findViews() {
        buttonSubmit = view?.findViewById(R.id.button_bottom_sheet_skill_submit)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_submit)
        closeImageView = view?.findViewById(R.id.image_view_bottom_sheet_close)
        countryMenu = view?.findViewById(R.id.edit_text_bottom_sheet_country)
        cityMenu = view?.findViewById(R.id.edit_text_bottom_sheet_city)
        countryLayout = view?.findViewById(R.id.text_input_layout_bottom_sheet_country)
        cityLayout = view?.findViewById(R.id.text_input_layout_bottom_sheet_city)
    }

    override fun onValidationSucceeded() {
        fragment.countryCitySubmitted(
            viewModel.getCountryId(countryMenu?.text?.toString()!!),
            viewModel.getCityId(cityMenu?.text?.toString()!!),
            cityMenu?.text?.toString()!!
        )
        behavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

}
