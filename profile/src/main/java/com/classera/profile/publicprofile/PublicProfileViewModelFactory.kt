package com.classera.profile.publicprofile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.profile.publicprofile.PublicProfileRepository
import javax.inject.Inject

class PublicProfileViewModelFactory  @Inject constructor(
    private val publicProfileRepository: PublicProfileRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PublicProfileViewModel(publicProfileRepository) as T
    }
}
