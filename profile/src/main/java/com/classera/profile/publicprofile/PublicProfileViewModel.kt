package com.classera.profile.publicprofile

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.profile.publicprofile.PublicProfileRepository
import kotlinx.coroutines.Dispatchers

class PublicProfileViewModel(
    private val publicProfileRepository: PublicProfileRepository
) : BaseViewModel() {

    fun getPublicProfile(userId: String): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { publicProfileRepository.getPublicProfileByUserId(userId) }
        emit(resource)
        emit(Resource.Loading(show = false))
    }
}
