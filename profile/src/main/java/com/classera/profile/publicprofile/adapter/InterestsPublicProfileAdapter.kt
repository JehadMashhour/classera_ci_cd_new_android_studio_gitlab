package com.classera.profile.publicprofile.adapter

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.data.models.profile.PublicProfileWrapper
import com.classera.profile.databinding.EducationListRowItemBinding
import com.classera.profile.databinding.InterestsListRowItemBinding
import com.classera.profile.databinding.WorkListRowItemBinding


class InterestsPublicProfileAdapter(
    private val publicProfileData: PublicProfileWrapper?
) : BaseAdapter<InterestsPublicProfileAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = InterestsListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return publicProfileData?.profile?.interests?.size!!
    }

    inner class ViewHolder(binding: InterestsListRowItemBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<InterestsListRowItemBinding> {
                this.interests = publicProfileData?.profile?.interests?.get(position)
            }
        }
    }
}
