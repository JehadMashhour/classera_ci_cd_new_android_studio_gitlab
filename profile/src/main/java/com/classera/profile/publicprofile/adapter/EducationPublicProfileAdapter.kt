package com.classera.profile.publicprofile.adapter

import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.isVisible
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.data.models.profile.PublicProfileWrapper
import com.classera.profile.R
import com.classera.profile.databinding.EducationListRowItemBinding


class EducationPublicProfileAdapter(
    private val publicProfileData: PublicProfileWrapper?
) : BaseAdapter<EducationPublicProfileAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = EducationListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return publicProfileData?.profile?.educations?.size!!
    }

    inner class ViewHolder(binding: EducationListRowItemBinding) : BaseBindingViewHolder(binding) {

        private var editImageView: ImageView? = null

        init {
            editImageView = itemView.findViewById(R.id.profile_skill_edit)
        }

        override fun bind(position: Int) {

            editImageView?.isVisible = false

            bind<EducationListRowItemBinding> {
                this.education = publicProfileData?.profile?.educations?.get(position)
            }
        }
    }
}
