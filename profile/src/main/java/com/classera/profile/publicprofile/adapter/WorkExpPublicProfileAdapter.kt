package com.classera.profile.publicprofile.adapter

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.data.models.profile.PublicProfileWrapper
import com.classera.profile.databinding.EducationListRowItemBinding
import com.classera.profile.databinding.WorkListRowItemBinding


class WorkExpPublicProfileAdapter(
    private val publicProfileData: PublicProfileWrapper?
) : BaseAdapter<WorkExpPublicProfileAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = WorkListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return publicProfileData?.profile?.workExperience?.size!!
    }

    inner class ViewHolder(binding: WorkListRowItemBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<WorkListRowItemBinding> {
                this.workexp = publicProfileData?.profile?.workExperience?.get(position)
            }
        }
    }
}
