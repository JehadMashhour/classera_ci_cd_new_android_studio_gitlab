package com.classera.profile.publicprofile.adapter

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.data.models.profile.PublicProfileWrapper
import com.classera.profile.databinding.BadgeListRowItemBinding
import com.classera.profile.databinding.EducationListRowItemBinding


class BadgePublicProfileAdapter(
    private val publicProfileData: PublicProfileWrapper?
) : BaseAdapter<BadgePublicProfileAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = BadgeListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return publicProfileData?.profile?.badges?.size!!
    }

    inner class ViewHolder(binding: BadgeListRowItemBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<BadgeListRowItemBinding> {
                this.badge = publicProfileData?.profile?.badges?.get(position)
            }
        }
    }
}
