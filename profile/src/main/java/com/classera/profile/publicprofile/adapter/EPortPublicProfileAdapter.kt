package com.classera.profile.publicprofile.adapter

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.data.models.profile.PublicProfileWrapper
import com.classera.profile.databinding.EducationListRowItemBinding
import com.classera.profile.databinding.EportListRowItemBinding


class EPortPublicProfileAdapter(
    private val publicProfileData: PublicProfileWrapper?
) : BaseAdapter<EPortPublicProfileAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = EportListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return publicProfileData?.profile?.ePortfolio?.size!!
    }

    inner class ViewHolder(binding: EportListRowItemBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<EportListRowItemBinding> {
                this.eport = publicProfileData?.profile?.ePortfolio?.get(position)
            }
        }
    }
}
