package com.classera.profile.publicprofile

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.viewpager2.widget.ViewPager2
import com.classera.core.Screen
import com.classera.core.activities.BaseToolbarActivity
import com.classera.core.custom.views.ErrorView
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.profile.PublicProfileWrapper
import com.classera.data.network.errorhandling.Resource
import com.classera.profile.R
import com.classera.profile.publicprofile.adapter.PublicProfileViewPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import javax.inject.Inject

@Screen("Public Profile")
class PublicProfileActivity : BaseToolbarActivity() {

    @Inject
    lateinit var viewModel: PublicProfileViewModel

    private val userId by lazy { intent.getStringExtra("userId") }

    private var titles: Array<String>? = null

    private var viewpager: ViewPager2? = null
    private var tabLayout: TabLayout? = null
    private var viewpagerAdapter: PublicProfileViewPagerAdapter? = null

    private var progressDialog: ProgressDialog? = null

    private var errorView: ErrorView? = null
    private var progressBar: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_public_profile)
        findViews()
        initTabTitles()
        getPublicProfile()
    }

    private fun initTabTitles() {
        titles = resources.getStringArray(R.array.public_profile_tabs)
    }

    private fun findViews() {
        viewpager = findViewById(R.id.profile_viewpager)
        tabLayout = findViewById(R.id.public_profile_tab_layout)
        errorView = findViewById(R.id.error_view_activity_public_profile)
        progressBar = findViewById(R.id.progress_bar_activity_public_profile)
    }

    private fun getPublicProfile() {
        userId?.let { viewModel.getPublicProfile(it).observe(this, this::handleResource) }
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResources(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<PublicProfileWrapper>>)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        Toast.makeText(this,resource.error.message,Toast.LENGTH_LONG).show()
        this.finish()
    }

    private fun handleLoadingResources(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            tabLayout?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            tabLayout?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource(success: Resource.Success<BaseWrapper<PublicProfileWrapper>>) {
        viewpagerAdapter = PublicProfileViewPagerAdapter(this, success.data?.data)
        viewpager?.adapter = viewpagerAdapter
        TabLayoutMediator(tabLayout!!, viewpager!!) { tab, position -> tab.text = titles?.get(position) }.attach()
    }
}
