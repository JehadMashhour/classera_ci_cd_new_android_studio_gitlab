package com.classera.profile.publicprofile.fragments


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.Screen
import com.classera.profile.R
import com.classera.profile.publicprofile.adapter.InterestsPublicProfileAdapter
import com.classera.profile.publicprofile.adapter.LanguagePublicProfileAdapter

/**
 * Project: Classera
 * Created: March 7, 2020
 *
 * @author Saeed Halawani
 */
@Screen("Public Profile Languages")
class PublicProfileLanguagesFragment : Fragment(R.layout.fragment_languages_public_profile) {

    private var recyclerView: RecyclerView? = null
    private var adapter: LanguagePublicProfileAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        recyclerView = view?.findViewById(R.id.public_profile_education_list)
    }

    private fun initListeners() {
        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.notifyDataSetChanged()
        }
    }

    private fun initAdapter() {
        adapter = LanguagePublicProfileAdapter(arguments?.getParcelable("public_profile"))
        recyclerView?.adapter = adapter
    }


    override fun onDestroyView() {
        recyclerView = null
        adapter = null
        super.onDestroyView()
    }
}

