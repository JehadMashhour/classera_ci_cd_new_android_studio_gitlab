package com.classera.profile

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Module
abstract class ProfileFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ProfileViewModelFactory
        ): ProfileViewModel {
            return ViewModelProvider(fragment, factory)[ProfileViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: ProfileFragment): Fragment

}
