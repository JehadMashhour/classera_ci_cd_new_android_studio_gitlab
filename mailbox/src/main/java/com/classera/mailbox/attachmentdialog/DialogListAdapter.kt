package com.classera.mailbox.attachmentdialog

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.core.utils.android.startAppSettingsIntent
import com.classera.mailbox.R
import com.classera.mailbox.databinding.RowMailboxAttachmentBinding
import com.classera.storage.Storage
import com.github.abdularis.buttonprogress.DownloadButtonProgress
import com.koushikdutta.ion.Ion
import com.koushikdutta.ion.future.ResponseFuture
import java.io.File
import java.io.InputStream

class DialogListAdapter(
    private val commentAttachment: List<Attachment>,
    private val storage: Storage,
    private val mContext: Activity
) : BasePagingAdapter<DialogListAdapter.ViewHolder>() {
    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowMailboxAttachmentBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return commentAttachment.size
    }

    inner class ViewHolder(binding: RowMailboxAttachmentBinding) : BaseBindingViewHolder(binding) {
        var downloadFuture: ResponseFuture<InputStream>? = null
        private var linearLayoutDownload: LinearLayout? = null
        private var buttonDownload: DownloadButtonProgress? = null

        init {
            linearLayoutDownload = itemView.findViewById(R.id.linear_layout_row_mailbox_download)
            buttonDownload = itemView.findViewById(R.id.button_activity_mailbox_attachment_details_download)
        }

        override fun bind(position: Int) {
            bind<RowMailboxAttachmentBinding> {
                this.mailboxattachment = commentAttachment[position]
            }

            linearLayoutDownload?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    if (checkExternalStoragePermission()) {
                        openFileIfExistOrStartDownload(clickedPosition)
                    }
                }
            }

            buttonDownload?.setOnClickListener {
                linearLayoutDownload?.callOnClick()

            }
        }

        private fun openFileIfExistOrStartDownload(position: Int) {
            if (storage.checkIfExist(commentAttachment[position].name)) {
                openFile(position)
            } else {
                startDownload(position)
            }
        }

        private fun openFile(position: Int) {
            val uri = storage.getFileUri(commentAttachment[position].name)
            val mimeTypeMap = MimeTypeMap.getSingleton()
            val mimeType = mimeTypeMap.getMimeTypeFromExtension(File(uri.toString()).extension)

            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(uri, mimeType)
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

            try {
                context?.startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(context, context?.getString(R.string.there_is_no_application), Toast.LENGTH_LONG).show()
            }
        }

        private fun startDownload(position: Int) {
            val attachment = commentAttachment[position]
            val url = attachment.url

            when (buttonDownload?.currState) {
                DownloadButtonProgress.STATE_IDLE -> {
                    if (storage.canProceedWithDownload()) {
                        downloadFuture = Ion.with(context)
                            .load(url)
                            .progress { downloaded, total ->
                                if (attachment.state != DownloadButtonProgress.STATE_DETERMINATE) {
                                    attachment.state = DownloadButtonProgress.STATE_DETERMINATE
                                }
                                attachment.progress =
                                    ((downloaded.toFloat() / total.toFloat()) * 100f).toInt()
                            }
                            .asInputStream()
                        downloadFuture?.setCallback { error, result ->
                            if (attachment.progress == 100) {
                                buttonDownload?.setFinish()
                            }
                            if (error == null) {
                                storage.saveFile(attachment.name, result, true)
                            }
                        }
                    }
                    buttonDownload?.setIndeterminate()
                }
                DownloadButtonProgress.STATE_INDETERMINATE, DownloadButtonProgress.STATE_DETERMINATE -> {
                    attachment.state = DownloadButtonProgress.STATE_IDLE
                    buttonDownload?.setIdle()
                    downloadFuture?.cancel(true)
                }
                DownloadButtonProgress.STATE_FINISHED -> {
                    openFile(position)
                }
            }
        }

        private fun checkExternalStoragePermission(): Boolean {
            val permission = Manifest.permission.WRITE_EXTERNAL_STORAGE
            return when (PackageManager.PERMISSION_GRANTED) {
                ContextCompat.checkSelfPermission(
                    mContext,
                    permission
                ) -> {
                    true
                }
                else -> {
                    ActivityCompat.requestPermissions(
                        mContext,
                        arrayOf(permission),
                        REQUEST_CODE_PERMISSION
                    )
                    false
                }
            }
        }
    }

    private companion object {

        private const val REQUEST_CODE_PERMISSION = 10
    }
}
