package com.classera.mailbox.draft

import android.view.ViewGroup
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.mailbox.databinding.RowDraftItemBinding

class DraftAdapter(
    private val viewModel: DraftViewModel
) : BasePagingAdapter<DraftAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowDraftItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getDraftMessagesListCount()
    }

    fun removeItem(position: Int) {
        viewModel.removeFromList(position)
        notifyItemRemoved(position)
    }

    inner class ViewHolder(binding: RowDraftItemBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<RowDraftItemBinding> {
                this.draft = viewModel.getDraftItem(position)
            }
        }
    }

}
