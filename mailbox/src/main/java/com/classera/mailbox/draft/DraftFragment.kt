package com.classera.mailbox.draft

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.custom.callbacks.SwipeToDeleteCallback
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.mailbox.MailboxFragmentDirections
import com.classera.mailbox.MailboxViewPagerFragment
import com.classera.mailbox.R
import javax.inject.Inject

class DraftFragment : BaseFragment(), MailboxViewPagerFragment {

    @Inject
    lateinit var viewModel: DraftViewModel

    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    private var adapter: DraftAdapter? = null
    private var swipeToDeleteBehavior: SwipeToDeleteCallback? = null

    override val layoutId: Int = R.layout.fragment_draft_layout

    private var searchValue: String = ""
        set(value) {
            field = value
            getDraftList(DEFAULT_PAGE)
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()

        viewModel.getDraftMessagesList(searchValue, DEFAULT_PAGE)
            .observe(this, this::handleResource)
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_mailbox)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_mailbox)
        errorView = view?.findViewById(R.id.error_view_fragment_mailbox)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_mailbox)

        requireContext().let {
            swipeToDeleteBehavior = SwipeToDeleteCallback(
                0,
                ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT,
                it,
                recyclerView
            )
        }

        recyclerView?.itemAnimator = null
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            getDraftList(DEFAULT_PAGE)
        }
    }

    private fun initAdapter() {
        recyclerView?.recycledViewPool?.clear()
        adapter = DraftAdapter(viewModel)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreListener(::getDraftList)
        initSwipeToDeleteListener()
    }

    private fun getDraftList(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            adapter?.resetPaging()
        }
        viewModel.getDraftMessagesList(searchValue, pageNumber).observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
            initAdapterListeners()
        } else {
            recyclerView?.recycledViewPool?.clear()
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if (viewModel.getDraftMessagesListCount() == 0) {
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getDraftList() }
        adapter?.finishLoading()

    }

    override fun search(searchValue: String) {
        this.searchValue = searchValue
    }

    override fun onDestroyView() {
        swipeRefreshLayout = null
        adapter = null
        errorView = null
        progressBar = null
        recyclerView = null
        super.onDestroyView()
    }

    private fun initAdapterListeners() {
        adapter?.setOnItemClickListener { _, position ->
            val draftMessage = viewModel.getDraftItem(position)
            val id = draftMessage?.id
            val title = draftMessage?.title
            val body = draftMessage?.body

            findNavController().navigate(
                MailboxFragmentDirections.mailboxComposeDirection(
                    false,
                    id,
                    title,
                    null,
                    null,
                    null,
                    body
                )
            )
        }
    }

    private fun initSwipeToDeleteListener() {
        val itemTouchHelper = ItemTouchHelper(swipeToDeleteBehavior!!)
        itemTouchHelper.attachToRecyclerView(recyclerView)
        swipeToDeleteBehavior?.getSwiped()?.observe(this) {
            if (it) {
                AlertDialog.Builder(context!!)
                    .setTitle(R.string.title_delete_mailboix_dialog)
                    .setMessage(R.string.message_delete_mailbox_dialog_permanently)
                    .setPositiveButton(R.string.button_positive_delete_discussion_dialog)
                    { _, _ ->
                        swipeToDeleteBehavior?.position?.let { item ->
                            viewModel.deleteMessage(item)
                                .observe(this, this::handleDeleteResource)
                        }
                    }
                    .setNegativeButton(R.string.button_negative_delete_mailbox_dialog) { _, _ ->
                        getDraftList(DEFAULT_PAGE)
                    }
                    .setCancelable(false)
                    .show()
            }
        }
    }

    private fun handleDeleteResource(resource: Resource) {
        if (resource is Resource.Error) {
            Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
            swipeToDeleteBehavior?.position?.let {
                adapter?.notifyItemChanged(it)
            }
        } else {
            Toast.makeText(
                context, getString(R.string.message_deleted_successfully),
                Toast.LENGTH_LONG
            ).show()
            swipeToDeleteBehavior?.position?.let {
                adapter?.removeItem(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        searchValue = ""
        getDraftList(DEFAULT_PAGE)
    }

    companion object {
        private const val DRAFT_TYPE = "draft"
        fun newInstance(): Fragment {
            return DraftFragment()
        }
    }
}
