package com.classera.mailbox.compose

import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.daos.mailbox.LocalMailboxDao
import com.classera.data.models.mailbox.RecipientByRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.repositories.mailbox.MailboxRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

/**
 * Project: Classera
 * Created: Jan 12, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Suppress("LongParameterList")
class ComposeViewModel(
    private val mailboxRepository: MailboxRepository,
    private val mailboxDao: LocalMailboxDao
) : BaseViewModel() {

    private var localRecipientUsers: MutableList<RecipientByRole>? = null

    fun replyMessage(msgId: String, msgTitle: String, msgBody: String, recipientId: String) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryNoContentResource {
            mailboxRepository.replyMessage(msgId, msgTitle, recipientId, msgBody)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun sendMessage(
        msgTitle: String,
        msgBody: String,
        priority: String,
        attachmentFileList: ArrayList<String>,
        attachmentTypeList: ArrayList<String>,
        messageType: String = ""
    ) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val paramsList = ArrayList<MultipartBody.Part>()
        getRecipients(paramsList)
        requiredPayload(paramsList, msgTitle, msgBody, priority,messageType)
        attachmentPayload(paramsList, attachmentFileList, attachmentTypeList)
        val resource = tryNoContentResource { mailboxRepository.sendMessage(paramsList) }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private fun attachmentPayload(
        paramsList: ArrayList<MultipartBody.Part>,
        attachmentFileList: ArrayList<String>,
        attachmentFileTypeList: ArrayList<String>
    ) {
        attachmentFileList.forEachIndexed { index, _ ->
            val attachmentFile = File(attachmentFileList[index])
            val imageRequestBody =
                RequestBody.create(MediaType.parse(attachmentFileTypeList[index]), attachmentFile)
            paramsList.add(
                MultipartBody.Part.createFormData(
                    "attachment[$index]",
                    attachmentFile.name,
                    imageRequestBody
                )
            )
        }
    }

    private fun requiredPayload(
        paramsList: ArrayList<MultipartBody.Part>,
        msgTitle: String,
        msgBody: String,
        priority: String,
        messageType: String
    ) {
        paramsList.add(MultipartBody.Part.createFormData("title", msgTitle))
        paramsList.add(MultipartBody.Part.createFormData("body", msgBody))
        paramsList.add(MultipartBody.Part.createFormData("priority", priority))
        if(messageType.isNotBlank() || messageType.isNotEmpty() || messageType != null) {
            paramsList.add(MultipartBody.Part.createFormData("type", messageType))
        }
    }

    private suspend fun getRecipients(paramsList: ArrayList<MultipartBody.Part>) {
        val recipients = mailboxDao.getAllRecipients().first()
        val recipientsCount = recipients?.size ?: 0
        if (recipientsCount > 0) {
            val recipientsArray = Array(recipientsCount) {
                recipients?.get(it)?.id
            }
            recipientsArray?.forEachIndexed { index, value ->
                paramsList.add(MultipartBody.Part.createFormData("recipients_ids[$index]", value))
            }
        }
    }

    fun getLocalRecipientUsers(): MutableList<RecipientByRole> {
        if (localRecipientUsers == null) {
            viewModelScope.launch {
                mailboxRepository.getAllRecipients().collect {
                    localRecipientUsers = mutableListOf()
                    localRecipientUsers?.addAll(it!!)
                }
            }
        }

        return localRecipientUsers ?: mutableListOf()
    }

    fun deleteAllLocalRecipients() {
        viewModelScope.launch {
            mailboxRepository.deleteAllRecipient()
        }
    }

    fun deleteRecipient(recipientUser: RecipientByRole) {
        viewModelScope.launch {
            mailboxRepository.deleteRecipient(recipientUser)
        }
    }

}
