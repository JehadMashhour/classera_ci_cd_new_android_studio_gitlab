package com.classera.mailbox.compose

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.classera.mailbox.MailboxViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 12, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Module
abstract class ComposeActivityModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: MailboxViewModelFactory
        ): ComposeViewModel {
            return ViewModelProvider(activity, factory)[ComposeViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: ComposeActivity): AppCompatActivity
}
