package com.classera.mailbox.inbox

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.mailbox.Inbox
import com.classera.data.models.mailbox.InboxResponse
import com.classera.data.models.mailbox.InboxSearchResponse
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.mailbox.MailboxRepository
import kotlinx.coroutines.Dispatchers

/**
 * Project: Classera
 * Created: Jan 10, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class InboxViewModel (
    private val mailboxRepository: MailboxRepository
) : BaseViewModel() {

    private var inbox: MutableList<Inbox> = mutableListOf()

    fun deleteMessage(position: Int) =
        liveData(Dispatchers.IO) {
            val resource = tryNoContentResource {
                mailboxRepository.deleteMessage(inbox[position].messageId!!)
            }
            emit(resource)
        }

    private fun getInbox(pageNumber: Int, showProgress: Boolean, searchValue: String = "") =
        liveData(Dispatchers.IO) {
            if (showProgress) {
                emit(Resource.Loading(show = true))
            }

            val resource: Resource = if (searchValue.isEmpty()) {
                tryResource {
                    mailboxRepository.getInbox(pageNumber)
                }
            } else {
                tryResource {
                    mailboxRepository.searchInbox(searchValue, pageNumber)
                }
            }

            if (pageNumber == DEFAULT_PAGE) {
                inbox.clear()
            }

            if (searchValue.isEmpty())
                inbox.addAll(resource.element<BaseWrapper<InboxResponse>>()?.data?.mail ?: mutableListOf())
            else
                inbox.addAll(resource.element<BaseWrapper<InboxSearchResponse>>()?.data?.mail ?: mutableListOf())

            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getInbox(searchValue: String = "", pageNumber: Int): LiveData<Resource> {
        return getInbox(pageNumber,  pageNumber == DEFAULT_PAGE, searchValue)
    }

    fun refreshInbox(searchValue: String = "") = getInbox(DEFAULT_PAGE, false, searchValue)

    fun getSpecificInbox(position: Int): Inbox? {
        return inbox[position]
    }

    fun getInboxCount(): Int {
        return inbox.size
    }

    fun removeFromList(position: Int) {
        inbox.removeAt(position)
    }

}
