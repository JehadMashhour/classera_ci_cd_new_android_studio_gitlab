package com.classera.mailbox.trash

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.mailbox.Trash
import com.classera.data.models.user.User
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.mailbox.MailboxRepository
import com.classera.data.repositories.user.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

/**
 * Project: Classera
 * Created: Jan 10, 2019
 *
 * @author Abdulrhman Hasan Agha
 */
class TrashViewModel (
    private val mailboxRepository: MailboxRepository,
    private val userRepository: UserRepository
) : BaseViewModel(), LifecycleObserver {

    private var trash: MutableList<Trash> = mutableListOf()
    private var user: User? = null

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        viewModelScope.launch(Dispatchers.IO) {
            user = userRepository.getLocalUser().first()?.firstOrNull()
        }
    }

    fun deleteMessage(position: Int) =
        liveData(Dispatchers.IO) {
            val resource = tryNoContentResource {
                mailboxRepository.deletePermanently(trash[position].id!!)
            }
            emit(resource)
        }

    fun restoreMessage(position: Int) =
        liveData(Dispatchers.IO) {
            val resource = tryNoContentResource {
                mailboxRepository.returnToBox(trash[position].id!!)
            }
            emit(resource)
        }

    private fun getTrash(pageNumber: Int, showProgress: Boolean, searchValue: String = "") =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = showProgress))
            val resource: Resource =
                tryResource {
                    mailboxRepository.getTrash(pageNumber, searchValue)
                }

            if (pageNumber == DEFAULT_PAGE) {
                trash.clear()
            }

            trash.addAll(resource.element<BaseWrapper<List<Trash>>>()?.data?: mutableListOf())

            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getTrash(searchValue: String = "", pageNumber: Int): LiveData<Resource> {
        return getTrash(pageNumber,  pageNumber == DEFAULT_PAGE, searchValue)
    }

    fun refreshTrash(searchValue: String = "") = getTrash(DEFAULT_PAGE, false, searchValue)

    fun getSpecificTrash(position: Int): Trash? {
        return trash[position]
    }

    fun getTrashCount(): Int {
        return trash.size
    }

    fun removeFromList(position: Int) {
        trash.removeAt(position)
    }

    fun getLocalUser(): User?{
        return user
    }

}
