package com.classera.mailbox

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 10, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Module
abstract class MailboxFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: MailboxViewModelFactory
        ): MailboxViewModel {
            return ViewModelProvider(fragment, factory)[MailboxViewModel::class.java]
        }

    }

    @Binds
    abstract fun bindActivity(activity: MailboxFragment): Fragment

}
