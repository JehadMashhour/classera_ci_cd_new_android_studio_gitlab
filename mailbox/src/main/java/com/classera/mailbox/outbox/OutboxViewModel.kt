package com.classera.mailbox.outbox

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.mailbox.Outbox
import com.classera.data.models.mailbox.OutboxResponse
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.mailbox.MailboxRepository
import kotlinx.coroutines.Dispatchers

/**
 * Project: Classera
 * Created: Jan 10, 2019
 *
 * @author Abdulrhman Hasan Agha
 */
class OutboxViewModel(
    private val mailboxRepository: MailboxRepository
) : BaseViewModel() {

    private var outbox: MutableList<Outbox> = mutableListOf()

    fun deleteMessage(position: Int) =
        liveData(Dispatchers.IO) {
            val resource = tryNoContentResource {
                mailboxRepository.deleteMessage(outbox[position].id!!)
            }
            emit(resource)
        }

    private fun getOutbox(pageNumber: Int, showProgress: Boolean, searchValue: String = "") =
        liveData(Dispatchers.IO) {
            if (showProgress) {
                emit(Resource.Loading(show = true))
            }
            val resource: Resource = tryResource {
                mailboxRepository.getOutbox(searchValue, pageNumber)
            }
            if (pageNumber == DEFAULT_PAGE) {
                outbox.clear()
            }
            outbox.addAll(
                resource.element<BaseWrapper<OutboxResponse>>()?.data?.mail ?: mutableListOf()
            )
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getOutbox(searchValue: String = "", pageNumber: Int): LiveData<Resource> {
        return getOutbox(pageNumber, pageNumber == DEFAULT_PAGE, searchValue)
    }

    fun refreshOutbox(searchValue: String = "") = getOutbox(DEFAULT_PAGE, false, searchValue)

    fun getSpecificOutbox(position: Int): Outbox? {
        return outbox[position]
    }

    fun getOutboxCount(): Int {
        return outbox.size
    }

    fun removeFromList(position: Int) {
        outbox.removeAt(position)
    }

}
