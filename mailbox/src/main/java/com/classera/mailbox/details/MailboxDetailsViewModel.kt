package com.classera.mailbox.details

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.mailbox.OutboxDetailsResponse
import com.classera.data.models.user.User
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.mailbox.MailboxRepository
import com.classera.data.repositories.mailbox.TYPE_INBOX
import com.classera.data.repositories.user.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

/**
 * Project: Classera
 * Created: Jan 10, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class MailboxDetailsViewModel (
    private val mailboxRepository: MailboxRepository,
    private val userRepository: UserRepository
) : BaseViewModel(), LifecycleObserver {

    private var user: User? = null

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        viewModelScope.launch(Dispatchers.IO) {
            user = userRepository.getLocalUser().first()?.firstOrNull()
        }
    }

    fun getMsgDetails(msgId: String, type: String): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource: Resource = if (type == TYPE_INBOX){
            tryResource {
                mailboxRepository.getInboxDetails(
                    msgId
                )
            }
        } else {
            tryResource {
                mailboxRepository.getOutboxDetails(
                    msgId
                )
            }
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getUser(): User? {
        return user
    }

    fun formRecipients(resource: Resource.Success<BaseWrapper<OutboxDetailsResponse>>): String? {
        var returnValue = ""
        for (recipient in resource.data?.data?.message?.get(0)?.recipientsId!!)
            returnValue += recipient.firstName + " " + recipient.familyName + ", "

        return returnValue
    }
}
