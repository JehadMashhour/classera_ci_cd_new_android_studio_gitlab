package com.classera.mailbox.details

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.Button
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseBindingFragment
import com.classera.data.models.BaseWrapper
import com.classera.data.models.mailbox.InboxDetailsResponse
import com.classera.data.models.mailbox.OutboxDetailsResponse
import com.classera.data.models.mailbox.OutboxMessageDetails
import com.classera.data.network.errorhandling.Resource
import com.classera.data.repositories.mailbox.TYPE_INBOX
import com.classera.data.repositories.mailbox.TYPE_OUTBOX
import com.classera.mailbox.R
import com.classera.mailbox.attachmentdialog.CustomAlertDialog
import com.classera.mailbox.databinding.FragmentMailboxDetailsBinding
import javax.inject.Inject


/**
 * Project: Classera
 * Created: Jan 10, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Screen("MailboxDetails")
class MailboxDetailsFragment : BaseBindingFragment() {

    @Inject
    lateinit var viewModel: MailboxDetailsViewModel

    private var progressBar: ProgressBar? = null
    private var relativeLayout: RelativeLayout? = null
    private var errorView: ErrorView? = null
    private var replyImageIcon: AppCompatImageView? = null

    private var webViewMessageDetails: WebView? = null

    override val layoutId: Int = R.layout.fragment_mailbox_details

    // navigation args
    private var msgId: String? = null
    private var type: String? = null
    private var inboxDetailsResponse: InboxDetailsResponse? = null
    private var outboxDetailsResponse: OutboxMessageDetails? = null

    private var downloadAttachmentButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        extractArgs()
        findViews()
        initListeners()
        getMsgDetails()
    }

    private fun initListeners() {
        replyImageIcon?.setOnClickListener {
            findNavController().navigate(MailboxDetailsFragmentDirections.mailboxReplyDirection(
                true,
                msgId,
                inboxDetailsResponse?.title,
                inboxDetailsResponse?.senderId,
                "${inboxDetailsResponse?.firstName} ${inboxDetailsResponse?.familyName}",
                inboxDetailsResponse?.imageUrl,
                null
            ))
        }

        downloadAttachmentButton?.setOnClickListener {
            getMailBoxAttachments()?.let { attachments ->
                val dialog = CustomAlertDialog(
                    requireActivity(), attachments
                )
                dialog.show()
            }
        }
    }

    private fun extractArgs() {
        val args: MailboxDetailsFragmentArgs by navArgs()
        msgId = args.msgId
        type = args.type

        bind<FragmentMailboxDetailsBinding> { this?.args = args }
    }

    private fun getMsgDetails() {
        viewModel.getMsgDetails(msgId!!, type ?: TYPE_INBOX)
            .observe(this, this::handleResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                progressBar?.visibility = View.GONE
                relativeLayout?.visibility = View.VISIBLE
                errorView?.visibility = View.GONE

                when (type) {
                    TYPE_INBOX ->
                        handleInboxSuccessResource(resource as Resource.Success<BaseWrapper<InboxDetailsResponse>>)
                    TYPE_OUTBOX ->
                        handleOutboxSuccessResource(resource as Resource.Success<BaseWrapper<OutboxDetailsResponse>>)
                }

            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleInboxSuccessResource(
        resource: Resource.Success<BaseWrapper<InboxDetailsResponse>>
    ) {
        bind<FragmentMailboxDetailsBinding> {
            this?.user = viewModel.getUser()
            this?.inbox = resource.data?.data
            this?.hasAttachment = resource.data?.data?.hasAttachments ?: false
        }

        if (resource.data?.data?.senderId == ZERO) {
            replyImageIcon?.visibility = View.GONE
        }
        inboxDetailsResponse = resource.data?.data
        loadMessageDetails(resource.data?.data?.body)
    }

    private fun handleOutboxSuccessResource(resource: Resource.Success<BaseWrapper<OutboxDetailsResponse>>) {
        bind<FragmentMailboxDetailsBinding> {
            this?.user = viewModel.getUser()
            this?.outbox = resource.data?.data?.message?.get(0)?.messageDetails
            this?.recipients = viewModel.formRecipients(resource)
            this?.hasAttachment =
                resource.data?.data?.message?.get(0)?.messageDetails?.hasAttachments ?: false
        }
        outboxDetailsResponse = resource.data?.data?.message?.get(0)?.messageDetails
        loadMessageDetails(resource.data?.data?.message?.get(0)?.messageDetails?.body)
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getMsgDetails() }
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_mailbox_details)
        relativeLayout = view?.findViewById(R.id.mailbox_details_content)
        errorView = view?.findViewById(R.id.error_view_fragment_mailbox_details)
        replyImageIcon = view?.findViewById(R.id.mailbox_details_reply)
        downloadAttachmentButton =
            view?.findViewById(R.id.button_mailbox_details_download_attachment_button)
        webViewMessageDetails = view?.findViewById(R.id.mailbox_details_body)
    }

    private fun getMailBoxAttachments(): List<String>? {
        return if (type == TYPE_INBOX) {
            inboxDetailsResponse?.attachments
        } else {
            outboxDetailsResponse?.attachments
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadMessageDetails(message: String?) {
        webViewMessageDetails?.apply {
            setBackgroundColor(Color.TRANSPARENT)
            settings?.javaScriptEnabled = WEB_VIEW_ALLOW_SETTINGS
            webChromeClient = WebChromeClient()
            settings?.defaultFontSize = WEB_VIEW_DEFAULT_TEXT_SIZE
            settings?.allowFileAccess = WEB_VIEW_ALLOW_SETTINGS
            settings?.loadsImagesAutomatically = WEB_VIEW_ALLOW_SETTINGS
            scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
            settings?.javaScriptCanOpenWindowsAutomatically = WEB_VIEW_ALLOW_SETTINGS
            settings?.mediaPlaybackRequiresUserGesture = WEB_VIEW_ALLOW_SETTINGS
            settings?.javaScriptCanOpenWindowsAutomatically = WEB_VIEW_ALLOW_SETTINGS
            settings?.loadWithOverviewMode = WEB_VIEW_ALLOW_SETTINGS
            scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
            isScrollbarFadingEnabled = WEB_VIEW_DISALLOW_SETTINGS
            isScrollContainer = WEB_VIEW_DISALLOW_SETTINGS
        }

        if (message.toString().isContainsArabic()) {
            webViewMessageDetails?.loadDataWithBaseURL(
                null, FORMAT_ARA + message.toString() + FORMAT_END,
                MIME_TYPE, ENCODING, null
            )
            evaluateJSCode()
        } else {
            webViewMessageDetails?.loadDataWithBaseURL(
                null, FORMAT_ENG + message.toString() + FORMAT_END,
                MIME_TYPE, ENCODING, null
            )
            evaluateJSCode()
        }
    }

    private fun evaluateJSCode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webViewMessageDetails?.evaluateJavascript(JS_EVALUATE_FORMAT_CODE, null)
        } else {
            webViewMessageDetails?.loadUrl(JS_EVALUATE_FORMAT_CODE)
        }
    }


    private fun String.isContainsArabic(): Boolean {
        return this.mapIndexed { index, _ -> this.codePointAt(index) }
            .any { it in LOWER_LETTER..UPPER_LETTER }
    }

    private companion object {

        private const val ZERO = "0"
        private const val LOWER_LETTER = 0x0600
        private const val UPPER_LETTER = 0x06E0
        private const val MIME_TYPE = "text/html"
        private const val ENCODING = "UTF-8"
        private const val WEB_VIEW_DEFAULT_TEXT_SIZE = 14
        private const val WEB_VIEW_ALLOW_SETTINGS = true
        private const val WEB_VIEW_DISALLOW_SETTINGS = false
        private const val FORMAT_ENG =
            "<head><meta name='viewport' content='width=device-width, initial-scale=1.0," +
                    " maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'>" +
                    "<style>table,a,img {width: 100% !important; word-break: break-all;" +
                    "height:auto !important;}" +
                    "</style> </head><body><div id='height'>"
        private const val FORMAT_END = "</div></body>"
        private const val FORMAT_ARA =
            "<head><meta name='viewport' content='width=device-width, initial-scale=1.0," +
                    " maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'>" +
                    "<style>table,a,img,p {width: 100% !important; text-align: right !important; " +
                    "word-break: break-all;" +
                    "height:auto !important;}</style> " +
                    "</head><body dir='rtl'><div id='height'>"
        private const val JS_EVALUATE_FORMAT_CODE =
            "var images = document.getElementsByTagName('img'); \n" +
                    "var srcList = [];\n" +
                    "for(var i = 0; i < images.length; i++) {\n" +
                    "    srcList.push(images[i].style.height= 'auto !important');\n" +
                    "    srcList.push(images[i].style.width= '100% !important');\n" +
                    "}"
    }

}

