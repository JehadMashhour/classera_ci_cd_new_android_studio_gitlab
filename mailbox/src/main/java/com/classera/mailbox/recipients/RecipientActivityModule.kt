package com.classera.mailbox.recipients

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.classera.mailbox.MailboxViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 14, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Module
abstract class RecipientActivityModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: MailboxViewModelFactory
        ): RecipientViewModel {
            return ViewModelProvider(activity, factory)[RecipientViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: RecipientActivity): AppCompatActivity
}
