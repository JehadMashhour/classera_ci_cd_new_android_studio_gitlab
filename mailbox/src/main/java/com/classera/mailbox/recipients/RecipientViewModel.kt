package com.classera.mailbox.recipients

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.mailbox.MessageRoleResponse
import com.classera.data.models.mailbox.RecipientByRole
import com.classera.data.models.mailbox.Role
import com.classera.data.models.mailbox.School
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.mailbox.MailboxRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Project: Classera
 * Created: Jan 14, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class RecipientViewModel (
    private val mailboxRepository: MailboxRepository,
    private val prefs: Prefs
) : BaseViewModel() {

    private var roles: MutableList<Role> = mutableListOf()
    private var remoteRecipientUsers: MutableList<RecipientByRole> = mutableListOf()
    private var filteredRemoteRecipientUsers: List<RecipientByRole> = listOf()
    private var schools: MutableList<School> = mutableListOf()
    private var selectedSchool: School? = null
    var localRecipientUsers: MutableList<RecipientByRole>? = null

    var currentRole: String? = null
    var currentRoleId: String? = null
    var currentSearchValue: String = "" // default

    fun getMessageRole() =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = true))
            val resource = tryResource {
                mailboxRepository.getMessageRoles(selectedSchool?.id)
            }

            if (resource is Resource.Success<*>) {
                roles.clear()
                for (response: MessageRoleResponse? in
                        resource.element<BaseWrapper<List<MessageRoleResponse>>>()?.data!!) {
                    roles.add(response?.role ?: Role())

                }
            }

            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getFilteredRemoteRecipientUsers(): List<RecipientByRole> {
        viewModelScope.launch {
            filteredRemoteRecipientUsers = if (currentSearchValue.isEmpty())
                remoteRecipientUsers.filter {
                    it.fullName?:"" == it.fullName
                }
                else {
                    remoteRecipientUsers.filter {
                         it.fullName?.contains(currentSearchValue, true)?: false
                    }
                }

        }

        return filteredRemoteRecipientUsers
    }

    fun getFilteredRemoteRecipientUsersSize(): Int {
        return getFilteredRemoteRecipientUsers().size
    }

    fun addRecipient(position: Int){
        viewModelScope.launch {
            mailboxRepository.addRecipient(filteredRemoteRecipientUsers[position])
        }
    }

    fun deleteRecipient(recipientUser: RecipientByRole){
        viewModelScope.launch {
            mailboxRepository.deleteRecipient(recipientUser)
        }
    }

    fun getUserByRole(roleId: String?) : LiveData<Resource> = liveData (Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            if(prefs.userRole == UserRole.STUDENT){
                mailboxRepository.getUserByRole(roleId, prefs.schoolId)
            }else{
                mailboxRepository.getUserByRole(roleId, selectedSchool?.id)
            }
        }

        if (resource is Resource.Success<*>) {
            remoteRecipientUsers.clear()
            remoteRecipientUsers.addAll(resource.element<BaseWrapper<List<RecipientByRole>>>()?.data ?: mutableListOf())
            remoteRecipientUsers.map {
                localRecipientUsers?.map { localRecipientUser -> localRecipientUser.checked = false  }
                if (localRecipientUsers?.contains(it) == true) {
                    it.checked = true
                }
            }
        }
        getFilteredRemoteRecipientUsers()
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSchoolList() = liveData {
        val resource = tryResource { mailboxRepository.getSchoolList() }
        if (resource.isSuccess()) {
            schools.addAll(resource.element<BaseWrapper<List<School>>>()?.data ?: mutableListOf())
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSchoolListTitles(): Array<String?> {
        return schools.map { it.name }.toTypedArray()
    }

    fun setSelectedSchool(position: Int) {
        selectedSchool = schools[position]
    }

    fun getSelectedSchool(): School? {
        return selectedSchool
    }

    fun deleteAllRecipientInSchool() {
        viewModelScope.launch {
            mailboxRepository.deleteAllRecipientInSchool(prefs.selectedSchoolId)
        }
    }

}

const val MAX_RECIPIENTS_RECORDS = 10
