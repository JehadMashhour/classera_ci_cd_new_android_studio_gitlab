package com.classera.surveys

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.surveys.SurveysRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 2/18/2020.
 * Classera
 * r.altheeb@classera.com
 */
class SurveysViewModelFactory @Inject constructor(
    private val surveysRepository: SurveysRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SurveysViewModel(surveysRepository) as T
    }
}
