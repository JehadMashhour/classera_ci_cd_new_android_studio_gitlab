package com.classera.attachment.add

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.attachment.AttachmentData
import com.classera.data.models.attachment.Lecture
import com.classera.data.models.attachment.Library
import com.classera.data.models.attachment.Preparation
import com.classera.data.models.attachment.Student
import com.classera.data.models.selection.Selectable
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.attachment.AttachmentRepository
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class AddAttachmentViewModel(
    private val attachmentRepository: AttachmentRepository
) : BaseViewModel() {

    var lectures: Array<out Lecture>? = null
    var selectedLectures: Array<Selectable>? = null
        private set

    var students: Array<out Student>? = null
    var selectedStudents: Array<Selectable>? = null
        private set

    var libraries: Array<out Library>? = null
    var selectedLibraries: Array<Selectable>? = null
        private set

    private val preparations = mutableListOf<Preparation>()

    fun getAttachmentData(courseId: String?, type: String?) = liveData {
        emit(Resource.Loading(show = true))
        val resource = tryResource { attachmentRepository.getAttachmentData(courseId, type) }
        lectures = resource.element<BaseWrapper<AttachmentData>>()?.data?.lectures?.toTypedArray()
        students = resource.element<BaseWrapper<AttachmentData>>()?.data?.students?.toTypedArray()
        libraries = resource.element<BaseWrapper<AttachmentData>>()?.data?.libraries?.toTypedArray()
        preparations.addAll(resource.element<BaseWrapper<AttachmentData>>()?.data?.preparations ?: listOf())
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getPreparationsTitles(): List<String?> {
        return preparations.map { it.title }
    }

    fun getPreparationsIds(): List<String?> {
        return preparations.map { it.id }
    }

    fun setSelectedLectures(lectures: Array<Selectable>?) {
        this.selectedLectures = lectures
    }

    fun setSelectedStudents(students: Array<Selectable>?) {
        this.selectedStudents = students
    }

    fun setSelectedLibraries(libraries: Array<Selectable>?) {
        this.selectedLibraries = libraries
    }

    fun uploadAttachment(
        data: Map<String, Any?>,
        filePath: String,
        fileType: String,
        imagePath: String,
        imageType: String
    ) = liveData {
        emit(Resource.Loading(show = true))
        val paramsList = ArrayList<MultipartBody.Part>()
        filesParts(filePath, fileType, paramsList, imagePath, imageType)
        part1(paramsList, data)
        part2(paramsList, data)
        arrayParts(data, paramsList)

        val resource = tryNoContentResource { attachmentRepository.uploadAttachment(paramsList) }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private fun filesParts(
        filePath: String,
        fileType: String,
        paramsList: ArrayList<MultipartBody.Part>,
        imagePath: String,
        imageType: String
    ) {
        if (imagePath.isNotBlank() || imagePath.isNotEmpty()) {
            val image = File(imagePath)
            val imageRequestBody = RequestBody.create(MediaType.parse(IMAGE_TYPE), image)
            paramsList.add(
                MultipartBody.Part.createFormData(
                    COVER_KEY,
                    IMAGE_FILE_NAME_KEY,
                    imageRequestBody
                )
            )
        }

        if (filePath.isNotBlank() || filePath.isNotEmpty()) {
            val file = File(filePath)
            val attachmentRequestBody = RequestBody.create(MediaType.parse(fileType), file)
            paramsList.add(
                MultipartBody.Part.createFormData(
                    FILE_KEY,
                    file.name,
                    attachmentRequestBody
                )
            )
        }
    }

    private fun arrayParts(data: Map<String, Any?>, paramsList: ArrayList<MultipartBody.Part>) {
        if (data["studentsIds"].toString().isNotBlank() || data["studentsIds"].toString().isNotEmpty()) {
            (data["studentsIds"] as? ArrayList<String>?)?.forEachIndexed { index, value ->
                paramsList.add(MultipartBody.Part.createFormData("users[$index]", value))
            }
        }

        if (data["lecturesIds"].toString().isNotBlank() || data["lecturesIds"].toString().isNotEmpty()) {
            (data["lecturesIds"] as? ArrayList<String>?)?.forEachIndexed { index, value ->
                paramsList.add(MultipartBody.Part.createFormData("lectures[$index]", value))
            }
        }

        if (data["librariesIds"].toString().isNotBlank() || data["librariesIds"].toString().isNotEmpty()) {
            (data["librariesIds"] as? ArrayList<String>?)?.forEachIndexed { index, value ->
                paramsList.add(MultipartBody.Part.createFormData("library[$index]", value))
            }
        }
    }

    private fun part2(paramsList: ArrayList<MultipartBody.Part>, data: Map<String, Any?>) {
        paramsList.add(MultipartBody.Part.createFormData("course_id", data["course_id"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("allow_download", data["allow_download"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("preparation_id", data["preparation_id"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("is_premium", data["is_premium"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("sharing_status", data["sharing_status"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("upload_from", data["upload_from"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("tags", data["tags"].toString()))
    }

    private fun part1(paramsList: ArrayList<MultipartBody.Part>, data: Map<String, Any?>) {
        paramsList.add(MultipartBody.Part.createFormData("title", data["title"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("comments", data["comments"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("publish_date", data["publish_date"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("type", data["type"].toString()))

        if (data["youtube_url"].toString().isNotBlank() || data["youtube_url"].toString().isNotEmpty()) {
            paramsList.add(MultipartBody.Part.createFormData("youtube_url", data["youtube_url"].toString()))
        }

        if (data["vimeo_url"].toString().isNotBlank() || data["vimeo_url"].toString().isNotEmpty()) {
            paramsList.add(MultipartBody.Part.createFormData("vimeo_url", data["vimeo_url"].toString()))
        }

        paramsList.add(MultipartBody.Part.createFormData("share_on_timeline", data["share_on_timeline"].toString()))
    }

    companion object {
        private const val COVER_KEY = "cover"
        private const val IMAGE_FILE_NAME_KEY = "image.png"
        private const val IMAGE_TYPE = "image/png"

        private const val FILE_KEY = "file"
    }
}
