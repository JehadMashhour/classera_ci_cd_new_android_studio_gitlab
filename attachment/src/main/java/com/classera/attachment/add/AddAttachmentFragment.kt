package com.classera.attachment.add

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.CheckBox
import android.widget.DatePicker
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.TimePicker
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.classera.attachment.R
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseValidationFragment
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.saveBitmapFromVideo
import com.classera.data.models.BaseWrapper
import com.classera.data.models.attachment.AttachmentData
import com.classera.data.models.selection.Selectable
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.classera.data.toDate
import com.classera.data.toString
import com.classera.selection.MultiSelectionActivity
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.textfield.TextInputLayout
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import java.io.File
import java.util.*
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 27, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("LongMethod", "TooManyFunctions", "LargeClass","ComplexMethod")
@Screen("Add Attachment")
class AddAttachmentFragment : BaseValidationFragment(), DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {

    @Inject
    lateinit var prefs: Prefs

    private val args: AddAttachmentFragmentArgs by navArgs()

    private var image: MediaFile? = null

    private var file: MediaFile? = null

    private var thumbFile: File? = null

    @Inject
    lateinit var viewModel: AddAttachmentViewModel

    @NotEmpty(message = "validation_fragment_add_attachment_title")
    private var editTextTitle: EditText? = null

    @NotEmpty(message = "validation_fragment_add_attachment_description")
    private var editTextDescription: EditText? = null

    @NotEmpty(message = "validation_fragment_add_attachment_date")
    private var editTextDate: EditText? = null

    @NotEmpty(message = "validation_fragment_add_attachment_time")
    private var editTextTime: EditText? = null

    private var editTextLectures: EditText? = null

    private var editTextStudents: EditText? = null

    private var editTextAddTag: EditText? = null

    private var editTextYoutubeLink: EditText? = null

    private var editTextVimeoLink: EditText? = null

    private var editTextShareToLibrary: EditText? = null

    private var textViewAttachedImageName: TextView? = null

    private var textViewAttachedFileName: TextView? = null

    private var autoCompleteTextViewSharingLevel: AutoCompleteTextView? = null

    private var autoCompleteTextViewPreparations: AutoCompleteTextView? = null

    private var checkBoxAllowDownload: CheckBox? = null

    private var checkBoxAllowDownloadVideos: CheckBox? = null

    private var checkBoxShareOnTimeline: CheckBox? = null

    private var radioButtonYoutube: RadioButton? = null

    private var radioButtonVimeo: RadioButton? = null

    private var radioButtonFile: RadioButton? = null

    private var chipGroupTags: ChipGroup? = null

    private var imageViewAttachCoverImage: ImageView? = null

    private var imageViewAttachFile: ImageView? = null

    private var buttonAddTag: Button? = null

    private var buttonUpload: Button? = null
    private var progressBar: ProgressBar? = null
    private var progressBarUpload: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var linearLayoutParent: LinearLayout? = null
    private var layoutLectures: TextInputLayout? = null
    private var layoutPreparations: TextInputLayout? = null
    private var layoutStudent: TextInputLayout? = null
    private var layoutAttachFile: LinearLayout? = null
    private var layoutAttachImage: LinearLayout? = null
    private var layoutYoutube: TextInputLayout? = null
    private var layoutVimeo: TextInputLayout? = null
    private var sharingCustomLibrary: TextInputLayout? = null

    private var radioGroupVideoUrls: RadioGroup? = null

    private val currentDayCalendar = Calendar.getInstance(Locale.ENGLISH)

    private var year: Int = currentDayCalendar.get(Calendar.YEAR)
    private var month: Int = currentDayCalendar.get(Calendar.MONTH)
    private var dayOfMonth: Int = currentDayCalendar.get(Calendar.DAY_OF_MONTH)
    private var hourOfDay: Int = currentDayCalendar.get(Calendar.HOUR_OF_DAY)
    private var minute: Int = currentDayCalendar.get(Calendar.MINUTE)

    private var sharingLevel: Array<String>? = null
    private var selectedSharingLevelPosition = 0

    private var preparation: String? = null
    private var selectedPreparationsPosition = 0

    private var allowDownload: String? = null
    private var shareOnTimeline: String? = null
    private var uploadFrom: List<Int>? = null
    private var type: String? = null
    private var date: String? = null
    private var time: String? = null

    private var lecturesIds: List<String?>? = null
    private var studentsIds: List<String?>? = null
    private var librariesIds: List<String?>? = null

    override val layoutId: Int = R.layout.fragment_add_attachment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (args.type.equals("videos"))
            type = "Video"
        else if (args.type.equals("materials"))
            type = "Material"

        findViews()

        initSubmitButtonListener()

        initDateListener()

        initSharingContentListener()

        initSharingLevelAdapter()

        initVideosUrlListener()

        initErrorViewListener()

        initLecturesListener()

        initStudentsListener()

        initLibrariesListener()

        initAddTagListener()

        initAttachmentListener()

        getAttachmentData()

        initOtherVideosListener()
    }

    private fun findViews() {
        linearLayoutParent = view?.findViewById(R.id.linear_layout_fragment_add_attachment_parent)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_attachment)
        errorView = view?.findViewById(R.id.error_view_fragment_add_attachment)
        buttonUpload = view?.findViewById(R.id.button_fragment_add_attachment_upload)
        progressBarUpload = view?.findViewById(R.id.progress_bar_fragment_add_attachment_upload)

        editTextTitle = view?.findViewById(R.id.edit_text_fragment_add_attachment_title)
        editTextDescription = view?.findViewById(R.id.edit_text_fragment_add_attachment_description)
        editTextDate = view?.findViewById(R.id.edit_text_fragment_add_attachment_publish_date)
        editTextTime = view?.findViewById(R.id.edit_text_fragment_add_attachment_publish_time)
        editTextLectures = view?.findViewById(R.id.edit_text_fragment_add_attachment_lecture)
        editTextStudents = view?.findViewById(R.id.edit_text_fragment_add_attachment_student)
        editTextAddTag = view?.findViewById(R.id.edit_text_fragment_add_attachment_add_tag)
        editTextYoutubeLink =
            view?.findViewById(R.id.edit_text_fragment_add_attachment_youtube_link)
        editTextVimeoLink = view?.findViewById(R.id.edit_text_fragment_add_attachment_vimeo_link)
        editTextShareToLibrary =
            view?.findViewById(R.id.edit_text_fragment_add_attachment_sharing_to_custom_library)
        textViewAttachedImageName =
            view?.findViewById(R.id.text_view_fragment_add_attachment_image_name)
        textViewAttachedFileName =
            view?.findViewById(R.id.text_view_fragment_add_attachment_file_name)

        autoCompleteTextViewSharingLevel =
            view?.findViewById(R.id.auto_complete_text_view_fragment_add_attachment_sharing_level)

        autoCompleteTextViewPreparations =
            view?.findViewById(R.id.auto_complete_text_view_fragment_add_attachment_preparations)

        checkBoxAllowDownload =
            view?.findViewById(R.id.checkbox_fragment_add_attachment_allow_download)
        checkBoxAllowDownloadVideos =
            view?.findViewById(R.id.checkbox_fragment_add_attachment_allow_download_videos)

        checkBoxShareOnTimeline =
            view?.findViewById(R.id.checkbox_fragment_add_attachment_share_on_timeline)
        radioButtonYoutube = view?.findViewById(R.id.checkbox_fragment_add_attachment_youtube)
        radioButtonVimeo = view?.findViewById(R.id.checkbox_fragment_add_attachment_viemo)
        radioButtonFile = view?.findViewById(R.id.checkbox_fragment_add_attachment_local_file)

        chipGroupTags = view?.findViewById(R.id.chip_group_fragment_add_attachment_tags)

        imageViewAttachCoverImage =
            view?.findViewById(R.id.image_view_fragment_add_attachment_attach_cover_image)
        imageViewAttachFile =
            view?.findViewById(R.id.image_view_fragment_add_attachment_attach_file)
        buttonAddTag = view?.findViewById(R.id.button_fragment_add_attachment_add_tag)

        layoutPreparations = view?.findViewById(R.id.layout_fragment_add_attachment_preparations)
        layoutLectures = view?.findViewById(R.id.layout_fragment_add_attachment_lecture)
        layoutStudent = view?.findViewById(R.id.layout_fragment_add_attachment_student)
        layoutAttachFile = view?.findViewById(R.id.layout_fragment_add_attachment_attach_file)
        layoutAttachImage = view?.findViewById(R.id.layout_fragment_add_attachment_attach_image)

        radioGroupVideoUrls =
            view?.findViewById(R.id.radio_group_fragment_add_attachment_video_urls)
        layoutYoutube = view?.findViewById(R.id.layout_fragment_add_attachment_youtube_link)
        layoutVimeo = view?.findViewById(R.id.layout_fragment_add_attachment_vimeo_link)
        sharingCustomLibrary = view?.findViewById(
            R.id.text_input_layout_fragment_add_attachment_sharing_to_custom_library
        )
    }

    private fun initAttachmentListener() {
        if (type.equals("Video")) {
            layoutAttachImage?.visibility = View.GONE
            checkBoxAllowDownloadVideos?.visibility = View.GONE
        }

        if (type.equals("Material")) {
            layoutAttachImage?.visibility = View.VISIBLE
            layoutAttachFile?.visibility = View.VISIBLE
            checkBoxAllowDownloadVideos?.visibility = View.VISIBLE
        }

        imageViewAttachCoverImage?.setOnClickListener {
            val intent = Intent(requireContext(), FilePickerActivity::class.java)
            intent.putExtra(
                FilePickerActivity.CONFIGS, Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(false)
                    .setShowImages(true)
                    .setShowVideos(false)
                    .enableImageCapture(false)
                    .setSingleChoiceMode(true)
                    .setSkipZeroSizeFiles(true)
                    .build()
            )
            startActivityForResult(intent, ATTACH_IMAGE_REQUEST_CODE)
        }

        imageViewAttachFile?.setOnClickListener {
            if (type.equals("Video")) {
                val intent = Intent(requireContext(), FilePickerActivity::class.java)
                intent.putExtra(
                    FilePickerActivity.CONFIGS, Configurations.Builder()
                        .setCheckPermission(true)
                        .setShowFiles(true)
                        .setShowImages(true)
                        .enableImageCapture(false)
                        .setSingleChoiceMode(true)
                        .setSkipZeroSizeFiles(true)
                        .setSuffixes(
                            ".mp4",
                            ".flv",
                            ".avi",
                            ".msvideo",
                            ".x-msvideo",
                            ".quicktime",
                            ".x-flv",
                            ".mpeg",
                            ".3gp",
                            ".x-ms-wmv",
                            ".webm",
                            ".mov",
                            ".wmv"
                        )
                        .build()
                )
                startActivityForResult(intent, ATTACH_FILE_REQUEST_CODE)
            } else if (type.equals("Material")) {
                val intent = Intent(requireContext(), FilePickerActivity::class.java)
                intent.putExtra(
                    FilePickerActivity.CONFIGS, Configurations.Builder()
                        .setCheckPermission(true)
                        .setShowFiles(true)
                        .setShowImages(false)
                        .setShowVideos(false)
                        .enableImageCapture(false)
                        .setSingleChoiceMode(true)
                        .setSkipZeroSizeFiles(true)
                        .setSuffixes(
                            "pdf", "html", "zip", "rar", "doc", "docx", "odt", "ibooks",
                            "ppt", "pptx", "pps", "xls", "xlsx", "SCORM", "ots"
                        )
                        .setIgnoreHiddenFile(false)
                        .setIgnoreNoMedia(false)
                        .build()
                )
                startActivityForResult(intent, ATTACH_FILE_REQUEST_CODE)
            }
        }
    }

    private fun initDateListener() {
        editTextDate?.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                year,
                month,
                dayOfMonth
            ).show()
            hideKeyboard()
        }

        editTextTime?.setOnClickListener {
            TimePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                hourOfDay,
                minute,
                false
            ).show()
            hideKeyboard()
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        this.year = year
        this.month = month
        this.dayOfMonth = dayOfMonth
        val humanMonth = month + 1
        editTextDate?.setText(
            getString(
                R.string.text_fragment_add_attachment_date_format,
                year,
                humanMonth,
                dayOfMonth
            )
        )
        removeError(editTextDate?.parent?.parent as TextInputLayout)
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        this.hourOfDay = hourOfDay
        this.minute = minute
        val stringTime =
            getString(R.string.text_fragment_add_attachment_time_format, hourOfDay, minute)
        val formattedTime = stringTime.toDate("HH:mm")?.toString("HH:mm")
        editTextTime?.setText(formattedTime)
        removeError(editTextTime?.parent?.parent as TextInputLayout)
    }

    private fun initErrorViewListener() {
        errorView?.setOnRetryClickListener {
            getAttachmentData()
        }
    }

    private fun initSharingContentListener() {
        allowDownload = DO_NOT_ALLOW_DOWNLOAD
        checkBoxAllowDownload?.setOnCheckedChangeListener { _, isChecked ->
            allowDownload = if (isChecked) {
                ALLOW_DOWNLOAD
            } else {
                DO_NOT_ALLOW_DOWNLOAD
            }
        }

        checkBoxAllowDownloadVideos?.setOnCheckedChangeListener { _, isChecked ->
            allowDownload = if (isChecked) {
                ALLOW_DOWNLOAD
            } else {
                DO_NOT_ALLOW_DOWNLOAD
            }
        }

        shareOnTimeline = DO_NOT_SHARE_ON_TIMELINE
        checkBoxShareOnTimeline?.setOnCheckedChangeListener { _, isChecked ->
            shareOnTimeline = if (isChecked) {
                DO_NOT_SHARE_ON_TIMELINE
            } else {
                SHARE_ON_TIMELINE
            }
        }
    }

    private fun initVideosUrlListener() {
        if (type.equals("Material")) {
            radioGroupVideoUrls?.visibility = View.GONE
        }

        radioGroupVideoUrls?.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.checkbox_fragment_add_attachment_youtube -> {
                    editTextYoutubeLink?.visibility = View.VISIBLE
                    layoutYoutube?.visibility = View.VISIBLE
                    layoutVimeo?.visibility = View.GONE
                    layoutAttachFile?.visibility = View.GONE
                    editTextVimeoLink?.setText("")
                }

                R.id.checkbox_fragment_add_attachment_viemo -> {
                    editTextVimeoLink?.visibility = View.VISIBLE
                    layoutVimeo?.visibility = View.VISIBLE
                    layoutYoutube?.visibility = View.GONE
                    layoutAttachFile?.visibility = View.GONE
                    editTextYoutubeLink?.setText("")
                }

                R.id.checkbox_fragment_add_attachment_local_file -> {
                    checkBoxAllowDownloadVideos?.visibility = View.VISIBLE
                    layoutAttachFile?.visibility = View.VISIBLE
                    layoutYoutube?.visibility = View.GONE
                    layoutVimeo?.visibility = View.GONE
                    editTextVimeoLink?.setText("")
                    editTextYoutubeLink?.setText("")
                }
            }
            hideKeyboard()
        }
    }

    private fun initSharingLevelAdapter() {
        sharingLevel =
            resources.getStringArray(R.array.fragment_add_attachment_sharing_status_entries)
        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            sharingLevel ?: arrayOf()
        )
        autoCompleteTextViewSharingLevel?.setAdapter(adapter)

        autoCompleteTextViewSharingLevel?.setOnItemClickListener { _, _, position, _ ->
            selectedSharingLevelPosition = position
            if (position == 0) {
                editTextShareToLibrary?.visibility = View.GONE
                sharingCustomLibrary?.visibility = View.GONE
            } else {
                editTextShareToLibrary?.visibility = View.VISIBLE
                sharingCustomLibrary?.visibility = View.VISIBLE
            }
        }
    }

    private fun initPreparationsAdapter() {
        if (viewModel.getPreparationsTitles().isEmpty())
            layoutPreparations?.visibility = View.GONE

        val adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.dropdown_menu_popup_item,
                viewModel.getPreparationsTitles()
            )
        autoCompleteTextViewPreparations?.setAdapter(adapter)

        autoCompleteTextViewPreparations?.setOnItemClickListener { _, _, position, _ ->
            selectedPreparationsPosition = position
            preparation = viewModel.getPreparationsIds()[selectedPreparationsPosition]
        }
    }

    private fun getAttachmentData() {
        viewModel.getAttachmentData(args.courseId, type).observe(this, ::handleDataResource)
    }

    private fun handleDataResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleDataLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleDataSuccessResource(resource as Resource.Success<BaseWrapper<AttachmentData>>)
            }
            is Resource.Error -> {
                handleDataErrorResource(resource)
            }
        }
    }

    private fun handleDataLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            linearLayoutParent?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            linearLayoutParent?.visibility = View.VISIBLE
        }
    }

    private fun handleDataSuccessResource(success: Resource.Success<BaseWrapper<AttachmentData>>) {
        initPreparationsAdapter()
        uploadFrom = success.data?.data?.uploadFrom
    }

    private fun handleDataErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    private fun initLecturesListener() {
        if (viewModel.lectures?.size == 0) {
            layoutLectures?.visibility = View.GONE
        }

        editTextLectures?.setOnClickListener {
            MultiSelectionActivity.start(
                this,
                viewModel.lectures,
                key = LECTURES_REQUEST_CODE,
                selectedFilter = viewModel.selectedLectures
            )
            hideKeyboard()
        }
    }

    private fun initStudentsListener() {
        if (type.equals("Video"))
            layoutStudent?.visibility = View.GONE

        editTextStudents?.setOnClickListener {
            MultiSelectionActivity.start(
                this,
                viewModel.students,
                key = STUDENTS_REQUEST_CODE,
                selectedFilter = viewModel.selectedStudents
            )
        }
    }

    private fun initLibrariesListener() {
        editTextShareToLibrary?.setOnClickListener {
            MultiSelectionActivity.start(
                this,
                viewModel.libraries,
                key = LIBRARIES_REQUEST_CODE,
                selectedFilter = viewModel.selectedLibraries
            )
            hideKeyboard()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when {
            MultiSelectionActivity.isDone(requestCode, resultCode, data, STUDENTS_REQUEST_CODE) -> {
                clearLectureChips()
                addStudentsChips(MultiSelectionActivity.getSelectedFilterFromIntent(data))
            }
            MultiSelectionActivity.isDone(requestCode, resultCode, data, LECTURES_REQUEST_CODE) -> {
                clearStudentChips()
                addLectureChips(MultiSelectionActivity.getSelectedFilterFromIntent(data))
            }
            MultiSelectionActivity.isDone(
                requestCode,
                resultCode,
                data,
                LIBRARIES_REQUEST_CODE
            ) -> {
                addLibrariesChips(MultiSelectionActivity.getSelectedFilterFromIntent(data))
            }
        }

        if (requestCode == ATTACH_IMAGE_REQUEST_CODE &&
            resultCode == Activity.RESULT_OK && data != null
        ) {
            val images = data.getParcelableArrayListExtra<MediaFile>(
                FilePickerActivity.MEDIA_FILES
            )
            image = images?.firstOrNull()
            textViewAttachedImageName?.text = image?.name

        } else if (requestCode == ATTACH_FILE_REQUEST_CODE &&
            resultCode == Activity.RESULT_OK && data != null
        ) {
            val files = data.getParcelableArrayListExtra<MediaFile>(
                FilePickerActivity.MEDIA_FILES
            )
            file = files?.firstOrNull()
            textViewAttachedFileName?.text = file?.name
        }
    }

    private fun clearStudentChips() {
        viewModel.setSelectedStudents(null)
        editTextStudents?.setText("")
    }

    private fun addStudentsChips(students: Array<Selectable>?) {
        viewModel.setSelectedStudents(students)
        editTextStudents?.setText(students?.joinToString(separator = " | ") { it.title ?: "" })
        removeError(editTextStudents?.parent?.parent as TextInputLayout)
    }

    private fun clearLectureChips() {
        viewModel.setSelectedLectures(null)
        editTextLectures?.setText("")
    }

    private fun addLectureChips(lectures: Array<Selectable>?) {
        viewModel.setSelectedLectures(lectures)
        editTextLectures?.setText(lectures?.joinToString(separator = " | ") { it.title ?: "" })
        removeError(editTextLectures?.parent?.parent as TextInputLayout)
    }

    private fun addLibrariesChips(libraries: Array<Selectable>?) {
        viewModel.setSelectedLibraries(libraries)
        editTextShareToLibrary?.setText(libraries?.joinToString(separator = " | ") {
            it.title ?: ""
        })
        removeError(editTextShareToLibrary?.parent?.parent as TextInputLayout)
    }

    private fun initAddTagListener() {
        buttonAddTag?.setOnClickListener {
            val chip = Chip(requireContext())
            chip.isCloseIconVisible = true
            chip.isCheckable = true
            chip.id = View.generateViewId()


            chip.setOnCloseIconClickListener {
                chipGroupTags?.removeView(chip)
            }

            chip.text = editTextAddTag?.text
            chipGroupTags?.addView(chip)
            editTextAddTag?.setText("")
        }
    }

    private fun initSubmitButtonListener() {
        buttonUpload?.setOnClickListener { validator.validate() }
    }

    override fun onValidationSucceeded() {
        val pathFile = file?.path ?: ""
        val mimeTypeFile = file?.mimeType ?: ""

        var pathImage = image?.path ?: ""
        var mimeTypeImage = image?.mimeType ?: ""

        if (isVideoFile()) {
            generateVideoThumbnail()

            pathImage = thumbFile?.path ?: ""
            mimeTypeImage = thumbFile?.name?.split(".")?.get(1) ?: ""
        }

        studentsIds = viewModel.selectedStudents?.filter { it.selected }?.map { it.id }
        lecturesIds = viewModel.selectedLectures?.filter { it.selected }?.map { it.id }
        librariesIds = viewModel.selectedLibraries?.filter { it.selected }?.map { it.id }

        val tags = mutableListOf<String>()
        for (x in 0 until chipGroupTags?.childCount!!)
            tags.add((chipGroupTags?.getChildAt(x) as Chip).text.toString())

        date = editTextDate?.text?.toString()
        time = editTextTime?.text?.toString()
        val data = mutableMapOf(
            "title" to editTextTitle?.text?.toString(),
            "comments" to editTextDescription?.text?.toString(),
            "publish_date" to "$date ${time?.toDate("HH:mm")?.toString("HH:mm:ss")}",
            "type" to type,
            "vimeo_url" to editTextVimeoLink?.text?.toString(),
            "youtube_url" to editTextYoutubeLink?.text?.toString(),
            "share_on_timeline" to shareOnTimeline,
            "course_id" to args.courseId,
            "sharing_status" to selectedSharingLevelPosition,
            "user_id" to prefs.userId,
            "is_premium" to "0",
            "upload_from" to uploadFrom?.get(0),
            "tags" to tags.joinToString(separator = ",") { it },
            "preparation_id" to preparation,
            "allow_download" to allowDownload,
            "lecturesIds" to lecturesIds,
            "studentsIds" to studentsIds,
            "librariesIds" to librariesIds
        )

        viewModel.uploadAttachment(
            data,
            pathFile,
            mimeTypeFile,
            pathImage,
            mimeTypeImage
        ).observe(this, ::handleSubmitResource)

    }

    private fun generateVideoThumbnail() {
        thumbFile = file?.saveBitmapFromVideo(context)
    }

    private fun isVideoFile(): Boolean = file?.mimeType?.contains("video") == true

    private fun handleSubmitResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSubmitLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSubmitSuccessResource()
            }
            is Resource.Error -> {
                handleSubmitErrorResource(resource)
            }
        }
    }

    private fun handleSubmitLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBarUpload?.visibility = View.VISIBLE
            buttonUpload?.text = ""
            buttonUpload?.isEnabled = false
        } else {
            progressBarUpload?.visibility = View.GONE
            buttonUpload?.setText(R.string.button_fragment_add_attachment_upload)
            buttonUpload?.isEnabled = true
        }
    }

    private fun handleSubmitSuccessResource() {
        findNavController().popBackStack()
    }

    private fun handleSubmitErrorResource(resource: Resource.Error) {
        if (resource.error.message == null) {
            Toast.makeText(
                requireContext(), getString(R.string.cannot_upload_this_type), Toast.LENGTH_LONG)
                .show()
            findNavController().popBackStack()
        } else {
            Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
        }
    }

    private fun initOtherVideosListener() {
        radioButtonYoutube?.setOnCheckedChangeListener { group, checked ->
            if (checked) {
                allowDownload = DO_NOT_ALLOW_DOWNLOAD
            }
            hideKeyboard()
        }

        radioButtonVimeo?.setOnCheckedChangeListener { group, checked ->
            if (checked) {
                allowDownload = DO_NOT_ALLOW_DOWNLOAD
            }
            hideKeyboard()
        }

        radioButtonFile?.setOnCheckedChangeListener { group, checked ->
            if (checked) {
                allowDownload = when {
                    args.type.equals("videos") ->
                        if(checkBoxAllowDownloadVideos?.isChecked == true){
                        ALLOW_DOWNLOAD
                    }else{
                        DO_NOT_ALLOW_DOWNLOAD
                    }
                    args.type.equals("materials") ->
                        if(checkBoxAllowDownload?.isChecked == true){
                        ALLOW_DOWNLOAD
                    }else{
                        DO_NOT_ALLOW_DOWNLOAD
                    }
                    else -> DO_NOT_ALLOW_DOWNLOAD
                }
            }
            hideKeyboard()
        }
    }

    private companion object {

        private const val ALLOW_DOWNLOAD = "1"
        private const val DO_NOT_ALLOW_DOWNLOAD = "0"
        private const val SHARE_ON_TIMELINE = "1"
        private const val DO_NOT_SHARE_ON_TIMELINE = "0"

        private const val STUDENTS_REQUEST_CODE = 101
        private const val LECTURES_REQUEST_CODE = 102
        private const val LIBRARIES_REQUEST_CODE = 103

        private const val ATTACH_IMAGE_REQUEST_CODE = 104
        private const val ATTACH_FILE_REQUEST_CODE = 105

    }
}
