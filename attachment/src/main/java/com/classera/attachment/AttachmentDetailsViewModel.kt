package com.classera.attachment

import android.app.Application
import android.content.Intent
import android.net.Uri
import androidx.annotation.DrawableRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseAndroidViewModel
import com.classera.data.models.BackgroundColor
import com.classera.data.models.BaseWrapper
import com.classera.data.models.attachment.AttachmentComment
import com.classera.data.models.attachment.AttachmentCommentsWrapper
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.digitallibrary.SubAttachment
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.storage.Storage
import com.koushikdutta.ion.Ion
import com.koushikdutta.ion.future.ResponseFuture
import kotlinx.coroutines.Dispatchers
import java.io.InputStream

class AttachmentDetailsViewModel(
    application: Application,
    private val attachmentId: String?,
    val attachmentRepository: AttachmentRepository,
    val storage: Storage,
    private val imageBackground: BackgroundColor
) : BaseAndroidViewModel(application) {

    private var downloadFuture: ResponseFuture<InputStream>? = null

    private var streamingFileUri: Uri? = null

    private var attachment: Attachment? = null
    private var pageNumber: Int = DEFAULT_PAGE


    private var _notifyViewLiveData = MutableLiveData<Attachment?>()
    var notifyViewLiveData: LiveData<Attachment?> = _notifyViewLiveData

    private var comments: MutableList<AttachmentComment> = mutableListOf()


    @DrawableRes
    fun getBackgroundImage(): Int = imageBackground.resId

    fun getDetails() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            attachmentRepository.getAttachmentDetails(attachmentId)
        }
        attachment = resource.element<BaseWrapper<Attachment>>()?.data
        addMainVideoToSubAttachmentList()

        attachment?.backgroundColor = imageBackground
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private fun addMainVideoToSubAttachmentList() {
        if (attachment?.subAttachment?.size ?: 0 > 0) {
            val mainSubAttachment = SubAttachment(
                isRun = true,
                url = attachment?.attachmentUrl,
                coverImgUrl = attachment?.coverImgUrl
            )
            attachment?.selectedSubAttachmentPosition = 0
            attachment?.subAttachment?.add(0, mainSubAttachment)
        }
    }

    fun getFileUri(): Uri {
        val uri =
            streamingFileUri ?: storage.getFileUri(getIdExtension()) ?: Uri.EMPTY
        return uri
    }


    fun isFileDownloaded(): Boolean {
        val boolean = streamingFileUri != Uri.EMPTY && (storage.checkIfExist(getIdExtension()))
        return boolean
    }


    fun getComments() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            attachmentRepository.getAttachmentComments(
                attachmentId,
                pageNumber
            )
        }
        resource.element<BaseWrapper<AttachmentCommentsWrapper>>()?.data?.comments?.let {
            comments.addAll(
                it
            )
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getAttachmentId(): String? {
        return attachmentId
    }

    fun getAttachmentDetails(): Attachment? {
        return attachment
    }

    fun getSubAttachmentCount() = attachment?.subAttachment?.size ?: 0

    fun getSubAttachment(position: Int?): SubAttachment? {
        return position?.let { attachment?.subAttachment?.get(it) }
    }


    fun onLikeClicked() {
        if (attachment?.liked == true) {
            attachment?.likesCount = (attachment?.likesCount ?: 0) + 1
        } else {
            attachment?.likesCount = (attachment?.likesCount ?: 0) - 1
        }
        _notifyViewLiveData.value = attachment
    }

    fun onUnderstandClicked() {
        if (attachment?.understand == true) {
            attachment?.understandCount = (attachment?.understandCount ?: 0) + 1
        } else {
            attachment?.understandCount = (attachment?.understandCount ?: 0) - 1
        }
        _notifyViewLiveData.value = attachment
    }

    fun downloadAttachment() {
        downloadFuture = Ion.with(application)
            .load(attachment?.attachmentUrl)
            .asInputStream()
        downloadFuture?.setCallback { error, result ->
            if (error == null) {
                storage.saveFile(getIdExtension(), result, false)
                sendFinishDownloadBroadcast()
            }
        }
    }

    private fun sendFinishDownloadBroadcast() {
        val intent = Intent("FINISH_DOWNLOAD")
        intent.putExtra(
            "uri",
            storage.getFileUri(getIdExtension())
        )
        application.sendBroadcast(intent)
    }

    private fun getIdExtension(): String {
        val id = getSubAttachment(attachment?.selectedSubAttachmentPosition)?.id ?: attachment?.id

        val fileExtension = getFileExtension()

        return "${id}.${fileExtension}"
    }


    private fun getFileExtension(): String? {
        val url = attachment?.attachmentUrl
        var extension = url?.substring(url.lastIndexOf('.') + 1)
        val lastIndexOfFirstCharacterAfterExtension = extension?.indexOf('%') ?: -1
        if (lastIndexOfFirstCharacterAfterExtension != -1) {
            extension = extension?.substring(0, lastIndexOfFirstCharacterAfterExtension)
        }
        return extension
    }

}
