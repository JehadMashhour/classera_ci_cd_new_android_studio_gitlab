package com.classera.attachment

import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.classera.attachment.AttachmentDetailsViewModel
import com.classera.attachment.R
import com.classera.attachment.databinding.RowAttachmentCommentBinding
import com.classera.attachment.databinding.RowAttachmentSubAttachmentBinding
import com.classera.attachmentcomponents.AttachmentLayout
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.facebook.litho.ComponentContext
import com.facebook.litho.LithoView

/**
 * Project: Classera
 * Created: Dec 31, 2019
 *
 * @author Mohamed Hamdan
 */
class SubAttachmentAdapter(
    private val viewModel: AttachmentDetailsViewModel
) : BasePagingAdapter<SubAttachmentAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowAttachmentSubAttachmentBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getSubAttachmentCount()
    }

    inner class ViewHolder(binding: RowAttachmentSubAttachmentBinding) :
        BaseBindingViewHolder(binding) {



        override fun bind(position: Int) {
            bind<RowAttachmentSubAttachmentBinding> {
                attachment = viewModel.getAttachmentDetails()
                subAttachment = viewModel.getSubAttachment(position)
            }
        }

    }
}
