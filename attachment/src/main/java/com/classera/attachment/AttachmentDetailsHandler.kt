package com.classera.attachment

import android.view.View

interface AttachmentDetailsHandler {
    fun onCommentsClicked(view: View)
}
