package com.classera.attachment

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.RecyclerView
import com.classera.attachment.actions.AttachmentActionsView
import com.classera.attachment.bootomsheet.AttachmentBottomSheet
import com.classera.attachment.comments.AttachmentCommentsBottomSheetFragment
import com.classera.attachment.databinding.ActivityAttachmentDetailsBinding
import com.classera.attachmentcomponents.AttachmentLayout
import com.classera.core.Screen
import com.classera.core.activities.BaseBindingActivity
import com.classera.core.custom.views.ErrorView
import com.classera.core.utils.android.observe
import com.classera.data.glide.GlideApp
import com.classera.data.models.BaseWrapper
import com.classera.data.models.attachment.AttachmentCommentsWrapper
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.digitallibrary.AttachmentTypes
import com.classera.data.network.errorhandling.Resource
import com.facebook.litho.ComponentContext
import com.facebook.litho.LithoView
import com.google.android.material.textview.MaterialTextView
import com.stfalcon.imageviewer.StfalconImageViewer
import java.io.File
import javax.inject.Inject


/**
 * Project: Classera
 * Created: Dec 27, 2019
 *
 * @author Mohamed Hamdan
 */
@Screen("Attachment Details")
// TODO Change to fragment
class AttachmentDetailsActivity : BaseBindingActivity(), AttachmentDetailsHandler {

    @Inject
    lateinit var viewModel: AttachmentDetailsViewModel

    private var lithoView: LithoView? = null

    private var canLoadMoreComments = false
    private var subAttachmentAdapter: SubAttachmentAdapter? = null
    private var subAttachmentRecyclerView: RecyclerView? = null
    private var frameLayoutAttachment: FrameLayout? = null
    private var cardViewRoot: CardView? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var scrollView: NestedScrollView? = null
    private var viewActionsLine: View? = null
    private var attachmentActionsView: AttachmentActionsView? = null
    private var textViewAttachmentDetails: MaterialTextView? = null
    private var commentCountTextView: MaterialTextView? = null

    @Inject
    lateinit var customTabsIntent: CustomTabsIntent

    private val ratingReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            viewModel.getAttachmentDetails()?.rate = intent?.getStringExtra("new_rating")
            bind<ActivityAttachmentDetailsBinding> {
                this?.library = viewModel.getAttachmentDetails()
                this?.attachmentDetailsHandler = this@AttachmentDetailsActivity
            }
        }
    }

    private val finishDownloadReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            handleDownloadedFiles(context)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attachment_details)

        findViews()
        initViews()
        initViewsListeners()
        initViewModelListeners()
        getDetails()
        registerReceiver(ratingReceiver, IntentFilter("RATING_CHANGED"))
        registerReceiver(finishDownloadReceiver, IntentFilter("FINISH_DOWNLOAD"))
    }

    private fun findViews() {
        cardViewRoot = findViewById(R.id.card_view_activity_attachment_details_root)
        progressBar = findViewById(R.id.progress_bar_activity_attachment_details)
        errorView = findViewById(R.id.error_view_activity_attachment_details)
        frameLayoutAttachment =
            findViewById(R.id.frame_layout_activity_attachment_details_attachment)
        subAttachmentRecyclerView =
            findViewById(R.id.recycler_view_activity_attachment_details_sub_videos)
        scrollView = findViewById(R.id.scroll_view_activity_attachment_details)
        viewActionsLine = findViewById(R.id.view_activity_attachment_details_actions_line)
        attachmentActionsView =
            findViewById(R.id.attachment_actions_view_activity_attachment_details)
        textViewAttachmentDetails = findViewById(R.id.text_view_fragment_attachment_details)
        commentCountTextView = findViewById(R.id.activity_attachment_details_tv_comment_count)
    }

    private fun handleAttachmentDetailsLabel(attachmentDetails: Attachment?) {
        if (attachmentDetails?.isDetailsExpanded == true) {
            textViewAttachmentDetails?.maxLines = Int.MAX_VALUE
            textViewAttachmentDetails?.text = textViewAttachmentDetails?.text.toString()
                .replace(getString(R.string.label_see_more), getString(R.string.label_see_less))
        } else {
            textViewAttachmentDetails?.text = textViewAttachmentDetails?.text.toString()
                .replace(getString(R.string.label_see_less), getString(R.string.label_see_more))
            textViewAttachmentDetails?.maxLines = DETAILS_LABEL_MAX_TEXT_LINES
        }
    }

    private fun initViews() {
        //ظظframeLayoutAttachment?.background = getDrawable(viewModel.getBackgroundImage())
    }

    private fun initViewsListeners() {
        errorView?.setOnRetryClickListener(::getDetails)
        attachmentActionsView?.setOnLikeClickListener {
            viewModel.onLikeClicked()
        }

        attachmentActionsView?.setOnUnderstandClickListener {
            viewModel.onUnderstandClicked()
        }

        val textViewDetailsText: String = textViewAttachmentDetails?.text.toString()
        val replacedText1 = "..."
        val seeMoreText = getString(R.string.label_see_more)
        val replacedText = textViewDetailsText.replace(
            oldValue = replacedText1, newValue = seeMoreText
        )
        textViewAttachmentDetails?.text = Html.fromHtml(replacedText)
        textViewAttachmentDetails?.setOnClickListener {
            viewModel.getAttachmentDetails()?.isDetailsExpanded =
                viewModel.getAttachmentDetails()?.isDetailsExpanded != true
            handleAttachmentDetailsLabel(viewModel.getAttachmentDetails())
        }

    }

    private fun initViewModelListeners() {

        viewModel.notifyViewLiveData.observe(this) { attachment ->
            bind<ActivityAttachmentDetailsBinding> {
                this?.library = attachment
                this?.attachmentDetailsHandler = this@AttachmentDetailsActivity
            }
        }
    }

    private fun getDetails() {
        viewModel.getDetails().observe(this, ::handleDetailsResource)
        getComments()
    }

    private fun getComments() {
        viewModel.getComments().observe(this, ::handleCommentsResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleDetailsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleDetailsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleDetailsSuccessResource(resource as Resource.Success<BaseWrapper<Attachment>>)
            }
            is Resource.Error -> {
                handleDetailsErrorResource(resource)
            }
        }
    }

    private fun handleDetailsLoadingResource(resource: Resource.Loading) {
        progressBar?.post {
            progressBar?.visibility = if (resource.show) View.VISIBLE else View.GONE
            cardViewRoot?.visibility = if (!resource.show) View.VISIBLE else View.GONE
        }
    }

    private fun handleDetailsSuccessResource(resource: Resource.Success<BaseWrapper<Attachment>>) {
        val dataMessage = resource.data?.message
        if (dataMessage?.isNotEmpty() == true || dataMessage?.isNotBlank() == true) {
            Toast.makeText(this, resource.data?.message, Toast.LENGTH_SHORT).show()
        }
        bind<ActivityAttachmentDetailsBinding> {
            val attachment = viewModel.getAttachmentDetails()
            attachment?.blockMessage = resource.data?.message
            supportActionBar?.title = attachment?.title ?: attachment?.title
            initAttachmentFrameLayout(attachment)

            attachment?.id = viewModel.getAttachmentId()
            attachmentActionsView?.setAttachmentRepository(viewModel.attachmentRepository)
            attachmentActionsView?.setStorage(viewModel.storage)
            attachmentActionsView?.setAttachment(attachment)
            attachmentActionsView?.setActivityContext(this@AttachmentDetailsActivity)

            this?.library = attachment
            this?.attachmentDetailsHandler = this@AttachmentDetailsActivity
        }
    }

    @Suppress("ComplexMethod", "LongMethod")
    //TOdo complex method
    private fun initAttachmentFrameLayout(attachment: Attachment?) {
        val context = ComponentContext(this@AttachmentDetailsActivity.applicationContext)
        val component = AttachmentLayout.create(context).attachment(attachment).build()
        lithoView = LithoView.create(context, component)

        when {
            attachment?.isCacheFileNeeded() == true -> {
                frameLayoutAttachment?.setOnClickListener {
                    if (attachment.type == SCORM) {
                        handleScormType(attachment)
                    }

                    if (!attachment.isCacheFileNeeded()) {
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = attachment.description?.toUri()
                        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY

                        try {
                            startActivity(intent)
                        } catch (e: ActivityNotFoundException) {
                            Toast.makeText(
                                this, context.getString(R.string.there_is_no_application),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                        return@setOnClickListener
                    }

                    if (viewModel.isFileDownloaded()) {
                        if (checkExternalStoragePermission()) {
                            handleDownloadedFiles(this@AttachmentDetailsActivity)
                        }
                    } else if (attachment.isDownloadable()) {
                        if (checkExternalStoragePermission()) {
                            viewModel.downloadAttachment()
                            val toast = Toast.makeText(
                                this,
                                resources.getString(R.string.toast_fragment_attachment_details_please_wait),
                                Toast.LENGTH_LONG
                            )
                            toast.show()

                            val handler = Handler()
                            handler.postDelayed(
                                {
                                    toast.cancel()
                                },
                                DELAY
                            )
                        }
                    }
                }

                if (attachment?.contentType == AttachmentTypes.EXTERNAL_RESOURCES) {
                    AttachmentBottomSheet.show((this as AppCompatActivity).supportFragmentManager) {
                        finish()
                    }
                }
            }

            attachment?.contentType == AttachmentTypes.WEBSITE -> {
                frameLayoutAttachment?.setOnClickListener {
                    startActivity(Intent(Intent.ACTION_VIEW, attachment.description?.toUri()))
                }
            }

            else -> {
                frameLayoutAttachment?.background = null
            }
        }

        frameLayoutAttachment?.addView(lithoView)
    }

    private fun handleDownloadedFiles(context: Context?) {
        val uri = viewModel.getFileUri()

        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(uri, getMimeType(uri))
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                this, context?.getString(R.string.there_is_no_application),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun getMimeType(uri: Uri): String? {
        var mimeType: String? = null
        mimeType = if (ContentResolver.SCHEME_CONTENT == uri.scheme) {
            val cr: ContentResolver = contentResolver
            cr.getType(uri)
        } else {
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())

            MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                fileExtension.toLowerCase()
            )
        }
        return mimeType
    }

    private fun handleScormType(attachment: Attachment) {
//        val intent = Intent(this, WebViewActivity::class.java)
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//        intent.putExtra(
//            "url", ATTACHMENT_LINK.format(
//                attachment.id, prefs.authenticationToken,
//                attachment?.courseId, prefs.userId
//            )
//        )
//        ContextCompat.startActivity(this, intent, null)
        customTabsIntent.launchUrl(
            this,
            Uri.parse(
                getAttachmentLink().format(
                    attachment.id, prefs.authenticationToken,
                    attachment.courseId, prefs.userId
                )
            )
        )
    }

    private fun handleDetailsErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleCommentsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCommentsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCommentsSuccessResource(resource)
            }
        }
    }

    private fun handleCommentsLoadingResource(resource: Resource.Loading) {
        canLoadMoreComments = !resource.show
    }

    private fun handleCommentsSuccessResource(resource: Resource) {
        setCommentCountText(resource.element<BaseWrapper<AttachmentCommentsWrapper>>()?.data?.count)
        if (subAttachmentAdapter == null) {
            initAdapter()
            return
        }
        subAttachmentAdapter?.notifyDataSetChanged()
    }

    private fun initAdapter() {
        subAttachmentAdapter = SubAttachmentAdapter(viewModel)
        subAttachmentAdapter?.setOnItemClickListener { view, position ->


            viewModel.getAttachmentDetails()?.let {
                if (it.selectedSubAttachmentPosition != position) {

                    it.selectedSubAttachmentPosition?.let { t ->
                        viewModel.getAttachmentDetails()?.subAttachment?.get(t)?.apply {
                            isRun = false
                        }
                    }

                    val subAttachment =
                        viewModel.getAttachmentDetails()?.subAttachment?.get(position)?.apply {
                            isRun = true
                        }

                    it.selectedSubAttachmentPosition = position
                    it.attachmentUrl = subAttachment?.url
                    it.coverImgUrl = subAttachment?.coverImgUrl

                    subAttachmentAdapter?.notifyDataSetChanged()

                    frameLayoutAttachment?.removeAllViews()
                    initAttachmentFrameLayout(it)
                    attachmentActionsView?.reInitializeView()
                }
            }

        }
        subAttachmentRecyclerView?.adapter = subAttachmentAdapter
    }

    private fun checkExternalStoragePermission(): Boolean {
        val permission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        return when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(
                this,
                permission
            ) -> {
                true
            }
            else -> {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(permission),
                    REQUEST_CODE_PERMISSION
                )
                false
            }
        }
    }

    override fun onPause() {
        frameLayoutAttachment?.removeAllViews()
        super.onPause()
    }

    override fun onResume() {
        viewModel.getAttachmentDetails()?.let {
            initAttachmentFrameLayout(it)
        }
        super.onResume()
    }

    override fun onDestroy() {
        unregisterReceiver(ratingReceiver)
        unregisterReceiver(finishDownloadReceiver)
        super.onDestroy()
    }

    private fun getAttachmentLink() =
        "${BuildConfig.flavorData.webBaseUrl}/WebViews/webview_scorm?aidh=%s&Authtoken=%s&cid=%s&user_id=%s"

    private companion object {

        private const val SCORM = "Scorm"
        private const val REQUEST_CODE_PERMISSION = 10
        private const val DELAY = 30000L
        private const val DETAILS_LABEL_MAX_TEXT_LINES = 3
    }

    override fun onCommentsClicked(view: View) {
        AttachmentCommentsBottomSheetFragment.show(
            activity = this,
            attachmentId = viewModel.getAttachmentId(),
            height = (window.decorView.findViewById(android.R.id.content) as View).height
                    - frameLayoutAttachment!!.height
        ) { commentCount -> setCommentCountText(commentCount) }
    }

    private fun setCommentCountText(commentCount: Int?) {
        val fullCommentCount = "${getString(R.string.comment)} ${commentCount ?: 0}"
        commentCountTextView?.text = fullCommentCount
    }
}
