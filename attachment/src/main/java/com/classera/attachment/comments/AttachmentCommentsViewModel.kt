package com.classera.attachment.comments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.attachment.AttachmentComment
import com.classera.data.models.attachment.AttachmentCommentsWrapper
import com.classera.data.moshi.timeago.TimeAgoAdapter
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.data.toString
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import java.util.*

class AttachmentCommentsViewModel(
    private val attachmentId: String?,
    private val attachmentRepository: AttachmentRepository,
    private val userRepository: UserRepository
) : BaseViewModel() {

    private var comments: MutableList<AttachmentComment> = mutableListOf()

    private var _notifyCommentsAdapterLiveData = MutableLiveData<Unit>()
    var notifyCommentsAdapterLiveData: LiveData<Unit> = _notifyCommentsAdapterLiveData

    fun getComments(pageNumber: Int) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            attachmentRepository.getAttachmentComments(
                attachmentId,
                pageNumber
            )
        }
        if (pageNumber == DEFAULT_PAGE) {
            comments.clear()
        }

        resource.element<BaseWrapper<AttachmentCommentsWrapper>>()?.data?.comments?.let {
            comments.addAll(
                it
            )
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getCommentCount(): Int {
        return comments.size
    }

    fun getComment(position: Int): AttachmentComment? {
        return comments[position]
    }

    fun addComment(comment: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            val id = System.currentTimeMillis().toString()
            val user = userRepository.getLocalUser().first()?.firstOrNull()
            val attachmentComment = AttachmentComment(
                localId = id,
                userId = user?.id,
                text = comment,
                title = comment,
                created = getCreatedAt(),
                fullName = user?.fullName,
                imageUrl = user?.imageUrl
            )
            comments.add(0, attachmentComment)

            startCommentRequest(attachmentComment)
        }
    }

    fun onCommentRetryClicked(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val attachmentComment = comments[position]
            startCommentRequest(attachmentComment)
        }
    }

    private fun getCreatedAt(): String? {
        val date = Calendar.getInstance().time
        return TimeAgoAdapter().fromJson(date.toString("yyyy-MM-dd HH:mm:ss"))
    }


    private suspend fun startCommentRequest(attachmentComment: AttachmentComment) {
        val index = comments.indexOfFirst { it.localId == attachmentComment.localId }
        if (index != -1) {
            comments[index] = attachmentComment.copy(isLoading = true, isFailed = false)

            val comment = attachmentComment.text
            val resource = tryNoContentResource {
                attachmentRepository.addAttachmentComment(attachmentId, comment, comment)
            }

            if (resource is Resource.Error) {
                comments[index] = attachmentComment.copy(isLoading = false, isFailed = true)
            } else {
                comments[index] = attachmentComment.copy(isLoading = false, isFailed = false)
            }

            _notifyCommentsAdapterLiveData.postValue(Unit)
        }
    }

}
