package com.classera.attachment.comments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.classera.attachment.R
import com.classera.attachment.databinding.FragmentAttachmentCommentsBottomSheetBinding
import com.classera.core.fragments.BaseBottomSheetDialogBindingFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.attachment.AttachmentCommentsWrapper
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.google.android.material.textview.MaterialTextView
import javax.inject.Inject


class AttachmentCommentsBottomSheetFragment : BaseBottomSheetDialogBindingFragment(),
    AttachmentCommentsHandler {

    @Inject
    lateinit var viewModel: AttachmentCommentsViewModel

    @Inject
    lateinit var commentsAdapter: AttachmentCommentsAdapter

    private var parentConstraintLayout: ConstraintLayout? = null
    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var relativeLayoutAddCommentSubmit: View? = null
    private var editTextAddComment: EditText? = null
    private var commentCountTextView: MaterialTextView? = null

    override val layoutId: Int = R.layout.fragment_attachment_comments_bottom_sheet


    override fun isCancelable(): Boolean {
        return false
    }

    override fun isDraggable(): Boolean {
        return false
    }

    override fun enableDependencyInjection(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.BottomSheetDialog)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        setHeight()
        setPassOutsideTouchToBelowScreenListener()
        bindData()
        setupRecyclerView()
        initViewModelListeners()
        getComments()
    }


    private fun findViews() {
        view?.let {
            parentConstraintLayout = it.findViewById(R.id.fragment_attachment_comments_cl_parent)
            recyclerView = it.findViewById(R.id.fragment_attachment_comments_recycler_view)
            progressBar = it.findViewById(R.id.fragment_attachment_comments_progress_bar)
            relativeLayoutAddCommentSubmit =
                it.findViewById(R.id.fragment_attachment_comments_rl_submit_add_comment)
            editTextAddComment = it.findViewById(R.id.fragment_attachment_comments_et_add_comment)
            commentCountTextView =
                it.findViewById(R.id.fragment_attachment_comments_txt_view_comment_count)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setPassOutsideTouchToBelowScreenListener() {
        val touchOutsideView =
            dialog!!.window?.decorView?.findViewById<View>(com.google.android.material.R.id.touch_outside)
        touchOutsideView?.setOnTouchListener { _, event ->
            activity?.findViewById<View>(R.id.frame_layout_activity_attachment_details_attachment)
                ?.dispatchTouchEvent(event)!!
        }
    }


    private fun bindData() {
        bind<FragmentAttachmentCommentsBottomSheetBinding> {
            this?.attachmentCommentsHandler = this@AttachmentCommentsBottomSheetFragment
        }
    }

    private fun setHeight() {
        val params = parentConstraintLayout?.layoutParams
        params?.height = arguments?.getInt(HEIGHT) ?: params?.height
        parentConstraintLayout?.layoutParams = params
    }

    private fun setupRecyclerView() {
        recyclerView?.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        commentsAdapter.setOnLoadMoreListener(::getComments)
        recyclerView?.adapter = commentsAdapter

    }


    private fun initViewModelListeners() {
        viewModel.notifyCommentsAdapterLiveData.observe(this) {
            commentsAdapter.resetPaging()
            getComments()
        }

    }


    private fun getComments(pageNumber: Int = DEFAULT_PAGE) {
        viewModel.getComments(pageNumber).observe(viewLifecycleOwner, ::handleCommentsResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleCommentsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCommentsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCommentsSuccessResource(resource)
            }
            is Resource.Error -> {
                handleCommentsErrorResource(resource)
            }
        }
    }

    private fun handleCommentsLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show && commentsAdapter.isFirstPage()) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun handleCommentsSuccessResource(resource: Resource) {
        recyclerView?.adapter?.notifyDataSetChanged()

        commentsAdapter.finishLoading()

        val commentCount =
            resource.element<BaseWrapper<AttachmentCommentsWrapper>>()?.data?.count

        if (commentsAdapter.isFirstPage()) {
            setCommentCountText(commentCount)
            setFragmentResult(
                DEFAULT_REQUEST_KEY,
                bundleOf(
                    COMMENT_COUNT to commentCount
                )
            )
        }
    }

    private fun handleCommentsErrorResource(resource: Resource.Error) {
        val message =
            context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()

        commentsAdapter.finishLoading()
    }

    override fun onSubmitAddCommentClicked(view: View) {
        if (editTextAddComment?.text?.isNotEmpty() == true && editTextAddComment?.text?.isNotBlank() == true) {
            viewModel.addComment(editTextAddComment?.text?.toString())
            editTextAddComment?.text = null
        }
    }

    override fun onCloseClicked(view: View) {
        dismiss()
    }

    private fun setCommentCountText(commentCount: Int?) {
        val fullCommentCount = "${getString(R.string.comment)} ${commentCount ?: 0}"
        commentCountTextView?.text = fullCommentCount
    }


    companion object {
        private const val DEFAULT_REQUEST_KEY = "default_request_key"
        private const val DISMISS_REQUEST_KEY = "dismiss_request_key"
        private const val COMMENT_COUNT = "comments_count"
        private const val IS_DISMISSED = "is_dismissed"
        const val ATTACHMENT_ID = "attachmentId"
        const val HEIGHT = "height"
        fun show(
            activity: AppCompatActivity,
            attachmentId: String?,
            height: Int,
            dismissListener: ((dismissed: Boolean) -> Unit)? = null,
            listener: ((commentCount: Int?) -> Unit)? = null
        ) {

            activity.supportFragmentManager.setFragmentResultListener(
                DEFAULT_REQUEST_KEY,
                activity
            ) { _, bundle ->
                listener?.invoke(bundle.get(COMMENT_COUNT) as Int?)
            }

            activity.supportFragmentManager.setFragmentResultListener(
                DISMISS_REQUEST_KEY,
                activity
            ) { _, bundle ->
                dismissListener?.invoke(bundle.get(IS_DISMISSED) as Boolean)
            }

            val fragment = AttachmentCommentsBottomSheetFragment()
            fragment.arguments = bundleOf(ATTACHMENT_ID to attachmentId, HEIGHT to height)
            fragment.show(activity.supportFragmentManager, "AttachmentCommentsBottomSheetFragment")
        }
    }


}
