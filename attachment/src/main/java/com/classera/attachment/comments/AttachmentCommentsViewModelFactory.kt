package com.classera.attachment.comments

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.attachment.comments.AttachmentCommentsViewModel
import com.classera.data.models.BackgroundColor
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.storage.Storage

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class AttachmentCommentsViewModelFactory(
    private val application: Application,
    private val attachmentId: String?,
    private val attachmentRepository: AttachmentRepository,
    private val userRepository: UserRepository
) : ViewModelProvider.AndroidViewModelFactory(application) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AttachmentCommentsViewModel(attachmentId, attachmentRepository, userRepository) as T
    }
}

