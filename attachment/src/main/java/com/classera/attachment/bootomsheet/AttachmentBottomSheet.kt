package com.classera.attachment.bootomsheet

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.FragmentManager
import com.classera.attachment.R
import com.classera.core.fragments.BaseBottomSheetDialogFragment


/**
 * Created by Rawan Al-Theeb on 3/9/2021.
 * Classera
 * r.altheeb@classera.com
 */
class AttachmentBottomSheet private constructor() : BaseBottomSheetDialogFragment() {

    private var callback: (() -> Unit)? = null
    private var buttonOk: Button? = null
    private var buttonClose: ImageView? = null

    override val layoutId: Int = R.layout.bottom_sheet_attachment

    override fun enableDependencyInjection(): Boolean {
        return false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    override fun onPause() {
        dismiss()
        super.onPause()
    }

    override fun onDismiss(dialog: DialogInterface) {
        callback?.invoke()
        super.onDismiss(dialog)
    }

    private fun findViews() {
        buttonOk = view?.findViewById(R.id.button_bottom_sheet_attachment_ok)
        buttonClose = view?.findViewById(R.id.image_view_bottom_sheet_close)
    }

    private fun initListeners() {
        buttonOk?.setOnClickListener {
            callback?.invoke()
        }

        buttonClose?.setOnClickListener {
            callback?.invoke()
        }
    }

    companion object {

        fun show(supportFragmentManager: FragmentManager, callback: () -> Unit) {
            val attachmentBottomSheet = AttachmentBottomSheet()
            attachmentBottomSheet.callback = callback
            attachmentBottomSheet.show(supportFragmentManager, "")
        }
    }
}

