package com.classera.attachment.actions

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.storage.Storage

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class AttachmentActionsViewModelFactory(
    private val application: Application,
    private val attachment: Attachment?,
    private val attachmentRepository: AttachmentRepository,
    private val storage: Storage
) : ViewModelProvider.AndroidViewModelFactory(application) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AttachmentActionsViewModel(application, attachment, attachmentRepository, storage) as T
    }
}
