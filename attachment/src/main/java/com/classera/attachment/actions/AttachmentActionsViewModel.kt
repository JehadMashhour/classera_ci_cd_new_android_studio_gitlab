package com.classera.attachment.actions

import android.app.Application
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseAndroidViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.digitallibrary.SubAttachment
import com.classera.data.models.rating.RatingResult
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.storage.Storage
import com.koushikdutta.ion.Ion
import com.koushikdutta.ion.future.ResponseFuture
import kotlinx.coroutines.launch
import java.io.InputStream

/**
 * Project: Classera
 * Created: Jan 04, 2020
 *
 * @author Mohamed Hamdan
 */
class AttachmentActionsViewModel(
    application: Application,
    private val attachment: Attachment?,
    private val attachmentRepository: AttachmentRepository,
    private val storage: Storage
) : BaseAndroidViewModel(application) {

    private var downloadFuture: ResponseFuture<InputStream>? = null

    private var _downloadProgressLiveData = MutableLiveData<Float>()
    var downloadProgressLiveData: LiveData<Float> = _downloadProgressLiveData

    private var _openFileLiveData = MutableLiveData<Uri?>()
    var openFileLiveData: LiveData<Uri?> = _openFileLiveData

    private val _toastLiveData = MutableLiveData<Any>()
    val toastLiveData: LiveData<Any> = _toastLiveData

    private val _notifyLiveData = MutableLiveData<Attachment>()
    val notifyLiveData: LiveData<Attachment> = _notifyLiveData

    private var newLikeCount: Int? = null

    init {
        checkIfPathExists()
        newLikeCount = attachment?.likesCount
    }


    fun onLikeClicked() {
        attachment?.liked = !attachment!!.liked
        handelAttachmentLike()

        _notifyLiveData.postValue(attachment)
        viewModelScope.launch {
            val resource = tryNoContentResource {
                attachmentRepository.addLike(attachment.id)
            }
            if (resource is Resource.Error) {
                attachment.liked = !attachment.liked
                handelAttachmentLike()

                if (resource.error.message != null) {
                    _toastLiveData.postValue(resource.error.message)
                } else {
                    _toastLiveData.postValue(resource.error.resourceMessage)
                }
            }
            sendLikeBroadcast()
            _notifyLiveData.postValue(attachment)
        }
    }

    private fun handelAttachmentLike() {
        newLikeCount = if (attachment?.liked == true) {
            newLikeCount?.plus(1)
        } else {
            newLikeCount?.minus(1)
        }
    }

    private fun sendLikeBroadcast() {
        val intent = Intent("LIKE_CHANGED")
        intent.putExtra("new_count", newLikeCount ?: attachment?.likeCount)
        intent.putExtra("is_liked", attachment?.liked)
        application.sendBroadcast(intent)
    }

    fun onUnderstandClicked() {
        attachment?.understand = !attachment!!.understand

        _notifyLiveData.postValue(attachment)
        viewModelScope.launch {
            val resource = tryNoContentResource {
                attachmentRepository.addUnderstand(attachment.id)
            }
            if (resource is Resource.Error) {
                attachment.understand = !attachment.understand

                if (resource.error.message != null) {
                    _toastLiveData.postValue(resource.error.message)
                } else {
                    _toastLiveData.postValue(resource.error.resourceMessage)
                }
            }
            sendUnderstandBroadcast()
            _notifyLiveData.postValue(attachment)
        }
    }

    private fun sendUnderstandBroadcast() {
        val intent = Intent("UNDERSTAND_CHANGED")
        intent.putExtra("new_count", attachment?.understandCount)
        intent.putExtra("understand", attachment?.understand)
        application.sendBroadcast(intent)
    }

    fun onSubmitRatingClicked(rating: Float) {
        viewModelScope.launch {
            val resource = tryResource {
                attachmentRepository.addRate(attachment?.id, rating.toString(), attachment?.userId)
            }
            if (resource is Resource.Error) {
                if (resource.error.message != null) {
                    _toastLiveData.postValue(resource.error.message)
                } else {
                    _toastLiveData.postValue(resource.error.resourceMessage)
                }
            } else if (resource is Resource.Success<*>) {
                val ratingResult = resource.element<BaseWrapper<RatingResult>>()
                attachment?.rate = ratingResult?.data?.newRating
                sendRatingBroadcast()
            }
            _notifyLiveData.postValue(attachment)
        }
    }

    private fun sendRatingBroadcast() {
        val intent = Intent("RATING_CHANGED")
        intent.putExtra("new_rating", attachment?.rate)
        application.sendBroadcast(intent)
    }

    fun checkIfPathExists() {
        if (storage.checkIfExist(getIdExtension())) {
            _downloadProgressLiveData.postValue(100f)
        }
    }

    fun checkIfCanDownloadAndStart(): Boolean {
        if (storage.canProceedWithDownload()) {
            downloadFuture = Ion.with(application)
                .load(attachment?.attachmentUrl)
                .progress { downloaded, total ->
                    _downloadProgressLiveData.postValue((downloaded.toFloat() / total.toFloat()) * 100f)
                }
                .asInputStream()
            downloadFuture?.setCallback { error, result ->
                if (error == null) {
                    storage.saveFile(getIdExtension(), result, true)
                }
            }
            return true
        }
        return false
    }

    fun cancelDownload() {
        downloadFuture?.cancel(true)
    }

    fun openFile() {
        _openFileLiveData.postValue(storage.getFileUri(getIdExtension()))
    }

    private fun getSubAttachment(position: Int?): SubAttachment? {
        return position?.let { attachment?.subAttachment?.get(it) }
    }

    private fun getIdExtension(): String {
        val id = getSubAttachment(attachment?.selectedSubAttachmentPosition)?.id ?: attachment?.id

        val fileExtension = getFileExtension()

        return "${id}.${fileExtension}"
    }


    private fun getFileExtension(): String? {
        val url = attachment?.attachmentUrl
        var extension = url?.substring(url.lastIndexOf('.') + 1)
        val lastIndexOfFirstCharacterAfterExtension = extension?.indexOf('%') ?: -1
        if (lastIndexOfFirstCharacterAfterExtension != -1) {
            extension = extension?.substring(0, lastIndexOfFirstCharacterAfterExtension)
        }
        return extension
    }

    fun isRated(): Double? {
        return attachment?.rateValue()
    }
}
