package com.classera.homelocation

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.classera.core.Screen
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.homelocation.w3w.W3WResponse
import com.classera.data.network.errorhandling.NetworkException
import com.classera.data.network.errorhandling.Resource
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

@Screen("Home Location")
@Suppress("UNCHECKED_CAST")
class HomeLocationFragment : BaseFragment(), OnMapReadyCallback, GoogleMap.OnMarkerDragListener {

    @Inject
    lateinit var viewModel: HomeLocationViewModel

    private var canAddMarker = false
    private var marker: Marker? = null
    private var googleMap: GoogleMap? = null
    private var buttonSave: Button? = null
    private var progressBar: ProgressBar? = null
    private var progressBarSave: ProgressBar? = null
    private var coordinatorLayoutRoot: CoordinatorLayout? = null

    override val layoutId: Int = R.layout.fragment_home_location

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
        initMap()
        getCoordinates()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_home_location)
        buttonSave = view?.findViewById(R.id.button_fragment_home_location_save)
        progressBarSave = view?.findViewById(R.id.progress_bar_fragment_home_location_save)
        coordinatorLayoutRoot = view?.findViewById(R.id.coordinator_layout_fragment_home_location_root)
    }

    private fun initListeners() {
        buttonSave?.setOnClickListener {
            viewModel.saveCoordinates(marker).observe(this, ::handleSaveResource)
        }
    }

    private fun handleSaveResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSaveLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSaveSuccessResource(resource as Resource.Success<W3WResponse>)
            }
            is Resource.Error -> {
                handleSaveErrorResource(resource)
            }
        }
    }

    private fun handleSaveLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBarSave?.visibility = View.VISIBLE
            buttonSave?.text = null
            buttonSave?.isEnabled = false
        } else {
            progressBarSave?.visibility = View.GONE
            buttonSave?.setText(R.string.button_home_location_save)
        }
    }

    private fun handleSaveSuccessResource(resource: Resource.Success<W3WResponse>) {
        Toast.makeText(context, R.string.message_home_location_fragment_success_add_location, Toast.LENGTH_LONG).show()
        addMarker(resource.data)
    }

    private fun handleSaveErrorResource(resource: Resource.Error) {
        buttonSave?.isEnabled = true
        Snackbar.make(coordinatorLayoutRoot!!, resource.error.resourceMessage, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.button_view_error_retry) { buttonSave?.callOnClick() }
            .show()
    }

    private fun initMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.fragment_home_location_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun getCoordinates() {
        viewModel.getCoordinates().observe(this, ::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<W3WResponse>)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
            canAddMarker = true
        }
    }

    private fun handleSuccessResource(resource: Resource.Success<W3WResponse>) {
        addMarker(resource.data)
    }

    private fun handleErrorResource(resource: Resource.Error) {
        val exception = resource.error
        if (exception is NetworkException.Business && exception.code == NO_DATA_FOUND_ERROR_CODE) {
            Toast.makeText(requireContext(), getString(R.string.toast_w3w_not_found), Toast.LENGTH_LONG).show()
            return
        }
        Snackbar.make(coordinatorLayoutRoot!!, resource.error.resourceMessage, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.button_view_error_retry) { getCoordinates() }
            .show()
    }

    private fun addMarker(response: W3WResponse?) {
        googleMap?.clear()
        val words = response?.words
        val coordinates = response?.coordinates
        val latitude = coordinates?.lat ?: 0.0
        val longitude = coordinates?.lng ?: 0.0
        val latLng = LatLng(latitude, longitude)

        marker = googleMap?.addMarker(MarkerOptions().position(latLng).title(words).draggable(true))
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, MARKER_ZOOM))
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_style))
        googleMap.setOnMarkerDragListener(this)

        googleMap.setOnMapLongClickListener { latLng ->
            if (canAddMarker && marker == null) {
                googleMap.clear()
                marker = googleMap.addMarker(MarkerOptions().position(latLng).draggable(true))
                buttonSave?.isEnabled = true
            }
        }
    }

    override fun onMarkerDragEnd(marker: Marker?) {
        this.marker = marker
    }

    override fun onMarkerDragStart(marker: Marker?) {
        buttonSave?.isEnabled = true
    }

    override fun onMarkerDrag(marker: Marker?) {
        // No impl
    }

    private companion object {

        private const val NO_DATA_FOUND_ERROR_CODE = 1002
        private const val MARKER_ZOOM = 12f
    }
}
