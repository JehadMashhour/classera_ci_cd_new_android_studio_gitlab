package com.classera.homelocation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.homelocation.HomeLocationRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 1/7/2020.
 * Classera
 * r.altheeb@classera.com
 */
class HomeLocationViewModelFactory @Inject constructor(
    private val homeLocationRepository: HomeLocationRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeLocationViewModel(homeLocationRepository) as T
    }
}
