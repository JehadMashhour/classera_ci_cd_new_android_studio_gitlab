package com.classera.main

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.Keep
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.forEach
import androidx.core.view.size
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.transition.TransitionManager
import com.classera.core.Activities
import com.classera.core.activities.BaseToolbarActivity
import com.classera.core.intentTo
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.keepFocusWhile
import com.classera.core.utils.android.onDrawerClosed
import com.classera.core.utils.android.onTextChanged
import com.classera.data.glide.GlideApp
import com.classera.data.models.BaseWrapper
import com.classera.data.models.authentication.settings.GoogleRestrictionSettings
import com.classera.data.models.user.SchoolSettings
import com.classera.data.models.user.User
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.main.databinding.HeaderActivityMainNavigationViewBinding
import com.classera.main.fragment.MainFragment
import com.classera.main.logout.ConfirmLogoutBottomSheet
import com.classera.notigation.NavigationHandler
import com.google.android.material.navigation.NavigationView
import javax.inject.Inject
import com.classera.main.R.id.image_view_header_activity_main_navigation_view_toggle_actions_menu as toggle_actions_menu_id

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("TooManyFunctions")
class MainActivity : BaseToolbarActivity() {

    private var systemUiVisibility: Int = 0

    @Inject
    lateinit var viewModel: MainViewModel

    @Inject
    lateinit var navigationHandler: NavigationHandler

    private val navController by lazy { findNavController(R.id.fragment_activity_main_navigation) }

    private var drawerLayoutRoot: DrawerLayout? = null
    private var navigationView: NavigationView? = null
    private var navigationMenu: Menu? = null
    private var newMenu: MutableList<MenuItem?>? = null
    private var navigationViewHeader: View? = null
    private var navigationViewHeaderBinding: HeaderActivityMainNavigationViewBinding? = null
    private var imageViewClose: ImageView? = null
    private var childImage: ImageView? = null
    private var userAvatar: AppCompatImageView? = null
    private var editTextSearch: EditText? = null
    private var imageViewToggleActionsMenu: ImageView? = null
    private var scoreTextView: AppCompatTextView? = null

    private var focusTime: Long = 0
    private var focusTarget: View? = null
    private var imageViewClearSearch: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViews()
        initBinding()
        saveAndGetSchoolSetting()
        initNavigationGraph()
        initNavController()
        initNavigationViewActions()
        initDrawerLayoutCloseListener()
        hideSuccessPartnersForGlobalSchools()

        systemUiVisibility = window.decorView.systemUiVisibility
        transparentStatusBar()

        imageViewToggleActionsMenu?.visibility =
            if (viewModel.canShowActionMenu()) View.VISIBLE else View.GONE

        navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_success_partners)
            ?.setOnMenuItemClickListener {
                openApp()
                return@setOnMenuItemClickListener false
            }

        parentCases()
        adminCase()
        switchRoleHandle()
        handelNotificationNavigation()

        registerReceiver(userProfilePictureReceiver, IntentFilter("USER_IMAGE_CHANGED"))
    }

    private fun switchRoleHandle() {
        if (viewModel.wasTeacherRole()) {
            scoreTextView?.visibility = View.GONE
        }
    }

    private fun handelNotificationNavigation() {
        val notificationDataExtra = Activities.Splash.NOTIFICATION_DATA_EVENT_EXTRA
        if (intent.hasExtra(notificationDataExtra)) {
            intent.getStringExtra(Activities.Splash.NOTIFICATION_DATA_UUID_EXTRA)?.let {
                viewModel.updateNotificationStatus(
                    it
                )
            }
            val notificationEvent = intent.getStringExtra(notificationDataExtra)
            val notificationBody =
                intent.getStringExtra(Activities.Splash.NOTIFICATION_DATA_BODY_EXTRA)
            notificationEvent?.let {
                navigationView?.post {
                    navigationHandler.process(it, notificationBody, navController)
                }
            }
        }
    }

    private fun saveAndGetSchoolSetting() {
        viewModel.getSchoolSettings().observe(this, this::handleSchoolSettingsResource)
    }

    private fun openApp() {
        val packageName = "com.classera.successpartner"
        if (isAppInstalled(this, packageName))
            startActivity(packageManager.getLaunchIntentForPackage(packageName))
        else {
            Toast.makeText(this, getString(R.string.not_installed), Toast.LENGTH_LONG)
                .show()

            val url = "https://play.google.com/store/apps/details?id=$packageName"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }

    private fun isAppInstalled(activity: Activity, packageName: String): Boolean {
        val pm = activity.packageManager
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
            Log.e("error", "error can't find")
        }
        return false
    }

    private fun findViews() {
        drawerLayoutRoot = findViewById(R.id.drawer_layout_activity_main_root)
        navigationView = findViewById(R.id.navigation_view_activity_main)
        navigationMenu = navigationView?.menu
        newMenu = MutableList(navigationMenu?.size ?: 0) { null }
        navigationViewHeader = navigationView?.getHeaderView(0)
        imageViewClose =
            navigationViewHeader?.findViewById(R.id.image_view_header_activity_main_navigation_view_close)
        editTextSearch =
            navigationViewHeader?.findViewById(R.id.edit_text_header_activity_main_navigation_view_search)
        imageViewToggleActionsMenu = navigationViewHeader?.findViewById(toggle_actions_menu_id)
        childImage =
            navigationViewHeader?.findViewById(R.id.image_view_header_activity_main_navigation_view_avatar_student)
        userAvatar =
            navigationViewHeader?.findViewById(R.id.image_view_header_activity_main_navigation_view_avatar)
        scoreTextView =
            navigationViewHeader?.findViewById(R.id.text_view_header_activity_main_navigation_view_rating)
        imageViewClearSearch =
            navigationViewHeader?.findViewById(R.id.image_view_header_activity_main_navigation_search_close)
    }

    private fun adminCase() {
        if (prefs.userRole == UserRole.ADMIN) {
            scoreTextView?.visibility = View.GONE
        }
    }

    private fun parentCases() {
        if (prefs.userRole == UserRole.GUARDIAN) {
            childImage?.visibility = View.VISIBLE
            scoreTextView?.visibility = View.GONE
        }

        if (prefs.userRole == UserRole.GUARDIAN
            &&
            prefs.allowParentAccessToSmartClass.equals("0", true)
        ) {
            navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_virtual_class_room)?.isVisible =
                false
        }
    }

    private fun handleSuccessPartnerSettings() {
        viewModel.successPartnerVisibilitySettings()
            .observe(this, this::handleSuccessPartnerSettingsResource)
    }

    private fun handleSuccessPartnerSettingsResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                handleSuccessPartnerSettingsSuccessResource(
                    resource as Resource.Success<BaseWrapper<GoogleRestrictionSettings>>
                )
            }
            is Resource.Error -> {
                handleSuccessPartnerSettingsErrorResource(resource)
            }
        }
    }

    private fun handleSuccessPartnerSettingsSuccessResource(
        success: Resource.Success<BaseWrapper<GoogleRestrictionSettings>>
    ) {
        success.data?.data?.hideSuccessPartner?.let {
            navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_success_partners)?.isVisible =
                !it
        }
    }

    private fun handleSuccessPartnerSettingsErrorResource(resource: Resource.Error) {
        navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_success_partners)?.isVisible =
            false
    }

    private fun hideSuccessPartnersForGlobalSchools() {
        navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_success_partners)?.isVisible =
            !viewModel.isGlobalSchool()

        navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_calendar)?.isVisible =
            (!handleGeneralAndSpecficRolesCase())

        // hide blocked assessment
        if (prefs.userRole == UserRole.STUDENT || prefs.userRole == UserRole.GUARDIAN) {
            navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_assessments)?.isVisible =
                !viewModel.isAssessmentBlocked()
        }

        //hide call child
        if (prefs.userRole == UserRole.GUARDIAN) {
            navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_courses_guardian_call_student)
                ?.isVisible = !viewModel.isBlockedCallChild()
        } else if (prefs.userRole == UserRole.DRIVER) {
            navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_courses_driver_call_student)
                ?.isVisible = !viewModel.isBlockedCallChild()
        }
    }

    private fun handleGeneralAndSpecficRolesCase() = (
            prefs.userRole?.value == KAGAN_COACH || prefs.userRole?.value == MANAGERS_SUPERVISOR
                    || prefs.userRole?.value == CLINIC_OFFICER || prefs.userRole?.value == ACCOUNTANT
                    || prefs.userRole?.value == ADMISSION_OFFICER || prefs.userRole?.value == PERMISSION_OFFICER
                    || prefs.userRole?.value == EDUCATION_PARTNER
            )

    private fun initBinding() {
        navigationViewHeaderBinding =
            HeaderActivityMainNavigationViewBinding.bind(navigationViewHeader!!)
    }

    private fun initNavigationGraph() {
        val navigationResource = viewModel.getNavigationViewNavigationResource()
        val navigation = resources.getIdentifier(navigationResource, "navigation", packageName)
        val graphInflater = navController.navInflater
        val navGraph = graphInflater.inflate(navigation)
        navController.graph = navGraph
    }

    private fun initNavController() {
        navController.addOnDestinationChangedListener { _, destination, _ ->
            onDestinationChangedListener(destination)
        }
    }

    private fun initNavigationViewActions() {
        val menu = resources.getIdentifier(viewModel.getNavigationViewMenu(), "menu", packageName)
        navigationView?.inflateMenu(menu)
        imageViewToggleActionsMenu?.setOnClickListener { toggleActionsMenu() }
        userAvatar?.setOnClickListener {
            drawerLayoutRoot?.closeDrawer(GravityCompat.START)
            navController.navigate(R.id.item_menu_activity_main_navigation_view_profile)
        }
    }

    private fun toggleActionsMenu() {
        if (imageViewToggleActionsMenu?.rotation == ACTIONS_MENU_OPENED_ROTATION) {
            hideActionsMenu()
        } else {
            showActionsMenu()
        }
        navigationMenu = navigationView?.menu
        initNavigationMenuItemsClick()
    }

    private fun showActionsMenu() {
        imageViewToggleActionsMenu?.animate()
            ?.rotation(ACTIONS_MENU_OPENED_ROTATION)
            ?.setDuration(ACTIONS_MENU_TOGGLE_ANIMATION_DURATION)
            ?.start()

        TransitionManager.beginDelayedTransition(navigationView!!)
        navigationView?.menu?.clear()
        val menu = resources.getIdentifier(viewModel.getNavigationViewActionsMenu(), "menu", packageName)
        navigationView?.inflateMenu(menu)
        hideSuccessPartnersForGlobalSchools()
    }

    private fun hideActionsMenu() {
        imageViewToggleActionsMenu?.animate()
            ?.rotation(ACTIONS_MENU_CLOSED_ROTATION)
            ?.setDuration(ACTIONS_MENU_TOGGLE_ANIMATION_DURATION)
            ?.start()

        TransitionManager.beginDelayedTransition(navigationView!!)
        navigationView?.menu?.clear()
        val menu = resources.getIdentifier(viewModel.getNavigationViewMenu(), "menu", packageName)
        navigationView?.inflateMenu(menu)
        hideSuccessPartnersForGlobalSchools()
    }

    private fun initDrawerLayoutCloseListener() {
        drawerLayoutRoot?.onDrawerClosed {
            hideKeyboard()
        }
    }

    fun changeToolbarTitle(title: String?) {
        supportActionBar?.title = title
    }

    @Keep
    @Suppress("unused")
    fun openDrawerMenu() {
        drawerLayoutRoot?.openDrawer(GravityCompat.START)
    }

    @Keep
    @Suppress("unused")
    fun goToFragment(distention: Int, args: Bundle?) {
        val builder = NavOptions.Builder()
            .setLaunchSingleTop(true)
            .setEnterAnim(R.anim.nav_default_enter_anim)
            .setExitAnim(R.anim.nav_default_exit_anim)
            .setPopEnterAnim(R.anim.nav_default_pop_enter_anim)
            .setPopExitAnim(R.anim.nav_default_pop_exit_anim)
        builder.setPopUpTo(R.id.item_menu_activity_main_navigation_view_main, false)

        navController.navigate(distention, args, builder.build())
    }

    fun onDestinationChangedListener(destination: NavDestination) {
        if (destination.id == R.id.item_menu_activity_main_navigation_view_message_bottom_sheet)
            return

        if (destination.id != R.id.item_menu_activity_main_bottom_navigation_view_home) {
            showToolbar()
        } else {
            hideToolbar()
        }
    }

    private fun showToolbar() {
        if (toolbar?.visibility == View.VISIBLE) {
            return
        }
        toolbar?.visibility = View.VISIBLE
        coloredStatusBar()
    }

    private fun hideToolbar() {
        if (toolbar?.visibility == View.GONE) {
            return
        }
        toolbar?.visibility = View.GONE
        transparentStatusBar()
    }

    private fun coloredStatusBar() {
        window.decorView.systemUiVisibility = systemUiVisibility
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
    }

    private fun transparentStatusBar() {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = Color.TRANSPARENT
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(drawerLayoutRoot) || super.onSupportNavigateUp()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        initNavigation()
        changeNavigationViewToFullWidth()
        initListeners()
    }

    private fun initNavigation() {
        navigationView?.setupWithNavController(navController)
        setupActionBarWithNavController(navController, drawerLayoutRoot)
        toolbar?.setNavigationOnClickListener { onSupportNavigateUp() }
    }

    private fun changeNavigationViewToFullWidth() {
        val layoutParams = navigationView?.layoutParams as? DrawerLayout.LayoutParams?
        layoutParams?.width = Resources.getSystem().displayMetrics.widthPixels

        navigationView?.layoutParams = layoutParams
        navigationView?.requestLayout()
    }

    private fun initListeners() {
        viewModel.getUserInfo().observe(this, this::handleResource)
        imageViewClose?.setOnClickListener { onBackPressed() }
        navigationMenu?.forEach { newMenu?.add(it) }
        handleNavigationSearchView()

        initNavigationMenuItemsClick()

        if (prefs.schoolChat == NOT_ALLOWED)
            navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_chat)?.isVisible =
                false

        hideSuccessPartnersForGlobalSchools()
    }

    private fun handleNavigationSearchView() {
        editTextSearch?.keepFocusWhile {
            true
        }

        editTextSearch?.onTextChanged { text ->
            val menu = navigationView?.menu
            if (text != null) {
                menu?.clear()
                newMenu?.filterNotNull()?.filter { it.title.contains(text, true) && it.isVisible }
                    ?.forEach { item ->
                        menu?.add(item.groupId, item.itemId, item.order, item.title)?.apply {
                            icon = item.icon
                            isChecked = item.isChecked
                        }
                    }
            }
            initNavigationMenuItemsClick()
            hideSuccessPartnersForGlobalSchools()
            navigationView?.requestFocus()
            editTextSearch?.keepFocusWhile {
                true
            }
        }

        val onFocusChangeListener =
            OnFocusChangeListener { view, hasFocus ->
                val systemCurrentTime = System.currentTimeMillis()
                val delta = systemCurrentTime - focusTime
                if (hasFocus) {
                    if (delta > MIN_DELTA) {
                        focusTime = systemCurrentTime
                        focusTarget = view
                    }
                } else {
                    if (delta <= MIN_DELTA && view === focusTarget) {
                        focusTarget?.post { focusTarget?.requestFocus() }
                    }
                }
            }
        editTextSearch?.onFocusChangeListener = onFocusChangeListener

        imageViewClearSearch?.setOnClickListener {
            editTextSearch?.setText("")
        }
    }

    private fun handleKherkomSchool(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                (resource as Resource.Success<BaseWrapper<SchoolSettings>>).data?.data?.let {
                    if (it.isKherkom == true && it.allowAdminToAddVcr == true) {
                        navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_virtual_class_room_admin)
                            ?.isVisible =
                            true
                    }
                }
            }
            else -> {
            }
        }
    }

    private fun initNavigationMenuItemsClick() {
        navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_child_list)
            ?.setOnMenuItemClickListener {
                val intent = intentTo(Activities.ParentChildActivity)
                intent.putExtra("close", false)
                startActivity(intent)
                return@setOnMenuItemClickListener false
            }

        navigationMenu?.findItem(R.id.item_menu_activity_main_navigation_view_logout)
            ?.setOnMenuItemClickListener { menuItem ->
                drawerLayoutRoot?.closeDrawer(GravityCompat.START)
                menuItem?.isEnabled = false
                ConfirmLogoutBottomSheet.show((this as AppCompatActivity).supportFragmentManager) {
                    if (it) {
                        menuItem?.isEnabled = false
                        viewModel.logout().observe(this, Observer { resource ->
                            when (resource) {
                                is Resource.Loading -> {
                                    menuItem?.isEnabled = false
                                }
                                is Resource.Success<*> -> {
                                    val intent = intentTo(Activities.Login)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                    startActivity(intent)
                                }
                                is Resource.Error -> {
                                    menuItem?.isEnabled = true
                                }
                            }
                        })
                    } else {
                        menuItem?.isEnabled = true
                    }
                }
                return@setOnMenuItemClickListener false
            }
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<User>>)
            }
        }
    }

    private fun handleSuccessResource(success: Resource.Success<BaseWrapper<User>>) {
        navigationViewHeaderBinding?.user = success.data?.data
        navigationViewHeaderBinding?.pref = prefs
        navigationViewHeaderBinding?.executePendingBindings()
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleSchoolSettingsResource(resource: Resource) {
        when (resource) {
            is Resource.Error -> {
                handleSettingsErrorResource(resource)
            }
        }
        hideSuccessPartnersForGlobalSchools()
        handleSuccessPartnerSettings()
        handleKherkomSchool(resource)
    }

    private fun handleSettingsErrorResource(resource: Resource.Error) {
        Toast.makeText(this, resource.error.message, Toast.LENGTH_LONG).show()
    }

    override fun onBackPressed() {
        if (this.drawerLayoutRoot?.isDrawerOpen(GravityCompat.START) == true) {
            this.drawerLayoutRoot?.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private val userProfilePictureReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            userAvatar?.let {
                GlideApp.with(this@MainActivity).load(intent?.getStringExtra("newUserProfileImage"))
                    .circleCrop().into(it)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(userProfilePictureReceiver)
    }

    override fun onResume() {
        editTextSearch?.clearFocus()
        editTextSearch?.requestFocus()
        handleNavigationSearchView()
        super.onResume()
    }

    override fun onPause() {
        editTextSearch?.clearFocus()
        editTextSearch?.requestFocus()
        handleNavigationSearchView()
        super.onPause()
    }

    private companion object {

        private const val ACTIONS_MENU_TOGGLE_ANIMATION_DURATION = 300L
        private const val ACTIONS_MENU_OPENED_ROTATION = 180f
        private const val ACTIONS_MENU_CLOSED_ROTATION = 0f
        private const val NOT_ALLOWED = "0"
        private const val ADMISSION_OFFICER = "14"
        private const val ACCOUNTANT = "7   "
        private const val CLINIC_OFFICER = "22"
        private const val MANAGERS_SUPERVISOR = "26"
        private const val KAGAN_COACH = "13"
        private const val EDUCATION_PARTNER = "28"
        private const val PERMISSION_OFFICER = "29"
        private const val MIN_DELTA = 300
    }
}
