package com.classera.main.parent

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.parent.ParentChildWrapper
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.parent.ParentChildRepository
import kotlinx.coroutines.Dispatchers

class ParentChildViewModel(
    private val parentChildRepository: ParentChildRepository,private val prefs: Prefs) : BaseViewModel() {

    private var parentChildList: MutableList<ParentChildWrapper> = mutableListOf()

    fun getParentChildList(): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { parentChildRepository.getStudents() }

        parentChildList.clear()
        parentChildList.addAll(
            resource.element<BaseWrapper<List<ParentChildWrapper>>>()?.data ?: mutableListOf()
        )

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getChild(position: Int): ParentChildWrapper {
        return parentChildList[position]
    }

    fun saveChildIdAndImage(position: Int){
        prefs.childId = parentChildList[position].id
        prefs.childAvatar = parentChildList[position].profilePicture
    }

    fun getChildListCount(): Int {
        return parentChildList.size
    }
}
