package com.classera.main.parent

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Activities
import com.classera.core.activities.BaseToolbarActivity
import com.classera.core.custom.views.ErrorView
import com.classera.core.intentTo
import com.classera.core.utils.android.observe
import com.classera.data.network.errorhandling.Resource
import com.classera.main.R
import com.classera.main.parent.adapter.ParentChildListAdapter
import javax.inject.Inject

class ParentChildActivity : BaseToolbarActivity() {

    @Inject
    lateinit var viewModel: ParentChildViewModel

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var adapter: ParentChildListAdapter? = null
    private var errorView: ErrorView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_child)
        findViews()
        initListeners()
    }

    override fun onBackPressed() {
        if (prefs.childId.isNullOrBlank() && intent.getBooleanExtra("close",true)) {
            Toast.makeText(this@ParentChildActivity, getString(R.string.please_select_child), Toast.LENGTH_LONG).show()
        } else {
            super.onBackPressed()
        }
    }

    private fun findViews() {
        progressBar = findViewById(R.id.progress_bar_fragment_parent_child_list)
        recyclerView = findViewById(R.id.recycler_view_fragment_parent_child_list)
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout_fragment_parent_child_list)
        errorView = findViewById(R.id.error_view_fragment_parent_child_list)
    }


    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            adapter?.resetPaging()
            getChildList()
        }
        getChildList()
    }

    private fun getChildList() {
        viewModel.getParentChildList().observe(this, this::handleResource)
    }


    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun initAdapter() {
        adapter = ParentChildListAdapter(viewModel)
        adapter?.setOnItemClickListener { _, position ->
            viewModel.saveChildIdAndImage(position)
            //move to MainActivity
            val intent = intentTo(Activities.Main)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
        recyclerView?.adapter = adapter
    }


    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getChildList() }
        adapter?.finishLoading()
    }

}
