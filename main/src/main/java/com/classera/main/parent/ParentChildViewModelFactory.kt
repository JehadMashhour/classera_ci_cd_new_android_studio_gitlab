package com.classera.main.parent

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.parent.ParentChildRepository
import javax.inject.Inject

class ParentChildViewModelFactory  @Inject constructor(
    private val parentChildRepository: ParentChildRepository,
    private val prefs: Prefs
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ParentChildViewModel(parentChildRepository,prefs) as T
    }
}
