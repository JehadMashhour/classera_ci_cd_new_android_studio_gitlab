package com.classera.support.add

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.URLUtil
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseValidationFragment
import com.classera.core.utils.android.observe
import com.classera.data.network.errorhandling.Resource
import com.classera.support.R
import com.google.android.material.textfield.TextInputLayout
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import javax.inject.Inject


/**
 * Project: Classera
 * Created: JUNE 01, 2020
 *
 * @author Saeed Halawani
 */
@Screen("Add Support screen")
class AddSupportTicketFragment : BaseValidationFragment() {

    @Inject
    lateinit var viewModel: AddSupportTicketViewModel

    @NotEmpty(message = "validation_fragment_add_support_ticket_title")
    private var editTextTitle: EditText? = null

    @NotEmpty(message = "validation_fragment_add_support_ticket_body")
    private var editTextBody: EditText? = null

    @NotEmpty(message = "validation_fragment_add_support_ticket_type")
    private var autoCompleteTextViewSupportType: AutoCompleteTextView? = null

    @NotEmpty(message = "validation_fragment_add_support_ticket_module")
    private var autoCompleteTextViewSupportModule: AutoCompleteTextView? = null

    private var autoCompleteTextViewSupportPriority: AutoCompleteTextView? = null

    private var autoCompleteTextViewSchoolSchool: AutoCompleteTextView? = null

    private var schoolsSection: TextInputLayout? = null

    private var editTextUrl: EditText? = null

    private var addTicket: Button? = null

    private var checkBoxGeneralProblem: CheckBox? = null
    private var checkBoxRelatedToSchool: CheckBox? = null

    private var imageViewAttachFile: ImageView? = null
    private var file: MediaFile? = null
    private var textViewAttachedFileName: TextView? = null
    private var linearLayoutFile: LinearLayout? = null

    private var progressBar: ProgressBar? = null
    private var progressBarUpload: ProgressBar? = null
    private var errorView: ErrorView? = null

    private var selectedSupportTypeId: String? = null
    private var selectedSupportTypePosition = 0

    private var selectedSupportModuleId: String? = null
    private var selectedSupportModulePosition = 0

    private var selectedSupportPriorityId: String? = null
    private var selectedSupportPriorityPosition = 0

    private var selectedSupportSchoolId: String? = null
    private var selectedSupportSchoolPosition = 0

    private var generalProblem = 1

    private var attachmentList: ArrayList<MediaFile?>? = null

    override val layoutId: Int = R.layout.fragment_add_support_ticket

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        getSupportData()
        supportViewsListeners()
    }

    private fun findViews() {
        addTicket = view?.findViewById(R.id.button_fragment_add_ticket_upload)
        editTextTitle = view?.findViewById(R.id.edit_text_fragment_add_support_ticket_title)
        editTextBody = view?.findViewById(R.id.edit_text_fragment_add_support_ticket_description)
        autoCompleteTextViewSupportType =
            view?.findViewById(R.id.auto_complete_text_view_fragment_add_ticket_type)
        autoCompleteTextViewSupportModule =
            view?.findViewById(R.id.auto_complete_text_view_fragment_add_ticket_module)
        autoCompleteTextViewSupportPriority =
            view?.findViewById(R.id.auto_complete_text_view_fragment_add_ticket_priority)
        autoCompleteTextViewSchoolSchool =
            view?.findViewById(R.id.auto_complete_text_view_fragment_add_ticket_schools_list)

        editTextUrl = view?.findViewById(R.id.edit_text_fragment_add_support_ticket_problem_link)
        checkBoxGeneralProblem =
            view?.findViewById(R.id.checkbox_fragment_add_support_ticket_general_problem)
        checkBoxRelatedToSchool =
            view?.findViewById(R.id.checkbox_fragment_add_support_ticket_related_to_school)

        linearLayoutFile = view?.findViewById(R.id.layout_fragment_add_ticket_attach_file)
        textViewAttachedFileName = view?.findViewById(R.id.text_view_fragment_add_ticket_file_name)
        imageViewAttachFile = view?.findViewById(R.id.image_view_fragment_add_ticket_attach_file)

        progressBar = view?.findViewById(R.id.progress_bar_fragment_attachment)
        errorView = view?.findViewById(R.id.error_view_fragment_add_ticket)
        progressBarUpload = view?.findViewById(R.id.progress_bar_fragment_add_ticket_upload)
        schoolsSection = view?.findViewById(R.id.schools_layout)
    }

    private fun getSupportData() {
        viewModel.getSupportTypes().observe(this, ::handleSupportTypeResource)
        viewModel.getSupportModules().observe(this, ::handleSupportModuleResource)
        viewModel.getSupportPriorities().observe(this, ::handleSupportPriorityResource)
    }

    private fun handleSupportTypeResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSupportTypeLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSupportTypeSuccessResource()
            }
            is Resource.Error -> {
                handleSupportTypeErrorResource(resource)
            }
        }
    }

    private fun handleSupportTypeErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleSupportTypeLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun handleSupportTypeSuccessResource() {
        initSupportTypeAdapter()
        linearLayoutFile?.visibility = View.VISIBLE
    }

    private fun initSupportTypeAdapter() {
        val adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.dropdown_menu_popup_item,
                viewModel.getSupportTypeTitles()
            )
        autoCompleteTextViewSupportType?.setAdapter(adapter)
        autoCompleteTextViewSupportType?.setOnItemClickListener { _, _, position, _ ->
            selectedSupportTypePosition = position
            selectedSupportTypeId = viewModel.getSelectedSupportTypeId()
            removeError(autoCompleteTextViewSupportType?.parent?.parent as TextInputLayout)
        }
    }

    private fun handleSupportModuleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSupportModuleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSupportModuleSuccessResource()
            }
            is Resource.Error -> {
                handleSupportModuleErrorResource(resource)
            }
        }
    }

    private fun handleSupportModuleErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleSupportModuleLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun handleSupportModuleSuccessResource() {
        val adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.dropdown_menu_popup_item,
                viewModel.getSupportModulesTitles()
            )
        autoCompleteTextViewSupportModule?.setAdapter(adapter)
        autoCompleteTextViewSupportModule?.setOnItemClickListener { _, _, position, _ ->
            selectedSupportModulePosition = position
            selectedSupportModuleId = viewModel.getSelectedSupportModulesId()
            removeError(autoCompleteTextViewSupportModule?.parent?.parent as TextInputLayout)
        }
    }

    private fun handleSupportPriorityResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSupportPriorityLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSupportPrioritySuccessResource()
            }
            is Resource.Error -> {
                handleSupportPriorityErrorResource(resource)
            }
        }
    }

    private fun handleSupportPriorityErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleSupportPriorityLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun handleSupportPrioritySuccessResource() {
        initSupportPriorityAdapter()
    }

    private fun initSupportPriorityAdapter() {
        val adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.dropdown_menu_popup_item,
                viewModel.getSupportPrioritiesTitles()
            )
        autoCompleteTextViewSupportPriority?.setAdapter(adapter)
        autoCompleteTextViewSupportPriority?.setOnItemClickListener { _, _, position, _ ->
            selectedSupportPriorityPosition = position
            selectedSupportPriorityId = viewModel.getSelectedSupportPrioritiesId()
            removeError(autoCompleteTextViewSupportPriority?.parent?.parent as TextInputLayout)
        }
    }

    private fun handleSupportSchoolsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSupportSchoolsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSupportSchoolsSuccessResource()
            }
            is Resource.Error -> {
                handleSupportSchoolsErrorResource(resource)
            }
        }
    }

    private fun handleSupportSchoolsErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleSupportSchoolsLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun handleSupportSchoolsSuccessResource() {
        val adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.dropdown_menu_popup_item,
                viewModel.getSupportSchoolsTitles()
            )
        autoCompleteTextViewSchoolSchool?.setAdapter(adapter)
        autoCompleteTextViewSchoolSchool?.setOnItemClickListener { _, _, position, _ ->
            selectedSupportSchoolPosition = position
            selectedSupportSchoolId = viewModel.getSelectedSupportSchoolId()
            removeError(autoCompleteTextViewSchoolSchool?.parent?.parent as TextInputLayout)
        }
    }

    private fun supportViewsListeners() {

        imageViewAttachFile?.setOnClickListener {
            val intent = Intent(requireContext(), FilePickerActivity::class.java)
            intent.putExtra(
                FilePickerActivity.CONFIGS, Configurations.Builder()
                    .setMaxSelection(-1)
                    .setCheckPermission(true)
                    .setShowFiles(true)
                    .setShowImages(true)
                    .setShowAudios(true)
                    .setShowVideos(true)
                    .enableImageCapture(false)
                    .setSkipZeroSizeFiles(true)
                    .build()
            )
            startActivityForResult(intent, ATTACH_FILE_REQUEST_CODE)
        }

        addTicket?.setOnClickListener { validator.validate() }

        checkBoxGeneralProblem?.setOnCheckedChangeListener { _, checked ->
            if (checked) {
                this.generalProblem = 1
                checkBoxRelatedToSchool?.isChecked = false
                schoolsSection?.visibility = View.GONE
            } else this.generalProblem = 0
        }

        checkBoxRelatedToSchool?.setOnCheckedChangeListener { _, checked ->
            if (checked) {
                schoolsSection?.visibility = View.VISIBLE
                checkBoxGeneralProblem?.isChecked = false
                viewModel.getSchoolsList().observe(this, ::handleSupportSchoolsResource)
            } else schoolsSection?.visibility = View.GONE
        }
    }

    private fun isUrlValid(): Boolean {
        if (editTextUrl?.text?.isNotEmpty() == true) {
            if (!URLUtil.isValidUrl(editTextUrl?.text.toString())) {
                Toast.makeText(
                    requireContext(),
                    R.string.validation_fragment_add_support_invalid_url,
                    Toast.LENGTH_SHORT
                ).show()
                return false
            }
        }
        return true
    }

    override fun onValidationSucceeded() {
        if (!isUrlValid()) {
            return
        }

        if (!((!checkBoxGeneralProblem?.isChecked!!) && (!checkBoxRelatedToSchool?.isChecked!!))) {
            val attachmentPathList = ArrayList<String>()
            attachmentList?.forEachIndexed { index, value ->
                attachmentPathList.add(index, value?.path ?: "")
            }

            val attachmentMimeTypeList = ArrayList<String>()
            attachmentList?.forEachIndexed { index, value ->
                attachmentMimeTypeList.add(index, value?.mimeType ?: "")
            }

            val data = mutableMapOf(
                "url" to editTextUrl?.text?.toString(),
                "title" to editTextTitle?.text?.toString(),
                "body" to editTextBody?.text?.toString(),
                "general_problem" to generalProblem,
                "school_id" to viewModel.getSelectedSupportSchoolId(),
                "type" to viewModel.getSelectedSupportTypeId(),
                "module" to viewModel.getSelectedSupportModulesId(),
                "priority" to viewModel.getSelectedSupportPrioritiesId()
            )
            if (viewModel.getSelectedSupportPrioritiesId() == "-1") {
                data.remove("priority")
            }
            if (viewModel.getSelectedSupportSchoolId() == "-1") {
                data.remove("school_id")
            }
            viewModel.addSupportTicket(data, attachmentPathList, attachmentMimeTypeList)
                .observe(this, ::handleSubmitResource)

        } else {
            Toast.makeText(
                requireContext(),
                getString(R.string.please_add_general_problem),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun handleSubmitResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSubmitLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSubmitSuccessResource()
            }
            is Resource.Error -> {
                handleSubmitErrorResource(resource)
            }
        }
    }

    private fun handleSubmitLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBarUpload?.visibility = View.VISIBLE
            addTicket?.text = ""
            addTicket?.isEnabled = false
        } else {
            progressBarUpload?.visibility = View.GONE
            addTicket?.setText(R.string.button_fragment_add_support_ticket_upload)
            addTicket?.isEnabled = true
        }
    }

    private fun handleSubmitSuccessResource() {
        findNavController().popBackStack()
        Toast.makeText(
            context,
            resources.getString(R.string.toast_fragment_add_support_ticket_success_add_ticket_msg),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun handleSubmitErrorResource(resource: Resource.Error) {
        if (resource.error.message != null) {
            Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(
                requireContext(),
                resources.getString(R.string.toast_fragment_add_support_ticket_error_add_ticket_msg),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ATTACH_FILE_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val files = data.getParcelableArrayListExtra<MediaFile>(FilePickerActivity.MEDIA_FILES)
            textViewAttachedFileName?.text = files?.joinToString(", \n") { it.name }
            attachmentList = files
        }
    }

    private companion object {
        private const val ATTACH_FILE_REQUEST_CODE = 105
    }
}
