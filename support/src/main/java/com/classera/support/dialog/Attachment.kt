package com.classera.support.dialog

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.classera.support.BR

/**
 * Project: Classera
 * Created: Mar 27, 2020
 *
 * @author Mohamed Hamdan
 */
data class Attachment(val url: String?) : BaseObservable() {

    val name: String
        get() = url!!.substring(url.lastIndexOf("/") + 1)

    @get:Bindable
    var progress: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.progress)
        }

    @get:Bindable
    var state: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.state)
        }
}
