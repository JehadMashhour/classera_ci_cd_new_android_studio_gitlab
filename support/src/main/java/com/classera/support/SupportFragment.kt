package com.classera.support

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.custom.views.QuickFilterView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject


const val NAVIGATE_DEBOUNCE = 1000L

@Screen("Support")
class SupportFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: SupportViewModel

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var adapter: SupportAdapter? = null
    private var errorView: ErrorView? = null
    private var filterView: QuickFilterView? = null
    private var addSupportTicket: FloatingActionButton? = null
    private var searchView: SearchView? = null

    override val layoutId: Int = R.layout.fragment_support

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
        initFilter()
    }

    private fun initFilter() {
        filterView?.setAdapter(R.array.support_filter_entries, R.array.support_filter_entry_values)
        filterView?.setOnFilterSelectedListener {
            adapter?.resetPaging()
            recyclerView?.scrollToPosition(RECYCLER_VIEW_SCROLL_POSITION)
            getSupport(DEFAULT_PAGE)
        }
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_support)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_support)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_support)
        errorView = view?.findViewById(R.id.error_view_fragment_support)
        filterView = view?.findViewById(R.id.filter_view_fragment_support)
        addSupportTicket = view?.findViewById(R.id.floating_action_button_fragment_add_support)
    }

    private fun initListeners() {

        addSupportTicket?.setOnClickListener {
            findNavController().navigate(SupportFragmentDirections.addSupportTicket())
        }

        swipeRefreshLayout?.setOnRefreshListener {
            viewModel.getSupport(
                searchView?.query,
                filterView?.getSelectedFilterKey(),
                DEFAULT_PAGE
            )
                .observe(this, this::handleResource)
        }
        getSupport()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_support, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_support_search)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange { getSupport() }

        searchView?.setOnCloseListener {
            getSupport()
            return@setOnCloseListener false
        }
    }

    private fun getSupport(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            adapter?.resetPaging()
        }
        viewModel.getSupport(
            searchView?.query,
            filterView?.getSelectedFilterKey(),
            pageNumber
        )
            .observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
            initAdapterListeners()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = SupportAdapter(viewModel)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreListener (::getSupport)
    }

    private fun initAdapterListeners() {
        adapter?.setOnItemClickListener { _, position ->
           val ticket = viewModel.getSupportTicket(position)
           val id = ticket?.ticketId
           val title = ticket?.ticketTitle
           val directions = SupportFragmentDirections.actionNavigationFragmentMainSupportDetails(title,id)

            GlobalScope.launch(Dispatchers.Main) {
                findNavController().navigate(directions)
                delay(NAVIGATE_DEBOUNCE)
            }
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getSupport() }
        adapter?.finishLoading()
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        filterView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }

    private companion object {

        private const val RECYCLER_VIEW_SCROLL_POSITION = 0
    }
}
