package com.classera.support.details

import android.app.Application
import android.os.Build
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.classera.data.repositories.support.SupportRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.storage.Storage
import com.classera.storage.StorageImpl
import com.classera.storage.StorageLegacyImpl
import dagger.Binds
import dagger.Module
import dagger.Provides


@Module
abstract class SupportDetailsFragmentModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: SupportDetailsViewModelFactory
        ): SupportDetailsViewModel {
            return ViewModelProvider(fragment, factory)[SupportDetailsViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideSupportDetailsViewModelFactory(
            fragment: Fragment,
            supportRepository: SupportRepository,
            userRepository: UserRepository,
            application: Application
        ): SupportDetailsViewModelFactory {
            val args by fragment.navArgs<SupportDetailsFragmentArgs>()
            return SupportDetailsViewModelFactory(args, supportRepository, userRepository, application)
        }

        @Provides
        @JvmStatic
        fun provideStorage(fragment: Fragment): Storage {
            return if (Build.VERSION.SDK_INT == Build.VERSION_CODES.Q) {
                StorageImpl(fragment.requireActivity())
            } else {
                StorageLegacyImpl(fragment.requireActivity())
            }
        }
    }

    @Binds
    abstract fun bindActivity(activity: SupportDetailsFragment): Fragment
}
