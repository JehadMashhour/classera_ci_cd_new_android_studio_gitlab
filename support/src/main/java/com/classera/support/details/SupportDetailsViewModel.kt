package com.classera.support.details

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseAndroidViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.support.CommentWrapper
import com.classera.data.models.support.SupportDataWrapper
import com.classera.data.models.support.SupportWrapper
import com.classera.data.moshi.timeago.TimeAgoAdapter
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.support.SupportRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.data.toString
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import java.util.*

// TODO revamp network exception
class SupportDetailsViewModel(
    private val ticketId: String?,
    private val supportRepository: SupportRepository,
    private val userRepository: UserRepository,
    application: Application
) : BaseAndroidViewModel(application) {

    var support: SupportWrapper? = null
    private var pageNumber: Int = DEFAULT_PAGE

    private var _notifyCommentsAdapterLiveData = MutableLiveData<Unit>()
    var notifyCommentsAdapterLiveData: LiveData<Unit> = _notifyCommentsAdapterLiveData

    private var comments: MutableList<CommentWrapper> = mutableListOf()

    private val _toastLiveData = MutableLiveData<Any>()
    val toastLiveData: LiveData<Any> = _toastLiveData

    private val type = mutableListOf<SupportDataWrapper>()
    private val modules = mutableListOf<SupportDataWrapper>()
    private val priorities = mutableListOf<SupportDataWrapper>()
    private val statuses = mutableListOf<SupportDataWrapper>()

    fun getDetails() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            supportRepository.getSupportDetails(ticketId)
        }
        support = resource.element<BaseWrapper<SupportWrapper>>()?.data
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getComments() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            supportRepository.getSupportComments(
                ticketId,
                pageNumber
            )
        }
        resource.element<BaseWrapper<List<CommentWrapper>>>()?.data?.let { comments.addAll(it) }
        emit(resource)
        emit(Resource.Loading(show = false))
        if (resource is Resource.Success<*>) {
            pageNumber++
        }
    }

    fun getCommentCount(): Int {
        return comments.size
    }

    fun getCommentAttachmentList(position: Int): List<String>? {
        return comments[position].commentAttachment
    }

    fun getSupportProblemFullLink(): String {
        return support?.problemLink ?: ""
    }

    fun getSupportAttachmentList(): List<String>? {
        return support?.attachments
    }

    fun getComment(position: Int): CommentWrapper? {
        return comments[position]
    }

    fun addComment(comment: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            val id = System.currentTimeMillis().toString()
            val user = userRepository.getLocalUser().first()?.firstOrNull()
            val supportComment = CommentWrapper(
                id = id,
                postContent = comment,
                created = getCreatedAt(),
                fullName = user?.fullName,
                postUserPhoto = user?.imageUrl
            )
            comments.add(0, supportComment)

            startCommentRequest(supportComment)
        }
    }

    private suspend fun startCommentRequest(supportComment: CommentWrapper) {
        val index = comments.indexOfFirst { it.id == supportComment.id }
        if (index != -1) {
            comments[index] = supportComment.copy(isLoading = true, isFailed = false)
            _notifyCommentsAdapterLiveData.postValue(Unit)
            val comment = supportComment.postContent
            val resource = tryNoContentResource {
                supportRepository.addComment(ticketId, comment)
            }

            if (resource is Resource.Error) {
                comments[index] = supportComment.copy(isLoading = false, isFailed = true)
            } else {
                comments[index] = supportComment.copy(isLoading = false, isFailed = false)
            }

            _notifyCommentsAdapterLiveData.postValue(Unit)
        }
    }

    private fun getCreatedAt(): String? {
        val date = Date()
        return TimeAgoAdapter().fromJson(date.toString("yyyy-MM-dd HH:mm:ss"))
    }

    fun onCommentRetryClicked(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val attachmentComment = comments[position]
            startCommentRequest(attachmentComment)
        }
    }

    fun getSupportTypes() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource =
            tryResource { supportRepository.getSupportTypes() }

        type.clear()
        type.addAll(resource.element<BaseWrapper<List<SupportDataWrapper>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSupportTypeTitles(): List<String> {
        return type.mapNotNull { it.title }
    }

    fun getSupportModules() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource =
            tryResource { supportRepository.getSupportModules() }

        modules.clear()
        modules.addAll(resource.element<BaseWrapper<List<SupportDataWrapper>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSupportModulesTitles(): List<String> {
        return modules.mapNotNull { it.title }
    }

    fun getSupportPriorities() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource =
            tryResource { supportRepository.getSupportPriority() }

        priorities.clear()
        priorities.addAll(resource.element<BaseWrapper<List<SupportDataWrapper>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSupportPrioritiesTitles(): List<String> {
        return priorities.mapNotNull { it.title }
    }

    fun getSupportStatuses() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource =
            tryResource { supportRepository.getSupportStatuses() }

        statuses.clear()
        statuses.addAll(resource.element<BaseWrapper<List<SupportDataWrapper>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSupportStatusesTitles(): List<String> {
        return statuses.mapNotNull { it.title }
    }

    fun updateStatus(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            updateSupportTicketStatus(position)
        }
    }

    private suspend fun updateSupportTicketStatus(position: Int) {
        val resource = tryNoContentResource {
            supportRepository.updateSupportTicketStatus(
                ticketId,
                statuses[position].id.toString()
            )
        }
        if (resource is Resource.Error) {
            handleErrorMessage(resource)
        }
    }

    fun updateModule(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            updateSupportTicketModule(position)
        }
    }

    private suspend fun updateSupportTicketModule(position: Int) {
        val resource = tryNoContentResource {
            supportRepository.updateSupportTicketModule(
                ticketId,
                modules[position].id.toString()
            )
        }
        if (resource is Resource.Error) {
            handleErrorMessage(resource)
        }
    }

    fun updatePriority(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            updateSupportTicketPriority(position)
        }
    }

    private suspend fun updateSupportTicketPriority(position: Int) {
        val resource = tryNoContentResource {
            supportRepository.updateSupportTicketPriority(
                ticketId,
                priorities[position].id.toString()
            )
        }
        if (resource is Resource.Error) {
            handleErrorMessage(resource)
        }
    }

    fun updateType(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            updateSupportTicketType(position)
        }
    }

    private suspend fun updateSupportTicketType(position: Int) {
        val resource = tryNoContentResource {
            supportRepository.updateSupportTicketType(
                ticketId,
                type[position].id.toString()
            )
        }
        if (resource is Resource.Error) {
            handleErrorMessage(resource)
        }
    }

    private fun handleErrorMessage(resource: Resource.Error) {
        if (resource.error.resourceMessage != null) {
            _toastLiveData.postValue(resource.error.message)
        } else {
            _toastLiveData.postValue(resource.error.resourceMessage)
        }
    }

    fun getCreatorId(): String? {
        support?.creatorId.let { creatorId ->
            return creatorId
        }
    }
}
