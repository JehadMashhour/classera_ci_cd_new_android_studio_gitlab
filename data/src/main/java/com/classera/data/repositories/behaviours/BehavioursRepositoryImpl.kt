package com.classera.data.repositories.behaviours

import com.classera.data.daos.behaviours.RemoteBehavioursDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.behaviours.Behaviour
import com.classera.data.models.behaviours.teacher.BehaviorActionsWraper
import com.classera.data.models.behaviours.teacher.BehaviorGroup
import com.classera.data.models.user.User
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
class BehavioursRepositoryImpl(private val remoteBehavioursDao: RemoteBehavioursDao) : BehavioursRepository {

    override suspend fun getStudentBehaviours(
        pageNumber: Int,
        filter: String?,
        text: CharSequence?
    ): BaseWrapper<List<Behaviour>> {
        val fields = mapOf(
            "p" to pageNumber,
            "text" to (text ?: ""),
            "positive" to (filter ?: "")
        )
        return remoteBehavioursDao.getStudentBehaviours(fields)
    }

    override suspend fun getTeacherBehaviours(
        pageNumber: Int,
        filter: String?,
        text: CharSequence?
    ): BaseWrapper<List<Behaviour>> {
        val fields = mapOf(
            "p" to pageNumber,
            "text" to (text ?: ""),
            "positive" to (filter ?: "")
        )
        return remoteBehavioursDao.getTeacherBehaviours(fields)
    }

    override suspend fun getBehaviourGroups(behaviorType: String): BaseWrapper<List<BehaviorGroup>> {
        val fields = mapOf(
            "type" to behaviorType
        )
        return remoteBehavioursDao.getBehaviourGroups(fields)
    }

    override suspend fun getBehaviourActions(behaviorGroupId: String?): BaseWrapper<BehaviorActionsWraper> {
        val fields = mapOf("behavior_group_id" to behaviorGroupId)
        return remoteBehavioursDao.getBehaviourActions(fields)
    }

    override suspend fun getStudentsByCourse(courseId: String?): BaseWrapper<List<User>> {
        val fields = mapOf("course_id" to courseId)
        return remoteBehavioursDao.getStudentsByCourse(fields)
    }

    override suspend fun deleteBehavior(
        studentId: String?,
        studentBehaviorId: String?
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "student_id" to studentId,
            "student_behavior_id" to studentBehaviorId
        )
        return remoteBehavioursDao.deleteBehavior(fields)
    }

    override suspend fun addBehaviour(
        fields: Map<String, Any?>,
        attachmentPath: String,
        mediaType: String
    ): NoContentBaseWrapper {
        if (attachmentPath.isNullOrEmpty()) {
            val paramsList = ArrayList<MultipartBody.Part>()
            fields.forEach {
                paramsList.add(MultipartBody.Part.createFormData(it.key, it.value.toString()))
            }
            return remoteBehavioursDao.addBehavior(paramsList)
        } else {
            val file = File(attachmentPath)
            val attachmentRequestBody = RequestBody.create(MediaType.parse(mediaType), file)
            val part = MultipartBody.Part.createFormData("attachment", file.name, attachmentRequestBody)
            val parts = mutableListOf(part)
            fields.forEach {
                parts.add(MultipartBody.Part.createFormData(it.key, it.value.toString()))
            }
            return remoteBehavioursDao.addBehavior(parts)
        }
    }

    override suspend fun editBehaviour(
        fields: Map<String, Any?>,
        attachmentPath: String,
        mediaType: String
    ): NoContentBaseWrapper {
        if (attachmentPath.isNullOrEmpty()) {
            val paramsList = ArrayList<MultipartBody.Part>()
            fields.forEach {
                paramsList.add(MultipartBody.Part.createFormData(it.key, it.value.toString()))
            }
            return remoteBehavioursDao.editBehavior(paramsList)
        } else {
            val file = File(attachmentPath)
            val attachmentRequestBody = RequestBody.create(MediaType.parse(mediaType), file)
            val part = MultipartBody.Part.createFormData("attachment", file.name, attachmentRequestBody)
            val parts = mutableListOf(part)
            fields.forEach {
                parts.add(MultipartBody.Part.createFormData(it.key, it.value.toString()))
            }
            return remoteBehavioursDao.editBehavior(parts)
        }
    }
}
