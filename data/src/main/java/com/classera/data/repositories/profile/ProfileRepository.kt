package com.classera.data.repositories.profile

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.profile.CityResponse
import com.classera.data.models.profile.CountryResponse
import com.classera.data.models.profile.Education
import com.classera.data.models.profile.Interest
import com.classera.data.models.profile.Language
import com.classera.data.models.profile.Skill
import com.classera.data.models.profile.Experiences
import okhttp3.MultipartBody

interface ProfileRepository {

    suspend fun getUserSkills(): BaseWrapper<List<Skill>>

    suspend fun deleteUserSkill(
        id: String
    ): NoContentBaseWrapper

    suspend fun editUserSkill(
        id: String,
        skill: String,
        skillLevel: String
    ): NoContentBaseWrapper

    suspend fun createUserSkill(
        skill: String,
        skillLevel: String
    ): NoContentBaseWrapper

    suspend fun getUserInterests(): BaseWrapper<List<Interest>>

    suspend fun deleteUserInterests(
        interestId: String
    ): NoContentBaseWrapper

    suspend fun editUserInterests(
        id: String,
        interest: String,
        description: String
    ): NoContentBaseWrapper

    suspend fun createUserInterests(
        interest: String,
        description: String
    ): NoContentBaseWrapper

    suspend fun getUserLanguages(): BaseWrapper<List<Language>>

    suspend fun deleteUserLanguages(
        languageId: String
    ): NoContentBaseWrapper

    suspend fun editUserLanguages(
        id: String,
        language: String,
        proficiency: String
    ): NoContentBaseWrapper

    suspend fun createUserLanguages(
        language: String,
        proficiency: String
    ): NoContentBaseWrapper

    @Suppress("LongParameterList")
    suspend fun editUserProfile(
        placeOfBirthCountry: String,
        address: String,
        cityOfBirth: String,
        bio: String,
        email: String,
        phoneNumber: String
    ): NoContentBaseWrapper

    suspend fun getUserEducations(): BaseWrapper<List<Education>>

    @Suppress("LongParameterList")
    suspend fun createUserEducation(
        countryId: String,
        cityId: String,
        school: String,
        fromYear: String,
        toYear: String,
        degree: String,
        fieldOfStudy: String,
        description: String
    ): NoContentBaseWrapper

    @Suppress("LongParameterList")
    suspend fun editUserEducation(
        id: String?,
        countryId: String,
        cityId: String,
        school: String,
        fromYear: String,
        toYear: String,
        degree: String,
        fieldOfStudy: String,
        description: String
    ): NoContentBaseWrapper

    suspend fun deleteUserEducation(
        id: String
    ): NoContentBaseWrapper

    suspend fun getCountries(): BaseWrapper<CountryResponse>

    suspend fun getCities(countryId: String): BaseWrapper<CityResponse>

    suspend fun changeUserProfilePicture(parts: List<MultipartBody.Part>): NoContentBaseWrapper

    suspend fun getUserExperiences(): BaseWrapper<List<Experiences>>

    suspend fun deleteUserExperience(
        id: String?
    ): NoContentBaseWrapper

    @Suppress("LongParameterList")
    suspend fun addUserExperiences(
        jobTitle: String?,
        jobType: String?,
        companyName: String?,
        startDate: String?,
        endDate: String?,
        description: String?
    ): NoContentBaseWrapper

    @Suppress("LongParameterList")
    suspend fun editUserExperiences(
        id: String?,
        jobTitle: String?,
        jobType: String?,
        companyName: String?,
        startDate: String?,
        endDate: String?,
        description: String?
    ): NoContentBaseWrapper

}
