package com.classera.data.repositories.attachment

import com.classera.data.daos.attachment.RemoteAttachmentDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.attachment.AttachmentCommentsWrapper
import com.classera.data.models.attachment.AttachmentData
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.rating.RatingResult
import okhttp3.MultipartBody

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class AttachmentRepositoryImpl(
    private val attachmentDao: RemoteAttachmentDao
) : AttachmentRepository {

    override suspend fun getAttachmentDetails(attachmentId: String?): BaseWrapper<Attachment> {
        val fields = mapOf("attachment_id" to attachmentId)
        return attachmentDao.getAttachmentDetails(fields)
    }

    override suspend fun getAttachmentComments(
        attachmentId: String?,
        page: Int
    ): BaseWrapper<AttachmentCommentsWrapper> {
        val fields = mapOf(
            "attachment_id" to attachmentId,
            "p" to page
        )
        return attachmentDao.getAttachmentComments(fields)
    }

    override suspend fun addLike(attachmentId: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "attachment_id" to attachmentId,
            "type" to 1
        )
        return attachmentDao.addReaction(fields)
    }

    override suspend fun addUnderstand(attachmentId: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "attachment_id" to attachmentId,
            "type" to 2
        )
        return attachmentDao.addReaction(fields)
    }

    override suspend fun addAttachmentComment(
        attachmentId: String?,
        title: String?,
        text: String?
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "attachment_id" to attachmentId,
            "title" to title,
            "text" to text
        )
        return attachmentDao.addAttachmentComment(fields)
    }

    override suspend fun addRate(
        attachmentId: String?,
        value: String,
        attachmentAuthorId: String?
    ): BaseWrapper<RatingResult> {
        val fields = mapOf(
            "model_id" to attachmentId,
            "model" to "Attachment",
            "author_id" to attachmentAuthorId,
            "value" to value
        )
        return attachmentDao.addRate(fields)
    }

    override suspend fun getAttachmentData(courseId: String?, type: String?): BaseWrapper<AttachmentData> {
        val fields = mapOf(
            "course_id" to courseId,
            "type" to type
        )
        return attachmentDao.getAttachmentData(fields)
    }

    override suspend fun uploadAttachment(parts: List<MultipartBody.Part>): NoContentBaseWrapper {
        return attachmentDao.uploadAttachment(parts)
    }
}
