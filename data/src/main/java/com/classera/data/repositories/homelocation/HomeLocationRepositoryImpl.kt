package com.classera.data.repositories.homelocation

import com.classera.data.daos.homeloction.RemoteHomeLocationDao
import com.classera.data.daos.homeloction.RemoteW3WDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.homelocation.ThreeWordsWraper
import com.classera.data.models.homelocation.w3w.W3WResponse


/**
 * Created by Rawan Al-Theeb on 1/7/2020.
 * Classera
 * r.altheeb@classera.com
 */
class HomeLocationRepositoryImpl(
    private val remoteMyLocationDao: RemoteHomeLocationDao,
    private val remoteW3WDao: RemoteW3WDao
) : HomeLocationRepository {

    override suspend fun getW3W(): BaseWrapper<ThreeWordsWraper> {
        return remoteMyLocationDao.getW3W()
    }

    override suspend fun saveW3W(w3w: String?): NoContentBaseWrapper {
        val fields = mutableMapOf("w3w" to w3w)
        return remoteMyLocationDao.saveW3W(fields)
    }

    override suspend fun convertToCoordinates(words: String): W3WResponse {
        return remoteW3WDao.convertToCoordinates(words)
    }

    override suspend fun convertTo3wa(coordinates: String): W3WResponse {
        return remoteW3WDao.convertTo3wa(coordinates)
    }
}
