package com.classera.data.repositories.parent

import com.classera.data.daos.parent.RemoteParentChildsDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.parent.ParentChildWrapper

class ParentChildRepositoryImpl(private val remoteParentChildDao: RemoteParentChildsDao) : ParentChildRepository {

    override suspend fun getStudents(): BaseWrapper<List<ParentChildWrapper>> {
        return remoteParentChildDao.getParentChildList()
    }

}
