package com.classera.data.repositories.classvisits

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.classvisits.Assessment
import com.classera.data.models.classvisits.ClassVisit
import com.classera.data.models.classvisits.Teacher
import com.classera.data.models.classvisits.Timeslot
import retrofit2.http.FieldMap
import retrofit2.http.QueryMap

/**
 * Created by Rawan Al-Theeb on 2/12/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Suppress("LongParameterList")
interface ClassVisitsRepository {
    suspend fun classVisits() : BaseWrapper<List<ClassVisit>>

    suspend fun getTeachers(): BaseWrapper<List<Teacher>>

    suspend fun getAssessments(): BaseWrapper<List<Assessment>>

    suspend fun getTimeslots(
        date: String?,
        teacherId: String?
    ): BaseWrapper<List<Timeslot>>

    suspend fun addClassVisit(
        date : String?,
        teacherId : String?,
        assessmentId : String?,
        privateVisit : String?,
        timeslotTitle : String?,
        timeslotId : String?
    ): NoContentBaseWrapper
}
