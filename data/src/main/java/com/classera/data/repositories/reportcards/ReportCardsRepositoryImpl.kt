package com.classera.data.repositories.reportcards


import com.classera.data.daos.reportcards.RemoteReportCardsDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.reportcards.ReportCardsWrapper
import com.classera.data.models.reportcards.reportcardsdetails.ReportCardDetails
import okhttp3.ResponseBody

class ReportCardsRepositoryImpl(private val remoteReportCardsDao: RemoteReportCardsDao) :
    ReportCardsRepository {

    override suspend fun getReportCardsList(
    ): BaseWrapper<ReportCardsWrapper> {
        return remoteReportCardsDao.getReportCards()
    }

    override suspend fun getReportCardDetails(url: String?, reportId: String?): BaseWrapper<ReportCardDetails> {
        return remoteReportCardsDao.getReportCardDetails(url, reportId)
    }

    override suspend fun downloadFile(url: String?): ResponseBody {
        return remoteReportCardsDao.downloadFile(url)
    }
}
