package com.classera.data.repositories.assessments

import com.classera.data.models.BaseWrapper
import com.classera.data.models.assessments.AssessmentResponse
import com.classera.data.models.assessments.AssessmentsDetails

interface AssessmentsRepoistory {

    suspend fun getAssessmentsList(
        pageNumber: Int,
        text: CharSequence? = null
    ): BaseWrapper<AssessmentResponse>

    suspend fun getAssessmentDetails(assessmentId: String?): BaseWrapper<AssessmentsDetails>
}
