package com.classera.data.repositories.user

import com.classera.data.daos.user.LocalUserDao
import com.classera.data.daos.user.RemoteUserDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.calendar.teacher.eventusers.UserSelectionRole
import com.classera.data.models.user.SchoolSettings
import com.classera.data.models.user.User
import com.classera.data.prefs.Prefs
import kotlinx.coroutines.flow.Flow

/**
 * Created by Odai Nazzal on 12/16/2019.
 * Classera
 * o.nazzal@classera.com
 */
class UserRepositoryImpl(
    private val remoteUserDao: RemoteUserDao,
    private val localUserDao: LocalUserDao,
    private val prefs: Prefs
) : UserRepository {

    override suspend fun getUserInfo(): BaseWrapper<User> {
        val data = remoteUserDao.getUserInfo()
        localUserDao.insert(data.data)
        if (prefs.selectedSemesterId == null) {
            prefs.semesterId = data.data?.currentSemester?.semesterId
        } else {
            prefs.semesterId = prefs.selectedSemesterId
        }
        prefs.userProfilePicture = data.data?.imageUrl
        prefs.userFullName = data.data?.fullName
        return data
    }

    override suspend fun getUserRoles(url: String?): BaseWrapper<List<UserSelectionRole>> {
        return remoteUserDao.getUserRoles(url)
    }

    override suspend fun getLocalUser(): Flow<List<User>?> {
        return localUserDao.getCurrentUser()
    }

    override suspend fun deleteUserFromLocalDatabase() {
        localUserDao.deleteAllData()
    }

    override suspend fun getSchoolSettings(): BaseWrapper<SchoolSettings> {
        val schoolSettings = remoteUserDao.getSchoolSettings()
        if (schoolSettings.success == true) {
            prefs.usersUploadEportfolios = schoolSettings.data?.allowUsersUploadEportfolios
                    prefs.studentsCreateDiscussions = schoolSettings.data?.allowStudentsCreateDiscussions
            prefs.studentsChangePhoneNumber = schoolSettings.data?.allowStudentsChangePhoneNumber
            prefs.studentsChangeEmail = schoolSettings.data?.allowStudentsChangeEmail
            prefs.studentsChangeProfilePic = schoolSettings.data?.allowStudentsChangeProfilePic
            prefs.allowParentAccessToSmartClass = schoolSettings.data?.allowParentAccessToSmartClass
            prefs.schoolChat = schoolSettings.data?.allowSchoolChat
            prefs.schoolType = schoolSettings.data?.schoolType
            prefs.isAssessmentBlocked = schoolSettings.data?.isAssessmentBlock
            prefs.allowStudentsRequests = schoolSettings.data?.allowStudentsRequests
        }
        return schoolSettings
    }
}
