package com.classera.data.repositories.switchroles

import com.classera.data.daos.switchroles.RemoteSwitchRolesDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.switchroles.AuthWrapper
import com.classera.data.models.switchroles.Role
import com.classera.data.models.user.UserRole
import com.classera.data.prefs.Prefs

class SwitchRolesRepositoryImpl(
    private val remoteSwitchRolesDao: RemoteSwitchRolesDao,
    private val prefs: Prefs
) : SwitchRolesRepository {

    override suspend fun generateNewAuth(role: UserRole): BaseWrapper<AuthWrapper> {
        val fields = mapOf("user_role" to role.value.toString())
        val generateJwsToken = remoteSwitchRolesDao.generateJwtToken(fields = fields)
        if (generateJwsToken.success == true) {
            prefs.authenticationToken = generateJwsToken.data?.token
            prefs.previousUserRole = prefs.userRole
            prefs.userRole = role
            prefs.childId = null
        }
        return generateJwsToken
    }

    override suspend fun getSwitchRolesList(): BaseWrapper<List<Role>> {
        return remoteSwitchRolesDao.getRoles()
    }
}
