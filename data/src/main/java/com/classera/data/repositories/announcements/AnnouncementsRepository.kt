package com.classera.data.repositories.announcements

import com.classera.data.models.BaseWrapper
import com.classera.data.models.announcements.Announcement
import com.classera.data.models.announcements.AnnouncementsWrapper


/**
 * Created by Rawan Al-Theeb on 1/1/2020.
 * Classera
 * r.altheeb@classera.com
 */
interface AnnouncementsRepository {

    suspend fun getAnnouncementsList(
        text: CharSequence? = null,
        pageNumber: Int
    ): BaseWrapper<AnnouncementsWrapper>

    suspend fun getAnnouncementDetails(announcementId: String?) : BaseWrapper<Announcement>
}
