package com.classera.data.repositories.calendar

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.calendar.AcademicCalendarEvent
import com.classera.data.models.calendar.teacher.teacherlectures.TeacherLecture

/**
 * Created by Odai Nazzal on 12/27/2019.
 * Classera
 * o.nazzal@classera.com
 */
@Suppress("LongParameterList")
interface CalendarRepository {

    suspend fun getUserEvents(start: String?, end: String? = null): BaseWrapper<List<AcademicCalendarEvent>>

    suspend fun teacherDeleteEvent(eventId: String?): NoContentBaseWrapper

    suspend fun teacherLectures(): BaseWrapper<List<TeacherLecture>>

    suspend fun teacherAddEvent(
        title: String?,
        startingDate: String?,
        endingDate: String?,
        allDay: String?,
        description: String?,
        lecturesIds: List<String?>?,
        usersIds: List<String?>?
    ): NoContentBaseWrapper

    suspend fun teacherEditEvent(
        title: String?,
        eventId: String?,
        startingDate: String?,
        endingDate: String?,
        allDay: String?,
        description: String?
    ): NoContentBaseWrapper
}
