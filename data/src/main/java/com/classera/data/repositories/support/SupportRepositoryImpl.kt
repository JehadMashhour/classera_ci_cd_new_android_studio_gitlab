package com.classera.data.repositories.support


import com.classera.data.daos.support.RemoteSupportDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.support.CommentWrapper
import com.classera.data.models.support.SupportDataWrapper
import com.classera.data.models.support.SupportWrapper
import okhttp3.MultipartBody


class SupportRepositoryImpl(private val remoteSupportDao: RemoteSupportDao) : SupportRepository {

    override suspend fun addComment(
        ticketId: String?,
        body: String?
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "ticket_id" to ticketId,
            "body" to body
        )
        return remoteSupportDao.addSupportComment(fields)
    }

    override suspend fun getSupportDetails(ticketId: String?): BaseWrapper<SupportWrapper> {
        return remoteSupportDao.getSupportDetails(ticketId)
    }

    override suspend fun getSupportComments(ticketId: String?, pageNumber: Int): BaseWrapper<List<CommentWrapper>> {
        val fields = mapOf(
            "ticket_id" to ticketId,
            "p" to pageNumber
        )
        return remoteSupportDao.getSupportComments(fields)
    }

    override suspend fun getSupport(
        searchText: CharSequence?,
        pageNumber: Int,
        text: CharSequence?
    ): BaseWrapper<List<SupportWrapper>> {
        val fields = mutableMapOf<String, Any?>(
            "p" to pageNumber,
            "title" to (searchText ?: "")
        )
        return if (text?.isNotEmpty() == true) {
            fields["status"] = text
            remoteSupportDao.getSupportByType(fields)
        } else {
            remoteSupportDao.getSupport(fields)
        }
    }

    override suspend fun addSupportTicket(parts: List<MultipartBody.Part>) {
        return remoteSupportDao.addSupportTicket(parts)
    }

    override suspend fun getSupportTypes(): BaseWrapper<List<SupportDataWrapper>> {
        return remoteSupportDao.getSupportTypes()
    }

    override suspend fun getSupportModules(): BaseWrapper<List<SupportDataWrapper>> {
        return remoteSupportDao.getSupportModules()
    }

    override suspend fun getSupportPriority(): BaseWrapper<List<SupportDataWrapper>> {
        return remoteSupportDao.getSupportPriorities()
    }

    override suspend fun getSupportStatuses(): BaseWrapper<List<SupportDataWrapper>> {
        return remoteSupportDao.getSupportStatuses()
    }

    override suspend fun updateSupportTicketStatus(ticketId: String?, statusId: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "ticket_id" to ticketId,
            "status" to statusId
        )
        return remoteSupportDao.updateSupportTicketStatus(fields)
    }

    override suspend fun updateSupportTicketModule(ticketId: String?, moduleId: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "ticket_id" to ticketId,
            "module" to moduleId
        )
        return remoteSupportDao.updateSupportTicketModules(fields)
    }

    override suspend fun updateSupportTicketType(ticketId: String?, typeId: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "ticket_id" to ticketId,
            "type" to typeId
        )
        return remoteSupportDao.updateSupportTicketType(fields)
    }

    override suspend fun updateSupportTicketPriority(ticketId: String?, priority: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "ticket_id" to ticketId,
            "priority" to priority
        )
        return remoteSupportDao.updateSupportTicketPriority(fields)
    }
}
