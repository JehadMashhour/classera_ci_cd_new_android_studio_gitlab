package com.classera.data.repositories.profile.publicprofile

import com.classera.data.daos.profile.publicprofile.RemotePublicProfileDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.profile.PublicProfileWrapper

class PublicProfileRepositoryImpl(
    private val remotePublicProfileDao: RemotePublicProfileDao
) : PublicProfileRepository {

    override suspend fun getPublicProfileByUserId(userId: String): BaseWrapper<PublicProfileWrapper> {
        val fields = mapOf("id" to userId)
        return remotePublicProfileDao.getPublicProfile(fields)
    }
}
