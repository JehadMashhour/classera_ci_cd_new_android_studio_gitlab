package com.classera.data.repositories.switchsemester

import com.classera.data.models.BaseWrapper
import com.classera.data.models.switchsemester.Semester
import com.classera.data.models.switchsemester.SemesterWrapper

interface SwitchSemesterRepository {

    suspend fun getSwitchSemesterList(
    ): BaseWrapper<List<Semester>>

    suspend fun generateSemester(semesterId: String):
            BaseWrapper<SemesterWrapper>

}
