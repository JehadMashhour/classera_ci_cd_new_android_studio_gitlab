package com.classera.data.repositories.mycard

import com.classera.data.models.BaseWrapper
import com.classera.data.models.mycard.MyCardResponse

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Rawan Altheeb
 */
interface MyCradRepository {

    suspend fun getUserCard(): BaseWrapper<MyCardResponse>
}
