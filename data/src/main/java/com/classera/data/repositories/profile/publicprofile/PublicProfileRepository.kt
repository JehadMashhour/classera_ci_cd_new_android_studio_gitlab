package com.classera.data.repositories.profile.publicprofile

import com.classera.data.models.BaseWrapper
import com.classera.data.models.profile.PublicProfileWrapper

interface PublicProfileRepository {

    suspend fun getPublicProfileByUserId(userId: String): BaseWrapper<PublicProfileWrapper>
}
