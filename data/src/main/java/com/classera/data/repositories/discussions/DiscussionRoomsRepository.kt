package com.classera.data.repositories.discussions

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.discussions.DiscussionCommentsWrapper
import com.classera.data.models.discussions.DiscussionPost
import com.classera.data.models.discussions.DiscussionRoom
import com.classera.data.models.discussions.RoomDetails
import com.classera.data.models.discussions.Student
import retrofit2.http.FieldMap

/**
 * Created by Odai Nazzal on 12/30/2019.
 * Classera
 *
 * o.nazzal@classera.com
 */
@Suppress("LongParameterList")
interface DiscussionRoomsRepository {

    suspend fun getStudentDiscussions(pageNumber: Int, text: CharSequence? = null): BaseWrapper<List<DiscussionRoom>>

    suspend fun getDiscussionDetails(
        roomId: String?,
        pageNumber: Int,
        text: CharSequence? = null
    ): BaseWrapper<List<DiscussionPost>>

    suspend fun getDiscussionComments(postId: String?, pageNumber: Int): BaseWrapper<DiscussionCommentsWrapper>

    suspend fun addComment(postId: String?, text: String?): NoContentBaseWrapper

    suspend fun getStudents(): BaseWrapper<List<Student>>

    suspend fun addRoom(
        title: String?,
        instructions: String?,
        hideComments: String?,
        lectures: List<String?>?,
        users : List<String?>?
    ): NoContentBaseWrapper

    suspend fun addPost(
        roomId: String?,
        text: String?
    ): NoContentBaseWrapper

    suspend fun deleteDiscussionComment(commentId: String?, type: String?): NoContentBaseWrapper

    suspend fun deleteRoom(roomId : String?) : NoContentBaseWrapper

    suspend fun getRoomDetails(roomId: String?): BaseWrapper<RoomDetails>

    suspend fun approveTopic(postId: String?, approve:String?) : NoContentBaseWrapper

    suspend fun editRoomSettings(roomId: String?, close: String?, approve: String?) : NoContentBaseWrapper

    suspend fun editRoom(
        title: String?,
        instructions: String?,
        hideComments: String?,
        roomId : String?,
        lectures: List<String?>?,
        users : List<String?>?
    ): NoContentBaseWrapper

}
