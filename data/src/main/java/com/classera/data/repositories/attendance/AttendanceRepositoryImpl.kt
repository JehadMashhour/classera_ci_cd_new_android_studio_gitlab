package com.classera.data.repositories.attendance

import com.classera.data.daos.attendance.RemoteAttendancesDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.attendance.AbsenceSection
import com.classera.data.models.attendance.Absences
import com.classera.data.models.attendance.AttendanceTimeSlot
import com.classera.data.models.courses.Course
import com.classera.data.models.user.User
import okhttp3.MultipartBody


/**
 * Created by Rawan Al-Theeb on 12/24/2019.
 * Classera
 * r.altheeb@classera.com
 */
class AttendanceRepositoryImpl(private val remoteAttendanceDao: RemoteAttendancesDao) : AttendanceRepository {

    override suspend fun getCourses(): BaseWrapper<List<Course>> {
        return remoteAttendanceDao.getCourses()
    }


    override suspend fun getAbsences(
        quickFilterKey: String?,
        page: Int,
        filterKey: String?
    ): BaseWrapper<List<Absences>> {
        return remoteAttendanceDao.getAttendance(quickFilterKey, page, filterKey)
    }

    override suspend fun getSections(courseId: String?): BaseWrapper<List<AbsenceSection>> {
        return remoteAttendanceDao.getSections(courseId)
    }

    override suspend fun getStudents(
        sectionId: String?,
        courseId: String?,
        lectureId: String?,
        timeSlotId: String?,
        date: String?,
        page: Int
    ): BaseWrapper<List<User>> {
        val query = mutableMapOf<String, Any?>()
        query["date"] = "$date"

        if (courseId != null) query["course_id"] = courseId
        if (lectureId != null) query["lecture_id"] = lectureId
        if (timeSlotId != null) query["timeslot_id"] = timeSlotId
        query["p"] = page
        query["r"] = ROWS_COUNT

        if (sectionId != null) {
            addSectionIds(query, sectionId)
        }

        return remoteAttendanceDao.getStudents(query)
    }

    private fun addSectionIds(query: MutableMap<String, Any?>, sectionId: String?) {
        val sectionIds = sectionId?.split(",")

        if (sectionIds?.size == 1) {
            query["section_id[0]"] = sectionId
        } else {
            sectionIds?.forEachIndexed { index, value ->
                query["section_id[$index]"] = value
            }
        }
    }

    override suspend fun getSectionTimeSlots(lectureId: String?): BaseWrapper<List<AttendanceTimeSlot>> {
        return remoteAttendanceDao.getSectionTimeSlots(lectureId)
    }

    override suspend fun getAllTimeSlots(): BaseWrapper<List<AttendanceTimeSlot>> {
        return remoteAttendanceDao.getAllTimeSlots()
    }

    override suspend fun createNewTimeSlot(
        lectureId: String?,
        timeSlotId: String?
    ): NoContentBaseWrapper {
        return remoteAttendanceDao.createNewTimeSlot(lectureId, timeSlotId)
    }

    override suspend fun changeStatusForStudent(body: Map<String, Any?>): NoContentBaseWrapper {
        return remoteAttendanceDao.changeStatusForStudent(body)
    }

    override suspend fun setLeaveWithPermission(parts: List<MultipartBody.Part>): NoContentBaseWrapper {
        return remoteAttendanceDao.setLeaveWithPermission(parts)
    }

    override suspend fun takeAttendanceByFace(parts: List<MultipartBody.Part>): NoContentBaseWrapper {
        return remoteAttendanceDao.takeAttendanceByFace(parts)
    }

    companion object {

       private const val ROWS_COUNT = 20
    }
}
