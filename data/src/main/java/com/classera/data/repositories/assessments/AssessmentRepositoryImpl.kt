package com.classera.data.repositories.assessments

import com.classera.data.daos.asessments.RemoteAssessmentsDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assessments.AssessmentResponse
import com.classera.data.models.assessments.AssessmentsDetails

class AssessmentRepositoryImpl(
    private val remoteAssessmentsDao: RemoteAssessmentsDao
) : AssessmentsRepoistory {

    override suspend fun getAssessmentsList(pageNumber: Int, text: CharSequence?): BaseWrapper<AssessmentResponse> {
        val fields = mapOf(
            "p" to pageNumber,
            "text" to (text ?: "")
        )
        return remoteAssessmentsDao.getAssessmentsList(fields)
    }

    override suspend fun getAssessmentDetails(assessmentId: String?): BaseWrapper<AssessmentsDetails> {
        val fields = mapOf("assessment_id" to assessmentId)
        return remoteAssessmentsDao.getAssessmentsDetails(fields)
    }
}



