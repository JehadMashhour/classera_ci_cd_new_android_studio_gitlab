package com.classera.data.repositories.lectures

import com.classera.data.daos.lectures.RemoteLecturesDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.vcr.Lecture
import com.classera.data.models.vcr.lecturetimeslotpreparation.LectureTimeslotPreparation

/**
 * Project: Classera
 * Created: Jan 29, 2020
 *
 * @author Mohamed Hamdan
 */
class LecturesRepositoryImpl(private val remoteLecturesDao: RemoteLecturesDao) : LecturesRepository {

    override suspend fun getLectures(): BaseWrapper<List<Lecture>> {
        return remoteLecturesDao.getLectures()
    }

}
