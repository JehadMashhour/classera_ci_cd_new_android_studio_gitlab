package com.classera.data.repositories.switchsemester


import com.classera.data.daos.switchsemester.RemoteSwitchSemestersDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.switchsemester.Semester
import com.classera.data.models.switchsemester.SemesterWrapper
import com.classera.data.prefs.Prefs

class SwitchSemesterRepositoryImpl(
    private val remoteSwitchSemesterDao: RemoteSwitchSemestersDao
    , private val prefs: Prefs
) : SwitchSemesterRepository {

    override suspend fun generateSemester(semesterId: String): BaseWrapper<SemesterWrapper> {
        val fields = mapOf("semester_id" to semesterId)

        val generateSemesterToken = remoteSwitchSemesterDao.generateSemesterToken(fields = fields)
        if (generateSemesterToken.success == true) {
            prefs.semesterToken = generateSemesterToken.data?.token
            prefs.selectedSemesterId = semesterId
        }
        return generateSemesterToken
    }

    override suspend fun getSwitchSemesterList(): BaseWrapper<List<Semester>> {
        return remoteSwitchSemesterDao.getSemesters()
    }
}
