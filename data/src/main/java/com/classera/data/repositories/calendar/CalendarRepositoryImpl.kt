package com.classera.data.repositories.calendar

import com.classera.data.daos.calendar.RemoteCalendarDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.calendar.AcademicCalendarEvent
import com.classera.data.models.calendar.teacher.teacherlectures.TeacherLecture

/**
 * Created by Odai Nazzal on 12/27/2019.
 * Classera
 * o.nazzal@classera.com
 */
class CalendarRepositoryImpl(private val remoteCalendarDao: RemoteCalendarDao) : CalendarRepository {

    override suspend fun getUserEvents(start: String?, end: String?): BaseWrapper<List<AcademicCalendarEvent>> {
        val fields = mapOf(
            "start" to start,
            "end" to end
        )
        return remoteCalendarDao.getUserEvents(fields)
    }

    override suspend fun teacherDeleteEvent(eventId: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "event_id" to eventId
        )
        return remoteCalendarDao.teacherDeleteEvent(fields)
    }

    override suspend fun teacherLectures(): BaseWrapper<List<TeacherLecture>> {
        return remoteCalendarDao.teacherLectures()
    }

    override suspend fun teacherAddEvent(
        title: String?,
        startingDate: String?,
        endingDate: String?,
        allDay: String?,
        description: String?,
        lecturesIds: List<String?>?,
        usersIds: List<String?>?
    ): NoContentBaseWrapper {
        val fields = mutableMapOf(
            "title" to title,
            "start" to startingDate,
            "end" to endingDate,
            "all_day" to allDay,
            "description" to description
        )
        lecturesIds?.forEachIndexed { index, value ->
            fields["lectures_ids[$index]"] = value
        }
        usersIds?.forEachIndexed { index, value ->
            fields["users[$index]"] = value
        }
        return remoteCalendarDao.teacherAddEvent(fields)
    }

    override suspend fun teacherEditEvent(
        title: String?,
        eventId: String?,
        startingDate: String?,
        endingDate: String?,
        allDay: String?,
        description: String?
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "title" to title,
            "event_id" to eventId,
            "start" to startingDate,
            "end" to endingDate,
            "all_day" to allDay,
            "description" to description
        )
        return remoteCalendarDao.teacherEditEvent(fields)
    }
}
