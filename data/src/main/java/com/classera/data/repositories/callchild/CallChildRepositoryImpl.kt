package com.classera.data.repositories.callchild

import com.classera.data.daos.callchild.RemoteCallChildDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.callchildren.*

class CallChildRepositoryImpl(private val remoteCallChildDao: RemoteCallChildDao) :
    CallChildRepository {

    override suspend fun getGuardianChildStudents(): BaseWrapper<List<ChildStudent>> {
        return remoteCallChildDao.getParentChildList()
    }

    override suspend fun callChild(userId: String?): BaseWrapper<Request> {
        val fields = mapOf(
            "user_id" to userId
        )
        return remoteCallChildDao.callChild(fields)
    }

    override suspend fun changeCallStatus(
        requestId: String?,
        status: String?
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "id" to requestId,
            "status" to status
        )
        return remoteCallChildDao.changeCallStatus(fields)
    }

    override suspend fun getPersonalDriverStudents(): BaseWrapper<List<DriverStudent>> {
        return remoteCallChildDao.getPersonalDriverStudents()
    }

    override suspend fun getPersonalDrivers(): BaseWrapper<List<Driver>> {
        return remoteCallChildDao.getPersonalDrivers()
    }

    override suspend fun getCallStatus(requestId: String?): BaseWrapper<CallStatus> {
        val fields = mapOf(
            "request_id" to requestId
        )
        return remoteCallChildDao.getCallStatus(fields)
    }

}
