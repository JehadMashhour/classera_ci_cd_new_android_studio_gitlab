package com.classera.data.repositories.notification

import com.classera.data.daos.notification.RemoteNotificationsDao
import com.classera.data.models.notification.NotificationWrapper
import com.classera.data.models.user.UserRole
import com.classera.data.prefs.Prefs

class NotificationRepositoryImpl(
    private val remoteNotificationDao: RemoteNotificationsDao,
    private val prefs: Prefs
) : NotificationRepository {

    override suspend fun getNotificationList(
        pageNumber: Int,
        text: CharSequence?
    ): NotificationWrapper {
        val fields = mutableMapOf<String, Any?>(
            "page" to pageNumber,
            "limit" to "20",
            "types[0]" to "announcement",
            "types[1]" to "homework",
            "types[2]" to "exam",
            "types[3]" to "attachment",
            "types[4]" to "Discussion Room Comment",
            "types[5]" to "mail",
            "types[6]" to "vcr",
            "source" to "mobile"
        )
        if (prefs.userRole == UserRole.ADMIN) {
            fields["types[7]"] = "ticket"
            fields["types[8]"] = "internal ticket"
        }
        return remoteNotificationDao.getNotificationList(prefs.userId, fields)
    }

    override suspend fun getNotificationCount(
        pageNumber: Int,
        text: CharSequence?
    ): NotificationWrapper {
        val fields = mutableMapOf<String, Any?>(
            "page" to pageNumber,
            "limit" to "20",
            "types[0]" to "announcement",
            "types[1]" to "homework",
            "types[2]" to "exam",
            "types[3]" to "attachment",
            "types[4]" to "Discussion Room Comment",
            "types[5]" to "mail",
            "types[6]" to "vcr",
            "source" to "mobile"
        )
        if (prefs.userRole == UserRole.ADMIN) {
            fields["types[7]"] = "ticket"
            fields["types[8]"] = "internal ticket"
        }
        return remoteNotificationDao.getNotificationCount(prefs.userId, fields)
    }
}
