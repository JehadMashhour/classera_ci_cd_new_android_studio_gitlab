package com.classera.data.repositories.vcr

import com.classera.data.crypto.removeNull
import com.classera.data.daos.vcr.RemoteVcrDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.vcr.Lecture
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.models.vcr.vendor.VcrStatusWrapper
import com.classera.data.models.vcr.lecturetimeslotpreparation.LectureTimeslotPreparation
import com.classera.data.models.vcr.teacher.Teacher
import com.classera.data.models.vcr.vcrconfirm.VcrConfirmResponse
import com.classera.data.models.vcr.vendor.VendorResponse
import com.classera.data.prefs.Prefs

/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */
class VcrRepositoryImpl(
    private val remoteVcrDao: RemoteVcrDao,
    private val prefs: Prefs
) : VcrRepository {

    override suspend fun getSmartClassrooms(
        key: String?,
        pageNumber: Int,
        text: CharSequence?
    ): BaseWrapper<List<VcrResponse>> {
        val fields = mapOf(
            "old" to key,
            "p" to pageNumber,
            "q" to text
        )
        return remoteVcrDao.getSmartClassrooms(fields.removeNull())
    }

    override suspend fun getTeacherSmartClassrooms(
        key: String?,
        pageNumber: Int,
        text: CharSequence?
    ): BaseWrapper<List<VcrResponse>> {
        val fields = mapOf(
            "old" to key,
            "p" to pageNumber,
            "q" to text
        )
        return remoteVcrDao.getTeacherSmartClassrooms(fields.removeNull())
    }

    override suspend fun getUpcomingVcrUrl(vcrId: String): BaseWrapper<VcrConfirmResponse> {
        val fields = mapOf("id" to vcrId)
        return remoteVcrDao.getUpcomingVcrUrl(fields)
    }

    override suspend fun getPassedVcrUrl(vcrId: String): BaseWrapper<VcrConfirmResponse> {
        val fields = mapOf("session_code" to vcrId)
        return remoteVcrDao.getPassedVcrUrl(fields)
    }

    override suspend fun deleteVcr(sessionCode: String?, vcrId: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "session_code" to sessionCode,
            "id" to vcrId
        )
        return remoteVcrDao.deleteVcr(fields)
    }

    override suspend fun addVcr(
        vendor: String?,
        teacherId: String?,
        title: String?,
        zoomLink: String?,
        externalLink: String?,
        passcode: String?,
        lectures: List<Lecture?>?,
        startingTime: String?,
        duration: String?,
        sharingStatus: String?,
        allowStart: String?,
        weeklyRepeat: String?,
        repeatUntil: String?,
        recurring: String?
    ): NoContentBaseWrapper {
        val fields = mutableMapOf(
            "vendor" to vendor,
            "tuid" to teacherId,
            "title" to title,
            "zoom_link" to zoomLink,
            "external_link" to externalLink,
            "passcode" to passcode,
            "starting_time" to startingTime,
            "duration" to duration,
            "sharing_status" to sharingStatus,
            "allow_student_start_vcr" to allowStart,
            "weekly_repeat" to weeklyRepeat,
            "repeat_until" to repeatUntil,
            "recurring" to recurring
        )
        lectures?.forEachIndexed { index, value ->
            fields["lectures_ids[$index][lecture_id]"] = value?.id
            fields["lectures_ids[$index][timeslot_id]"] = value?.timeslot?.id
            fields["lectures_ids[$index][preparation_id]"] = value?.preparation?.id
        }
        return remoteVcrDao.addVcr(fields.removeNull())
    }

    override suspend fun editVcr(
        id: String?,
        vendor: String?,
        title: String?,
        zoomLink: String?,
        externalLink: String?,
        passcode: String?,
        lectures: List<Lecture?>?,
        startingTime: String?,
        duration: String?,
        sharingStatus: String?,
        allowStart: String?,
        weeklyRepeat: String?,
        repeatUntil: String?,
        recurring: String?
    ): NoContentBaseWrapper {
        val fields = mutableMapOf(
            "id" to id,
            "vendor" to vendor,
            "title" to title,
            "zoom_link" to zoomLink,
            "external_link" to externalLink,
            "passcode" to passcode,
            "starting_time" to startingTime,
            "duration" to duration,
            "sharing_status" to sharingStatus,
            "allow_student_start_vcr" to allowStart,
            "weekly_repeat" to weeklyRepeat,
            "repeat_until" to repeatUntil,
            "recurring" to recurring
        )
        lectures?.forEachIndexed { index, value ->
            fields["lectures_ids[$index][lecture_id]"] = value?.id
            fields["lectures_ids[$index][timeslot_id]"] = value?.timeslot?.id
            fields["lectures_ids[$index][preparation_id]"] = value?.preparation?.id
        }
        return remoteVcrDao.editVcr(fields.removeNull())
    }

    override suspend fun getVcrStatus(vendor: Int?): BaseWrapper<VcrStatusWrapper> {
        val fields = mapOf("vendor" to vendor)
        return remoteVcrDao.getVcrStatus(fields)
    }

    override suspend fun getVendors(): BaseWrapper<VendorResponse> {
        return remoteVcrDao.getVendors()
    }

    override suspend fun getLectureTimeSlotsPreparations(lecturesIds: List<String?>)
            : BaseWrapper<List<LectureTimeslotPreparation>> {
        val fields = mutableMapOf<String, Any?>()
        lecturesIds.let {
            for ((index, value) in lecturesIds.withIndex()) {
                fields["lecture_ids[$index]"] = value
            }
        }

        return remoteVcrDao.getLectureTimeslotsPreparations(fields)
    }


    override suspend fun getTeachers(): BaseWrapper<List<Teacher>> {
        return remoteVcrDao.getTeachers()
    }

    override suspend fun getVcrDetails(
        id: String?,
        vendorId: String?
    ): BaseWrapper<VcrResponse> {
        val fields = mapOf(
            "id" to id,
            "vendor_id" to vendorId
        )
        return remoteVcrDao.getVcrDetails(fields)
    }

    override suspend fun getUpcomingVCRs(): BaseWrapper<List<VcrResponse>> =
        remoteVcrDao.getUpcomingVCRs()
}

