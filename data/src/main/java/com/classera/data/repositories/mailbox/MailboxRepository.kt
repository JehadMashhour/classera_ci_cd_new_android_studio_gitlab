package com.classera.data.repositories.mailbox

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.mailbox.DraftWrapper
import com.classera.data.models.mailbox.InboxDetailsResponse
import com.classera.data.models.mailbox.InboxResponse
import com.classera.data.models.mailbox.InboxSearchResponse
import com.classera.data.models.mailbox.MessageRoleResponse
import com.classera.data.models.mailbox.OutboxDetailsResponse
import com.classera.data.models.mailbox.OutboxResponse
import com.classera.data.models.mailbox.OutboxSearchResponse
import com.classera.data.models.mailbox.RecipientByRole
import com.classera.data.models.mailbox.Trash
import com.classera.data.models.mailbox.School
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody

/**
 * Project: Classera
 * Created: Jan 6, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
interface MailboxRepository {

    suspend fun returnToBox(
        msgId: String
    ): NoContentBaseWrapper

    suspend fun deletePermanently(
        msgId: String
    ): NoContentBaseWrapper

    suspend fun getTrash(
        pageNumber: Int,
        searchText: String
    ): BaseWrapper<List<Trash>>

    suspend fun getInbox(
        pageNumber: Int
    ): BaseWrapper<InboxResponse>

    suspend fun getOutbox(
        searchText: String,
        pageNumber: Int
    ): BaseWrapper<OutboxResponse>

    suspend fun deleteMessage(
        msgId: String
    ): NoContentBaseWrapper

    suspend fun searchInbox(
        searchText: String,
        pageNumber: Int
    ): BaseWrapper<InboxSearchResponse>

    suspend fun getInboxDetails(
        msgId: String
    ): BaseWrapper<InboxDetailsResponse>

    suspend fun getOutboxDetails(
        msgId: String
    ): BaseWrapper<OutboxDetailsResponse>

    suspend fun replyMessage(
        msgId: String,
        msgTitle: String,
        recipientId: String,
        msgBody: String
    ): NoContentBaseWrapper

    suspend fun sendMessage(parts: List<MultipartBody.Part>): NoContentBaseWrapper

    suspend fun getMessageRoles(schoolId: String?): BaseWrapper<List<MessageRoleResponse>>

    fun getAllRecipients(): Flow<List<RecipientByRole>?>

    suspend fun addRecipient(recipientUser: RecipientByRole)

    suspend fun deleteRecipient(recipientUser: RecipientByRole)

    suspend fun deleteAllRecipient()

    suspend fun getUserByRole(roleId: String?, schoolId: String?): BaseWrapper<List<RecipientByRole>>

    suspend fun getDraftList(pageNumber: Int, text: CharSequence? = null): BaseWrapper<List<DraftWrapper>>

    suspend fun getSchoolList(): BaseWrapper<List<School>>

    suspend fun deleteAllRecipientInSchool(schoolId: String?)
}
