package com.classera.data.repositories.notification

import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.notification.NotificationWrapper

interface NotificationRepository {

    suspend fun getNotificationList(
        pageNumber: Int,
        text: CharSequence? = null
    ): NotificationWrapper

    suspend fun getNotificationCount(
        pageNumber: Int,
        text: CharSequence? = null
    ): NotificationWrapper
}
