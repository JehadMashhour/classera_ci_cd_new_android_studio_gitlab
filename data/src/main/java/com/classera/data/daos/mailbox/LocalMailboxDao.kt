package com.classera.data.daos.mailbox

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.classera.data.models.mailbox.RecipientByRole
import kotlinx.coroutines.flow.Flow

@Dao
interface LocalMailboxDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(recipientUser: RecipientByRole?)

    @Delete
    suspend fun delete(recipientUser: RecipientByRole)

    @Query("DELETE FROM recipient_user")
    suspend fun deleteAllData()

    @Query("SELECT * FROM recipient_user")
    fun getAllRecipients(): Flow<List<RecipientByRole>?>

    @Query("DELETE FROM recipient_user WHERE schoolId = :schoolId")
    suspend fun deleteAllRecipientInSchool(schoolId: String?)

}
