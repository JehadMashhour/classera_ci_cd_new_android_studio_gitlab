package com.classera.data.daos.mycard

import com.classera.data.models.BaseWrapper
import com.classera.data.models.mycard.MyCardResponse
import retrofit2.http.GET

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Rawan Altheeb
 */
interface RemoteMyCardDao {

    @GET("users/get_user_classera_card")
    suspend fun getUserCard(): BaseWrapper<MyCardResponse>
}
