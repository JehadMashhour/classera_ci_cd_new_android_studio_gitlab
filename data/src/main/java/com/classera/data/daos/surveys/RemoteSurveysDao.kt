package com.classera.data.daos.surveys

import com.classera.data.models.BaseWrapper
import com.classera.data.models.surveys.Survey
import com.classera.data.network.Pagination
import retrofit2.http.GET
import retrofit2.http.QueryMap


/**
 * Created by Rawan Al-Theeb on 2/18/2020.
 * Classera
 * r.altheeb@classera.com
 */
interface RemoteSurveysDao {
    @Pagination
    @GET("assessments/list_surveys")
    suspend fun getListSurveys(@QueryMap fields: Map<String, @JvmSuppressWildcards Any?>): BaseWrapper<List<Survey>>
}
