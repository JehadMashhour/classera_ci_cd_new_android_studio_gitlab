package com.classera.data.daos.discussions

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.discussions.DiscussionCommentsWrapper
import com.classera.data.models.discussions.DiscussionPost
import com.classera.data.models.discussions.DiscussionRoom
import com.classera.data.models.discussions.RoomDetails
import com.classera.data.models.discussions.Student
import com.classera.data.network.Pagination
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.QueryMap

/**
 * Created by Odai Nazzal on 12/30/2019.
 * Classera
 *
 * o.nazzal@classera.com
 */
interface RemoteDiscussionDao {

    @Pagination
    @GET("discussions/get_discussion_rooms")
    suspend fun getStudentDiscussions(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<DiscussionRoom>>

    @Pagination
    @GET("discussions/get_discussion_rooms")
    suspend fun searchStudentDiscussions(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<DiscussionRoom>>

    @Pagination
    @GET("discussions/get_discussion_posts")
    suspend fun getDiscussionPosts(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<DiscussionPost>>

    @Pagination
    @GET("discussions/search_discussion_post")
    suspend fun searchDiscussionPosts(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<DiscussionPost>>

    @Pagination
    @GET("discussions/get_posts_comments")
    suspend fun getDiscussionComments(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<DiscussionCommentsWrapper>

    @FormUrlEncoded
    @POST("discussions/add_discussion_comment")
    suspend fun addComment(@FieldMap fields: Map<String, @JvmSuppressWildcards Any?>): NoContentBaseWrapper

    @GET("discussions/get_lectures_for_discussion_room")
    suspend fun getStudents(): BaseWrapper<List<Student>>

    @FormUrlEncoded
    @POST("discussions/teacher_add_room")
    suspend fun addRoom(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("discussions/add_discussion_comment")
    suspend fun addPost(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("discussions/delete_comment")
    suspend fun deleteDiscussionComment(@FieldMap fields : Map<String, String?>) : NoContentBaseWrapper

    @FormUrlEncoded
    @POST("discussions/delete_discussion_room")
    suspend fun deleteRoom(@FieldMap fields : Map<String , String?>) : NoContentBaseWrapper

    @GET("discussions/teacher_get_room_details")
    suspend fun getRoomDetails(@QueryMap fields : Map<String , String?>): BaseWrapper<RoomDetails>

    @FormUrlEncoded
    @POST("discussions/approve")
    suspend fun approveTopic(@FieldMap fields : Map<String , String?>) : NoContentBaseWrapper

    @FormUrlEncoded
    @POST("discussions/edit_room_settings")
    suspend fun editRoomSettings(@FieldMap fields : Map<String , String?>) : NoContentBaseWrapper

    @FormUrlEncoded
    @POST("discussions/teacher_update_discussion_room")
    suspend fun editRoom(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper
}

