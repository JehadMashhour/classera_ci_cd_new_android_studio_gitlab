package com.classera.data.daos.announcements

import com.classera.data.models.BaseWrapper
import com.classera.data.models.announcements.Announcement
import com.classera.data.models.announcements.AnnouncementsWrapper
import com.classera.data.network.IncludeChildId
import com.classera.data.network.Pagination
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

/**
 * Created by Rawan Al-Theeb on 1/1/2020.
 * Classera
 * r.altheeb@classera.com
 */
interface RemoteAnnouncementsDao {

    @IncludeChildId
    @Pagination
    @GET("Announcements/get_all_announcement/")
    suspend fun getAnnouncementsList(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<AnnouncementsWrapper>

    @GET("announcements/get_announcement_details")
    suspend fun getAnnouncementDetails(
        @Query("announcement_id") announcementId: String?
    ): BaseWrapper<Announcement>
}
