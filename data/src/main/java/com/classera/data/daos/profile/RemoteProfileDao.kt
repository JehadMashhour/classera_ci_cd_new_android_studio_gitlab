package com.classera.data.daos.profile

import com.classera.data.daos.attachment.UPLOAD_ATTACHMENT_TIME
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.profile.CityResponse
import com.classera.data.models.profile.CountryResponse
import com.classera.data.models.profile.Education
import com.classera.data.models.profile.Interest
import com.classera.data.models.profile.Language
import com.classera.data.models.profile.Skill
import com.classera.data.models.profile.Experiences
import com.classera.data.network.CustomTimeout
import okhttp3.MultipartBody
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.QueryMap
import java.util.concurrent.TimeUnit


/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
interface RemoteProfileDao {

    // skills
    @GET("profiles/list_user_skills")
    suspend fun getUserSkills(): BaseWrapper<List<Skill>>

    @FormUrlEncoded
    @POST("profiles/delete_user_skill")
    suspend fun deleteUserSkill(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("profiles/add_user_skill")
    suspend fun createUserSkill(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("profiles/edit_user_skill")
    suspend fun editUserSkill(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    // interests
    @GET("profiles/get_user_interests")
    suspend fun getUserInterests(): BaseWrapper<List<Interest>>

    @FormUrlEncoded
    @POST("profiles/delete_user_interests")
    suspend fun deleteUserInterest(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("profiles/add_user_interests")
    suspend fun createUserInterest(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("profiles/edit_user_interests")
    suspend fun editUserInterest(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    // languages
    @GET("profiles/get_user_languages")
    suspend fun getUserLanguages(): BaseWrapper<List<Language>>

    @FormUrlEncoded
    @POST("profiles/delete_user_languages")
    suspend fun deleteUserLanguages(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("profiles/add_user_languages")
    suspend fun createUserLanguages(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("profiles/edit_user_languages")
    suspend fun editUserLanguages(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    // education
    @GET("profiles/get_user_educations")
    suspend fun getUserEducations(): BaseWrapper<List<Education>>

    @FormUrlEncoded
    @POST("profiles/add_user_education")
    suspend fun createUserEducation(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("profiles/delete_user_education")
    suspend fun deleteUserEducation(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    // profile
    @FormUrlEncoded
    @POST("users/edit_profile")
    suspend fun editUserProfile(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @GET("users/get_countries")
    suspend fun getCountries(): BaseWrapper<CountryResponse>

    @GET("users/get_cities")
    suspend fun getCities(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<CityResponse>

    @CustomTimeout(UPLOAD_ATTACHMENT_TIME, TimeUnit.MINUTES)
    @Multipart
    @POST("Users/change_profile_photo")
    suspend fun changeUserProfilePicture(@Part parts: List<MultipartBody.Part>): NoContentBaseWrapper

    @GET("Profiles/get_user_experiences")
    suspend fun getUserExperiences(): BaseWrapper<List<Experiences>>

    @FormUrlEncoded
    @POST("Profiles/delete_user_experiances")
    suspend fun deleteUserExperience(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("Profiles/add_user_experiances")
    suspend fun addUserExperiences(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

}
