package com.classera.data.daos.reportcards

import com.classera.data.models.BaseWrapper
import com.classera.data.models.reportcards.ReportCardsWrapper
import com.classera.data.models.reportcards.reportcardsdetails.ReportCardDetails
import com.classera.data.network.IncludeChildId
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface RemoteReportCardsDao {

    @IncludeChildId
    @GET("users/get_student_reports_list")
    suspend fun getReportCards(): BaseWrapper<ReportCardsWrapper>

    @IncludeChildId
    @GET
    suspend fun getReportCardDetails(
        @Url url: String?,
        @Query("report_id") reportId: String?
    ): BaseWrapper<ReportCardDetails>

    @GET
    suspend fun downloadFile(@Url url: String?): ResponseBody
}
