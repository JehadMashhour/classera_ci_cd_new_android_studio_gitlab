package com.classera.data.daos.vcr

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.models.vcr.vendor.VcrStatusWrapper
import com.classera.data.models.vcr.lecturetimeslotpreparation.LectureTimeslotPreparation
import com.classera.data.models.vcr.teacher.Teacher
import com.classera.data.models.vcr.vcrconfirm.VcrConfirmResponse
import com.classera.data.models.vcr.vendor.VendorResponse
import com.classera.data.network.IncludeChildId
import com.classera.data.network.Pagination
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.QueryMap


/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */
interface RemoteVcrDao {

    @IncludeChildId
    @Pagination
    @GET("vcrs/student_get_smart_classrooms")
    suspend fun getSmartClassrooms(@QueryMap fields: Map<String, @JvmSuppressWildcards Any?>)
            : BaseWrapper<List<VcrResponse>>

    @Pagination
    @GET("vcrs/teacher_vcrs_list")
    suspend fun getTeacherSmartClassrooms(@QueryMap fields: Map<String, @JvmSuppressWildcards Any?>)
            : BaseWrapper<List<VcrResponse>>

    @GET("vcrs/student_launch")
    suspend fun getUpcomingVcrUrl(@QueryMap fields: Map<String, String?>): BaseWrapper<VcrConfirmResponse>

    @IncludeChildId
    @GET("vcrs/view_recording")
    suspend fun getPassedVcrUrl(@QueryMap fields: Map<String, String?>): BaseWrapper<VcrConfirmResponse>

    @FormUrlEncoded
    @POST("Vcrs/teacher_delete_vcr")
    suspend fun deleteVcr(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("Vcrs/teacher_vcrs")
    suspend fun addVcr(@FieldMap fields: Map<String, @JvmSuppressWildcards Any?>): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("Vcrs/teacher_edit_vcr")
    suspend fun editVcr(@FieldMap fields: Map<String, @JvmSuppressWildcards Any?>): NoContentBaseWrapper

    @GET("Vcrs/vendor_setup")
    suspend fun getVcrStatus(@QueryMap fields: Map<String, @JvmSuppressWildcards Any?>): BaseWrapper<VcrStatusWrapper>

    @GET("Vcrs/get_vendors")
    suspend fun getVendors(): BaseWrapper<VendorResponse>

    @GET("Vcrs/get_lectures__timeslots")
    suspend fun getLectureTimeslotsPreparations(@QueryMap fields: Map<String, @JvmSuppressWildcards Any?>)
            : BaseWrapper<List<LectureTimeslotPreparation>>

    @GET("Vcrs/get_admin_teachers")
    suspend fun getTeachers(): BaseWrapper<List<Teacher>>

    @GET("vcrs/vcr_details")
    suspend fun getVcrDetails(@QueryMap fields: Map<String, @JvmSuppressWildcards Any?>): BaseWrapper<VcrResponse>

    @GET("vcrs/upcoming_vcrs")
    suspend fun getUpcomingVCRs(): BaseWrapper<List<VcrResponse>>

}
