package com.classera.data.daos.behaviours

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.behaviours.Behaviour
import com.classera.data.models.behaviours.teacher.BehaviorActionsWraper
import com.classera.data.models.behaviours.teacher.BehaviorGroup
import com.classera.data.models.user.User
import com.classera.data.network.IncludeChildId
import com.classera.data.network.Pagination
import okhttp3.MultipartBody
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.QueryMap

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
interface RemoteBehavioursDao {

    @IncludeChildId
    @Pagination
    @GET("Behaviors/get_student_behaviors/")
    suspend fun getStudentBehaviours(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<Behaviour>>

    @Pagination
    @GET("behaviors/teacher_behaviors")
    suspend fun getTeacherBehaviours(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<Behaviour>>

    @GET("behaviors/get_behavior_groups")
    suspend fun getBehaviourGroups(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<BehaviorGroup>>

    @GET("behaviors/get_behaviors_actions")
    suspend fun getBehaviourActions(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<BehaviorActionsWraper>

    @GET("behaviors/get_teacher_course_students")
    suspend fun getStudentsByCourse(@QueryMap fields: Map<String, @JvmSuppressWildcards Any?>): BaseWrapper<List<User>>

    @FormUrlEncoded
    @POST("behaviors/delete_student_behavior")
    suspend fun deleteBehavior(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper

    @Multipart
    @POST("behaviors/add_behavior")
    suspend fun addBehavior(@Part attachment: List<MultipartBody.Part>?): NoContentBaseWrapper

    @Multipart
    @POST("behaviors/edit_student_behavior")
    suspend fun editBehavior(@Part attachment: List<MultipartBody.Part>?): NoContentBaseWrapper
}
