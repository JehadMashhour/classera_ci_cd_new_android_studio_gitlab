package com.classera.data.daos.chat

import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.chat.GroupWrapper
import com.classera.data.models.chat.MessagesWrapper
import com.classera.data.models.chat.Status
import com.classera.data.models.chat.UserStatusWrapper
import okhttp3.MultipartBody
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.QueryMap

/**
 * Created by Odai Nazzal on 12/19/2019. Modified by Lana Manaseer
 * Classera
 * o.nazzal@classera.com
 */
interface RemoteChatServiceDao {

    @GET("v1/user-status/{id}/")
    suspend fun getUserStatus(
        @Path("id") id: String?,
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): UserStatusWrapper<Status>

    @FormUrlEncoded
    @POST("update-status/")
    suspend fun updateUserStatus(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): UserStatusWrapper<Status>

    @GET("list-messages/{threadId}/{pageState}/")
    suspend fun getListMessages(
        @Path("threadId") threadId: String?,
        @Path("pageState") pageState: String?,
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): MessagesWrapper

    @Multipart
    @POST("v1/attachment")
    suspend fun uploadAttachment(@Part parts: List<MultipartBody.Part>)

    @FormUrlEncoded
    @POST("v1/create-group")
    suspend fun createGroup(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): GroupWrapper

    @FormUrlEncoded
    @POST("v1/delete-group")
    suspend fun deleteGroup(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("v1/update-group-name")
    suspend fun updateGroupName(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("delete-participants")
    suspend fun deleteParticipants(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("add-participants")
    suspend fun addParticipants(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): NoContentBaseWrapper

}
