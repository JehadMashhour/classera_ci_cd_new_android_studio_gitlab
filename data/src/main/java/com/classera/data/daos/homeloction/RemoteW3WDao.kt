package com.classera.data.daos.homeloction

import com.classera.data.models.homelocation.w3w.W3WResponse
import retrofit2.http.GET
import retrofit2.http.Query


/**
 * Created by Rawan Al-Theeb on 1/8/2020.
 * Classera
 * r.altheeb@classera.com
 */

interface RemoteW3WDao {

    @GET("convert-to-coordinates")
    suspend fun convertToCoordinates(@Query("words") words: String): W3WResponse

    @GET("convert-to-3wa")
    suspend fun convertTo3wa(@Query("coordinates") coordinates: String): W3WResponse
}
