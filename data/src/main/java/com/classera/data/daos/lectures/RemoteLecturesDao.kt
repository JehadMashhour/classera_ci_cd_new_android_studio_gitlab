package com.classera.data.daos.lectures

import com.classera.data.models.BaseWrapper
import com.classera.data.models.vcr.Lecture
import retrofit2.http.GET

/**
 * Project: Classera
 * Created: Jan 29, 2020
 *
 * @author Mohamed Hamdan
 */
interface RemoteLecturesDao {

    @GET("teachers/get_teacher_lecture")
    suspend fun getLectures(): BaseWrapper<List<Lecture>>

}
