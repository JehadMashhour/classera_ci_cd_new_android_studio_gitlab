package com.classera.data.daos.assignments

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.assignments.Assignment
import com.classera.data.models.assignments.AssignmentDetails
import com.classera.data.models.assignments.AssignmentsResponse
import com.classera.data.models.assignments.Preparation
import com.classera.data.models.assignments.PublishUser
import com.classera.data.models.assignments.QuestionDraft
import com.classera.data.models.assignments.QuestionsWrapper
import com.classera.data.models.assignments.listquestions.Question
import com.classera.data.network.IncludeChildId
import com.classera.data.network.Pagination
import retrofit2.http.Field
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import retrofit2.http.QueryMap


/**
 * Created by Rawan Al-Theeb on 12/24/2019.
 * Classera
 * r.altheeb@classera.com
 */
interface RemoteAssignmentsDao {

    @IncludeChildId
    @Pagination
    @GET("assignments/get_student_assignment_list")
    suspend fun getAssignments(
        @Query("type") type: String?,
        @Query("p") page: Int,
        @Query("text") text: CharSequence?,
        @Query("course_id") courseId: String?
    ): BaseWrapper<AssignmentsResponse>


    @Pagination
    @GET("assignments/get_teacher_assignment_list")
    suspend fun getTeacherAssignments(
        @Query("type") type: String?,
        @Query("p") page: Int,
        @Query("title") text: CharSequence?,
        @Query("course_id") courseId: String?
    ): BaseWrapper<AssignmentsResponse>

    @IncludeChildId
    @GET("assignments/student_assignment_info")
    suspend fun getAssignmentInfo(@QueryMap query: Map<String, String?>): BaseWrapper<Assignment>

    @IncludeChildId
    @GET("Assignments/student_assignment_detail")
    suspend fun getAssignmentDetails(@QueryMap query: Map<String, String?>): BaseWrapper<Assignment>

    @FormUrlEncoded
    @POST("assignments/student_show_questions")
    suspend fun getQuestions(
        @Field("assignment_id") id: String?,
        @Field("course_id") courseId: String?
    ): BaseWrapper<QuestionsWrapper>

    @FormUrlEncoded
    @POST("Questions/teacher_create_question")
    suspend fun addTrueFalseQuestion(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): NoContentBaseWrapper

    @GET("Questions/teacher_question_list")
    suspend fun getQuestionList(@QueryMap query: Map<String, String?>): BaseWrapper<List<Question>>

    @FormUrlEncoded
    @POST("assignments/student_save_answer")
    suspend fun saveAnswers(@FieldMap map: Map<String, @JvmSuppressWildcards Any?>?): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("assignments/student_save_draft")
    suspend fun saveAsDraft(@FieldMap data: Map<String, @JvmSuppressWildcards Any?>?): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("assignments/student_submit_exam")
    suspend fun submitAnswers(@FieldMap data: Map<String, @JvmSuppressWildcards Any?>?): NoContentBaseWrapper

    @FormUrlEncoded

    @POST("Questions/teacher_create_question")
    suspend fun addMultipleChoiceQuestion(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("assignments/delete_assignment")
    suspend fun deleteAssignment(@Field("assignment_id") id: String?): NoContentBaseWrapper

    @GET("Assignments/get_publish_user")
    suspend fun getPublishUser(@Query("course_id") courseId: String?): BaseWrapper<PublishUser>

    @GET("Assignments/get_preparation_for_exame")
    suspend fun getPreparation(@Query("course_id") courseId: String?): BaseWrapper<List<Preparation>>

    @FormUrlEncoded
    @POST("Assignments/teacher_create_assignment")
    suspend fun addAssignment(@FieldMap fields: Map<String, @JvmSuppressWildcards Any?>): NoContentBaseWrapper

    @GET("Assignments/teacher_get_assignment_details")
    suspend fun getTeacherAssignmentDetails(
        @Query("assignment_id") assignmentId: String?
    ): BaseWrapper<AssignmentDetails>

    @FormUrlEncoded
    @POST("Assignments/teacher_edit_assignment")
    suspend fun editTeacherAssignment(@FieldMap fields: Map<String, @JvmSuppressWildcards Any?>): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("Questions/teacher_create_question")
    suspend fun createEssayQuestion(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper

    @GET("assignments/get_student_draft")
    suspend fun getDraft(@QueryMap query: Map<String, String?>): BaseWrapper<List<QuestionDraft>>

    @FormUrlEncoded
    @POST("Assignments/teacher_recall")
    suspend fun recallAssignment(@Field("assignment_id") id: String?): NoContentBaseWrapper
}
