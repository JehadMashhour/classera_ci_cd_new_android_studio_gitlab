package com.classera.data

import android.graphics.Bitmap

object ThumbnailMapper {
    val hashMap: HashMap<String, Bitmap?> = hashMapOf()
}
