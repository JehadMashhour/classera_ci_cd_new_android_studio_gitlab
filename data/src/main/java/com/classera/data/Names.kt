package com.classera.data

/**
 * Project: Classera
 * Created: Mar 17, 2020
 *
 * @author Mohamed Hamdan
 */
fun String?.getFormalUserName(): String {
    val splittedName = this?.split(" ")?.filter { it.isNotEmpty() }
    return splittedName?.mapIndexed { index, string ->
        if (index == splittedName.lastIndex) {
            return@mapIndexed string
        }
        return@mapIndexed string.toCharArray().firstOrNull().toString()
    }?.joinToString(separator = ". ") { it } ?: ""
}
