package com.classera.data.glide

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.StrictMode
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions
import com.classera.data.R
import com.classera.data.glide.baseurl.BaseUrlLoaderFactory
import com.classera.data.glide.vimeo.Vimeo
import com.classera.data.glide.vimeo.VimeoApis
import com.classera.data.glide.vimeo.VimeoDiskCache
import com.classera.data.glide.vimeo.VimeoLoaderFactory
import com.classera.data.glide.youtube.YoutubeUrlLoaderFactory
import okhttp3.OkHttpClient
import tech.linjiang.pandora.Pandora
import java.io.InputStream

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@GlideModule
class MyGlideModule : AppGlideModule() {

    private val okHttpClient by lazy { OkHttpClient.Builder().addInterceptor(Pandora.get().interceptor).build() }

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        super.applyOptions(context, builder)
        builder.setDefaultRequestOptions(
            RequestOptions()
                .format(DecodeFormat.PREFER_ARGB_8888)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
        )
    }

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        super.registerComponents(context, glide, registry)
        registry.append(String::class.java, InputStream::class.java, BaseUrlLoaderFactory())
        registry.append(String::class.java, InputStream::class.java, YoutubeUrlLoaderFactory())
        registry.append(
            Vimeo::class.java,
            InputStream::class.java,
            VimeoLoaderFactory(
                VimeoDiskCache(context),
                VimeoApis(okHttpClient),
                StrictMode.ThreadPolicy.Builder().permitAll().build()
            )
        )
    }

    companion object {

        private const val CROSS_FADE_DURATION = 600
    }
}
