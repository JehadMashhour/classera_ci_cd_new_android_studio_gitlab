package com.classera.data.glide.vimeo

/**
 * Project: Classera
 * Created: Jan 03, 2020
 *
 * @author Mohamed Hamdan
 */
data class Vimeo(val url:String)
