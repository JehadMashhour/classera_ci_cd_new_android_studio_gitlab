package com.classera.data.glide.vimeo

import android.os.StrictMode
import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.MultiModelLoaderFactory
import java.io.InputStream

/**
 * Project: Classera
 * Created: Jan 03, 2020
 *
 * @author Mohamed Hamdan
 */
class VimeoLoaderFactory(
    @VisibleForTesting var vimeoDiskCache: VimeoDiskCache?,
    @VisibleForTesting val vimeoApis: VimeoApis,
    @VisibleForTesting val threadPolicy: StrictMode.ThreadPolicy
) : ModelLoaderFactory<Vimeo, InputStream> {

    @NonNull
    override fun build(multiFactory: MultiModelLoaderFactory): ModelLoader<Vimeo, InputStream> {
        val urlLoader = multiFactory.build(GlideUrl::class.java, InputStream::class.java)
        return VimeoThumbnailUrlLoader(
            urlLoader,
            vimeoDiskCache,
            vimeoApis,
            threadPolicy
        )
    }

    override fun teardown() {
        vimeoDiskCache?.close()
        vimeoDiskCache = null
    }
}
