package com.classera.data.glide.vimeo

import android.net.Uri
import androidx.annotation.VisibleForTesting
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import java.util.*

/**
 * Project: Classera
 * Created: Jan 03, 2020
 *
 * @author Mohamed Hamdan
 */
class VimeoApis(private val okHttpClient: OkHttpClient) {

    fun getVideoThumbnail(model: Vimeo?): String? {
        if (model == null) {
            return null
        }

        val response = getVideoInfoApiRequest(model)?.execute()?.body()?.string()
        return if (response != null) {
            getVideoThumbnailFrom(response)
        } else {
            null
        }
    }

    @VisibleForTesting
    fun getVideoInfoApiRequest(model: Vimeo?): Call? {
        if (model == null) {
            return null
        }

        val url = VIDEO_INFO_API.format(Locale.ENGLISH, getIdFromUrl(model.url))

        return okHttpClient.newCall(Request.Builder().url(url).build())
    }

    private fun getIdFromUrl(url: String): String? {
        val uri = Uri.parse(url)
        return if (uri.getQueryParameter("v") != null) {
            uri.getQueryParameter("v")
        } else {
            Uri.parse(url).pathSegments.last()
        }
    }

    @VisibleForTesting
    fun getVideoThumbnailFrom(response: String): String? {
        return JSONObject(response)["thumbnail_url"] as String?
    }

    @VisibleForTesting
    companion object {

        const val VIDEO_INFO_API =
            "https://vimeo.com/api/oembed.json?url=https://player.vimeo.com/video/%s"
    }
}
