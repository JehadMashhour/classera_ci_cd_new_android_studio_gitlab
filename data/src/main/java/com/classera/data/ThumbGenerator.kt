package com.classera.data

import android.content.Context
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import androidx.core.net.toUri
import java.util.*

@Throws(Throwable::class)
fun generateThumbnailFromVideo(videoPath: String?): Bitmap {
    var bitmap: Bitmap? = null
    var mediaMetadataRetriever: MediaMetadataRetriever? = null
    try {
        mediaMetadataRetriever = MediaMetadataRetriever()
        mediaMetadataRetriever.setDataSource(
            videoPath,
            HashMap()
        )
        bitmap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST)
    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        mediaMetadataRetriever?.release()
    }

    return bitmap ?: Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)

}

@Throws(Throwable::class)
fun generateThumbnailFromLocalVideo(context: Context?, videoPath: String?): Bitmap {
    var bitmap: Bitmap? = null
    var mediaMetadataRetriever: MediaMetadataRetriever? = null
    try {
        mediaMetadataRetriever = MediaMetadataRetriever()
        mediaMetadataRetriever.setDataSource(context, videoPath?.toUri())
        bitmap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST)
    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        mediaMetadataRetriever?.release()
    }

    return bitmap ?: Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)

}
