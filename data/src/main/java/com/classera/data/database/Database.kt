package com.classera.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.classera.data.daos.assignments.LocalLastQuestionDao
import com.classera.data.daos.mailbox.LocalMailboxDao
import com.classera.data.daos.user.LocalUserDao
import com.classera.data.database.Database.Companion.DATABASE_VERSION_3
import com.classera.data.database.converters.AbsenceStatusConverter
import com.classera.data.database.converters.LevelTitleListConverter
import com.classera.data.database.converters.ScoreClassConverter
import com.classera.data.database.converters.UserRoleConverter
import com.classera.data.models.assignments.UserLastQuestion
import com.classera.data.models.mailbox.RecipientByRole
import com.classera.data.models.user.User

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@TypeConverters(
    LevelTitleListConverter::class,
    ScoreClassConverter::class,
    UserRoleConverter::class,
    AbsenceStatusConverter::class
)
@Database(entities = [User::class, RecipientByRole::class, UserLastQuestion::class], version = DATABASE_VERSION_3)
abstract class Database : RoomDatabase() {

    abstract fun localUserDao(): LocalUserDao

    abstract fun localMailboxDao(): LocalMailboxDao

    abstract fun localLastQuestionDao(): LocalLastQuestionDao

    companion object {

        const val DATABASE_VERSION_1 = 1
        const val DATABASE_VERSION_2 = 2
        const val DATABASE_VERSION_3 = 3
    }
}

