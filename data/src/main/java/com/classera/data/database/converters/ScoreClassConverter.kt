package com.classera.data.database.converters

import androidx.room.TypeConverter
import com.classera.data.models.score.ScoreClass

/**
 * Project: Classera
 * Created: Dec 17, 2019
 *
 * @author Mohamed Hamdan
 */
class ScoreClassConverter {

    @TypeConverter
    fun fromCountryLangList(value: ScoreClass?): String? {
        return value?.name
    }

    @TypeConverter
    fun toCountryLangList(value: String): ScoreClass? {
        return ScoreClass.valueOf(value)
    }
}
