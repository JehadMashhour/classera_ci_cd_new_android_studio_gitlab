package com.classera.data.database.converters

import androidx.room.TypeConverter
import com.classera.data.models.user.UserRole

/**
 * Project: Classera
 * Created: Dec 17, 2019
 *
 * @author Mohamed Hamdan
 */
class UserRoleConverter {

    @TypeConverter
    fun fromRole(value: UserRole): String {
        return value.name
    }

    @TypeConverter
    fun toRole(value: String): UserRole {
        return UserRole.valueOf(value)
    }
}
