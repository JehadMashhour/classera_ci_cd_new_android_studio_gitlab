package com.classera.data.database.converters

import androidx.room.TypeConverter
import com.classera.data.models.user.AbsenceStatus

/**
 * Project: Classera
 * Created: Dec 17, 2019
 *
 * @author Mohamed Hamdan
 */
class AbsenceStatusConverter {

    @TypeConverter
    fun fromRole(value: AbsenceStatus?): String? {
        return value?.name
    }

    @TypeConverter
    fun toRole(value: String?): AbsenceStatus? {
        return if (value != null) {
            AbsenceStatus.valueOf(value)
        } else {
            null
        }
    }
}
