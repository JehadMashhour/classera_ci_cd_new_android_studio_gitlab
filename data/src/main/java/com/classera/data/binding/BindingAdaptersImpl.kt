package com.classera.data.binding

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Guideline
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.core.text.HtmlCompat
import androidx.lifecycle.liveData
import at.grabner.circleprogress.CircleProgressView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.classera.data.R
import com.classera.data.ThumbnailMapper
import com.classera.data.dpToPx
import com.classera.data.extensions.createBitmapWithBorder
import com.classera.data.generateThumbnailFromVideo
import com.classera.data.glide.GlideApp
import com.classera.data.glide.vimeo.Vimeo
import com.classera.data.models.BackgroundColor
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.digitallibrary.AttachmentTypes
import com.classera.data.models.user.User
import com.devs.vectorchildfinder.VectorChildFinder
import com.devs.vectorchildfinder.VectorDrawableCompat
import com.github.abdularis.buttonprogress.DownloadButtonProgress
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.Dispatchers
import java.util.*


/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("MagicNumber", "ComplexMethod")
class BindingAdaptersImpl : BindingAdapters {

    override fun ImageView.setImageUrl(url: String?) {
        GlideApp.with(this).load(url).into(this)
    }

    override fun ImageView.setImageUrlWithDefaultProfilePlaceholder(url: String?) {
        GlideApp.with(this).load(url).placeholder(R.drawable.ic_default_user_avatar).into(this)
    }

    override fun ImageView.setImageUrlWithBackground(
        url: String?,
        @DrawableRes backgroundResId: Int?
    ) {
        backgroundResId?.let { this.setBackgroundResource(it) }
        GlideApp.with(this).load(url).into(this)
    }

    override fun ImageView.setCircleImageUrl(url: String?) {
        GlideApp.with(this).load(url).apply(
            RequestOptions.circleCropTransform()
        ).into(this)
    }

    override fun ImageView.setRoundedImageUrl(url: String?, radius: Int) {
        val requestOptions =
            RequestOptions().transform(CenterCrop(), RoundedCorners(radius.dpToPx(context).toInt()))
        GlideApp.with(this).load(url).apply(requestOptions).into(this)
    }

    override fun ImageView.setCircleBorderedImageUrl(
        url: String?,
        borderSize: Float?,
        borderColor: Int?
    ) {
        val bSize = borderSize ?: 4F
        val bColor = borderColor ?: ContextCompat.getColor(context, R.color.colorLightGreen)

        GlideApp.with(context)
            .asBitmap()
            .load(url)
            .apply(RequestOptions.circleCropTransform())
            .into(object : BitmapImageViewTarget(this) {
                override fun setResource(resource: Bitmap?) {
                    setImageDrawable(
                        resource?.run {
                            RoundedBitmapDrawableFactory.create(
                                resources,
                                if (bSize > 0) {
                                    createBitmapWithBorder(bSize, bColor)
                                } else {
                                    this
                                }
                            ).apply {
                                isCircular = true
                            }
                        }
                    )
                }
            })
    }

    override fun CircleProgressView.setCircleProgressValue(value: Int) {
        setValueAnimated(value.toFloat(), PROGRESS_ANIMATION_DURATION)
    }

    override fun CircleProgressView.setCircleProgressColor(value: Int) {
        setBarColor(value)
    }

    override fun ImageView.setImageViewTint(value: Int) {
        setColorFilter(value)
    }

    override fun ImageView.setImageViewSrcCompat(value: Int) {
        setImageResource(value)
    }

    override fun ImageView.setImageViewSrcCompat(value: Drawable) {
        setImageDrawable(value)
    }

    override fun ImageView.setImageViewVectorColor(
        @DrawableRes backgroundResId: Int,
        pathName: String?,
        pathColor: String?
    ) {
        val vector = VectorChildFinder(context, backgroundResId, this)

        pathColor?.let {
            val path1: VectorDrawableCompat.VFullPath = vector.findPathByName(pathName)
            path1.fillColor = Color.parseColor(pathColor)
        }

    }

    override fun ImageView.setDigitalLibraryAttachment(
        url: String?,
        digitalLibraryTypes: AttachmentTypes?,
        @DrawableRes backgroundResId: Int?
    ) {

/*        val transformation = RoundedCorners(ATTACHMENT_MEDIA_RADIUS.dpToPx(context).toInt())

        val requestOptions =
            RequestOptions()
                .placeholder(backgroundResId ?: 0)
                .transform(CenterCrop(), transformation)
                .diskCacheStrategy(
                    DiskCacheStrategy.NONE
                ).skipMemoryCache(true)
                .error(null)*/

        scaleType = ImageView.ScaleType.CENTER_CROP

        if (digitalLibraryTypes == AttachmentTypes.VIDEO && url?.contains("png") != true) {
            try {
                if (ThumbnailMapper.hashMap[url ?: ""] != null) {
                    GlideApp
                        .with(this.context.applicationContext)
                        .load(ThumbnailMapper.hashMap[url ?: ""])
                        .placeholder(backgroundResId ?: BackgroundColor.GRAY.resId)
                        //.apply(requestOptions)
                        .into(this)

                    return
                }

                liveData(Dispatchers.IO) {
                    emit(generateThumbnailFromVideo(url))
                }.observeForever { bitmap ->
                    if (bitmap != null) {
                        ThumbnailMapper.hashMap[url ?: ""] = bitmap

                        GlideApp
                            .with(this.context.applicationContext)
                            .load(bitmap)
                            .placeholder(backgroundResId ?: BackgroundColor.GRAY.resId)
                            //.apply(requestOptions)
                            .into(this)
                    }
                }
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
            }
        } else if (digitalLibraryTypes == AttachmentTypes.VIMEO && url?.contains("vimeo") == true) {
            GlideApp.with(this.context.applicationContext)
                .load(Vimeo(url.trim()))/*.apply(requestOptions)*/
                .placeholder(backgroundResId ?: BackgroundColor.GRAY.resId)
                .into(this)
        } else {
            GlideApp.with(this.context.applicationContext).load(url)
                .placeholder(
                    backgroundResId ?: BackgroundColor.GRAY.resId
                )/*.apply(requestOptions)*/.into(this)
        }
    }

    @Suppress("ComplexMethod")
    //TOdo complex method
    override fun ImageView.setBadge(attachment: Attachment?) {

        var resId = when (attachment?.contentType) {
            AttachmentTypes.WEBSITE -> R.drawable.ic_website
            AttachmentTypes.DOCUMENT -> R.drawable.ic_doc
            AttachmentTypes.SHOW -> R.drawable.ic_presentation
            AttachmentTypes.VIDEO,
            AttachmentTypes.VIMEO,
            AttachmentTypes.YOUTUBE,
            AttachmentTypes.EXTERNAL_RESOURCES -> null
            AttachmentTypes.GAME -> R.drawable.ic_game
            AttachmentTypes.FLASH -> R.drawable.ic_flash
            AttachmentTypes.PDF -> R.drawable.ic_doc
            AttachmentTypes.IMAGE -> R.drawable.ic_image
            AttachmentTypes.AUDIO -> R.drawable.ic_headset
            AttachmentTypes.PROGRAM -> R.drawable.ic_program
            AttachmentTypes.CODE -> R.drawable.ic_code
            AttachmentTypes.UNKNOWN -> R.drawable.ic_material
            else -> {
                null
            }
        }

        if (attachment?.attachmentType?.equals("bgame", true) == true) {
            resId = R.drawable.ic_game
        }

        if (resId == null) {
            this.visibility = View.GONE
        } else {
            this.visibility = View.VISIBLE
            GlideApp.with(this).load(resId)
                .into(this)
        }
    }

    override fun ImageView.setBadgeVisibility(contentType: AttachmentTypes?) {
        when (contentType) {
            AttachmentTypes.IMAGE,
            AttachmentTypes.AUDIO,
            AttachmentTypes.FLASH,
            AttachmentTypes.GAME,
            AttachmentTypes.WEBSITE,
            AttachmentTypes.VIDEO,
            AttachmentTypes.VIMEO,
            AttachmentTypes.YOUTUBE,
            AttachmentTypes.CODE,
            AttachmentTypes.PROGRAM,
            AttachmentTypes.EXTERNAL_RESOURCES,
            AttachmentTypes.UNKNOWN -> this.visibility = View.GONE
            else -> this.visibility = View.VISIBLE
        }
    }

    override fun View.setBindingEnabled(enabled: Boolean) {
        this.isEnabled = enabled
    }

    override fun DownloadButtonProgress.setIdleIconDrawable(drawable: Drawable) {
        this.idleIcon = drawable
    }

    override fun DownloadButtonProgress.setState(state: Int) {
        when (state) {
            DownloadButtonProgress.STATE_IDLE -> this.setIdle()
            DownloadButtonProgress.STATE_DETERMINATE -> this.setDeterminate()
            DownloadButtonProgress.STATE_INDETERMINATE -> this.setIndeterminate()
            DownloadButtonProgress.STATE_FINISHED -> this.setFinish()
        }
    }

    override fun TextInputLayout.setBindingError(error: String?) {
        setError(error)
    }

    override fun PlayerView.setVideoUrl(url: String?) {
        setShowBuffering(PlayerView.SHOW_BUFFERING_ALWAYS)

        val trackSelector = DefaultTrackSelector(context)
        val loadControl = DefaultLoadControl()
        val renderersFactory = DefaultRenderersFactory(context)

        val exoPlayer = ExoPlayerFactory.newSimpleInstance(
            context,
            renderersFactory,
            trackSelector,
            loadControl
        )

        val userAgent = Util.getUserAgent(context, context.getString(R.string.app_name))
        val mediaSource = ExtractorMediaSource.Factory(DefaultDataSourceFactory(context, userAgent))
            .setExtractorsFactory(DefaultExtractorsFactory())
            .createMediaSource(Uri.parse(url))

        exoPlayer.prepare(mediaSource)

        val audioAttributes: AudioAttributes = AudioAttributes.Builder()
            .setUsage(C.USAGE_MEDIA)
            .setContentType(C.CONTENT_TYPE_MOVIE)
            .build()

        exoPlayer.audioAttributes = audioAttributes
        player = exoPlayer
    }

    override fun TextView.setFontFamily(fontFamily: Int) {
        typeface = ResourcesCompat.getFont(context, fontFamily)
    }

    override fun TextView.setHtmlText(value: String?) {
        text = HtmlCompat.fromHtml(value.toString(), HtmlCompat.FROM_HTML_MODE_COMPACT)
    }

    override fun TextView.setNameBasedOnLanguage(user: User?) {
        text = if (context.resources.configuration.locale.language == "en") {

            if ((user?.firstName?.length ?: 0) > 15) {
                user?.firstName?.trim()?.substring(0, 13)
            } else {
                user?.firstName
            }

        } else {

            if (user?.nameAr?.isEmpty() == true || user?.nameAr?.isNullOrEmpty() == true
                || user?.nameAr?.isNullOrBlank() == true
            ) {
                if ((user?.firstName?.length ?: 0) > 15) {
                    user?.firstName?.trim()?.substring(0, 13)
                } else {
                    user?.firstName
                }
            } else {
                if ((user?.nameAr?.length ?: 0) > 15) {
                    user?.nameAr?.trim()?.substring(0, 13)
                } else {
                    user?.nameAr
                }
            }

        }
    }

    override fun View.setVisibility(value: Boolean) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

    override fun TextView.setConcatenateText(textLeft: String?, textRight: String?) {
        text = ("$textLeft $textRight")
    }

    override fun TextView.setTextWithCapitalFirstCharacter(value: String) {
        text = value.toCharArray()[0].toUpperCase() + value.substring(1)
    }

    override fun View.flipXonArabicLanguage(value: Boolean) {
        if (value)
            scaleX = if (Locale.getDefault().language == "ar") -1f else scaleX
    }

    override fun Guideline.setLayoutConstraintGuidePercent(percent: Float) {
        val params = layoutParams as ConstraintLayout.LayoutParams
        params.guidePercent = percent
        layoutParams = params
    }

    private companion object {

        private const val PROGRESS_ANIMATION_DURATION = 2000L
        private const val ATTACHMENT_MEDIA_RADIUS = 4
        private const val THUMBNAIL = 0.1f
    }
}
