package com.classera.data.network.errorhandling

import com.classera.data.R
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.google.gson.Gson
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.JsonEncodingException
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

/**
 * Project: MoveFast
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
sealed class Resource {

    data class Success<out T>(val data: T?) : Resource()

    data class Error(val error: NetworkException) : Resource()

    data class Loading(val show: Boolean) : Resource()

    @Suppress("UNCHECKED_CAST")
    fun <T> element(): T? {
        return if (this is Success<*>) {
            data as? T?
        } else {
            null
        }
    }

    fun isSuccess(): Boolean {
        return this is Success<*>
    }
}

private const val HTTP_CODE_CLIENT_START = 400
private const val HTTP_CODE_CLIENT_END = 499

private const val HTTP_CODE_SERVER_START = 500
private const val HTTP_CODE_SERVER_END = 599
private const val HTTP_CODE_SERVER_USER_BLOCKED = 303
private const val HTTP_CODE_SERVER_FORCE_UPDATE = 426

inline fun <T> tryResource(func: () -> BaseWrapper<T>): Resource {
    return try {
        val result = func()
        if (result.success == true || result.message == "No Results"
            || result.message == "Not found" || result.message == "غير موجود"
        ) {
            Resource.Success(data = result)
        } else {
            Resource.Error(error = NetworkException.Business(result.message, result.code))
        }
    } catch (e: Exception) {
        Resource.Error(error = getRequestException(e))
    }
}

inline fun tryNoContentResource(func: () -> NoContentBaseWrapper): Resource {
    return try {
        val result = func()
        if (result.success == true || result.message == "No Results") {
            Resource.Success(data = result)
        } else {
            Resource.Error(error = NetworkException.Business(result.message, result.code))
        }
    } catch (e: Exception) {
        Resource.Error(error = getRequestException(e))
    }
}

inline fun <T> tryThirdPartyResource(func: () -> T): Resource {
    return try {
        val result = func()
        Resource.Success(data = result)
    } catch (e: Exception) {
        Resource.Error(error = getRequestException(e))
    }
}

fun getRequestException(cause: Throwable): NetworkException {
    return when (cause) {
        is HttpException -> {
            handleHttpException(cause)
        }
        is SocketTimeoutException -> {
            NetworkException.Timeout(
                R.string.err_network_timeout_title,
                R.string.err_network_timeout_message,
                null,
                cause
            )
        }
        is JsonDataException, is JsonEncodingException -> {
            NetworkException.Update(
                R.string.err_update_title,
                R.string.err_update_message,
                null,
                cause
            )
        }
        is IOException -> {
            NetworkException.Internet(
                R.string.err_network_internet_title,
                R.string.err_network_internet_message,
                null,
                cause
            )
        }
        else -> {
            NetworkException.Unexpected(
                R.string.err_network_unexpected_title,
                R.string.err_network_unexpected_message,
                null,
                cause
            )
        }
    }
}

private fun handleHttpException(cause: HttpException): NetworkException {
    return when (cause.code()) {
        HTTP_CODE_SERVER_FORCE_UPDATE -> {
            val errorBody = Gson().fromJson(
                cause.response()?.errorBody()?.string(), BaseWrapper::class.java)
            NetworkException.ForceUpdate(
                R.string.err_update_title,
                R.string.err_update_message,
                errorBody.message,
                cause
            )
        }
        in HTTP_CODE_CLIENT_START..HTTP_CODE_CLIENT_END -> {
            NetworkException.Client(
                R.string.err_network_client_title,
                R.string.err_network_client_message,
                null,
                cause
            )
        }
        in HTTP_CODE_SERVER_START..HTTP_CODE_SERVER_END -> {
            NetworkException.Server(
                R.string.err_network_server_title,
                R.string.err_network_server_message,
                null,
                cause
            )
        }
        HTTP_CODE_SERVER_USER_BLOCKED -> {
            val errorBody = Gson().fromJson(
                cause.response()?.errorBody()?.string(), BaseWrapper::class.java)
            NetworkException.Blocked(
                R.string.err_network_server_user_blocked_title,
                R.string.err_network_server_user_blocked_message,
                errorBody.message,
                cause
            )
        }
        else -> {
            NetworkException.Unexpected(
                R.string.err_network_unexpected_title,
                R.string.err_network_unexpected_message,
                null,
                cause
            )
        }
    }
}
