package com.classera.data.network

import java.util.concurrent.TimeUnit

/**
 * Project: Classera
 * Created: August 18, 2020
 *
 * @author Khaled Mohammad
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class CustomTimeout(val timeout: Int, val unit: TimeUnit)
