package com.classera.data.network.intercepters

import com.classera.data.network.IgnoreAuthToken
import com.classera.data.prefs.Prefs
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Invocation
import javax.inject.Inject

/**
 * Created by Odai Nazzal on 12/17/2019.
 * Classera
 * o.nazzal@classera.com
 */
class AuthenticationTokenInterceptor @Inject constructor(private val prefs: Prefs) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        val invocation = request.tag(Invocation::class.java)
        val method = invocation?.method()

        if (method?.getAnnotation(IgnoreAuthToken::class.java) == null) {
            prefs.authenticationToken?.let { accessToken ->
                request = request.newBuilder()
                    .addHeader(AUTHENTICATION_TOKEN_HEADER_NAME, accessToken)
                    .build()
            }
        }
        return chain.proceed(request)
    }

    private companion object {

        private const val AUTHENTICATION_TOKEN_HEADER_NAME = "Authtoken"
    }
}
