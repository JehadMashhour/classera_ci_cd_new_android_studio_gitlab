package com.classera.data.network.intercepters

import com.classera.data.prefs.Prefs
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

/**
 * Created by Odai Nazzal on 12/17/2019.
 * Classera
 * o.nazzal@classera.com
 */
class SemesterTokenInterceptor @Inject constructor(private val prefs: Prefs) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        prefs.semesterToken?.let { semesterToken ->
            request = request.newBuilder()
                .addHeader(SEMESTER_AUTHENTICATION_TOKEN_HEADER_NAME, semesterToken)
                .build()
        }
        return chain.proceed(request)
    }

    private companion object {

        private const val SEMESTER_AUTHENTICATION_TOKEN_HEADER_NAME = "Semester-Token"
    }
}
