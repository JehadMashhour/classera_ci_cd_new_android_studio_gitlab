package com.classera.data.network

/**
 * Created by Saeed Halawani on 30/08/2020.
 * Classera
 * s.halawani@classer.com
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class IgnoreAppType
