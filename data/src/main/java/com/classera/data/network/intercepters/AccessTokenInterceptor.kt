package com.classera.data.network.intercepters

import com.classera.data.BuildConfig
import com.classera.data.network.IgnoreAccessToken
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Invocation
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class AccessTokenInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        val invocation = request.tag(Invocation::class.java)
        val method = invocation?.method()

        if (method?.getAnnotation(IgnoreAccessToken::class.java) == null) {
            var url = request.url().toString()
            url += if (url.contains("?")) "&" else "?"
            url += "access_token=${BuildConfig.flavorData.accessToken}"

            request = request.newBuilder().url(url).build()
        }
        return chain.proceed(request)
    }
}
