package com.classera.data.models.profile

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Language(

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "language")
    val language: String? = null,

    @Json(name = "modified")
    val modified: String? = null,

    @Json(name = "proficiency")
    val proficiency: String? = null,

    @Json(name = "user_id")
    val userId: String? = null
) : Parcelable
