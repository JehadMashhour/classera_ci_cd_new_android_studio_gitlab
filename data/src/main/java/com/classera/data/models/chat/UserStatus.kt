package com.classera.data.models.chat

import com.classera.data.R
import com.squareup.moshi.Json

/**
 * Created by Odai Nazzal on 1/11/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
enum class UserStatus(val label: Int) {

    @Json(name = "online")
    ONLINE(R.string.label_online),

    @Json(name = "offline")
    OFFLINE(R.string.label_offline)
}
