package com.classera.data.models.courses

import com.classera.data.models.assignments.Assignment
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: Jan 09, 2020
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
data class CourseAssignmentsWrapper(

    @Json(name = "Exam")
    val exams: List<Assignment>? = null,

    @Json(name = "Homework")
    val homeworks: List<Assignment>? = null
)
