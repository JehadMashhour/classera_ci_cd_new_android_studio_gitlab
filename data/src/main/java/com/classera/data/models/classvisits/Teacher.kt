package com.classera.data.models.classvisits


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Rawan Al-Theeb on 2/13/2020.
 * Classera
 * r.altheeb@classera.com
 */
@JsonClass(generateAdapter = true)
data class Teacher(
    @Json(name = "id")
    val id: String? = null,

    @Json(name = "image")
    val image: String? = null,

    @Json(name = "name")
    val name: String? = null
)
