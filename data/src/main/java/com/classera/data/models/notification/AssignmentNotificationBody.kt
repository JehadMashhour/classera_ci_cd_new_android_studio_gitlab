package com.classera.data.models.notification

import com.google.gson.annotations.SerializedName

data class AssignmentNotificationBody(

    @SerializedName("courseId")
    var courseId: String? = null,

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("assignmentId")
    var assignmentId: String? = null
)

