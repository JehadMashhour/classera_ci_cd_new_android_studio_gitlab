package com.classera.data.models.courses

import com.classera.data.models.discussions.DiscussionRoom
import com.classera.data.models.vcr.VcrResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: Jan 09, 2020
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
data class CourseDetailsWrapper(

    @Json(name = "Attachments")
    val attachments: CourseAttachmentsWrapper? = null,

    @Json(name = "Assignments")
    val assignments: CourseAssignmentsWrapper? = null,

    @Json(name = "Discussion rooms")
    val discussionRooms: List<DiscussionRoom>? = null,

    @Json(name = "Vcrs")
    val vcrs: MutableList<VcrResponse>? = null
)
