package com.classera.data.models.attendance

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: Feb 15, 2020
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
data class AbsenceSection(

    @Json(name = "lecture_id")
    val lectureId: String? = null,

    @Json(name = "section_title")
    val sectionTitle: String? = null,

    @Json(name = "id")
    val id: String? = null
)
