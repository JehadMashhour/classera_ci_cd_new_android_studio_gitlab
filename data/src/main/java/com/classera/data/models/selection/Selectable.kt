package com.classera.data.models.selection

import android.os.Parcelable

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
interface Selectable : Parcelable {

    val id: String?

    val title: String?

    var selected: Boolean
}
