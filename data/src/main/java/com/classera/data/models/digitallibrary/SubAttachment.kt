package com.classera.data.models.digitallibrary

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class SubAttachment(

    @Json(name = "original_filename")
    val originalFilename: String? = null,

    @Json(name = "subtitle")
    val subtitle: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "cover_img_url")
    val coverImgUrl: String? = null,

    @Json(name = "url")
    val url: String? = null,


    var isRun: Boolean = false

) : Parcelable {

    fun getRightCoverImageUrl(): String? =  coverImgUrl ?: url

}
