package com.classera.data.models.switchroles


import com.classera.data.models.user.UserRole
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Role(

    @Json(name = "id")
    val id: UserRole? = null,

    @Json(name = "title")
    val title: String? = null
)
