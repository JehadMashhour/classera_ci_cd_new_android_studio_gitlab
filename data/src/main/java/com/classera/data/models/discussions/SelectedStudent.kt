package com.classera.data.models.discussions


import android.os.Parcelable
import com.classera.data.models.selection.Selectable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class SelectedStudent(

    @Json(name = "count")
    val count: Int? = null,

    @Json(name = "image")
    val image: String? = null,

    @Json(name = "lectures")
    val lectures: List<SelectedLecture?>? = null,

    @Json(name = "student_name")
    val studentName: String? = null,

    @Json(name = "user_id")
    val userId: String? = null,

    @Transient
    override var selected: Boolean = false,

    @Transient
    override var id: String? = null,

    @Transient
    override var title: String? = null
) : Selectable, Parcelable
