package com.classera.data.models.mailbox

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created by Rawan Al-Theeb on 4/15/2020.
 * Classera
 * r.altheeb@classera.com
 */


@Entity(tableName = "recipient_user")
@Parcelize
@JsonClass(generateAdapter = true)
data class RecipientByRole(

    @PrimaryKey
    @Json(name = "id")
    val id: String,

    @Json(name = "school_id")
    val schoolId: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    var checked: Boolean = false
) : Parcelable
