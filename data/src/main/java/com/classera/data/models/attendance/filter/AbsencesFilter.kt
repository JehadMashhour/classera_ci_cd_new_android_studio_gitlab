package com.classera.data.models.attendance.filter

import com.classera.data.models.filter.Filterable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Odai Nazzal on 1/27/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */

@Parcelize
data class AbsencesFilter(
    override val filterId: String? = null,

    override val title: String? = null,

    override var selected: Boolean = false
) : Filterable
