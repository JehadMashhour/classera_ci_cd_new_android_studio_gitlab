package com.classera.data.models.assignments

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.classera.data.BR
import com.classera.data.R
import com.classera.data.models.selection.Selectable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * Project: Classera
 * Created: Mar 11, 2020
 *
 * @author Mohamed Hamdan
 */
@Parcelize
data class SelectableChoice(
    override val id: String?,

    override val title: String?
) : BaseObservable(), Selectable {

    @get:Bindable
    @IgnoredOnParcel
    override var selected: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.selected)
        }

    @get:Bindable
    @IgnoredOnParcel
    var enabled: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.enabled)
        }

    @IgnoredOnParcel
    var correctAnswer: Boolean = false

    @IgnoredOnParcel
    var result: String? = null

    @IgnoredOnParcel
    var correctAnswerText: String? = null

    @IgnoredOnParcel
    var number: String? = null

    fun toggle() {
        selected = !selected
    }

    fun getBackgroundTint(context: Context): Int {
        return if (result == null) {
            if (selected) {
                ContextCompat.getColor(context, R.color.colorPrimary)
            } else {
                ContextCompat.getColor(context, R.color.colorLight)
            }
        } else if (correctAnswer) {
            ContextCompat.getColor(context, android.R.color.holo_green_dark)
        } else {
            ContextCompat.getColor(context, android.R.color.holo_red_dark)
        }
    }

    fun getTextColor(context: Context): Int {
        return if (result == null) {
            if (selected) {
                ContextCompat.getColor(context, android.R.color.white)
            } else {
                ContextCompat.getColor(context, R.color.colorPrimary)
            }
        } else if (correctAnswer) {
            ContextCompat.getColor(context, android.R.color.white)
        } else {
            ContextCompat.getColor(context, android.R.color.white)
        }
    }
}
