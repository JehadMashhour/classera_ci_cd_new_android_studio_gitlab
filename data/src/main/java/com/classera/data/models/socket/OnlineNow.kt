package com.classera.data.models.socket

import android.os.Parcelable
import com.classera.data.moshi.toStringNumber.ToStringNumber
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

/**
 * Project: classeraandroidv3
 * Created: Mar 14, 2020
 *
 * @author Odai Nazzal
 */
@Parcelize
data class OnlineNow(

    @Json(name= "2")
    val administrators: Int = 0,

    @Json(name= "3")
    val parents: Int = 0,

    @Json(name= "4")
    val teachers: Int = 0,

    @Json(name= "6")
    val students: Int = 0,

    @Json(name= "8")
    val managers: Int = 0,

    @Json(name= "9")
    val advisers: Int = 0,

    @Json(name= "11")
    val supervisors: Int = 0,

    @Json(name= "13")
    val kaganCoaches: Int = 0
) : Parcelable {

    @ToStringNumber
    fun totalCount(): Int{
        return (kaganCoaches
                +supervisors
                +advisers
                +managers
                +students
                +teachers
                +parents
                +administrators)
    }

}
