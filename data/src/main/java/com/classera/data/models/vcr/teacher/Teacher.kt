package com.classera.data.models.vcr.teacher

import android.os.Parcelable
import com.classera.data.models.selection.Selectable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Teacher(

	@Json(name = "user_id")
	override val id: String?,

	@Json(name = "name")
	override val title: String?,

	override var selected: Boolean = false

) : Selectable
