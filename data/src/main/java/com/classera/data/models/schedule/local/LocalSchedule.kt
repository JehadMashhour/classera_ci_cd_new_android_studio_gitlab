package com.classera.data.models.schedule.local

import com.classera.data.models.schedule.ScheduleSlot

data class LocalSchedule(val name: String?, val schedule: List<ScheduleSlot?>?)
