package com.classera.data.models.classvisits


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Rawan Al-Theeb on 2/13/2020.
 * Classera
 * r.altheeb@classera.com
 */
@JsonClass(generateAdapter = true)
data class Timeslot(
    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null
)
