package com.classera.data.models.vcr.vendor

import com.classera.data.models.selection.Selectable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Duration(
    override val id: String?,

    override val title: String?,

    override var selected: Boolean = false

) : Selectable
