package com.classera.data.models.eportfolio.details

import com.classera.data.moshi.timeago.TimeAgo
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Odai Nazzal on 12/24/2019.
 * Classera
 * o.nazzal@classera.com
 */
@JsonClass(generateAdapter = true)
data class EPortfolioComment(

    @Transient
    val localId: String? = null,

    @Json(name = "post_content")
    val text: String? = null,

    @Json(name = "post_title")
    val title: String? = null,

    @Json(name = "creator_id")
    val creatorId: String? = null,

    @TimeAgo
    @Json(name = "created")
    val created: String? = null,

    @Json(name = "approved")
    val approved: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    @Json(name = "post_user_photo")
    val imageUrl: String? = null,

    @Transient
    val isFailed: Boolean = false,

    @Transient
    val isLoading: Boolean = false
)
