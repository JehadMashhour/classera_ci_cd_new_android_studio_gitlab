package com.classera.data.models.attachment


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SharingStatus(

    @Json(name = "default")
    val default: Int? = null,

    @Json(name = "options")
    val options: List<String?>? = null

)
