package com.classera.data.models.assignments

import android.os.Parcelable
import com.classera.data.models.selection.Selectable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * Project: Classera
 * Created: May 18, 2020
 *
 * @author Kahled Mohammad
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class Student(

    @Json(name = "lecture_id")
    val lectureId: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    @Json(name = "user_id")
    val userId: String? = null,

    @Json(name = "image_url")
    val imageUrl: String? = null,

    override val id: String? = userId,

    override var selected: Boolean = false

) : Parcelable, Selectable {

    override val title: String?
        get() = fullName
}
