package com.classera.data.models.profile

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class City (

    @Json(name = "ID")
    val id: String? = null,

    @Json(name = "Name")
    val nameEng: String? = null,

    @Json(name = "Country_Id")
    val countryId: String? = null,

    @Json(name = "CountryCode")
    val countryCode: String? = null,

    @Json(name = "District")
    val district: String? = null,

    @Json(name = "mawhiba_id")
    val mawhibaId: String? = null

)
