package com.classera.data.models.behaviours.teacher

import com.classera.data.models.behaviours.Behaviour
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BehaviorActionsWraper(

    @Json(name = "Behaviors")
    val behaviors: List<Behaviour>? = null,

    @Json(name = "Actions")
    val actions: List<BehaviorAction>? = null

)
