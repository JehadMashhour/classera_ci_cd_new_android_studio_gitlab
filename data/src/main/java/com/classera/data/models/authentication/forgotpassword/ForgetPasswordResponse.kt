package com.classera.data.models.authentication.forgotpassword

import com.squareup.moshi.JsonClass

/**
 * Created by Odai Nazzal on 12/11/2019.
 * Classera
 * o.nazzal@classera.com
 */
@JsonClass(generateAdapter = true)
class ForgetPasswordResponse
