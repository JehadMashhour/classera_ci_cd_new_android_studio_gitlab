package com.classera.data.models.assignments

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
data class QuestionsWrapper(

    @Json(name = "assignment_settings")
    var assignmentSettings: AssignmentSettings? = null,

    @Json(name = "assignment_questions")
    val assignmentQuestions: List<Question>? = null
)
