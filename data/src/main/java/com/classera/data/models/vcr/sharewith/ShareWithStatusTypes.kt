package com.classera.data.models.vcr.sharewith

import android.content.Context
import android.content.res.Resources

import com.classera.data.R
import com.classera.data.moshi.enums.Json

enum class ShareWithStatusTypes(val id: Int, val title: Int?) {
    @Json(name = "0")
    NONE(
        0,
        R.string.none
    ),

    @Json(name = "1")
    CURRENT_SCHOOL(
        1,
        R.string.current_school
    ),

    @Json(name = "2")
    WHOLE_COMMUNITY(
        2,
        R.string.whole_community
    ),

    UNKNOWN(-1, null);

    companion object {
        fun getList(context: Context?): List<ShareWithStatus> = values()
                .filter { it != UNKNOWN }
                .mapIndexed { index, value ->
                    ShareWithStatus(value, index.toString(), value.title?.let { context?.getString(it) })
                }
    }
}
