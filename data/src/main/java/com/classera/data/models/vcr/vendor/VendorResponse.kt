package com.classera.data.models.vcr.vendor

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VendorResponse(

	@Json(name="durations")
	val durations: List<Int>? = null,

	@Json(name="vendors")
	val vendors: List<Vendor>? = null

)
