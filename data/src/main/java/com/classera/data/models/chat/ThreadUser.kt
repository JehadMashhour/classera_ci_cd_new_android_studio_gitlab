package com.classera.data.models.chat

import android.os.Parcelable
import com.classera.data.models.selection.Selectable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created on 25/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class ThreadUser(

    @Json(name = "status")
    val status: UserStatus? = null,

    @Json(name = "user_id")
    val userId: String? = null,

    @Json(name = "thread_id")
    val threadId: String? = null,

    @Json(name = "unread")
    val unread: String? = null,

    @Json(name = "last_message")
    var lastMessage: Message? = null,

    @Json(name = "photo_filename")
    val photoFilename: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    @Json(name = "username")
    val username: String? = null,

    @Json(name = "group")
    val group: Boolean? = null,

    @Json(name = "id")
    override val id: String? = null,

    @Json(name = "title")
    override val title: String? = null,

    @Transient
    override var selected: Boolean = false
) : Parcelable, Selectable
