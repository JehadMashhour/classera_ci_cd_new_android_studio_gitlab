package com.classera.data.models.vcr


import android.os.Parcelable
import com.classera.data.models.selection.Selectable
import com.classera.data.models.vcr.lecturetimeslotpreparation.Preparation
import com.classera.data.models.vcr.lecturetimeslotpreparation.Timeslot
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Lecture(

    @Json(name = "id")
    override val id: String? = null,

    @Json(name = "title")
    override val title: String? = null,

    @Json(name = "timslote")
    var timeslot: Timeslot? = null,

    @Json(name = "preparation")
    var preparation: Preparation? = null,

    @Transient
    override var selected: Boolean = false,

    @Transient
    var expanded: Boolean = false

) : Parcelable, Selectable
