package com.classera.data.models.profile

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CityResponse (

    @Json(name = "Cities")
    val cities: List<City>? = null
)
