package com.classera.data.models.chat

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created on 7/03/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class Block(

    @Json(name = "is_block_me")
    val isBlockMe: Boolean? = null

) : Parcelable
