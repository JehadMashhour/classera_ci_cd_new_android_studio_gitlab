package com.classera.data.models.assignments.listquestions


import android.content.Context
import androidx.core.content.ContextCompat
import com.classera.data.R
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Question(
    @Json(name = "assignment_id")
    val assignmentId: String? = null,

    @Json(name = "attachment_id")
    val attachmentId: Any? = null,

    @Json(name = "choices")
    val choices: List<String?>? = null,

    @Json(name = "correct_answer")
    val correctAnswer: String? = null,

    @Json(name = "correct_answer_response")
    val correctAnswerResponse: String? = null,

    @Json(name = "difficulty")
    val difficulty: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "mark")
    val mark: String? = null,

    @Json(name = "order")
    val order: String? = null,

    @Json(name = "parent_id")
    val parentId: Any? = null,

    @Json(name = "question_hint")
    val questionHint: String? = null,

    @Json(name = "randomize")
    val randomize: String? = null,

    @Json(name = "text")
    val text: String? = null,

    @Json(name = "type")
    val type: String? = null,

    @Json(name = "wrong_answer_response")
    val wrongAnswerResponse: String? = null
) {

    fun getQuestionDifficulty(): Int {

        return when (difficulty) {
            "0" -> R.string.label_fragment_questions_list_question_difficulty_easy
            "1" -> R.string.label_fragment_questions_list_question_difficulty_medium
            "2" -> R.string.label_fragment_questions_list_question_difficulty_hard
            else -> R.string.label_fragment_questions_list_question_difficulty_easy
        }

    }

    fun getQuestionColor(context: Context): Int {
        return when (difficulty) {
            "0" -> {
                ContextCompat.getColor(context, R.color.colorLightGreen)
            }
            "1" -> {
                ContextCompat.getColor(context, R.color.colorLightBlue)
            }
            "2" -> {
                ContextCompat.getColor(context, R.color.colorLightRed)
            }
            else -> {
                ContextCompat.getColor(context, R.color.colorLightGreen)
            }
        }
    }


    fun getQuestionType(): Int {
        return when (type) {
            "tfq" -> R.string.label_fragment_questions_list_question_type_true_false
            "mcq" -> R.string.label_fragment_questions_list_question_type_multiple_choice
            "short" -> R.string.label_fragment_questions_list_question_type_essay
            "matching" -> R.string.label_fragment_questions_list_question_type_matching
            "hotspot" -> R.string.label_fragment_questions_list_question_type_hot_spot
            "fillblank" -> R.string.label_fragment_questions_list_question_type_fill_blank
            "msa" -> R.string.label_fragment_questions_list_question_type_multiple_select
            else -> R.string.label_fragment_questions_list_question_type_true_false
        }

    }

}
