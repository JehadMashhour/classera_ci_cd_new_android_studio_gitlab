package com.classera.data.models

import androidx.annotation.DrawableRes
import com.classera.data.R

enum class BackgroundColor(@DrawableRes val resId: Int) {
    BLUE(R.drawable.bg_blue_gradient),
    BLUE_WHITE(R.drawable.bg_blue_white_gradient),
    PURPLE(R.drawable.bg_purple_gradient),
    BROWN(R.drawable.bg_brown_gradient),
    GRAY(R.drawable.bg_gray_gradient)
}
