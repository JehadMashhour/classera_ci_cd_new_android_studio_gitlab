package com.classera.data.models.home

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StudentHomeResponse(

    @Key("assessments", order = 8)
    @Json(name = "unseen_assessments_count")
    val assessmentsPercentage: Int? = 0,

    @Key("assessments", order = 8)
    @Json(name = "assessments_percentage")
    val countUnseenAssessments: Int? = 0,

    @Key("exam", order = 1)
    @Json(name = "exam_percentage")
    val examPercentage: Int? = 0,

    @Key("exam", order = 1)
    @Json(name = "count_new_exam")
    val countNewExam: Int? = 0,

    @Key("homework", order = 2)
    @Json(name = "homework_percentage")
    val homeworkPercentage: Int? = 0,

    @Key("homework", order = 2)
    @Json(name = "count_new_homework")
    val countNewHomework: Int? = 0,

    @Key("video", order = 3)
    @Json(name = "video_percentage")
    val videoPercentage: Int? = 0,

    @Key("video", order = 3)
    @Json(name = "count_new_video")
    val countNewVideo: Int? = 0,

    @Key("discussion", order = 5)
    @Json(name = "discussion_percentage")
    val discussionPercentage: Int? = 0,

    @Key("discussion", order = 5)
    @Json(name = "count_new_disc")
    val countNewDisc: Int? = 0,

    @Key("vcr", order = 4)
    @Json(name = "vcr_percentage")
    val vcrPercentage: Int? = 0,

    @Key("vcr", order = 4)
    @Json(name = "count_new_vcrs")
    val countNewVcrs: Int? = 0,

    @Key("report", order = 6)
    @Json(name = "report_cards_percentage")
    val reportCardsPercentage: Int? = 0,

    @Key("report", order = 6)
    @Json(name = "count_new_report_cards")
    val countNewReportCards: Int? = 0,

    @Key("events", order = 7)
    @Json(name = "events_percentage")
    val eventsPercentage: Int? = 0,

    @Key("events", order = 7)
    @Json(name = "unviewed_events_count")
    val countUnViewedEvents: Int? = 0
)
