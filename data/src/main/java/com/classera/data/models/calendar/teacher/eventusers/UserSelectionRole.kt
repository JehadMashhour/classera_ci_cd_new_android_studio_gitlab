package com.classera.data.models.calendar.teacher.eventusers


import com.classera.data.models.user.User
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserSelectionRole(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "user")
    val user: List<User?>? = null
)
