package com.classera.data.models.settings


import com.squareup.moshi.Json

data class Login(
    @Json(name = "School")
    val school: School? = School(),
    @Json(name = "User")
    val user: User? = User()
)
