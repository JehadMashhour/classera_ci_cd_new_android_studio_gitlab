package com.classera.data.models.homelocation.w3w

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Rawan Al-Theeb on 1/8/2020.
 * Classera
 * r.altheeb@classera.com
 */
@JsonClass(generateAdapter = true)
data class W3WResponse(

    @Json(name = "coordinates")
    val coordinates: Coordinates? = null,

    @Json(name = "words")
    val words: String? = null
)
