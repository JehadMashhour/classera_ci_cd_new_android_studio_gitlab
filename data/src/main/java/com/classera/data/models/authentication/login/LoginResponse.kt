package com.classera.data.models.authentication.login

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
data class LoginResponse(

    @Json(name = "url")
    val url: String = "",

    @Json(name = "auth_token")
    val authToken: String? = null,

    @Json(name = "login")
    var userData: UserData? = null
)
