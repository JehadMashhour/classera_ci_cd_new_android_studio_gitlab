package com.classera.data.models.home.admin

import com.classera.data.moshi.toStringNumber.ToStringNumber
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: classera
 * Created: Mar 06, 2020
 *
 * @author Odai Nazzal
 */

@JsonClass(generateAdapter = true)
data class Statistic(

    @Json(name = "title")
    val title: String? = null,

    @ToStringNumber
    @Json(name = "all")
    val all: String? = null,

    @ToStringNumber
    @Json(name = "semester")
    val semester: String? = null,

    @Json(name = "key")
    val key: StatisticKey? = null

)
