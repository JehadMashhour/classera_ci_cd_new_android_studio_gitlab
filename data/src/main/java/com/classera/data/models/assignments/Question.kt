package com.classera.data.models.assignments

import android.content.Context
import android.os.Parcelable
import androidx.core.text.HtmlCompat
import com.classera.data.R
import com.classera.data.moshi.subquestions.SubQuestionsConverter
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Project: Classera
 * Created: Feb 16, 2020
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
@Parcelize
data class Question(

    @Expose
    @SerializedName("id")
    @Json(name = "id")
    val id: String? = null,

    @Expose
    @SerializedName("type")
    @Json(name = "type")
    val type: QuestionType = QuestionType.UNKNOWN,

    @Expose
    @SerializedName("text")
    @Json(name = "text")
    val text: String? = null,

    @Expose
    @SerializedName("correct_answer")
    @Json(name = "correct_answer")
    val correctAnswer: String? = null,

    @Expose
    @SerializedName("attachment_id")
    @Json(name = "attachment_id")
    val attachmentId: String? = null,

    @Expose
    @SerializedName("mark")
    @Json(name = "mark")
    val mark: String? = null,

    @Expose
    @SerializedName("student_answer")
    @Json(name = "student_answer")
    var studentAnswer: String? = null,

    var draftAnswer: String? = null,

    @Expose
    @SerializedName("choices")
    @Json(name = "choices")
    val choices: List<String?>? = null,

    @Expose
    @SerializedName("result")
    @Json(name = "result")
    val result: String? = null,

    @Expose
    @SerializedName("parent_id")
    @Json(name = "parent_id")
    val parentId: String? = null,

    @Expose
    @SerializedName("question_hint")
    @Json(name = "question_hint")
    val questionHint: String? = null,

    @Expose
    @SerializedName("randomize")
    @Json(name = "randomize")
    val randomize: String? = null,

    @Expose
    @SerializedName("attachment_url")
    @Json(name = "attachment_url")
    val attachmentUrl: String? = null,

    @Expose
    @SerializedName("correct_answer_response")
    @Json(name = "correct_answer_response")
    val correctAnswerResponse: String? = null,

    @Expose
    @SerializedName("wrong_answer_response")
    @Json(name = "wrong_answer_response")
    val wrongAnswerResponse: String? = null,

    @Expose
    @SerializedName("Subquestions")
    @SubQuestionsConverter
    @Json(name = "Subquestions")
    val subQuestions: SubQuestions? = null,

    @Expose
    @SerializedName("position")
    @Json(name = "position")
    val position: List<String>? = null
) : Parcelable {

    fun getSelectableChoices(): List<SelectableChoice>? {
        return choices?.filterNotNull()
            ?.filter { it.isNotEmpty() }
            ?.mapIndexed { index, s ->
                val correctAnswers = correctAnswer?.split(",") ?: listOf()
                val studentAnswers = studentAnswer?.split(",") ?: listOf()
                val currentAnswer = (index + 1).toString()
                SelectableChoice(s, s).apply {
                    selected = studentAnswers.contains(currentAnswer)
                    enabled = studentAnswer == null
                    correctAnswer = (studentAnswers.contains(currentAnswer) && correctAnswers.contains(currentAnswer))
                            || correctAnswers.contains(currentAnswer)
                    correctAnswerText = this@Question.correctAnswer
                    result = this@Question.result
                    number = (index + 1).toString()
                }
            }
    }

    fun getCorrectAnswer(context: Context): String? {
        return if (result == "wrong") {
            context.getString(
                R.string.correct_answer,
                HtmlCompat.fromHtml(correctAnswer ?: "", HtmlCompat.FROM_HTML_MODE_COMPACT)
            )
        } else {
            null
        }
    }

    fun canShowAnswerState(): Boolean {
        return studentAnswer != null
    }

    fun isStudentAnswerCorrect(): Boolean {
        return result == "correct"
    }
}
