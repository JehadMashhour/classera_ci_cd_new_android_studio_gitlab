package com.classera.data.models.assignments

import android.os.Handler
import android.os.Looper
import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
@Parcelize
@Suppress("MagicNumber")
@JsonClass(generateAdapter = true)
data class AssignmentSettings(

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "comments")
    val comments: String? = null,

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "publishing_datetime")
    val publishingDatetime: String? = null,

    @Json(name = "due_datetime")
    val dueDatetime: String? = null,

    @Json(name = "maxMark")
    val maxMark: Int? = null,

    @Json(name = "show_hints")
    val showHints: Boolean? = null,

    @Json(name = "show_responses")
    val showResponses: Boolean? = null,

    @Json(name = "submit_attachments")
    val submitAttachments: Boolean? = null,

    @Json(name = "started_exam")
    val startedExam: Boolean? = null,

    @Json(name = "starting_time")
    val startingTime: String? = null,

    @Json(name = "time_left")
    var timeLeft: Long? = null,

    @Json(name = "question_time")
    val questionTime: Int? = null,

    @Json(name = "question_time_left")
    var questionTimeLeft: Long? = null,

    @Json(name = "last_question")
    val lastQuestion: Int? = null,

    @Json(name = "submission_id")
    val submissionId: String? = null,

    @Json(name = "questions_number")
    val questionsNumber: Int? = null,

    @Json(name = "tts_time_running_out")
    val ttsTimeRunningOut: String? = null
) : Parcelable {

    fun canNavigateBetweenQuestions(): Boolean {
        return showResponses == false && (questionTime == null || questionTime == 0)
    }

    @IgnoredOnParcel
    @Transient
    private val handler = Handler(Looper.getMainLooper())

    @IgnoredOnParcel
    @Transient
    private val runnable = Runnable {
        _timeLeftTimeLiveData.value = getRemainingTimeString()
        handleTimeLeft()
    }

    @IgnoredOnParcel
    @Transient
    private val _timeLeftTimeLiveData: MutableLiveData<String> = MutableLiveData(getRemainingTimeString())

    val timeLeftTimeLiveData: LiveData<String> get() = _timeLeftTimeLiveData.apply { handleTimeLeft() }

    private fun getRemainingTimeString(): String {

        val time = (timeLeft ?: 0)* 1000
        val seconds = (time / 1000) % 60
        val minutes = ((time / (1000 * 60)) % 60)
        val hours = ((time / (1000 * 60 * 60)) % 24)

        timeLeft = time - 1000
        timeLeft = (timeLeft ?: 0)/1000

        return "%02d.%02d.%02d".format(hours, minutes, seconds)
    }

    private fun handleTimeLeft() {
        handler.removeCallbacks(runnable)
        handler.postDelayed(runnable, 1000)
    }


    @IgnoredOnParcel
    @Transient
    private val runnableQuestion = Runnable {
        _questionTimeLeftTimeLiveData.value = getRemainingQuestionTimeString()
        handleQuestionTimeLeft()
    }

    @IgnoredOnParcel
    @Transient
    private val _questionTimeLeftTimeLiveData: MutableLiveData<String> =
        MutableLiveData(getRemainingQuestionTimeString())

    val questionTimeLeftTimeLiveData: LiveData<String> get() =
        _questionTimeLeftTimeLiveData.apply { handleQuestionTimeLeft() }

    private fun getRemainingQuestionTimeString(): String {

        val time = (questionTimeLeft ?: 0) * 1000
        val seconds = (time / 1000) % 60
        val minutes = ((time / (1000 * 60)) % 60)
        val hours = ((time / (1000 * 60 * 60)) % 24)

        questionTimeLeft = time - 1000
        questionTimeLeft = (questionTimeLeft ?: 0) / 1000

        return "%02d.%02d.%02d".format(hours, minutes, seconds)
    }

    private fun handleQuestionTimeLeft() {
        handler.removeCallbacks(runnableQuestion)
        handler.postDelayed(runnableQuestion, 1000)
    }

    fun isQuestionByQuestionType(): Boolean {
        return timeLeft ?: 0 <= 0L
    }
}
