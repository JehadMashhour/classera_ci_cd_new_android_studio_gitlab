package com.classera.data.models.mailbox

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Role(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "prefix")
    val prefix: String? = null,

    @Json(name = "title")
    val title: String? = null

)
