package com.classera.data.models.settings


import com.squareup.moshi.Json

data class School(
    @Json(name = "timezone")
    val timezone: String? = ""
)
