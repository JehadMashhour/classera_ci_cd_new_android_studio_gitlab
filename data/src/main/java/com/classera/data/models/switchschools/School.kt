package com.classera.data.models.switchschools


import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class School(

    @Json(name = "has_access")
    val hasAccess: Boolean? = false,

    @Json(name = "hashed_id")
    val hashedId: String? = "",

    @Json(name = "icon")
    val icon: String? = "",

    @Json(name = "id")
    val id: String? = "",

    @Json(name = "parent")
    val parent: String? = "",

    @Json(name = "school_name")
    val schoolName: String? = "",

    @Json(name = "sub_schools")
    val subSchools: List<School>? = listOf()
) : Parcelable
