package com.classera.data.models.home.admin

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: classeraandroidv3
 * Created: Mar 06, 2020
 *
 * @author Odai Nazzal
 */
@JsonClass(generateAdapter = true)
data class TopScoresSchool(
    @Json(name = "user_id")
    val userId: String? = null,

    @Json(name = "username")
    val userName: String? = null,

    @Json(name = "avatar")
    val avatar: String? = null,

    @Json(name = "school_id")
    val schoolId: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    @Json(name = "school_name")
    val schoolName: String? = null,

    @Json(name = "score")
    val score: Int? = null,

    @Json(name = "level")
    val level: String? = null
)
