package com.classera.data.models.reportcards


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ReportCardsWrapper(

    @Json(name = "report_cards")
    val reportCards: List<ReportCard>? = listOf()
)
