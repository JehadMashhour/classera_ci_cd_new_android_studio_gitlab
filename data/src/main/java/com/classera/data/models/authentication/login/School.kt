package com.classera.data.models.authentication.login

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class School(

    @Json(name = "timezone")
    val timezone: String? = null
)
