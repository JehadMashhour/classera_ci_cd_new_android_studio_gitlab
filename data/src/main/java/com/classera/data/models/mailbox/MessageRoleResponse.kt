package com.classera.data.models.mailbox

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MessageRoleResponse(

    @Json(name = "Role")
    val role: Role? = null,

    @Json(name = "User")
    val users: List<RecipientUser>? = null

)
