package com.classera.data.models.switchroles


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AuthWrapper(

    @Json(name = "token")
    val token: String? = ""
)
