package com.classera.data.models.attachment

import com.classera.data.moshi.timeago.TimeAgo
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Odai Nazzal on 12/24/2019.
 * Classera
 * o.nazzal@classera.com
 */
@JsonClass(generateAdapter = true)
data class AttachmentComment(

    @Transient
    val localId: String? = null,

    @Json(name = "text")
    val text: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @TimeAgo
    @Json(name = "created")
    val created: String? = null,

    @Json(name = "approved")
    val approved: String? = null,

    @Json(name = "user_id")
    val userId: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    @Json(name = "image_url")
    val imageUrl: String? = null,

    @Transient
    val isFailed: Boolean = false,

    @Transient
    val isLoading: Boolean = false
)
