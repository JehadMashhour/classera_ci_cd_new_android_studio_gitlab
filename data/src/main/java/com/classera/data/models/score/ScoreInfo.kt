package com.classera.data.models.score

import android.os.Parcelable
import androidx.room.TypeConverters
import com.classera.data.database.converters.ScoreClassConverter
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import java.math.RoundingMode
import java.text.DecimalFormat

/**
 * Created by Odai Nazzal on 12/16/2019.
 * Classera
 * o.nazzal@classera.com
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class ScoreInfo(

    @Json(name = "user_score")
    val userScore: String? = null,

    @Json(name = "current_card_title")
    val currentCardTitle: String? = null,

    @Json(name = "current_card_color")
    val currentCardColor: String? = null,

    @Json(name = "user_ratio")
    val userRatio: Float? = null,

    @Json(name = "class_index")
    val classIndex: Int? = null,

    @TypeConverters(ScoreClassConverter::class)
    @Json(name = "class_title")
    val classTitle: ScoreClass? = null,

    @Json(name = "class_color")
    val classColor: String? = null,

    val uiScore: Int = 0
) : Parcelable {

    fun getRoundedUserScore(): String {
        val num = userScore?.toDouble()
        val df = DecimalFormat("#.#")
        df.roundingMode = RoundingMode.CEILING
        return df.format(num).toString()
    }

    fun getFormattedUserRatio(): String {
        val num = userRatio?.toDouble()
        val df = DecimalFormat("#.#")
        return df.format(num).toString()
    }
}
