package com.classera.data.models.callchildren


import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Score(
    @Json(name = "class_color")
    val classColor: String? = null,
    @Json(name = "class_index")
    val classIndex: Int? = 0,
    @Json(name = "class_title")
    val classTitle: String? = null,
    @Json(name = "current_card_color")
    val currentCardColor: String? = null,
    @Json(name = "current_card_title")
    val currentCardTitle: String? = null,
    @Json(name = "user_ratio")
    val userRatio: Double? = 0.0,
    @Json(name = "user_score")
    val userScore: String? = null
): Parcelable
