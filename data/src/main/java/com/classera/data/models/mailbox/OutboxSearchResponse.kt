package com.classera.data.models.mailbox

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OutboxSearchResponse(

    @Json(name = "mail")
    val mail: List<Outbox>? = null

)
