package com.classera.data.models.weeklyplan

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Odai Nazzal on 12/27/2019.
 * Classera
 * o.nazzal@classera.com
 */
@JsonClass(generateAdapter = true)
data class WeeklyPlanPreparation(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "weekly_study_plan_comments")
    val description: String? = null,

    @Json(name = "publish_date")
    val publishDate: String? = null,

    @Json(name = "course_title")
    val courseTitle: String? = null,

    @Json(name = "section_title")
    val sectionTitle: String? = null
)
