package com.classera.data.models.level

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created by Odai Nazzal on 12/16/2019.
 * Classera
 * o.nazzal@classera.com
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class StudentLevel(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "student_id")
    val studentId: String? = null,

    @Json(name = "year_id")
    val yearId: String? = null,

    @Json(name = "level_id")
    val levelId: String? = null,

    @Json(name = "status")
    val status: String? = null,

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "modified")
    val modified: String? = null
) : Parcelable
