package com.classera.data.models.attachment

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


/**
 * Created by Rawan Al-Theeb on 3/13/2020.
 * Classera
 * r.altheeb@classera.com
 */
@JsonClass(generateAdapter = true)
data class Preparation(
    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null
)
