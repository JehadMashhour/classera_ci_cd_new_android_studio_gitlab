package com.classera.data.models.eportfolio

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Odai Nazzal on 1/14/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
@JsonClass(generateAdapter = true)
data class EPortfolioWraper(

    @Json(name = "Eportfolio")
    val eportfolio: EPortfolio? = null
)
