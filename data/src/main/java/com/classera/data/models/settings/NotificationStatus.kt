package com.classera.data.models.settings


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NotificationStatus(

    @Json(name = "enabled")
    val enabled: Boolean? = false
)
