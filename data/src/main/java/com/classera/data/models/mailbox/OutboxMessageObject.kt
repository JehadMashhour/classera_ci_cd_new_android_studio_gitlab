package com.classera.data.models.mailbox

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OutboxMessageObject(

    @Json(name = "Message")
    val messageDetails: OutboxMessageDetails? = null,

    @Json(name = "recipients_id")
    val recipientsId: List<Recipient>? = null,

    @Json(name = "recipients_count")
    val recipientsCount: Int? = null

)
