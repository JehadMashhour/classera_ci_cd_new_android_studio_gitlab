package com.classera.data.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BasePaginationWrapper<T>(

    @Json(name = "success")
    val success: Boolean? = null,

    @Json(name = "message")
    val message: String? = null,

    @Json(name = "code")
    val code: Int? = null,

    @Json(name = "url")
    val url: String? = null,

    @Json(name = "data")
    var data: T? = null,

    @Json(name = "count")
    val count: Int? = null,

    @Json(name = "pagination")
    val pagination: String? = null
)
