package com.classera.data.models.user

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.classera.data.database.converters.AbsenceStatusConverter
import com.classera.data.database.converters.UserRoleConverter
import com.classera.data.models.level.StudentLevel
import com.classera.data.models.score.ScoreInfo
import com.classera.data.models.selection.Selectable
import com.classera.data.models.semester.Semester
import com.classera.data.moshi.capitalize.Capitalize
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created by Odai Nazzal on 12/16/2019.
 * Classera
 * o.nazzal@classera.com
 */
@Entity
@Parcelize
@JsonClass(generateAdapter = true)
data class User(

    @PrimaryKey
    @Json(name = "id")
    override val id: String = "",

    @Json(name = "user_id")
    val userId: String = "",

    @Json(name = "username")
    val userName: String? = null,

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "student_user_id")
    val studentId: String? = null,

    @Json(name = "student_id")
    val studentBehaviorId: String? = null,

    @Json(name = "modified")
    val modified: String? = null,

    @TypeConverters(UserRoleConverter::class)
    @Json(name = "role_id")
    val roleId: UserRole? = null,

    @Json(name = "school_id")
    val schoolId: String? = null,

    @Json(name = "main_account_id")
    val mainAccountId: String? = null,

    @Json(name = "first_name")
    val firstName: String? = null,

    @Json(name = "father_name")
    val fatherName: String? = null,

    @Json(name = "grandfather_name")
    val grandFatherName: String? = null,

    @Json(name = "family_name")
    val familyName: String? = null,

    @Json(name = "name_ara")
    val nameAr: String? = null,

    @Json(name = "name_eng")
    val nameEng: String? = null,

    @Json(name = "guardian_id")
    val guardianId: String? = null,

    @Json(name = "father_mobile")
    val fatherMobile: String? = null,

    @Json(name = "father_number")
    val fatherNumber: String? = null,

    @Json(name = "mother_mobile")
    val motherMobile: String? = null,

    @Json(name = "date_of_birth")
    val dateOfBirth: String? = null,

    @Json(name = "place_of_birth")
    val placeOfBirth: String? = null,

    @Json(name = "place_of_birth_ara")
    val placeOfBirthAr: String? = null,

    @Json(name = "address")
    val address: String? = null,

    @Json(name = "phone_number")
    val phoneNumber: String? = null,

    @Json(name = "mobile_number")
    val mobileNumber: String? = null,

    @Json(name = "emergency_number_1")
    val emergencyNumberOne: String? = null,

    @Json(name = "emergency_number_2")
    val emergencyNumberTwo: String? = null,

    @Json(name = "email")
    val email: String? = null,

    @Json(name = "verified_email")
    val verifiedEmail: Boolean = false,

    @Json(name = "email2")
    val emailTwo: String? = null,

    @Json(name = "lives_with_parents")
    val livesWithParents: Boolean? = null,

    @Json(name = "siblings_count")
    val siblingsCount: String? = null,

    @Json(name = "nationality_id")
    val nationalityId: String? = null,

    @Json(name = "dob_hijri")
    val dobHijri: String? = null,

    @Json(name = "photo_filename")
    val photoFileName: String? = null,

    @Json(name = "theme_id")
    val themeId: String? = null,

    @Json(name = "deleted")
    val deleted: Boolean? = null,

    @Json(name = "status_id")
    val statusId: String? = null,

    @Json(name = "lang")
    val lang: String? = null,

    @Json(name = "date_format")
    val dateFormat: String? = null,

    @Json(name = "login_name")
    val loginName: String? = null,

    @Json(name = "google_login_token")
    val googleLoginToken: String? = null,

    @Json(name = "number")
    val number: String? = null,

    @Json(name = "specialization")
    val specialization: String? = null,

    @Json(name = "valid_number")
    val isValidNumber: Boolean? = null,

    @Json(name = "passport_number")
    val passportNumber: String? = null,

    @Json(name = "registration_number")
    val registrationNumber: String? = null,

    @Json(name = "old_school_id")
    val oldSchoolId: String? = null,

    @Json(name = "gender")
    val gender: String? = null,

    @Json(name = "block_reports")
    val blockReports: Boolean? = null,

    @Json(name = "mawhiba_id")
    val mawhibaId: String? = null,

    @Json(name = "pioneer_id")
    val pioneerId: String? = null,

    @Json(name = "cognitive_person_id")
    val cognitivePersonId: String? = null,

    @Json(name = "bio")
    val bio: String? = null,

    @Json(name = "facebook_account")
    val facebookAccount: String? = null,

    @Json(name = "twitter_account")
    val twitterAccount: String? = null,

    @Json(name = "google_plus_account")
    val googlePlusAccount: String? = null,

    @Json(name = "instagram_account")
    val instagramAccount: String? = null,

    @Json(name = "linkedin_account")
    val linkedinAccount: String? = null,

    @Json(name = "mobile_number2")
    val mobileNumberTwo: String? = null,

    @Json(name = "educational_administration")
    val educationalAdministration: String? = null,

    @Json(name = "external_school")
    val externalSchool: String? = null,

    @Json(name = "tmp_section_id")
    val tmpSectionId: String? = null,

    @Json(name = "level_pioneer_id")
    val levelPioneerId: String? = null,

    @Json(name = "section_pioneer_id")
    val sectionPioneerId: String? = null,

    @Json(name = "parent_pioneer_id")
    val parentPioneerId: String? = null,

    @Json(name = "thumb_done")
    val thumbDone: String? = null,

    @Json(name = "android_token")
    val androidToken: String? = null,

    @Json(name = "ios_token")
    val iosToken: String? = null,

    @Json(name = "restrict_login")
    val restrictLogin: Boolean? = null,

    @Json(name = "mawhiba_program_id")
    val mawhibaProgramId: String? = null,

    @Json(name = "last_activity_date")
    val lastActivityDate: String? = null,

    @Json(name = "fake_data")
    val fakeData: Boolean? = null,

    @Json(name = "duplicated")
    val duplicated: Boolean? = null,

    @Json(name = "scholarship_number")
    val scholarshipNumber: String? = null,

    @Json(name = "allow_notification")
    val allowNotification: Boolean? = null,

    @Json(name = "place_of_birth_city")
    val placeOfBirthCity: String? = null,

    @Json(name = "place_of_birth_city_other")
    val placeOfBirthCityOther: String? = null,

    @Json(name = "father_work_place")
    val fatherWorkPlace: String? = null,

    @Json(name = "mother_work_place")
    val motherWorkPlace: String? = null,

    @Json(name = "father_qualifications")
    val fatherQualifications: String? = null,

    @Json(name = "mother_qualifications")
    val motherQualifications: String? = null,

    @Json(name = "mother_name")
    val motherName: String? = null,

    @Json(name = "is_university_associate")
    val isUniversityAssociate: Boolean? = null,

    @Json(name = "has_siblings_in_schools")
    val hasSiblingsInSchools: Boolean? = null,

    @Json(name = "sibling_name_in_schools")
    val siblingNameInSchools: String? = null,

    @Json(name = "sibling_level_id")
    val siblingLevelId: String? = null,

    @Json(name = "admission_letter")
    val admissionLetter: String? = null,

    @Json(name = "guardian_university_number")
    val guardianUniversityNumber: String? = null,

    @Json(name = "guardian_university_association")
    val guardianUniversityAssociation: String? = null,

    @Json(name = "walkthrough")
    val walkThrough: Boolean? = null,

    @Json(name = "insertion_method")
    val insertionMethod: String? = null,

    @Json(name = "residence_country")
    val residenceCountry: String? = null,

    @Json(name = "mother_number")
    val motherNumber: String? = null,

    @Json(name = "erp_user_id")
    val erpUserId: String? = null,

    @Json(name = "back_to_referer_page")
    val backToRefererPage: Boolean? = null,

    @Json(name = "ms_graph_token")
    val msGraphToken: String? = null,

    @Json(name = "allow_theme_change")
    val allowThemeChange: String? = null,

    @Json(name = "acf_check")
    val acfCheck: Boolean? = null,

    @Json(name = "sub_role")
    val subRole: String? = null,

    @Json(name = "applicant_degree")
    val applicantDegree: String? = null,

    @Json(name = "applicant_recommendations")
    val applicantRecommendations: String? = null,

    @Json(name = "applicant_grade")
    val applicantGrade: String? = null,

    @Json(name = "applicant_notes")
    val applicantNotes: String? = null,

    @Json(name = "applicant_repaid")
    val applicantRepaid: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    @Capitalize
    @Json(name = "school_name")
    val schoolName: String? = null,

    @Json(name = "timezone")
    val timezone: String? = null,

    @Json(name = "image_url")
    val imageUrl: String? = null,

    @Json(name = "image")
    val image: String? = null,

    @Embedded(prefix = "student_level_")
    @Json(name = "student_level")
    val studentLevel: StudentLevel? = null,

    @Embedded(prefix = "score_info_")
    @Json(name = "score_info")
    val scoreInfo: ScoreInfo? = null,

    @Embedded(prefix = "current_semester_")
    @Json(name = "current_semester")
    val currentSemester: Semester? = null,

    @Json(name = "profilepic")
    val profilePicture: String? = null,

    @TypeConverters(AbsenceStatusConverter::class)
    @Json(name = "absence_status")
    var absenceStatus: AbsenceStatus? = null,

    @Json(name = "name")
    val name: String? = null,

    override var selected: Boolean = false
) : Selectable {

    override val title: String?
        get() = fullName

    fun isNotAllowedToLogin(): Boolean {
        return roleId == UserRole.UNKNOWN
    }
}
