package com.classera.data.models.vcr

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.os.Parcelable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.classera.data.R
import com.classera.data.models.vcr.sharewith.ShareWithStatusTypes
import com.classera.data.models.vcr.teacher.Teacher
import com.classera.data.models.vcr.vendor.Duration
import com.classera.data.models.vcr.vendor.Vendor
import com.classera.data.moshi.todate.ToDate
import com.classera.data.toDate
import com.classera.data.toString
import com.squareup.moshi.Json
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class VcrResponse(

    @Json(name = "lectures")
    var lectures: List<Lecture?>? = null,

    @Json(name = "courses")
    val courses: List<Course?>? = null,

    @Json(name = "duration")
    var duration: String? = null,

    @Json(name = "link")
    var link: String? = null,

    @Json(name = "external_link")
    var externalLink: String? = null,

    @Json(name = "zoom_link")
    var zoomLink: String? = null,

    @Json(name = "session_code")
    val sessionCode: String? = null,

    @Json(name = "sharing_status")
    var sharingStatus: ShareWithStatusTypes? = null,

    @ToDate
    @Json(name = "starting_time")
    var startingTime: Date? = null,

    @Json(name = "title")
    var title: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "vendor")
    var vendor: Vendor? = null,

    @Json(name = "teacher")
    var teacher: Teacher? = null,

    @Json(name = "passcode")
    var passcode: String? = null,

    @Json(name = "allow_student_start_vcr")
    var allowStudentStartVcr: Boolean? = null,

    @Json(name = "weekly_repeat")
    var weeklyRepeat: Boolean? = null,

    @ToDate
    @Json(name = "repeat_until")
    var repeatUntil: Date? = null,

    @Json(name = "recurring")
    var recurring: Boolean? = null,

    @Json(name = "can_edit_zoom")
    var canEditZoom: Boolean? = null,

    @Json(name = "course_title")
    var courseTitle: String? = null,

    @Json(name = "full_name")
    var fullName: String? = null,

    @Json(name = "image_url")
    var imageUrl: String? = null,

    @Json(name = "join_allowed")
    var joinAllowed: Boolean? = null,

    @Json(name = "human_date")
    var humanDate: String? = null,

    @Json(name = "state")
    var state: String? = null,

    @ToDate
    var timeValue: Date? = null,

    @ToDate
    var dateValue: Date? = null,

    var isUpComing: Boolean? = true


) : BaseObservable(), Parcelable {

    @get:Bindable
    @IgnoredOnParcel
    @Transient
    var deleting: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.deleting)
        }

    @get:Bindable
    @IgnoredOnParcel
    var lecturesText: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.lecturesText)
        }

    @get:Bindable
    @IgnoredOnParcel
    var durationText: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.durationText)
        }

    @get:Bindable
    @IgnoredOnParcel
    @Transient
    var dateText: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.dateText)
        }

    @get:Bindable
    @IgnoredOnParcel
    @Transient
    var timeText: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.timeText)
        }

    @get:Bindable
    @IgnoredOnParcel
    @Transient
    var shareWithText: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.shareWithText)
        }

    @get:Bindable
    @IgnoredOnParcel
    @Transient
    var repeatUntilText: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.repeatUntilText)
        }

    @get:Bindable
    @IgnoredOnParcel
    @Transient
    var repeatUntilVisible: Boolean? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.repeatUntilVisible)
        }


    fun getLectureNames(): String? {
        return lectures?.joinToString(separator = " - ") { it?.title ?: "" }
    }

    fun getCoursesNames(): String? {
        return courses?.joinToString(separator = " | ") { it?.title ?: "" }
    }

    fun getTime(): String? {
        return startingTime?.toString("hh:mm a")
    }


    fun getDate(): String? {
        return startingTime?.toString("yyyy-MM-dd")
    }

    fun getDateAndTime(): String? {
        return startingTime?.toString("yyyy-MM-dd hh:mm a")
    }

    fun setVendorLink() {
        when (vendor?.id) {
            VendorTypes.ZOOM_MEETINGS.id.toString() -> {
                setZoomLinkValue(link)
            }
            else -> {
                setExternalLinkValue(link)
            }
        }
    }


    fun getDurationList(suffix: String): List<Duration>? =
        this.vendor?.vcrStatusWrapper?.settings?.durations?.map { t ->
            Duration(t.toString(), "$t $suffix")
        }


    fun setSelectedLectures(lectures: List<Lecture?>? = this.lectures, suffix: String) {
        this.lectures = lectures
        lectures?.let {
            if (it.isNotEmpty()) {
                this.lecturesText = lectures.size.toString() + " " + suffix
            } else {
                this.lecturesText = ""
            }
        }
    }

    fun setDuration(
        duration: Duration = Duration(
            id = this.duration,
            title = this.duration?.let {
                "$it $suffix"
            }
        ), suffix: String
    ) {
        this.duration = duration.id
        this.durationText = duration.title
    }

    fun setDate(
        date: Date? = this.startingTime.toString("yyyy-MM-dd").toDate("yyyy-MM-dd")
    ) {
        this.dateValue = date
        this.dateText = date.toString("yyyy-MM-dd")
    }

    fun setTime(time: Date? = this.startingTime.toString("hh:mm a").toDate("hh:mm a")) {
        this.timeValue = time
        this.timeText = time.toString("hh:mm a")
    }

    fun setShareWithText(
        context: Context,
        sharingStatus: ShareWithStatusTypes? = this.sharingStatus
    ) {
        this.sharingStatus = sharingStatus
        this.shareWithText = sharingStatus?.title?.let { context.getString(it) }
    }

    fun setRepeatUntilText(repeatUntil: Date? = this.repeatUntil) {
        this.repeatUntil = repeatUntil
        this.repeatUntilText = repeatUntil.toString("yyyy-MM-dd")
    }

    fun setAllowStudentStartVcrValue(checked: Boolean? = allowStudentStartVcr ?: false) {
        if (this.vendor?.vcrStatusWrapper?.settings?.allowStudentStartVcr == true) {
            this.allowStudentStartVcr = checked
        }
    }

    fun setWeeklyRepeatValue(checked: Boolean? = false) {
        if (this.vendor?.vcrStatusWrapper?.settings?.weeklyRepeated == true) {
            this.weeklyRepeat = checked
            this.repeatUntilVisible = checked
        }
    }

    fun setRecurringValue(checked: Boolean? = false) {
        if (this.vendor?.vcrStatusWrapper?.settings?.recurring == true) {
            this.recurring = checked
        }
    }

    fun setTitleValue(title: String? = this.title) {
        this.title = title.takeIf { !it.isNullOrEmpty() }
    }

    fun setExternalLinkValue(externalLink: String?) {
        if (this.vendor?.vcrStatusWrapper?.settings?.showExternalLink == true) {
            this.externalLink = externalLink.takeIf { !it.isNullOrEmpty() }
        }
    }

    fun setZoomLinkValue(zoomLink: String?) {
        if (this.vendor?.vcrStatusWrapper?.settings?.showZoomLink == true) {
            this.zoomLink = zoomLink.takeIf { !it.isNullOrEmpty() }
        }
    }

    fun setPasscodeValue(passcode: String?) {
        if (this.vendor?.vcrStatusWrapper?.settings?.showPasscode == true) {
            this.passcode = passcode.takeIf { !it.isNullOrEmpty() }
        }
    }

    fun getStatusDrawable(context: Context): Drawable? {
        return when (state) {
            LIVE -> ContextCompat.getDrawable(context, R.drawable.ic_live)
            ENDED -> ContextCompat.getDrawable(context, R.drawable.ic_ended)
            UPCOMING -> ContextCompat.getDrawable(context, R.drawable.ic_upcoming)
            AWAY -> ContextCompat.getDrawable(context, R.drawable.ic_away)
            else -> null
        }
    }

    fun getStatusBackgroundColor(): String? {
        return when (state) {
            LIVE -> "#4cd84c"
            ENDED -> "#eaeaea"
            UPCOMING -> "#7cd8ff"
            AWAY -> "#ffd076"
            else -> null
        }
    }

    fun getStatusString(context: Context): String? {
        return when (state) {
            LIVE -> context.getString(R.string.live)
            ENDED -> context.getString(R.string.ended)
            UPCOMING -> context.getString(R.string.upcoming)
            AWAY -> context.getString(R.string.away)
            else -> null
        }
    }


    fun isLive(): Boolean {
        return when (state) {
            LIVE -> true
            else -> false
        }
    }

    fun isEnded(): Boolean {
        return when (state) {
            ENDED -> true
            else -> false
        }
    }

    fun isUpcoming(): Boolean {
        return when (state) {
            UPCOMING -> true
            else -> false
        }
    }

    companion object {
        private const val LIVE = "live"
        private const val ENDED = "ended"
        private const val UPCOMING = "upcoming"
        private const val AWAY = "away"
    }
}
