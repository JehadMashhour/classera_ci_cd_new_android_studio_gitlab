package com.classera.data.models.discussions


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RoomDetails(
    @Json(name = "approved")
    val approved: Boolean? = false,

    @Json(name = "author")
    val author: String? = null,

    @Json(name = "closed")
    val closed: Boolean? = false,

    @Json(name = "comments_count")
    val commentsCount: String? = null,

    @Json(name = "course_id")
    val courseId: String? = null,

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "deleted")
    val deleted: Boolean? = null,

    @Json(name = "hide_comments")
    val hideComments: Boolean? = false,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "lastposter")
    val lastposter: String? = null,

    @Json(name = "modified")
    val modified: String? = null,

    @Json(name = "parentid")
    val parentid: String? = null,

    @Json(name = "post")
    val post: String? = null,

    @Json(name = "repliesnum")
    val repliesnum: String? = null,

    @Json(name = "selected_students")
    val selectedStudents: List<Student?>? = null,

    @Json(name = "semester_id")
    val semesterId: String? = null,

    @Json(name = "teacher_name")
    val teacherName: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "user_id")
    val userId: String? = null,

    @Json(name = "word_count")
    val wordCount: String? = null
)
