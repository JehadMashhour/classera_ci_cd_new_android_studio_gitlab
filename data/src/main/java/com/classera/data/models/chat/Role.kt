package com.classera.data.models.chat

import com.classera.data.models.filter.Filterable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created on 15/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class Role(

    @Json(name = "id")
    override val filterId: String? = null,

    @Json(name = "title")
    override val title: String? = null,

    @Transient
    override var selected: Boolean = false
) : Filterable
