package com.classera.data.models.discussions

import android.os.Parcelable
import com.classera.data.models.selection.Selectable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Student(

    @Json(name = "user_id")
    override val id: String? = null,

    @Json(name = "count")
    val count: Int? = null,

    @Json(name = "image")
    val image: String? = null,

    @Json(name = "lectures")
    val lectures: List<Lecture?>? = null,

    @Json(name = "student_name")
    override val title: String? = null,

    @Transient
    override var selected: Boolean = false
) : Selectable, Parcelable
