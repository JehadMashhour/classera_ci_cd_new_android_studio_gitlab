package com.classera.data.models.homelocation


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ThreeWord(
    @Json(name = "created")
    val created: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "modified")
    val modified: String? = null,

    @Json(name = "reference_id")
    val referenceId: String? = null,

    @Json(name = "reference_type")
    val referenceType: String? = null,

    @Json(name = "w3w")
    val w3w: String? = null
)
