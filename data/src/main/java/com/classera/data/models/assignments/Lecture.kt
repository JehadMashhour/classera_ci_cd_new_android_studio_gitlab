package com.classera.data.models.assignments

import android.os.Parcelable
import com.classera.data.models.selection.Selectable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Project: Classera
 * Created: May 18, 2020
 *
 * @author Kahled Mohammad
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class Lecture(

    @Json(name = "id")
    override val id: String? = null,

    @Json(name = "course_title")
    val courseTitle: String? = null,

    @Json(name = "title")
    override val title: String? = null,

    override var selected: Boolean = false

) : Parcelable, Selectable
