package com.classera.data.models.settings


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AppVersion(

    @Json(name = "force")
    val force: Boolean? = null,

    @Json(name = "store")
    val store: String? = null,

    @Json(name = "Updated")
    val updated: Boolean? = null,

    @Json(name = "version")
    val version: Int? = null
)
