package com.classera.data.models.callchildren


import com.squareup.moshi.Json

/**
 * Created by Rawan Al-Theeb on 8/17/2021.
 * Classera
 * r.altheeb@classera.com
 */

data class Driver(

    @Json(name = "avatar")
    val avatar: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    @Json(name = "Guardian_driver_id")
    val guardianDriverId: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "last_activity_date")
    val lastActivityDate: String? = null,

    @Json(name = "mobile_number")
    val mobileNumber: String? = null,

    @Json(name = "number")
    val number: String? = null
)
