package com.classera.data.models.filter

import android.os.Parcelable

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
interface Filterable : Parcelable {

    val filterId: String?

    val title: String?

    var selected: Boolean
}
