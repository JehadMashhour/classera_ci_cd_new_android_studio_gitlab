package com.classera.data.moshi.capitalize

import com.squareup.moshi.FromJson

import com.squareup.moshi.ToJson
import java.util.*

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class CapitalizeAdapter {

    @ToJson
    fun toJson(@Capitalize value: String?): String? {
        return value
    }

    @ExperimentalStdlibApi
    @FromJson
    @Capitalize
    fun fromJson(value: String?): String? {
        return value?.capitalize(Locale.ENGLISH)
    }
}
