package com.classera.data.moshi.chattime

import com.classera.data.toString
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */

const val TWENTY_FOUR_HOURS = 24L

class ChatTimeAdapter {

    @ToJson
    fun toJson(@ChatTime value: String): String? {
        return value
    }

    @FromJson
    @ChatTime
    fun fromJson(value: String?): String? {
        val timeStamp = value?.toLongOrNull() ?: 0L
        val date = Date(timeStamp)
        val currentTimeStamp = System.currentTimeMillis()
        val diffTimeStamp = currentTimeStamp - timeStamp

        return if (diffTimeStamp < TimeUnit.HOURS.toMillis(TWENTY_FOUR_HOURS)) {
            date.toString("hh:mm a")
        } else {
            date.toString("yyyy-MM-dd hh:mm a")
        }
    }
}
