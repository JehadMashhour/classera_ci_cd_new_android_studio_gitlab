package com.classera.data.moshi.timeago

import android.text.format.DateUtils
import com.classera.data.toDate
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.*


/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class TimeAgoAdapter {

    @ToJson
    fun toJson(@TimeAgo value: String): String? {
        return value
    }

    @FromJson
    @TimeAgo
    fun fromJson(value: String?): String? {
        val date = value.toDate("yyyy-MM-dd HH:mm:ss")
        val niceDateStr = DateUtils.getRelativeTimeSpanString(
            date!!.time,
            Calendar.getInstance().timeInMillis,
            DateUtils.MINUTE_IN_MILLIS
        )

        return niceDateStr.toString()
    }
}
