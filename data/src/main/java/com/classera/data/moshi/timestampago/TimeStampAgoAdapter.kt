package com.classera.data.moshi.timestampago

import android.text.format.DateUtils
import com.classera.data.toDate
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.*


/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class TimeStampAgoAdapter {

    @ToJson
    fun toJson(@TimeStampAgo value: String): String? {
        return value
    }

    @FromJson
    @TimeStampAgo
    fun fromJson(value: String?): String? {
        val date = Date(value?.toLongOrNull() ?: 0L)
        val niceDateStr = DateUtils.getRelativeTimeSpanString(
            date.time,
            Calendar.getInstance().timeInMillis,
            DateUtils.MINUTE_IN_MILLIS
        )

        return niceDateStr.toString()
    }
}
