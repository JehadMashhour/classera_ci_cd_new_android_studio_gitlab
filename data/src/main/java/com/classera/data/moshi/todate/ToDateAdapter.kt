package com.classera.data.moshi.todate

import com.classera.data.toDate
import com.classera.data.toString
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.*

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class ToDateAdapter {

    @ToJson
    fun toJson(@ToDate value: Date): String? {
        return value.toString("yyyy-MM-dd HH:mm:ss")
    }

    @FromJson
    @ToDate
    fun fromJson(value: String?): Date? {
        return value.toDate("yyyy-MM-dd HH:mm:ss")
    }
}
