package com.classera.data.moshi.toStringNumber

import com.squareup.moshi.JsonQualifier

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Odai Nazzal
 */
@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class ToStringNumber
