package com.classera.data.moshi.duration

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.*

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("MagicNumber")
class DurationAdapter {

    @ToJson
    fun toJson(@Duration value: String?): String? {
        return value
    }

    @FromJson
    @Duration
    fun fromJson(value: String?): String? {
        val duration = value?.toIntOrNull() ?: 0
        val seconds = (duration / 1000) % 60
        val minutes = (duration / (1000 * 60) % 60)
        return "%d:%02d".format(Locale.ENGLISH, minutes, seconds)
    }
}
