package com.classera.data.moshi.subquestions

import com.classera.data.models.assignments.Question
import com.classera.data.models.assignments.SubQuestions
import com.google.gson.Gson
import com.squareup.moshi.FromJson
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson
import com.squareup.moshi.Types

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class SubQuestionsAdapter(private val gson: Gson) {

    private val questionsList = Types.newParameterizedType(List::class.java, Question::class.java)

    @ToJson
    fun toJson(@SubQuestionsConverter value: SubQuestions?): Any? {
        return value
    }

    @FromJson
    @SubQuestionsConverter
    fun fromJson(value: Any?): SubQuestions? {
        val json = gson.toJson(value)
        return try {
            gson.fromJson(json, SubQuestions::class.java)
        } catch (e: Exception) {
            val questions = gson.fromJson<List<Question>>(json, questionsList)
            SubQuestions(questions = questions)
        }
    }
}
