package com.classera.data.moshi.enums

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import kotlin.collections.set
import kotlin.reflect.KClass

/**
 * Project: Classera
 * Created: Jan 01, 2020
 *
 * @author Mohamed Hamdan
 */
class EnumsAdapter<T : Enum<T>> private constructor(
    enumType: Class<T>,
    private val fallbackValue: T
) : JsonAdapter<T?>() {

    private val nameStrings: MutableMap<Int, Array<String>> = hashMapOf()
    private val constants = enumType.enumConstants

    init {
        for (i in (constants?.indices ?: 0..0)) {
            val constantName = constants?.get(i)?.name ?: ""
            val annotation = enumType.getField(constantName).getAnnotation(Json::class.java)
            val name = annotation?.name ?: constantName
            val alternatives = annotation?.alternatives ?: arrayOf()
            nameStrings[i] = arrayOf(name, *alternatives)
        }
    }

    override fun fromJson(reader: JsonReader): T? {
        if (reader.peek() == JsonReader.Token.STRING && reader.hasNext()) {
            val value = reader.nextString()
            val index = nameStrings.filter { entry -> entry.value.any { it == value } }.keys.firstOrNull()
            if (index != null) {
                return constants?.get(index)
            }
        }
        return fallbackValue
    }

    override fun toJson(writer: JsonWriter, value: T?) {
        if (value == null) {
            throw NullPointerException("value was null! Wrap in .nullSafe() to write nullable values.")
        }
        writer.value(nameStrings[value.ordinal].toString())
    }

    companion object {

        fun <T : Enum<T>> create(enumType: KClass<T>, fallbackValue: T): EnumsAdapter<T> {
            return EnumsAdapter(enumType.java, fallbackValue)
        }
    }
}
