package com.classera.data.socket

import com.classera.data.BuildConfig
import com.classera.data.getFormalUserName
import com.classera.data.models.chat.Message
import com.classera.data.models.chat.MessageBody
import com.classera.data.models.chat.MessageTypeEnum
import com.classera.data.moshi.chattime.ChatTimeAdapter
import com.classera.data.moshi.duration.DurationAdapter
import com.classera.data.network.errorhandling.tryThirdPartyResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.chat.ChatRepository
import com.squareup.moshi.Moshi
import io.socket.client.Socket
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Mar 14, 2020
 *
 * @author Mohamed Hamdan
 */
class MessagesSocket @Inject constructor(
    private val socket: Socket,
    private val prefs: Prefs,
    private val chatRepository: ChatRepository,
    moshi: Moshi
) {

    private val messageBodyJsonAdapter = moshi.adapter(MessageBody::class.java)

    fun sendMessage(message: JSONObject) {
        socket.emit("messages", message)
    }

    fun onMessageReceived(callback: (Message?) -> Unit) {
        socket.on("messages") {
            val element = JSONObject(it[0].toString())
            val type = element.optString("type").toIntOrNull()
            val typeEnum = MessageTypeEnum.values().firstOrNull { enum -> enum.type == type }
            val messageBody = getMessageBody(type, element)

            val message = Message(
                threadId = element.optString("thread_id"),
                messageId = element.optString("message_id"),
                fromId = element.optLong("from_id").toString(),
                body = messageBody,
                sentDate = ChatTimeAdapter().fromJson(element.optString("sent_date")),
                type = typeEnum
            ).apply {
                duration = DurationAdapter().fromJson(messageBody?.duration)
            }
            GlobalScope.launch(Dispatchers.IO) {
                val resource = tryThirdPartyResource { getUserNameAndProfilePicture(message) }
                callback(message)
            }
        }
    }

    private suspend fun getUserNameAndProfilePicture(message: Message?) {
        val users = chatRepository.getUsersBasicDetails(listOf(message?.fromId)).data

        val user = users?.firstOrNull { it.userId == message?.fromId }
        message?.senderName = user?.fullName.getFormalUserName()
        message?.senderPicture = user?.photoFileName
    }

    private fun getMessageBody(type: Int?, element: JSONObject): MessageBody? {
        return if (type != MessageTypeEnum.MEDIA.type) {
            MessageBody(text = element.optString("message"))
        } else {
            if (element.optString("message").isNotEmpty()) {
                messageBodyJsonAdapter.fromJson(element.optString("message"))
            } else {
                messageBodyJsonAdapter.fromJson(element.toString())
            }
        }
    }

    fun updateSeen(messageId: String?, threadId: String?) {
        val args = JSONObject()
        args.put("message_id", messageId)
        args.put("source", BuildConfig.flavorData.socketSource)
        args.put("thread_id", threadId)
        args.put("user_id", prefs.userId)
        socket.emit("update-seen", args)
    }
}
