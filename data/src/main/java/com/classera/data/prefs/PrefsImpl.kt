package com.classera.data.prefs

import android.content.SharedPreferences
import androidx.core.content.edit
import com.classera.data.models.user.UserRole
import java.util.*

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class PrefsImpl(private val sharedPreferences: SharedPreferences) : Prefs {

    override var isLoggedIn: Boolean
        get() = sharedPreferences.getBoolean(IS_LOGGED_IN_KEY, false)
        set(value) {
            sharedPreferences.edit { putBoolean(IS_LOGGED_IN_KEY, value) }
        }

    override var userProfilePicture: String?
        get() = sharedPreferences.getString(PROFILE_PICTURE_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(PROFILE_PICTURE_KEY, value) }
        }

    override var userFullName: String?
        get() = sharedPreferences.getString(USER_FULL_NAME_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(USER_FULL_NAME_KEY, value) }
        }

    override var userId: String?
        get() = sharedPreferences.getString(USER_ID_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(USER_ID_KEY, value) }
        }

    override var creatorId: String?
        get() = sharedPreferences.getString(CREATOR_ID_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(CREATOR_ID_KEY, value) }
        }

    override var userRole: UserRole?
        get() = UserRole.valueOf(sharedPreferences.getString(USER_ROLE_KEY, UserRole.STUDENT.name)!!)
        set(value) {
            sharedPreferences.edit { putString(USER_ROLE_KEY, value?.name) }
        }

    override var mainRole: UserRole?
        get() = UserRole.valueOf(sharedPreferences.getString(MAIN_ROLE_KEY, UserRole.STUDENT.name)!!)
        set(value) {
            sharedPreferences.edit { putString(MAIN_ROLE_KEY, value?.name) }
        }

    override var previousUserRole: UserRole?
        get() = UserRole.valueOf(sharedPreferences.getString(PREVIOUS_USER_ROLE_KEY, UserRole.STUDENT.name)!!)
        set(value) {
            sharedPreferences.edit { putString(PREVIOUS_USER_ROLE_KEY, value?.name) }
        }

    override var authenticationToken: String?
        get() = sharedPreferences.getString(ACCESS_TOKEN_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(ACCESS_TOKEN_KEY, value) }
        }

    override var semesterId: String?
        get() = sharedPreferences.getString(SEMESTER_ID_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(SEMESTER_ID_KEY, value) }
        }

    override var semesterToken: String?
        get() = sharedPreferences.getString(SEMESTER_TOKEN_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(SEMESTER_TOKEN_KEY, value) }
        }

    override var schoolId: String?
        get() = sharedPreferences.getString(SCHOOL_ID_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(SCHOOL_ID_KEY, value) }
        }

    override var schoolToken: String?
        get() = sharedPreferences.getString(SCHOOL_TOKEN_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(SCHOOL_TOKEN_KEY, value) }
        }


    override var childId: String?
        get() = sharedPreferences.getString(CHILD_ID_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(CHILD_ID_KEY, value) }
        }

    override var chatPagination: String?
        get() = sharedPreferences.getString(CHAT_PAGINATION_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(CHAT_PAGINATION_KEY, value) }
        }

    override var userStatus: String?
        get() = sharedPreferences.getString(USER_STATUS_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(USER_STATUS_KEY, value) }
        }

    override var notificationEnabled: Boolean
        get() = sharedPreferences.getBoolean(NOTIFICATIONS_KEY, true)
        set(value) {
            sharedPreferences.edit { putBoolean(NOTIFICATIONS_KEY, value) }
        }

    override var isBlocked: Boolean
        get() = sharedPreferences.getBoolean(IS_BLOCKED_KEY, false)
        set(value) {
            sharedPreferences.edit { putBoolean(IS_BLOCKED_KEY, value) }
        }

    override var language: String
        get() {
            return sharedPreferences.getString(LANGUAGE_KEY, Locale.getDefault().language)
                ?: Locale.getDefault().language
        }
        set(value) {
            sharedPreferences.edit { putString(SCHOOL_TOKEN_KEY, value) }
        }

    override var usersUploadEportfolios: String?
        get() = sharedPreferences.getString(USERS_UPLOAD_EPORTFOLIOS_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(USERS_UPLOAD_EPORTFOLIOS_KEY, value) }
        }

    override var studentsCreateDiscussions: String?
        get() = sharedPreferences.getString(STUDENT_CREATE_DISCUSSIONS_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(STUDENT_CREATE_DISCUSSIONS_KEY, value) }
        }

    override var studentsChangePhoneNumber: String?
        get() = sharedPreferences.getString(STUDENT_CHANGE_PHONE_NUMBER_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(STUDENT_CHANGE_PHONE_NUMBER_KEY, value) }
        }

    override var studentsChangeEmail: String?
        get() = sharedPreferences.getString(STUDENT_CHANGE_EMAIL_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(STUDENT_CHANGE_EMAIL_KEY, value) }
        }

    override var studentsChangeProfilePic: String?
        get() = sharedPreferences.getString(STUDENT_CHANGE_PROFILE_PIC_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(STUDENT_CHANGE_PROFILE_PIC_KEY, value) }
        }

    override var schoolChat: String?
        get() = sharedPreferences.getString(SCHOOL_CHAT_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(SCHOOL_CHAT_KEY, value) }
        }

    override var childAvatar: String?
        get() = sharedPreferences.getString(CHILD_AVATAR, null)
        set(value) {
            sharedPreferences.edit { putString(CHILD_AVATAR, value) }
        }

    override var allowParentAccessToSmartClass: String?
        get() = sharedPreferences.getString(ALLOW_PARENT_ACCESS_TO_SMARTCLASS, null)
        set(value) {
            sharedPreferences.edit { putString(ALLOW_PARENT_ACCESS_TO_SMARTCLASS, value) }
        }

    override var schoolType: String?
        get() = sharedPreferences.getString(SCHOOL_TYPE_KEY, null)
        set(value) {
            sharedPreferences.edit { putString(SCHOOL_TYPE_KEY, value) }
        }

    override var uuid: String?
        get() = sharedPreferences.getString(UUID_NOTIFICATION, null)
        set(value) {
            sharedPreferences.edit { putString(UUID_NOTIFICATION, value) }
        }

    override fun deletePrefsData() {
        sharedPreferences.edit().clear().apply()
    }

    override var selectedSemesterId: String?
        get() = sharedPreferences.getString(SELECTED_SEMESTER_ID, null)
        set(value) {
            sharedPreferences.edit { putString(SELECTED_SEMESTER_ID, value) }
        }

    override var selectedSchoolId: String?
        get() = sharedPreferences.getString(SELECTED_SCHOOL_ID, null)
        set(value) {
            sharedPreferences.edit { putString(SELECTED_SCHOOL_ID, value) }
        }

    override var isAssessmentBlocked: String?
        get() = sharedPreferences.getString(ASSESSMENTS_BLOCKED, null)
        set(value) {
            sharedPreferences.edit { putString(ASSESSMENTS_BLOCKED, value) }
        }

    override var allowStudentsRequests: String?
        get() = sharedPreferences.getString(call_student_BLOCKED, null)
        set(value) {
            sharedPreferences.edit { putString(call_student_BLOCKED, value) }
        }

    private companion object {

        private const val PROFILE_PICTURE_KEY = "profilePictureKey"
        private const val USER_FULL_NAME_KEY = "userFullNameKey"
        private const val LANGUAGE_KEY = "key_fragment_settings_language"
        private const val IS_LOGGED_IN_KEY = "isLoggedIn"
        private const val USER_ID_KEY = "userId"
        private const val CREATOR_ID_KEY = "creatorId"
        private const val USER_ROLE_KEY = "userRole"
        private const val MAIN_ROLE_KEY = "mainUserRole"
        private const val PREVIOUS_USER_ROLE_KEY = "previousUserRole"
        private const val CHILD_AVATAR = "childAvatar"
        private const val SEMESTER_ID_KEY = "semesterId"
        private const val ACCESS_TOKEN_KEY = "accessToken"
        private const val SEMESTER_TOKEN_KEY = "semesterToken"
        private const val SCHOOL_TOKEN_KEY = "schoolToken"
        private const val SCHOOL_ID_KEY = "schoolId"
        private const val CHILD_ID_KEY = "childId"
        private const val CHAT_PAGINATION_KEY = "chatPagination"
        private const val USER_STATUS_KEY = "userStatus"
        private const val NOTIFICATIONS_KEY = "key_fragment_settings_notification"
        private const val IS_BLOCKED_KEY = "isBlocked"
        private const val USERS_UPLOAD_EPORTFOLIOS_KEY = "usersUploadEportfolios"
        private const val STUDENT_CREATE_DISCUSSIONS_KEY = "studentsCreateDiscussions"
        private const val STUDENT_CHANGE_PHONE_NUMBER_KEY = "studentsChangePhoneNumber"
        private const val STUDENT_CHANGE_EMAIL_KEY = "studentsChangeEmail"
        private const val STUDENT_CHANGE_PROFILE_PIC_KEY = "studentsChangeProfilePic"
        private const val ALLOW_PARENT_ACCESS_TO_SMARTCLASS = "allowParentAccessToSmartClass"
        private const val SCHOOL_TYPE_KEY = "schoolType"
        private const val SCHOOL_CHAT_KEY = "schoolChat"
        private const val UUID_NOTIFICATION = "uuidNotification"
        private const val SELECTED_SEMESTER_ID = "selectedSemester"
        private const val SELECTED_SCHOOL_ID = "selectedSchoolId"
        private const val ASSESSMENTS_BLOCKED = "assessmentsBlocked"
        private const val call_student_BLOCKED = "callchildBlocked"
    }
}
