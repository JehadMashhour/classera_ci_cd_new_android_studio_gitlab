package com.classera.data

import com.classera.data.models.assignments.AssignmentStatus
import com.classera.data.models.assignments.QuestionType
import com.classera.data.models.chat.MessageTypeEnum
import com.classera.data.models.digitallibrary.AttachmentTypes
import com.classera.data.models.reportcards.ReportCardType
import com.classera.data.models.user.UserRole
import com.classera.data.models.vcr.sharewith.ShareWithStatusTypes
import com.classera.data.moshi.enums.EnumsAdapter
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import kotlin.reflect.KClass

/**
 * Created by Odai Nazzal on 1/11/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
fun <T> Moshi.Builder.add(type: KClass<*>, jsonAdapter: JsonAdapter<T>): Moshi.Builder {
    return add(type.java, jsonAdapter)
}
