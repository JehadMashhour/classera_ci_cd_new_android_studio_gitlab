package com.classera.data.flavor.classlightflavors

import com.classera.data.flavor.BaseFlavorData

class ClassLightFlavorData : BaseFlavorData {
    override val appType: String
        get() = "classlight"
    override val production_api_baseUrl: String
        get() = "https://api.classera.com/v2/"
    override val staging_api_baseUrl: String
        get() = "http://qa1.classera.com/"
    override val development_api_baseUrl: String
        get() = "https://yazan-test.classera.com/"
    override val production_web_baseUrl: String
        get() = "https://classlight.com/"
    override val staging_web_baseUrl: String
        get() = "https://classlight.com/"
    override val development_web_baseUrl: String
        get() = "https://classlight.com/"

}
