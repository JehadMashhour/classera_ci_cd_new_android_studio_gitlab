package com.classera.discussionrooms

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.discussions.DiscussionRoomsRepository
import com.classera.discussionrooms.student.StudentDiscussionViewModel
import com.classera.discussionrooms.teacher.TeacherDiscussionViewModel
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */
class DiscussionsViewModelFactory @Inject constructor(
    private val discussionRoomsRepository: DiscussionRoomsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            StudentDiscussionViewModel::class.java -> {
                StudentDiscussionViewModel(discussionRoomsRepository) as T
            }
            TeacherDiscussionViewModel::class.java -> {
                TeacherDiscussionViewModel(discussionRoomsRepository) as T
            }
            else -> {
                throw IllegalAccessException("There is no view model called $modelClass")
            }
        }
    }
}
