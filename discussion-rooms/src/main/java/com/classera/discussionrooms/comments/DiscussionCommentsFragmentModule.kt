package com.classera.discussionrooms.comments

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.discussions.DiscussionRoomsRepository
import com.classera.data.repositories.user.UserRepository
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */

@Module
abstract class DiscussionCommentsFragmentModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: DiscussionCommentsViewModelFactory
        ): DiscussionCommentsViewModel {
            return ViewModelProvider(fragment, factory)[DiscussionCommentsViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideViewModelFactory(
            fragment: Fragment,
            discussionRoomsRepository: DiscussionRoomsRepository,
            userRepository: UserRepository,
            prefs: Prefs
        ): DiscussionCommentsViewModelFactory {
            val discussionDetailsFragmentArgs by fragment.navArgs<DiscussionCommentsFragmentArgs>()
            return DiscussionCommentsViewModelFactory(
                discussionDetailsFragmentArgs,
                discussionRoomsRepository,
                userRepository,
                prefs
            )
        }
    }

    @Binds
    abstract fun bindActivity(activity: DiscussionCommentsFragment): Fragment
}
