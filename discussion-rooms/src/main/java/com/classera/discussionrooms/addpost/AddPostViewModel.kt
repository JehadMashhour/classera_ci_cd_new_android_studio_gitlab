package com.classera.discussionrooms.addpost

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.repositories.discussions.DiscussionRoomsRepository

/**
 * Created by Rawan Al-Theeb on 3/3/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AddPostViewModel(private val discussionRoomsRepository: DiscussionRoomsRepository) : BaseViewModel() {

    fun onSaveClicked(
        roomId: String?,
        text: String?
    ) = liveData {
        emit(Resource.Loading(show = true))
        val resource = startAddingPost(roomId, text)
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private suspend fun startAddingPost(
        roomId: String?,
        text: String?
    ): Resource = tryNoContentResource {
        discussionRoomsRepository.addPost(
            roomId = roomId,
            text = text
        )
    }
}
