package com.classera.discussionrooms

import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource

@Screen("Discussions")
abstract class DiscussionsFragment : BaseFragment() {

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var searchView: SearchView? = null
    private var errorView: ErrorView? = null

    override val layoutId: Int = R.layout.fragment_discussions

    abstract fun getDiscussionViewModel(): DiscussionsViewModel

    abstract fun getDiscussionAdapter(): DiscussionsAdapter<*>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_discussion_rooms, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_discussion_rooms_search)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange {
            getDiscussionRooms()
        }

        searchView?.inputType = (
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)

        searchView?.setOnCloseListener {
            getDiscussionRooms()
            return@setOnCloseListener false
        }
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_discussions)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_discussions)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_discussions)
        errorView = view?.findViewById(R.id.error_view_fragment_discussions)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            getDiscussionAdapter()?.resetPaging()
            refreshDiscussionRooms()
        }
        getDiscussionRooms()
    }

    private fun getDiscussionRooms(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            getDiscussionAdapter()?.resetPaging()
        }
        getDiscussionViewModel().getDiscussionRooms(
            searchView?.query,
            pageNumber
        ).observe(this, this::handleResource)
    }

    private fun refreshDiscussionRooms() {
        getDiscussionViewModel().refreshDiscussionRooms(searchView?.query)
            .observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (getDiscussionViewModel().currentPage == DEFAULT_PAGE) {
            initAdapter()
            initAdapterListeners()
        } else {
            getDiscussionAdapter().notifyDataSetChanged()
        }
        getDiscussionAdapter().finishLoading()

    }

    private fun initAdapter() {
        recyclerView?.adapter = getDiscussionAdapter()
        getDiscussionAdapter().setOnLoadMoreListener(::getDiscussionRooms)
    }

    abstract fun initAdapterListeners()

    private fun handleErrorResource(resource: Resource.Error) {
        if (getDiscussionViewModel().getDiscussionRoomCount() == 0) {
            Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
            errorView?.visibility = View.VISIBLE
            errorView?.setError(resource)
            errorView?.setOnRetryClickListener { getDiscussionRooms() }
            getDiscussionAdapter()?.finishLoading()
            return
        }
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }

    abstract fun handleAddListeners()

}
