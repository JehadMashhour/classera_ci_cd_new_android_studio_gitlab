package com.classera.discussionrooms.teacher

import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.discussionrooms.DiscussionsAdapter
import com.classera.discussionrooms.DiscussionsViewModel
import com.classera.discussionrooms.R
import com.classera.discussionrooms.databinding.RowDiscussionRoomBinding

/**
 * Created by Rawan Al-Theeb on 2/24/2020.
 * Classera
 * r.altheeb@classera.com
 */
class TeacherDiscussionAdapter(
    private val viewModel: DiscussionsViewModel
) : DiscussionsAdapter<TeacherDiscussionAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowDiscussionRoomBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getDiscussionRoomCount()
    }

    inner class ViewHolder(binding: RowDiscussionRoomBinding) : BaseBindingViewHolder(binding) {

        private var imageViewMore: ImageView? = null

        init {
            imageViewMore = itemView.findViewById(R.id.image_view_row_discussion_room_more)
        }

        override fun bind(position: Int) {

            imageViewMore?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            bind<RowDiscussionRoomBinding> {
                this.discussionRoom = viewModel.getDiscussionRoom(position)
            }
        }
    }
}
