package com.classera.core

import android.app.Application

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
abstract class BaseAndroidViewModel(val application: Application) : BaseViewModel()
