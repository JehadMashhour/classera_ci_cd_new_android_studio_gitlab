package com.classera.core.utils.android

import android.app.Activity
import android.content.Context
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.IdRes
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import kotlinx.coroutines.*
import java.util.*

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
const val SEARCH_DEBOUNCE = 500L

@Deprecated(
    message = "Please don't use this delegate, use findViewById instead",
    replaceWith = ReplaceWith("findViewById()"),
    level = DeprecationLevel.WARNING
)
fun <T : View> Activity.bindView(@IdRes res: Int): Lazy<T?> {
    return lazy(LazyThreadSafetyMode.NONE) { findViewById<T>(res) }
}

@Deprecated(
    message = "Please don't use this delegate, use findViewById instead",
    replaceWith = ReplaceWith("findViewById()"),
    level = DeprecationLevel.WARNING
)
fun <T : View> Fragment.bindView(@IdRes res: Int): Lazy<T?> {
    return lazy(LazyThreadSafetyMode.NONE) { view?.findViewById<T>(res) }
}

@Deprecated(
    message = "Please don't use this delegate, use findViewById instead",
    replaceWith = ReplaceWith("findViewById()"),
    level = DeprecationLevel.WARNING
)
fun <T : View> View.bindView(@IdRes res: Int): Lazy<T?> {
    return lazy(LazyThreadSafetyMode.NONE) { findViewById<T>(res) }
}

inline fun EditText.onTextChanged(crossinline callback: (CharSequence?) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            // No impl
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            // No impl
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            callback(s)
        }
    })
}

inline fun TextView.onTextChanged(crossinline callback: (CharSequence?) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            // No impl
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            // No impl
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            callback(s)
        }
    })
}

inline fun EditText.onDoneClicked(crossinline callback: () -> Unit) {
    setOnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            callback()
            true
        } else {
            false
        }
    }
}

fun Activity.hideKeyboard() {
    val imm: InputMethodManager =
        getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
}

fun Fragment.hideKeyboard() {
    val imm =
        activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view!!.windowToken, 0)
}

fun View.hideKeyboard(): Boolean {
    try {
        val inputMethodManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (ignored: RuntimeException) {
    }
    return false
}

fun Activity.showKeyboard() {
    val imm: InputMethodManager =
        getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(currentFocus, 0)
}

inline fun View?.keepFocusWhile(crossinline callback: () -> Boolean) {
    this?.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
        if (!hasFocus && callback()) {
            v.requestFocus()
        }
    }
}

inline fun DrawerLayout?.onDrawerClosed(crossinline callback: () -> Unit) {
    this?.addDrawerListener(object : DrawerLayout.DrawerListener {

        override fun onDrawerStateChanged(newState: Int) {
            // No Impl
        }

        override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
            // No Impl
        }

        override fun onDrawerClosed(drawerView: View) {
            callback()
        }

        override fun onDrawerOpened(drawerView: View) {
            // No Impl
        }
    })
}

inline fun SearchView.onDebounceQueryTextChange(
    scope: CoroutineScope = GlobalScope,
    crossinline callback: (String?) -> Unit
) {
    var searchFor: String? = ""
    setOnQueryTextListener(object : SearchView.OnQueryTextListener {

        override fun onQueryTextSubmit(query: String?): Boolean {
            return false
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            val searchText = newText?.trim()
            if (searchText == searchFor) {
                return true
            }

            searchFor = searchText

            scope.launch(Dispatchers.Main) {
                delay(SEARCH_DEBOUNCE)
                if (searchText != searchFor) {
                    return@launch
                }
                callback(newText)
            }
            return true
        }
    })
}

inline fun AutoCompleteTextView.onItemClicked(crossinline callback: (Int) -> Unit) {
    onItemClickListener =
        AdapterView.OnItemClickListener { _, _, position, _ -> callback(position) }
}

inline fun ViewPager2.onPageChanged(crossinline callback: (position: Int) -> Unit) {
    val listener = object : ViewPager2.OnPageChangeCallback() {

        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            callback(position)
        }
    }
    tag = listener
    registerOnPageChangeCallback(listener)
}

fun ViewPager2.setCurrentItemWithoutListener(position: Int) {
    (tag as? ViewPager2.OnPageChangeCallback?)?.let { callback ->
        unregisterOnPageChangeCallback(callback)
        currentItem = position
        registerOnPageChangeCallback(callback)
    }
}

inline fun EditText.onDebounceTextChange(
    scope: CoroutineScope = GlobalScope,
    crossinline callback: (String?) -> Unit
) {
    this.addTextChangedListener(object : TextWatcher {
        private var searchFor = ""

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            val searchText = s.toString().trim()
            if (searchText == searchFor)
                return

            searchFor = searchText

            scope.launch {
                delay(SEARCH_DEBOUNCE)  //debounce timeOut
                if (searchText != searchFor)
                    return@launch
            }
            callback(s.toString())
        }

        override fun afterTextChanged(s: Editable?) = Unit
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
    })
}

fun TimePicker.getTime(minuteInterval: Int = 1): Date {
    val calendar = Calendar.getInstance()
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        calendar.set(Calendar.HOUR_OF_DAY, hour)
        calendar.set(Calendar.MINUTE, minute * minuteInterval)
    } else {
        calendar.set(Calendar.HOUR_OF_DAY, currentHour)
        calendar.set(Calendar.MINUTE, currentMinute * minuteInterval)
    }
    return calendar.time
}

fun TimePicker.setTime(date: Date?, minuteInterval: Int = 1) {
    date?.let {
        val calendar = Calendar.getInstance()
        calendar.time = it
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            hour = calendar.get(Calendar.HOUR_OF_DAY)
            minute = calendar.get(Calendar.MINUTE) / minuteInterval
        } else {
            currentHour = calendar.get(Calendar.HOUR_OF_DAY)
            currentMinute = calendar.get(Calendar.MINUTE) / minuteInterval
        }
    }
}

fun CalendarView.setDate(date: Date?, animate: Boolean, center: Boolean) {
    try {
        date?.let {
            setDate(it.time, animate, center)
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun CalendarView.minDate(date: Date?) {
    date?.let {
        minDate = it.time
    }
}

fun RecyclerView.setItemsClickable(clickable: Boolean) {
    this.isEnabled = clickable
    if (!clickable) {
        val itemTouchListener = object : RecyclerView.OnItemTouchListener {
            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                return !rv.isEnabled
            }

            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
                //Nothing to do
            }

            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
                //Nothing to do
            }

        }
        this.addOnItemTouchListener(itemTouchListener)
        this.tag = itemTouchListener
    } else {
        (this.tag as? RecyclerView.OnItemTouchListener)?.let {
            this.requestDisallowInterceptTouchEvent(true)
            this.removeOnItemTouchListener(it)
        }
    }
}

