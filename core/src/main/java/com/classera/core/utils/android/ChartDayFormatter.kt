package com.classera.core.utils.android

import android.content.Context
import com.classera.core.R
import com.github.mikephil.charting.formatter.ValueFormatter

/**
 * Project: classeraandroidv3
 * Created: Mar 11, 2020
 *
 * @author Odai Nazzal
 */
class ChartDayFormatter(val context: Context) : ValueFormatter() {

    override fun getFormattedValue(value: Float): String {
        return context.getString(Days.values()[value.toInt()].label)
    }
}

enum class Days(val label: Int) {

    SUNDAY(R.string.label_chart_day_sunday),

    MONDAY(R.string.label_chart_day_monday),

    TUESDAY(R.string.label_chart_day_tuesday),

    WEDNESDAY(R.string.label_chart_day_wednesday),

    THURSDAY(R.string.label_chart_day_thursday),

    FRIDAY(R.string.label_chart_day_friday),

    SATURDAY(R.string.label_chart_day_saturday)
}
