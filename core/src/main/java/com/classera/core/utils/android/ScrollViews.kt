package com.classera.core.utils.android

import android.view.View
import androidx.core.widget.NestedScrollView

/**
 * Project: Classera
 * Created: Jan 01, 2020
 *
 * @author Mohamed Hamdan
 */
const val PAGINATION_SCROLL_DIFF = 10

inline fun NestedScrollView.onLoadMore(crossinline callback: () -> Unit) {
    setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { _, _, scrollY, _, _ ->
        val view = getChildAt(childCount - 1) as View
        val diff: Int = view.bottom - (height + scrollY)

        if (diff <= PAGINATION_SCROLL_DIFF) {
            callback()
        }
    })
}
