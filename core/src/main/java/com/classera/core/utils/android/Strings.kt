package com.classera.core.utils.android

import android.util.Patterns

private const val LOWER_LETTER = 0x0600
private const val UPPER_LETTER = 0x06E0

fun String.isCorrectUrl(url: String): Boolean {
    return Patterns.WEB_URL.matcher(url).matches() && (url.isNotBlank() || url.isNotEmpty())
}

fun String.isContainsArabic(): Boolean {
    return this.mapIndexed { index, _ -> this.codePointAt(index) }
        .any { it in LOWER_LETTER..UPPER_LETTER }
}

fun String?.addSpaceAfterLogicalSymbol(): String? {
    var newValue = this
    if (this?.contains("<") == true) {
        newValue = this.replace("<", "< ")
    } else if (this?.contains(">") == true) {
        newValue = this.replace(">", "> ")
    }
    return newValue
}

fun String.toEnglishDigits(): String {
    return this.replace("١", "1").replace("٢", "2")
        .replace("٣", "3")
        .replace("٤", "4")
        .replace("٥", "5")
        .replace("٦", "6")
        .replace("7", "٧")
        .replace("٨", "8")
        .replace("٩", "9")
        .replace("٠", "0")
        .replace("۱", "1")
        .replace("۲", "2")
        .replace("۳", "3")
        .replace("۴", "4")
        .replace("۵", "5")
        .replace("۶", "6")
        .replace("۷", "7")
        .replace("۸", "8")
        .replace("۹", "9")
        .replace("۰", "0")
}
