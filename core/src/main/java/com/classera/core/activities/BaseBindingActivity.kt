package com.classera.core.activities

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
abstract class BaseBindingActivity : BaseToolbarActivity() {

    private var viewDataBinding: ViewDataBinding? = null

    open fun isBindingEnabled(): Boolean {
        return true
    }

    override fun setContentView(layoutResID: Int) {
        if (isBindingEnabled()) {
            viewDataBinding = DataBindingUtil.inflate(layoutInflater, layoutResID, null, false)
            super.setContentView(viewDataBinding?.root)
        } else {
            super.setContentView(layoutResID)
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : ViewDataBinding> bind(binding: T?.() -> Unit) {
        binding(viewDataBinding as? T?)
        viewDataBinding?.executePendingBindings()
    }

    // This injection is a workaround for a known issue in Gradle build system https://github.com/google/dagger/issues/955
    @Inject
    fun setDummy2(dummy: String) {
        // No impl
    }
}
