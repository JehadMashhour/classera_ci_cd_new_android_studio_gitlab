package com.classera.core.custom.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.ArrayRes
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.R

/**
 * Project: Classera
 * Created: Dec 25, 2019
 *
 * @author Mohamed Hamdan
 */
typealias OnFilterSelectedListener = (key: String) -> Unit

class QuickFilterView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyleAttr, defStyleRes) {

    private var onFilterSelectedListener: OnFilterSelectedListener? = null

    private var filtersAdapter: FiltersAdapter? = null
    private var entries: Array<String?>? = null
    private var recyclerViewFilter: RecyclerView? = null

    init {
        View.inflate(context, R.layout.view_filter, this)

        recyclerViewFilter = findViewById(R.id.recycler_view_filter)
    }

    fun setOnFilterSelectedListener(onFilterSelectedListener: OnFilterSelectedListener) {
        this.onFilterSelectedListener = onFilterSelectedListener
        onFilterSelectedListener.invoke(getSelectedFilterKey()!!)
    }

    fun setAdapter(@ArrayRes entries: Int, @ArrayRes entryValues: Int) {
        this.entries = resources.getStringArray(entries)
        val values = context.resources.getStringArray(entryValues)
        setAdapter(this.entries, values)
    }

    fun setAdapter(@ArrayRes entries: Int, entryValues: Array<String?>) {
        this.entries = resources.getStringArray(entries)
        setAdapter(this.entries, entryValues)
    }

    fun setAdapter(entries: Array<String?>?, entryValues: Array<String?>) {
        this.entries = entries
        filtersAdapter = FiltersAdapter(entryValues) {
            onFilterSelectedListener?.invoke(this.entries?.get(it)!!)
        }
        recyclerViewFilter?.adapter = filtersAdapter
    }

    fun setSelectedFilter(entry: String?) {
        filtersAdapter?.selectedPosition = entries?.indexOf(entry) ?: 0
        filtersAdapter?.notifyDataSetChanged()
    }

    fun getSelectedFilterKey(): String? {
        return entries?.getOrNull(filtersAdapter?.selectedPosition ?: 0)
    }

    fun getSelectedFilterPosition(): Int? {
        return filtersAdapter?.selectedPosition
    }
}
