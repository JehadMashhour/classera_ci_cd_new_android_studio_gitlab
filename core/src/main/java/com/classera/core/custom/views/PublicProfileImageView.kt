package com.classera.core.custom.views

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import com.classera.core.Activities
import com.classera.core.intentTo

class PublicProfileImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    var userId: String? = null

    init {
        setOnClickListener {
            if (userId != null) {
                val intent = context.intentTo(Activities.PublicProfile)
                intent.putExtra("userId", userId)
                context.startActivity(intent)
            }
        }
    }
}
