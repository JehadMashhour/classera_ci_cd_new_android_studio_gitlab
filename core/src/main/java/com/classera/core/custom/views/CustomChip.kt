package com.classera.core.custom.views

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.core.view.marginTop
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.chip.Chip
import com.bumptech.glide.request.target.Target
import com.classera.core.R


class CustomChip : Chip {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    /**
     * Set an image from an URL for the [Chip] using [com.bumptech.glide.Glide]
     * @param url icon URL
     * @param errDrawable error icon if Glide return failed response
     */
    fun setIconUrl(url: String, errDrawable: Drawable): CustomChip {
        Glide.with(this)
            .load(url)
            .apply(RequestOptions.circleCropTransform())
            .listener(object : RequestListener<Drawable> {
                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    chipIcon = resource
                    return false
                }

                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    chipIcon = errDrawable
                    return false
                }
            }).preload()
        return this
    }

    companion object {

        fun createImageChip(context: Context, name: String, avatarUrl: String): Chip {
            val chip = CustomChip(context)
            chip.isClickable = false
            chip.text = name
            chip.setIconUrl(avatarUrl, context.resources.getDrawable(R.drawable.ic_error))
            return chip
        }

        fun createCancellableChip(context: Context, name: String, id: String): Chip {
            val chip = CustomChip(context)
            chip.isClickable = true
            chip.isCloseIconVisible = true
            chip.text = name
            chip.tag = id
            return chip
        }

        fun createCheckableChip(context: Context, name: String, id: String): Chip {
            val chip = CustomChip(context)
            chip.isCheckable = true
            chip.isClickable = true
            chip.text = name
            chip.tag = id
            return chip
        }

    }

}
