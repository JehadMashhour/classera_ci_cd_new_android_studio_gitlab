package com.classera.core.custom.views

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.animation.LinearInterpolator
import androidx.core.text.HtmlCompat
import at.blogc.android.views.ExpandableTextView
import com.classera.core.R


/**
 * Project: Classera
 * Created: Feb 11, 2020
 *
 * @author Mohamed Hamdan
 */
@SuppressLint("ClickableViewAccessibility", "SetJavaScriptEnabled")
class HtmlTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = android.R.attr.textViewStyle
) : ExpandableTextView(context, attrs, defStyleAttr) {

    private var readMoreEnabled = false

    init {
        val typedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.HtmlTextView, defStyleAttr, 0)
        readMoreEnabled = typedArray.getBoolean(R.styleable.HtmlTextView_enable_readMore, false)
        if (readMoreEnabled) {
            this.setAnimationDuration(ANIMATION_DURATION)
            this.setInterpolator(LinearInterpolator())
            this.maxLines = MAX_LINE_COUNT
            this.setOnClickListener { this.toggle() }
            typedArray.recycle()
        }
    }

    override fun setText(text: CharSequence?, type: BufferType?) {
        val html = HtmlCompat.fromHtml(text.toString(), HtmlCompat.FROM_HTML_MODE_COMPACT)
        super.setText(html, type)
    }

    private companion object {

        private const val MAX_LINE_COUNT = 3
        private const val ANIMATION_DURATION = 300L
    }
}
