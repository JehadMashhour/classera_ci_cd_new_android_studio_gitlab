package com.classera.core.custom.views

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import com.classera.core.R
import com.classera.core.utils.android.bindView
import com.classera.data.network.errorhandling.NetworkException
import com.classera.data.network.errorhandling.Resource

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class ErrorView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyleAttr, defStyleRes) {

    private var retryListener: (() -> Unit)? = null

    private val textViewMessage: TextView? by bindView(R.id.text_view_error_message)
    private val buttonRetry: Button? by bindView(R.id.button_view_error_retry)

    init {
        View.inflate(context, R.layout.view_error, this)
        visibility = GONE

        buttonRetry?.setOnClickListener {
            retryListener?.invoke()
            visibility = View.GONE
        }
    }

    fun setError(error: Resource.Error) {
        post { visibility = View.VISIBLE }
        if (error.error.message == null) {
            textViewMessage?.setText(error.error.resourceMessage)
        } else {
            textViewMessage?.text = error.error.message
        }

        if (error.error is NetworkException.Update || error.error is NetworkException.ForceUpdate) {
            buttonRetry?.setText(R.string.button_application_update)
            buttonRetry?.setOnClickListener {
                val packageName: String = context.packageName
                try {
                    context.startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=$packageName")
                        )
                    )
                } catch (anfe: ActivityNotFoundException) {
                    val intent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                    )
                    context.startActivity(intent)
                }
            }
        }
    }

    fun setOnRetryClickListener(listener: (() -> Unit)?) {
        this.retryListener = listener
    }
}
