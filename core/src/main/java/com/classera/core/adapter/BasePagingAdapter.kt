package com.classera.core.adapter

import androidx.recyclerview.widget.RecyclerView
import com.paginate.Paginate

/**
 * Project: Classera
 * Created: Dec 25, 2019
 *
 * @author Mohamed Hamdan
 */
typealias OnLoadMoreListener = (page: Int) -> Unit

abstract class BasePagingAdapter<V : BaseViewHolder> : BaseAdapter<V>(), Paginate.Callbacks {

    private var onLoadMoreListener: OnLoadMoreListener? = null
    private var pageNumber: Int = 2
    private var isLoading: Boolean = true
    private var hasLoadedAllItems: Boolean = false

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        Paginate.with(recyclerView, this)
            .addLoadingListItem(false)
            .build()
    }

    fun setOnLoadMoreListener(onLoadMoreListener: OnLoadMoreListener?) {
        this.onLoadMoreListener = onLoadMoreListener
    }

    override fun onLoadMore() {
        isLoading = true
        onLoadMoreListener?.invoke(pageNumber)
        pageNumber++
    }

    override fun isLoading(): Boolean {
        return isLoading
    }

    override fun hasLoadedAllItems(): Boolean {
        return hasLoadedAllItems
    }

    fun finishLoading() {
        isLoading = false
    }

    fun finishPaging() {
        hasLoadedAllItems = true
    }

    fun resetPaging() {
        pageNumber = 2
        isLoading = true
        hasLoadedAllItems = false
    }


    fun isFirstPage() = pageNumber <= 2
}
