package com.classera.selection

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.activities.BaseToolbarActivity
import com.classera.data.models.selection.Selectable
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class MultiSelectionActivity : BaseToolbarActivity() {

    @Inject
    lateinit var viewModel: MultiSelectionViewModel

    private var recyclerViewFilter: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multi_selection)
        findViews()
        initAdapter()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_activity_multi_selection, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_activity_filter_done -> {
                val data = Intent()
                data.putExtra(EXTRA_SELECTED_FILTER, viewModel.getSelectedItems())
                setResult(Activity.RESULT_OK, data)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun findViews() {
        recyclerViewFilter = findViewById(R.id.recycler_view_activity_filter)
    }

    private fun initAdapter() {
        recyclerViewFilter?.adapter = MultiSelectionAdapter(viewModel)
    }

    companion object {

        private const val REQUEST_CODE = 100
        private const val EXTRA_SELECTED_FILTER = "selectedFilter"
        private const val EXTRA_FILTERABLE = "filterable"

        fun start(
            fragment: Fragment,
            filters: Array<out Selectable?>?,
            selectedFilter: Array<Selectable>? = null,
            key: Int = REQUEST_CODE
        ) {
            val intent = Intent(fragment.requireContext(), MultiSelectionActivity::class.java)
            intent.putExtra(EXTRA_FILTERABLE, filters)
            intent.putExtra(EXTRA_SELECTED_FILTER, selectedFilter)
            fragment.startActivityForResult(intent, key)
        }

        fun start(fragment: Fragment, filters: Array<out Selectable?>?, key: Int = REQUEST_CODE) {
            val intent = Intent(fragment.requireContext(), MultiSelectionActivity::class.java)
            intent.putExtra(EXTRA_FILTERABLE, filters)
            fragment.startActivityForResult(intent, key)
        }

        @Suppress("UNCHECKED_CAST")
        internal fun getFilters(activity: AppCompatActivity): Array<Selectable?>? {
            return activity.intent.getParcelableArrayExtra(EXTRA_FILTERABLE)
                ?.map { it as? Selectable? }?.toTypedArray()
        }

        @Suppress("UNCHECKED_CAST")
        internal fun getSelectedFilter(activity: AppCompatActivity): Array<Selectable>? {
            return activity.intent.getParcelableArrayExtra(EXTRA_SELECTED_FILTER)
                ?.map { it as Selectable }
                ?.toTypedArray()
        }

        @Suppress("UNCHECKED_CAST")
        fun getSelectedFilterFromIntent(intent: Intent?): Array<Selectable>? {
            return intent?.getParcelableArrayExtra(EXTRA_SELECTED_FILTER)?.map { it as Selectable }
                ?.toTypedArray()
        }

        fun isDone(
            requestCode: Int,
            resultCode: Int,
            data: Intent?,
            customRequestCode: Int = REQUEST_CODE
        ): Boolean {
            return requestCode == customRequestCode && resultCode == Activity.RESULT_OK
                    && data?.extras?.containsKey(EXTRA_SELECTED_FILTER) == true
        }
    }
}
