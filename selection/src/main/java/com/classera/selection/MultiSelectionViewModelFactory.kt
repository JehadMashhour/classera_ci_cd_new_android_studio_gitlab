package com.classera.selection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.models.selection.Selectable
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class MultiSelectionViewModelFactory @Inject constructor(
    private val filters: Array<out Selectable?>?
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MultiSelectionViewModel(filters) as T
    }
}
