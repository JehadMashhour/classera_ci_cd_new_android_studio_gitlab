package com.classera.selection

import com.classera.core.BaseViewModel
import com.classera.data.models.selection.Selectable

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class MultiSelectionViewModel(private val filters: Array<out Selectable?>?) : BaseViewModel() {

    fun getItemsCount(): Int {
        return filters?.size ?: 0
    }

    fun getSelectable(position: Int): Selectable? {
        return filters?.get(position)
    }

    fun toggleFilter(position: Int) {
        val selectable = filters?.get(position - 1)
        selectable?.selected = selectable?.selected?.not() == true
    }

    fun toggleAll() {
        if (filters?.all { it?.selected == true } == true) {
            filters.forEach { it?.selected = false }
        } else {
            filters?.forEach { it?.selected = true }
        }
    }

    fun getSelectedItems(): Array<out Selectable?>? {
        return filters?.filter { it?.selected == true }?.toTypedArray()
    }
}
