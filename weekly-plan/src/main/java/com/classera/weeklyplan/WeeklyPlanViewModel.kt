package com.classera.weeklyplan

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.add
import com.classera.data.models.BaseWrapper
import com.classera.data.models.weeklyplan.WeeklyPlan
import com.classera.data.models.weeklyplan.WeeklyPlanPreparation
import com.classera.data.models.weeklyplan.WeeklyPlanWrapper
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.weeklyplan.WeeklyPlanRepository
import com.classera.data.toDate
import com.classera.data.toString
import kotlinx.coroutines.Dispatchers
import java.util.*

/**
 * Project: Classera
 * Created: Dec 9, 2019
 * @author Odai Nazzal
 */
class WeeklyPlanViewModel(private val weeklyPlanRepository: WeeklyPlanRepository) : BaseViewModel() {

    var currentWeek: Week? = null
    private var plans: List<WeeklyPlan>? = null

    private var fromDate: String? = null
    private var toDate: String? = null

    fun getWeekDays(week: Week? = null) = liveData(Dispatchers.IO) {
        if (week != null) {
            currentWeek = week
        }
        emit(Resource.Loading(show = true))
        val resource = when (week ?: currentWeek!!) {
            Week.FIRST -> {
                tryResource { weeklyPlanRepository.getUserWeeklyPlan() }
            }
            Week.NEXT -> {
                tryResource {
                    weeklyPlanRepository.getUserWeeklyPlan(
                        from = fromDate
                    )
                }
            }
            Week.PREVIOUS -> {
                tryResource {
                    weeklyPlanRepository.getUserWeeklyPlan(
                        to = toDate
                    )
                }
            }
        }
        plans = resource.element<BaseWrapper<WeeklyPlanWrapper>>()?.data?.plans

        fromDate = plans?.last()?.dayDate.add(Calendar.DAY_OF_MONTH, 1)
        toDate = plans?.first()?.dayDate.add(Calendar.DAY_OF_MONTH, -1)

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getWeekDaysCount(): Int {
        return plans?.size ?: 0
    }

    fun getWeekDay(position: Int): WeeklyPlan? {
        return plans?.get(position)
    }

    fun getWeekDayPreparationCount(position: Int): Int {
        return plans?.get(position)?.preparations?.size ?: 0
    }

    fun getWeekDayPreparation(dayPosition: Int, position: Int): WeeklyPlanPreparation? {
        return plans?.get(dayPosition)?.preparations?.get(position)?.preparation
    }

    enum class Week {

        FIRST,

        NEXT,

        PREVIOUS
    }
}
