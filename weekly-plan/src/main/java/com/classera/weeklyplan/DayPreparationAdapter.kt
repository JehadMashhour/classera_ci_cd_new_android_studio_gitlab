package com.classera.weeklyplan

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.weeklyplan.databinding.RowPreparationBinding

/**
 * Created by Odai Nazzal on 12/28/2019.
 * Classera
 *
 * o.nazzal@classera.com
 */
class DayPreparationAdapter(
    private val viewModel: WeeklyPlanViewModel,
    private val selectedDayPosition: Int
) : BaseAdapter<DayPreparationAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowPreparationBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getWeekDayPreparationCount(selectedDayPosition)
    }

    inner class ViewHolder(binding: RowPreparationBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowPreparationBinding> {
                this.preparation = viewModel.getWeekDayPreparation(selectedDayPosition, position)
            }
        }
    }
}
