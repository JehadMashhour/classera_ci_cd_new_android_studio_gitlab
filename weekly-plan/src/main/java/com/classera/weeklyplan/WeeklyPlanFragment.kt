package com.classera.weeklyplan

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.weeklyplan.WeeklyPlanWrapper
import com.classera.data.network.errorhandling.Resource
import com.classera.weeklyplan.WeeklyPlanViewModel.Week
import com.classera.weeklyplan.databinding.FragmentWeeklyPlanBinding
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 26, 2019
 *
 * @author Mohamed Hamdan
 */
@Screen("Weekly Plan")
class WeeklyPlanFragment : BaseBindingFragment() {

    @Inject
    lateinit var viewModel: WeeklyPlanViewModel

    private var weekDaysAdapter: WeekDaysAdapter? = null
    private var dayPreparationAdapter: DayPreparationAdapter? = null

    private var progressBar: ProgressBar? = null
    private var recyclerViewWeekDays: RecyclerView? = null
    private var recyclerViewPreparations: RecyclerView? = null
    private var imageViewNextWeek: ImageView? = null
    private var imageViewPreviousWeek: ImageView? = null
    private var textViewMonthTitle: TextView? = null
    private var errorView: ErrorView? = null

    override val layoutId: Int = R.layout.fragment_weekly_plan

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
        getWeekDays(Week.FIRST)
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_weekly_plan)
        recyclerViewWeekDays = view?.findViewById(R.id.recycler_view_fragment_weekly_plan_week_days)
        recyclerViewPreparations = view?.findViewById(R.id.recycler_view_fragment_weekly_plan_preparations)
        imageViewNextWeek = view?.findViewById(R.id.image_view_fragment_weekly_plan_next_week)
        imageViewPreviousWeek = view?.findViewById(R.id.image_view_fragment_weekly_plan_previous_week)
        textViewMonthTitle = view?.findViewById(R.id.text_view_fragment_weekly_plan_month_title)
        errorView = view?.findViewById(R.id.error_view_fragment_weekly_plan)
    }

    private fun initListeners() {
        imageViewNextWeek?.setOnClickListener { getWeekDays(Week.NEXT) }
        imageViewPreviousWeek?.setOnClickListener { getWeekDays(Week.PREVIOUS) }
    }

    private fun getWeekDays(week: Week? = null) {
        viewModel.getWeekDays(week).observe(this, ::handleResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleWeekDaysLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleWeekDaysSuccessResource(resource as Resource.Success<BaseWrapper<WeeklyPlanWrapper>>)
            }
            is Resource.Error -> {
                handleWeekDaysErrorResource(resource)
            }
        }
    }

    private fun handleWeekDaysLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            recyclerViewWeekDays?.visibility = View.GONE
            recyclerViewPreparations?.visibility = View.GONE
            imageViewNextWeek?.visibility = View.GONE
            imageViewPreviousWeek?.visibility = View.GONE
            textViewMonthTitle?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            recyclerViewWeekDays?.visibility = View.VISIBLE
            recyclerViewPreparations?.visibility = View.VISIBLE
            imageViewNextWeek?.visibility = View.VISIBLE
            imageViewPreviousWeek?.visibility = View.VISIBLE
            textViewMonthTitle?.visibility = View.VISIBLE
        }
    }

    private fun handleWeekDaysSuccessResource(success: Resource.Success<BaseWrapper<WeeklyPlanWrapper>>) {
        bind<FragmentWeeklyPlanBinding> { this?.plan = success.data?.data }
        initWeekDaysAdapter()
    }

    private fun initWeekDaysAdapter() {
        weekDaysAdapter = WeekDaysAdapter(viewModel)
        recyclerViewWeekDays?.adapter = weekDaysAdapter

        dayPreparationAdapter = DayPreparationAdapter(viewModel, weekDaysAdapter!!.selectedPosition)
        recyclerViewPreparations?.adapter = dayPreparationAdapter

        initWeekDaysAdapterListener()
    }

    private fun initWeekDaysAdapterListener() {
        weekDaysAdapter?.setOnItemClickListener { _, position ->
            dayPreparationAdapter = DayPreparationAdapter(viewModel, position)
            recyclerViewPreparations?.adapter = dayPreparationAdapter
        }
    }

    private fun handleWeekDaysErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getWeekDays() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        weekDaysAdapter = null
        dayPreparationAdapter = null
        progressBar = null
        recyclerViewWeekDays = null
        recyclerViewPreparations = null
        imageViewNextWeek = null
        imageViewPreviousWeek = null
        textViewMonthTitle = null
    }
}
