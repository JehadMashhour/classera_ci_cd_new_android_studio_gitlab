package com.classera.weeklyplan

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.weeklyplan.WeeklyPlanRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class WeeklyPlanViewModelFactory @Inject constructor(
    private val weeklyPlanRepository: WeeklyPlanRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeeklyPlanViewModel(weeklyPlanRepository) as T
    }
}
