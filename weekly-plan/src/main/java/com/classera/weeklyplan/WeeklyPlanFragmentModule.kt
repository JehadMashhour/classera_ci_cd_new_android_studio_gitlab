package com.classera.weeklyplan

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Module
abstract class WeeklyPlanFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: WeeklyPlanViewModelFactory
        ): WeeklyPlanViewModel {
            return ViewModelProvider(fragment, factory)[WeeklyPlanViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: WeeklyPlanFragment): Fragment
}
