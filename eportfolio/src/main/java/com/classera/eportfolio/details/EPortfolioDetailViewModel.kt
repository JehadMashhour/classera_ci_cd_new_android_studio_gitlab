package com.classera.eportfolio.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.BackgroundColor
import com.classera.data.models.BaseWrapper
import com.classera.data.models.eportfolio.details.EPortfolioComment
import com.classera.data.models.eportfolio.details.EPortfolioDetails
import com.classera.data.models.eportfolio.details.EPortfolioDetailsWraper
import com.classera.data.moshi.timeago.TimeAgoAdapter
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.eportfolios.EPortfolioRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.data.toString
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import java.util.*

/**
 * Created by Odai Nazzal on 1/16/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
class EPortfolioDetailViewModel(
    private val ePortfolioId: String?,
    private val ePortfolioRepository: EPortfolioRepository,
    private val userRepository: UserRepository,
    private val argBackgroundColor: BackgroundColor?
) : BaseViewModel() {

    var ePortfolio: EPortfolioDetails? = null
    private var pageNumber: Int = DEFAULT_PAGE

    private var _notifyCommentsAdapterLiveData = MutableLiveData<Unit>()
    var notifyCommentsAdapterLiveData: LiveData<Unit> = _notifyCommentsAdapterLiveData

    private var comments: MutableList<EPortfolioComment> = mutableListOf()

    fun getBackgroundColor() = liveData(Dispatchers.Main) {
        emit(argBackgroundColor)
    }

    fun getDetails() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            ePortfolioRepository.getStudentEPortfolioDetails(ePortfolioId)
        }
        ePortfolio = resource.element<BaseWrapper<EPortfolioDetailsWraper>>()?.data?.eportfolio
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getComments() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            ePortfolioRepository.getEPortfolioComments(
                ePortfolioId,
                pageNumber
            )
        }
        resource.element<BaseWrapper<List<EPortfolioComment>>>()?.data?.let { comments.addAll(it) }
        emit(resource)
        emit(Resource.Loading(show = false))
        if (resource is Resource.Success<*>) {
            pageNumber++
        }
    }

    fun getCommentCount(): Int {
        return comments.size
    }

    fun getComment(position: Int): EPortfolioComment? {
        return comments[position]
    }

    fun addComment(comment: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            val id = System.currentTimeMillis().toString()
            val user = userRepository.getLocalUser().first()?.firstOrNull()
            val ePortfolioComment = EPortfolioComment(
                localId = id,
                text = comment,
                title = comment,
                created = getCreatedAt(),
                fullName = user?.fullName,
                imageUrl = user?.imageUrl
            )
            comments.add(0, ePortfolioComment)

            startCommentRequest(ePortfolioComment)
        }
    }

    private suspend fun startCommentRequest(ePortfolioComment: EPortfolioComment) {
        val index = comments.indexOfFirst { it.localId == ePortfolioComment.localId }
        if (index != -1) {
            comments[index] = ePortfolioComment.copy(isLoading = true, isFailed = false)
            _notifyCommentsAdapterLiveData.postValue(Unit)
            val comment = ePortfolioComment.text
            val resource = tryNoContentResource {
                ePortfolioRepository.addEPortfolioComment(ePortfolioId, comment)
            }

            if (resource is Resource.Error) {
                comments[index] = ePortfolioComment.copy(isLoading = false, isFailed = true)
            } else {
                comments[index] = ePortfolioComment.copy(isLoading = false, isFailed = false)
            }

            _notifyCommentsAdapterLiveData.postValue(Unit)
        }
    }

    private fun getCreatedAt(): String? {
        val date = Date()
        return TimeAgoAdapter().fromJson(date.toString("yyyy-MM-dd HH:mm:ss"))
    }


    fun onCommentRetryClicked(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val attachmentComment = comments[position]
            startCommentRequest(attachmentComment)
        }
    }
}
