package com.classera.eportfolio

import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.core.utils.android.getBackgroundColorBasedOn
import com.classera.eportfolio.databinding.RowEPortfolioBinding

/**
 * Created by Odai Nazzal on 1/14/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
class EPortfolioAdapter(
    private val viewModel: EPortfolioViewModel
) : BasePagingAdapter<EPortfolioAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowEPortfolioBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getEPortfoliosCount()
    }

    inner class ViewHolder(binding: RowEPortfolioBinding) : BaseBindingViewHolder(binding) {

        private var imageViewMore: ImageView? = null
        private var mainImageView: ImageView? = null

        init {
            imageViewMore = itemView.findViewById(R.id.image_view_row_eport_more)
            mainImageView = itemView.findViewById(R.id.imageViewRowEPortfolioImage)
        }

        override fun bind(position: Int) {

            mainImageView?.background =
                context?.getDrawable(getBackgroundColorBasedOn(position)?.resId ?: 0)

            imageViewMore?.setOnClickListener { view ->
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    getOnItemClickListener()?.invoke(view, clickedPosition)
                }
            }

            bind<RowEPortfolioBinding> {
                this.ePortfolio = viewModel.getEPortfolio(position)
                this.ePortfolio?.backgroundColor = getBackgroundColorBasedOn(position)
                this.prefs = viewModel.getSettings()
            }
        }
    }
}
