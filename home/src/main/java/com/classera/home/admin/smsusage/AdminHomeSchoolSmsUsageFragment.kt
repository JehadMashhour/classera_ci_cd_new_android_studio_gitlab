package com.classera.home.admin.smsusage

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.observe
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.data.models.BaseWrapper
import com.classera.data.models.home.admin.SmsUsage
import com.classera.data.network.errorhandling.Resource
import com.classera.home.R
import com.classera.home.admin.AdminHomeViewModel
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.MPPointF
import com.snapics.screenshot.Screenshot
import java.util.*
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@SuppressWarnings("MagicNumber")
class AdminHomeSchoolSmsUsageFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: AdminHomeViewModel

    @Inject
    lateinit var screenshot: Screenshot

    private var progressBarSmsUsage: ProgressBar? = null
    private var imageViewSmsUsageDownload: ImageView? = null
    private var pieChartSmsUsage: PieChart? = null

    override val layoutId: Int = R.layout.fragment_admin_home_sms_usage

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initBarChart()
        initViewModelListeners()
        initViewListeners()
    }

    private fun findViews() {
        progressBarSmsUsage = view?.findViewById(R.id.progress_bar_fragment_home_dashboard_school_sms_usage)
        pieChartSmsUsage = view?.findViewById(R.id.bar_chart_admin_home_school_sms_usage)
        imageViewSmsUsageDownload = view?.findViewById(R.id.image_view_admin_home_school_sms_usage_download)
    }

    private fun initBarChart() {
        pieChartSmsUsage?.setUsePercentValues(true)
        pieChartSmsUsage?.description?.isEnabled = false
        pieChartSmsUsage?.setExtraOffsets(5f, 10f, 30f, 10f)

        pieChartSmsUsage?.dragDecelerationFrictionCoef = 0.95f

        pieChartSmsUsage?.isDrawHoleEnabled = true
        pieChartSmsUsage?.setHoleColor(Color.WHITE)

        pieChartSmsUsage?.setTransparentCircleColor(Color.WHITE)
        pieChartSmsUsage?.setTransparentCircleAlpha(110)

        pieChartSmsUsage?.holeRadius = 58f
        pieChartSmsUsage?.transparentCircleRadius = 61f

        pieChartSmsUsage?.rotationAngle = 0f
        pieChartSmsUsage?.isRotationEnabled = true
        pieChartSmsUsage?.isHighlightPerTapEnabled = true

        pieChartSmsUsage?.animateY(1400, Easing.EaseInOutQuad)

        val legend = pieChartSmsUsage?.legend
        legend?.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        legend?.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        legend?.orientation = Legend.LegendOrientation.VERTICAL
        legend?.setDrawInside(false)
        legend?.xEntrySpace = 7f
        legend?.yEntrySpace = 0f
        legend?.yOffset = 0f

        pieChartSmsUsage?.setEntryLabelColor(Color.WHITE)
        pieChartSmsUsage?.setEntryLabelTextSize(8f)
    }

    private fun initViewModelListeners() {
        viewModel.getSMSUsage().observe(this, this::handleSmsUsageResource)

    }

    private fun initViewListeners() {
        imageViewSmsUsageDownload?.setOnClickListener {
            saveToGallery()
        }
    }

    private fun saveToGallery() {
        screenshot.take(this, pieChartSmsUsage!!)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleSmsUsageResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSmsUsageLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSmsUsageSuccessResource(resource as Resource.Success<BaseWrapper<List<SmsUsage>>>)
            }
            is Resource.Error -> {
                handleSmsUsageErrorResource(resource)
            }
        }
    }

    private fun handleSmsUsageLoadingResource(resource: Resource.Loading) {
        if (resource.show) progressBarSmsUsage?.visibility =
            View.VISIBLE else progressBarSmsUsage?.visibility = View.GONE
    }

    private fun handleSmsUsageSuccessResource(success: Resource.Success<BaseWrapper<List<SmsUsage>>>) {
        success.data?.data?.let { smsUsages ->
            val values = ArrayList<PieEntry>()

            smsUsages.forEach { smsUsage ->
                values.add(
                    PieEntry(
                        smsUsage.value.toFloat(),smsUsage.title,
                        ResourcesCompat.getDrawable(resources, R.drawable.ic_classera_pattern, null)
                    )
                )
            }

            val dataSet =
                PieDataSet(values, getString(R.string.label_admin_home_fragment_school_week_absences))
            dataSet.setDrawIcons(false)

            dataSet.sliceSpace = 3f
            dataSet.iconsOffset = MPPointF(0f, 40f)
            dataSet.selectionShift = 5f

            val colors = ArrayList<Int>()
            colors.add(ResourcesCompat.getColor(resources, R.color.sms_usage_remaining_color, null))
            colors.add(ResourcesCompat.getColor(resources, R.color.sms_usage_consumed_color, null))

            dataSet.colors = colors

            val data = PieData(dataSet)
            data.setValueFormatter(PercentFormatter(pieChartSmsUsage))
            data.setValueTextSize(8f)
            data.setValueTextColor(Color.WHITE)

            pieChartSmsUsage?.data = data

            pieChartSmsUsage?.highlightValues(null)

            pieChartSmsUsage?.invalidate()

        }
    }

    private fun handleSmsUsageErrorResource(resource: Resource.Error) {
        val message = context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(viewModel)
        pieChartSmsUsage = null
        progressBarSmsUsage = null
        imageViewSmsUsageDownload = null
    }
}
