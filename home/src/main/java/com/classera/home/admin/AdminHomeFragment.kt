package com.classera.home.admin

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.FragmentContainerView
import androidx.lifecycle.observe
import com.classera.core.fragments.BaseBindingFragment
import com.classera.home.R
import com.classera.home.databinding.FragmentAdminHomeBinding
import com.google.android.material.appbar.AppBarLayout
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
open class AdminHomeFragment : BaseBindingFragment(), AppBarLayout.OnOffsetChangedListener {

    @Inject
    lateinit var viewModel: AdminHomeViewModel

    private var appBarLayout: AppBarLayout? = null
    private var toolbar: Toolbar? = null
    private var allSchoolsActiveUsers: FragmentContainerView? = null
    private var nestedScrollView: NestedScrollView? = null

    override val layoutId: Int = R.layout.fragment_admin_home

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(viewModel)
        findViews()
        initAppBarLayoutOffsetListener()
        initViewModelListeners()
        initListeners()
        initToolbarTitleTextColor()
    }

    open fun findViews() {
        appBarLayout = view?.findViewById(R.id.app_bar_layout_fragment_home)
        toolbar = view?.findViewById(R.id.toolbar)
        allSchoolsActiveUsers = view?.findViewById(R.id.fragment_admin_home_all_school_active_users)
        nestedScrollView = view?.findViewById(R.id.scroll_view_fragment_admin_home)
    }

    private fun initAppBarLayoutOffsetListener() {
        appBarLayout?.addOnOffsetChangedListener(this)
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        appBarLayout?.post {
            if (appBarLayout.totalScrollRange + verticalOffset == 0) {
                toolbar?.title = getString(R.string.title_fragment_home_toolbar)
            } else {
                toolbar?.title = " "
            }
        }
    }

    private fun initViewModelListeners() {
        viewModel.userLiveData.observe(this) {
            bind<FragmentAdminHomeBinding> {
                this?.user = it
                this?.isGlobalSchool = viewModel.isGlobalSchool()
            }
        }

    }

    private fun initListeners() {
        toolbar?.setNavigationOnClickListener {
            val method = activity?.javaClass?.getDeclaredMethod("openDrawerMenu")
            method?.isAccessible = true
            method?.invoke(activity)
        }
    }

    private fun initToolbarTitleTextColor() {
        val titleTextViewField = Toolbar::class.java.getDeclaredField("mTitleTextView")
        titleTextViewField.isAccessible = true
        val titleTextView = titleTextViewField.get(toolbar) as TextView
        titleTextView.setTextColor(Color.WHITE)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(viewModel)
        appBarLayout?.removeOnOffsetChangedListener(this)
        appBarLayout = null
        toolbar = null
    }

    override fun onResume() {
        super.onResume()
        nestedScrollView?.fullScroll(View.FOCUS_UP)
    }
}
