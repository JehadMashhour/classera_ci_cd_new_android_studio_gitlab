package com.classera.home.admin.weekabsences

import android.os.Build
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.home.admin.AdminHomeViewModel
import com.classera.home.admin.AdminHomeViewModelFactory
import com.classera.storage.Storage
import com.classera.storage.StorageImpl
import com.classera.storage.StorageLegacyImpl
import com.snapics.screenshot.Screenshot
import com.snapics.screenshot.ScreenshotImpl
import com.snapics.screenshot.ScreenshotLegacyImpl
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Module
abstract class AdminHomeWeekAbsencesFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AdminHomeViewModelFactory
        ): AdminHomeViewModel {
            return ViewModelProvider(fragment, factory)[AdminHomeViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideStorage(fragment: Fragment): Storage {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                StorageImpl(fragment.requireActivity())
            } else {
                StorageLegacyImpl(fragment.requireActivity())
            }
        }

        @Provides
        @JvmStatic
        fun provideScreenshot(storage: Storage): Screenshot {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ScreenshotImpl(storage)
            } else {
                ScreenshotLegacyImpl(storage)
            }
        }
    }

    @Binds
    abstract fun bindActivity(activity: AdminHomeSchoolWeekAbsencesFragment): Fragment
}
