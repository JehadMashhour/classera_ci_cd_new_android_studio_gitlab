package com.classera.home.admin.contentstatistic

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.Activities
import com.classera.core.fragments.BaseFragment
import com.classera.core.intentTo
import com.classera.core.utils.android.getStringOrElse
import com.classera.data.models.BaseWrapper
import com.classera.data.models.authentication.settings.GoogleRestrictionSettings
import com.classera.data.models.home.admin.StatisticKey
import com.classera.data.models.user.SchoolSettings
import com.classera.data.network.errorhandling.Resource
import com.classera.home.R
import com.classera.home.admin.AdminHomeViewModel
import com.classera.home.teacher.TeacherHomeFragment
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class AdminHomeContentStatisticsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: AdminHomeViewModel

    private var recyclerViewContentStatistics: RecyclerView? = null
    private var progressBarContentStatistics: ProgressBar? = null

    override val layoutId: Int = R.layout.fragment_admin_home_content_statistics

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initViewModelListeners()
    }

    private fun findViews() {
        recyclerViewContentStatistics =
            view?.findViewById(R.id.recycler_view_admin_home_content_statistics)
        progressBarContentStatistics =
            view?.findViewById(R.id.progress_bar_fragment_home_dashboard_content_statistics)
    }

    private fun initViewModelListeners() {
        viewModel.getContentStatistics().observe(this, this::handleContentStatisticsResource)

    }

    @Suppress("UNCHECKED_CAST")
    private fun handleContentStatisticsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleContentStatisticsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleContentStatisticsSuccessResource()
            }
            is Resource.Error -> {
                handleContentStatisticsErrorResource(resource)
            }
        }
    }

    private fun handleContentStatisticsLoadingResource(resource: Resource.Loading) {
        if (resource.show) progressBarContentStatistics?.visibility =
            View.VISIBLE else progressBarContentStatistics?.visibility = View.GONE
    }

    private fun handleContentStatisticsSuccessResource() {
        val adapter =
            AdminHomeContentStatisticsAdapter(viewModel)
        recyclerViewContentStatistics?.adapter = adapter
    }


    private fun handleContentStatisticsErrorResource(resource: Resource.Error) {
        val message =
            context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        handleForceUpdate(resource)
    }

    private fun handleForceUpdate(resource: Resource.Error) {
        if (resource.error.code == HTTP_CODE_SERVER_FORCE_UPDATE) {
            val packageName: String = requireActivity().packageName
            AlertDialog.Builder(requireContext())
                .setTitle(getString(R.string.title_update_application_dialog))
                .setMessage(getString(R.string.message_update_application_dialog))
                .setPositiveButton(R.string.button_positive_update_application_dialog)
                { _, _ ->
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(
                                "https://play.google.com/store/apps/details?id=$packageName"
                            )
                        )
                    )
                }
                .setCancelable(false)
                .show()
        }
    }

    private companion object {

        private const val HTTP_CODE_SERVER_FORCE_UPDATE = 426
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(viewModel)
        recyclerViewContentStatistics = null
        progressBarContentStatistics = null
    }
}
