package com.classera.home.admin

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseAndroidViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.home.admin.Statistic
import com.classera.data.models.home.admin.TopScoresSchool
import com.classera.data.models.home.admin.TopScoresSchoolGroup
import com.classera.data.models.home.admin.TopSectionScore
import com.classera.data.models.socket.OnlineNow
import com.classera.data.models.switchschools.School
import com.classera.data.models.user.User
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.network.errorhandling.tryThirdPartyResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.home.HomeRepository
import com.classera.data.repositories.home.OnlineNowRepository
import com.classera.data.repositories.switchschools.SwitchSchoolsRepository
import com.classera.data.repositories.user.UserRepository
import io.socket.client.Socket
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlin.collections.List
import kotlin.collections.MutableList
import kotlin.collections.firstOrNull
import kotlin.collections.forEachIndexed
import kotlin.collections.mutableListOf
import kotlin.collections.mutableMapOf
import kotlin.collections.set


/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class AdminHomeViewModel(
    application: Application,
    private val userRepository: UserRepository,
    private val homeRepository: HomeRepository,
    private val prefs: Prefs,
    private val socket: Socket,
    private val switchSchoolsRepository: SwitchSchoolsRepository,
    private val onlineNowRepository: OnlineNowRepository
) : BaseAndroidViewModel(application), LifecycleObserver {

    private var contentStatistics: MutableList<Statistic> = mutableListOf()
    private var schoolTopSections: MutableList<TopSectionScore> = mutableListOf()
    private var schoolActiveUsers: MutableList<TopScoresSchool> = mutableListOf()
    private var schoolGroupActiveUsers: MutableList<TopScoresSchoolGroup> = mutableListOf()
    private var allSchoolActiveUsers: MutableList<TopScoresSchoolGroup> = mutableListOf()
    private var schoolAmbassadors: MutableList<TopScoresSchool> = mutableListOf()
    private var onlineNow: OnlineNow? = null
    private var schools: MutableList<School> = mutableListOf()

    private val _userLiveData = MutableLiveData<User>()
    val userLiveData: LiveData<User> = _userLiveData

    private val _refreshOnlineNowLiveData = MutableLiveData<OnlineNow>()
    val refreshOnlineNowLiveData: LiveData<OnlineNow> = _refreshOnlineNowLiveData

    private val _onlineNowLoadingLiveData = MutableLiveData<Boolean>()
    val onlineNowLoadingLiveData: LiveData<Boolean> = _onlineNowLoadingLiveData


    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        viewModelScope.launch(Dispatchers.IO) {
            userRepository.getLocalUser().collect { users ->
                val user = users?.firstOrNull()
                _userLiveData.postValue(user)

            }

        }

    }

    fun getSchools() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { switchSchoolsRepository.getSwitchSchoolsList() }

        schools.clear()
        schools.addAll(resource.element<BaseWrapper<List<School>>>()?.data ?: mutableListOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getContentStatistics() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryResource { homeRepository.getContentStatistics() }
        contentStatistics.clear()
        contentStatistics.addAll(
            resource.element<BaseWrapper<List<Statistic>>>()?.data ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSchoolActiveSection() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryResource { homeRepository.getTopSectionsScores(prefs.schoolId) }
        schoolTopSections.clear()
        schoolTopSections.addAll(
            resource.element<BaseWrapper<List<TopSectionScore>>>()?.data ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSchoolActiveUsers(roleId: String) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryResource { homeRepository.getTopScoresSchool(roleId) }
        schoolActiveUsers.clear()
        schoolActiveUsers.addAll(
            resource.element<BaseWrapper<List<TopScoresSchool>>>()?.data ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSchoolGroupActiveUsers(roleId: String) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryResource { homeRepository.getTopScoresSchoolGroup(roleId) }
        schoolGroupActiveUsers.clear()
        schoolGroupActiveUsers.addAll(
            resource.element<BaseWrapper<List<TopScoresSchoolGroup>>>()?.data ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getAllSchoolActiveUsers(roleId: String) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryResource { homeRepository.getTopScoresAllSchools(roleId) }
        allSchoolActiveUsers.clear()
        allSchoolActiveUsers.addAll(
            resource.element<BaseWrapper<List<TopScoresSchoolGroup>>>()?.data ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSchoolAmbassadors() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryResource { homeRepository.getAmbassadors() }
        schoolAmbassadors.clear()
        schoolAmbassadors.addAll(
            resource.element<BaseWrapper<List<TopScoresSchool>>>()?.data ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getWeekAbsences() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryResource { homeRepository.getDashboardTotalAbsences() }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSMSUsage() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { homeRepository.getSmsUsage() }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getOnlineNow() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val fieldsMap = mutableMapOf<String, String?>()
        fieldsMap["user_id"] = prefs.userId
        schools.forEachIndexed { index, schoolGroup ->
            schoolGroup?.subSchools?.let {
                it?.forEachIndexed { index, subSchools ->
                    fieldsMap["school_id[$index]"] = subSchools.id
                }
            }
        }
        val resource = tryThirdPartyResource {
            onlineNowRepository.getOnlineNow(fieldsMap)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSchoolSettings() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            userRepository.getSchoolSettings()!!
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun isGlobalSchool(): Boolean {
        val schoolType = prefs.schoolType
        schoolType?.let {
            if (it == GLOBAL_SCHOOL_TYPE)
                return true
        }
        return false
    }

    fun getContentStatisticsCount(): Int {
        return contentStatistics.size
    }

    fun getSchoolActiveSectionCount(): Int {
        return schoolTopSections.size
    }

    fun getContentStatistic(position: Int): Statistic? {
        return contentStatistics[position]
    }

    fun getSchoolActiveSection(position: Int): TopSectionScore? {
        return schoolTopSections[position]
    }

    fun getSchoolActiveusersCount(): Int {
        return schoolActiveUsers.size
    }

    fun getSchoolActiveUser(position: Int): TopScoresSchool? {
        return schoolActiveUsers[position]
    }

    fun getSchoolGroupActiveUsersCount(): Int {
        return schoolGroupActiveUsers.size
    }

    fun getSchoolGroupActiveUser(position: Int): TopScoresSchoolGroup? {
        return schoolGroupActiveUsers[position]
    }

    fun getAllSchoolsActiveUsersCount(): Int {
        return allSchoolActiveUsers.size
    }

    fun getAllSchoolActiveUser(position: Int): TopScoresSchoolGroup? {
        return allSchoolActiveUsers[position]
    }

    fun getSchoolAmbassadorsCount(): Int {
        return schoolAmbassadors.size
    }

    fun getSchoolAmbassador(position: Int): TopScoresSchool? {
        return schoolAmbassadors[position]
    }

    private companion object {
        private const val GLOBAL_SCHOOL_TYPE = "1"
    }

}
