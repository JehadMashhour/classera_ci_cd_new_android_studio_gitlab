package com.classera.home.admin

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.home.HomeRepository
import com.classera.data.repositories.home.OnlineNowRepository
import com.classera.data.repositories.switchschools.SwitchSchoolsRepository
import com.classera.data.repositories.user.UserRepository
import io.socket.client.Socket
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class AdminHomeViewModelFactory @Inject constructor(
    private val application: Application,
    private val userRepository: UserRepository,
    private val homeRepository: HomeRepository,
    private val prefs: Prefs,
    private val socket: Socket,
    private val switchSchoolsRepository: SwitchSchoolsRepository,
    private val onlineNowRepository: OnlineNowRepository

) : ViewModelProvider.AndroidViewModelFactory(application) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AdminHomeViewModel(application, userRepository,
            homeRepository,prefs,socket,switchSchoolsRepository,onlineNowRepository) as T
    }
}
