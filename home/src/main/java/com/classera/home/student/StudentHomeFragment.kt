package com.classera.home.student

import android.content.*
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.transition.AutoTransition
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.dp
import com.classera.core.utils.android.observe
import com.classera.data.glide.GlideApp
import com.classera.data.models.BaseWrapper
import com.classera.data.models.user.UserRole
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.models.vcr.vcrconfirm.VcrConfirmResponse
import com.classera.data.network.errorhandling.Resource
import com.classera.home.R
import com.classera.home.databinding.FragmentStudentHomeBinding
import com.classera.home.teacher.TeacherHomeVcrAdapter
import com.classera.utils.views.dialogs.messagebottomdialog.MessageBottomSheetFragment
import com.google.android.material.appbar.AppBarLayout
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Screen("Home")
open class StudentHomeFragment : BaseBindingFragment(), AppBarLayout.OnOffsetChangedListener {

    @Inject
    lateinit var viewModel: StudentHomeViewModel

    private var appBarLayout: AppBarLayout? = null
    private var imageViewUserIcon: ImageView? = null
    private var recyclerViewShortcuts: RecyclerView? = null
    private var recyclerViewUpcomingVcr: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var toolbar: Toolbar? = null
    private var constraintLayoutHeader: ConstraintLayout? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var textViewRightRatio: TextView? = null
    private var textViewLeftRatio: TextView? = null


    override val layoutId: Int = R.layout.fragment_student_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(viewModel)
        findViews()
        initAppBarLayoutOffsetListener()
        initViewModelListeners()
        initListeners()
        initToolbarTitleTextColor()
        getVirtualClassRooms()
        context?.registerReceiver(userProfilePictureReceiver, IntentFilter("USER_IMAGE_CHANGED"))
    }

    open fun findViews() {
        imageViewUserIcon = view?.findViewById(R.id.image_view_fragment_home_user_icon)
        recyclerViewShortcuts = view?.findViewById(R.id.recycler_view_fragment_home_shortcuts)
        recyclerViewUpcomingVcr = view?.findViewById(R.id.recycler_view_fragment_home_upcoming_vcr)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_home_dashboard)
        appBarLayout = view?.findViewById(R.id.app_bar_layout_fragment_home)
        toolbar = view?.findViewById(R.id.toolbar)
        constraintLayoutHeader = view?.findViewById(R.id.constraint_layout_fragment_home_header)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_dashboard)
        textViewRightRatio = view?.findViewById(R.id.text_view_fragment_home_percent_right)
        textViewLeftRatio = view?.findViewById(R.id.text_view_fragment_home_percent_left)
    }

    private fun initAppBarLayoutOffsetListener() {
        appBarLayout?.addOnOffsetChangedListener(this)
    }



    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        appBarLayout?.post {
            if (appBarLayout.totalScrollRange + verticalOffset == 0) {
                toolbar?.title = getString(R.string.title_fragment_home_toolbar)
            } else {
                toolbar?.title = " "
            }
        }
    }

    private fun initViewModelListeners() {
        viewModel.userProgressLiveData.observe(this) {
            val autoTransition = AutoTransition()
            autoTransition.duration = HEADER_ANIMATION_DURATION
            //TODO This make shaking animation for unknown views, should be enhanced later on
//            TransitionManager.beginDelayedTransition(constraintLayoutHeader, autoTransition)
            val params = imageViewUserIcon?.layoutParams as ConstraintLayout.LayoutParams

            if (viewModel.isRTL()) {
                params.rightMargin = (it.toInt() - USER_ICON_WITH_PLUS_MARGIN.dp)
            } else {
                params.leftMargin = (it.toInt() - USER_ICON_WITH_PLUS_MARGIN.dp)
            }
            imageViewUserIcon?.requestLayout()
        }

        viewModel.userLiveData.observe(this) { user ->
            bind<FragmentStudentHomeBinding> { this?.user = user }
            if (user?.roleId != UserRole.STUDENT) {
                textViewLeftRatio?.visibility = View.GONE
                textViewRightRatio?.visibility = View.GONE
            }
        }
        viewModel.getShortcuts().observe(this, this::handleResource)

        viewModel.directionLiveData.observe(this) {
            val method = activity?.javaClass?.getDeclaredMethod(
                "goToFragment",
                Int::class.java,
                Bundle::class.java
            )
            method?.isAccessible = true
            val bundle = it.second
            method?.invoke(activity, it.first, bundle)
        }

        viewModel.ratioIndicatorVisibility.observe(this) { gravity ->
            if (gravity == Gravity.START) {
                textViewRightRatio?.visibility = View.GONE
                textViewLeftRatio?.visibility = View.VISIBLE
            } else {
                textViewLeftRatio?.visibility = View.GONE
                textViewRightRatio?.visibility = View.VISIBLE
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) = if (resource.show) {
        if (swipeRefreshLayout?.isRefreshing == true) {
            progressBar?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.VISIBLE
        }
    } else {
        progressBar?.visibility = View.GONE
        swipeRefreshLayout?.isRefreshing = false
    }

    private fun handleSuccessResource() {
        val adapter = StudentHomeAdapter(viewModel)
        adapter.setOnItemClickListener { _, position ->
            toolbar?.visibility = View.GONE
            appBarLayout?.visibility = View.GONE
            recyclerViewShortcuts?.visibility = View.GONE
            viewModel.onShortcutClicked(position)
        }
        recyclerViewShortcuts?.adapter = adapter
    }

    private fun handleErrorResource(resource: Resource.Error) {
        Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
        handleForceUpdate(resource)
    }

    private fun handleForceUpdate(resource: Resource.Error) {
        if (resource.error.code == HTTP_CODE_SERVER_FORCE_UPDATE) {
            val packageName: String = requireActivity().packageName
            AlertDialog.Builder(requireContext())
                .setTitle(getString(R.string.title_update_application_dialog))
                .setMessage(getString(R.string.message_update_application_dialog))
                .setPositiveButton(R.string.button_positive_update_application_dialog) { _, _ ->
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(
                                "https://play.google.com/store/apps/details?id=$packageName"
                            )
                        )
                    )
                }
                .setCancelable(false)
                .show()
        }
    }

    private fun initListeners() {
        toolbar?.setNavigationOnClickListener {
            val method = activity?.javaClass?.getDeclaredMethod("openDrawerMenu")
            method?.isAccessible = true
            method?.invoke(activity)
        }

        swipeRefreshLayout?.setOnRefreshListener {
            viewModel.getShortcuts().observe(this, this::handleResource)
        }
    }

    private fun initToolbarTitleTextColor() {
        val titleTextViewField = Toolbar::class.java.getDeclaredField("mTitleTextView")
        titleTextViewField.isAccessible = true
        val titleTextView = titleTextViewField.get(toolbar) as TextView
        titleTextView.setTextColor(Color.WHITE)
    }

    private val userProfilePictureReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            imageViewUserIcon?.let {
                GlideApp.with(requireContext()).load(intent?.getStringExtra("newUserProfileImage"))
                    .circleCrop().into(it)
            }
        }
    }

    private fun getVirtualClassRooms() {
        viewModel.getUpcomingVCRs().observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Loading -> {

                }

                is Resource.Success<*> -> {
                    val vcrList = it.element<BaseWrapper<List<VcrResponse>>>()?.data
                    if (vcrList.isNullOrEmpty()) {
                        recyclerViewUpcomingVcr?.visibility = View.GONE
                        return@observe
                    }

                    recyclerViewUpcomingVcr?.adapter = StudentHomeVcrAdapter(viewModel)
                        .apply {
                            setOnItemClickListener(::onVcrItemClicked)
                        }


                }

                is Resource.Error -> {

                }
            }
        }
    }

    private fun onVcrItemClicked(view: View, position: Int) {
        val vcr = viewModel.getVcr(position)

        viewModel.getUpcomingVcrUrl(vcr?.id!!).observe(viewLifecycleOwner,
            {
                when (it) {
                    is Resource.Loading -> {

                    }

                    is Resource.Success<*> -> {
                        handleSuccessVcrUrl(it)
                    }

                    is Resource.Error -> {
                        MessageBottomSheetFragment.show(
                            fragment = this,
                            image = R.drawable.ic_info,

                            message = it.error.message ?: it.error.cause?.message,
                            buttonLeftText = getString(R.string.close)
                        )
                    }
                }
            })
    }

    private fun handleSuccessVcrUrl(resource: Resource) {
        val successResource = resource.element<BaseWrapper<VcrConfirmResponse>>()?.data
        when {
            successResource?.passcode?.isNotEmpty() == true -> {
                showPasscodeVcrDialog(successResource.passcode, successResource.sessionUrl)
            }
            successResource?.sessionUrl?.isNotEmpty() == true -> {
                showSuccessOrErrorVcrDialog(
                    successResource.sessionUrl,
                    getString(R.string.title_student_vcr_bottom_sheet),
                    getString(R.string.label_button_vcr_confirm_bottom_launch)
                )
            }
            else -> showSuccessOrErrorVcrDialog(
                successResource?.sessionUrl,
                getString(R.string.no_record_found),
                getString(R.string.close)
            )
        }

    }

    private fun showPasscodeVcrDialog(
        passcode: String?,
        sessionUrl: String?
    ) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,
            title = getString(R.string.are_you_sure_you_want_to_start_vcr),
            message = getString(R.string.this_is_your_zoom_virtual_class_passcode) + "<br>"
                    + "<br>" + "<b>" + passcode + "</b>",
            buttonLeftText = getString(R.string.copy_and_continue),
            buttonRightText = getString(R.string.cancel),
        ) {
            when (it) {
                getString(R.string.copy_and_continue) -> {
                    val clipboard: ClipboardManager? =
                        context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
                    val clip =
                        ClipData.newPlainText("label", passcode)
                    clipboard?.setPrimaryClip(clip)

                    val openURL = Intent(Intent.ACTION_VIEW)
                    openURL.data = Uri.parse(sessionUrl)
                    startActivity(openURL)
                }

                getString(R.string.cancel) -> {

                }
            }
        }
    }

    private fun showSuccessOrErrorVcrDialog(
        sessionUrl: String?,
        title: String?,
        buttonLeftText: String?
    ) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,
            title = title,
            buttonLeftText = buttonLeftText
        ) { t ->
            if (t == getString(R.string.label_button_vcr_confirm_bottom_launch)) {
                val openURL = Intent(Intent.ACTION_VIEW)
                openURL.data = Uri.parse(sessionUrl)
                startActivity(openURL)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(viewModel)
        imageViewUserIcon = null
        recyclerViewShortcuts = null
        recyclerViewUpcomingVcr = null
        progressBar = null
        appBarLayout?.removeOnOffsetChangedListener(this)
        appBarLayout = null
        toolbar = null
        constraintLayoutHeader = null
        swipeRefreshLayout = null
        textViewRightRatio = null
        textViewLeftRatio = null
        textViewLeftRatio = null
        textViewRightRatio = null
        context?.unregisterReceiver(userProfilePictureReceiver)
    }

    private companion object {

        private const val USER_ICON_WITH_PLUS_MARGIN = 36
        private const val HEADER_ANIMATION_DURATION = 1000L
        private const val HTTP_CODE_SERVER_FORCE_UPDATE = 426
    }
}
