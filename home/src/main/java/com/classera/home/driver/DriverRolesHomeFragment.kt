package com.classera.home.driver

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.Screen
import com.classera.core.fragments.BaseBindingFragment
import com.classera.data.network.errorhandling.Resource
import com.classera.home.R
import com.classera.home.databinding.FragmentOtherRolesHomeBinding
import com.google.android.material.appbar.AppBarLayout
import javax.inject.Inject

/**
 * Project: Classera
 * Created: 8/16/2021
 *
 * @author Jehad Abdalqader
 */
@Screen("Home")
open class DriverRolesHomeFragment : BaseBindingFragment(), AppBarLayout.OnOffsetChangedListener {

    @Inject
    lateinit var viewModel: DriverRolesHomeViewModel

    private var appBarLayout: AppBarLayout? = null
    private var recyclerViewShortcuts: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var toolbar: Toolbar? = null
    private var textViewScore: AppCompatTextView? = null

    override val layoutId: Int = R.layout.fragment_other_roles_home

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(viewModel)
        findViews()
        initAppBarLayoutOffsetListener()
        initViewModelListeners()
        initListeners()
        initToolbarTitleTextColor()
        hideScoreInfo()
    }

    open fun findViews() {
        recyclerViewShortcuts = view?.findViewById(R.id.recycler_view_fragment_home_shortcuts)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_home_dashboard)
        appBarLayout = view?.findViewById(R.id.app_bar_layout_fragment_home)
        toolbar = view?.findViewById(R.id.toolbar)
        textViewScore = view?.findViewById(R.id.text_view_fragment_home_user_rating)
    }

    private fun initAppBarLayoutOffsetListener() {
        appBarLayout?.addOnOffsetChangedListener(this)
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        appBarLayout?.post {
            if (appBarLayout.totalScrollRange + verticalOffset == 0) {
                toolbar?.title = getString(R.string.title_fragment_home_toolbar)
            } else {
                toolbar?.title = " "
            }
        }
    }

    private fun initViewModelListeners() {
        viewModel.userLiveData.observe(this) { bind<FragmentOtherRolesHomeBinding> { this?.user = it } }
        viewModel.shortcutsLiveData.observe(this) { handleSuccessResource() }
        viewModel.getShortcuts().observe(this, this::handleResource)

        viewModel.directionLiveData.observe(this) { navigationPair ->
            if (navigationPair.first == R.id.item_menu_activity_main_navigation_view_success_partners) {
                openSuccessPartnerApp()
            } else {
                val method = activity?.javaClass?.getDeclaredMethod("goToFragment", Int::class.java, Bundle::class.java)
                method?.isAccessible = true
                val bundle = navigationPair.second
                method?.invoke(activity, navigationPair.first, bundle)
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) progressBar?.visibility = View.VISIBLE else progressBar?.visibility = View.GONE
    }

    private fun handleSuccessResource() {
        val adapter = DriverRolesHomeAdapter(viewModel)
        adapter.setOnItemClickListener { _, position ->
            toolbar?.visibility = View.GONE
            appBarLayout?.visibility = View.GONE
            recyclerViewShortcuts?.visibility = View.GONE
            viewModel.onShortcutClicked(position)
        }
        recyclerViewShortcuts?.adapter = adapter
    }

    private fun handleErrorResource(resource: Resource.Error) {
        Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
        handleForceUpdate(resource)
    }

    private fun handleForceUpdate(resource: Resource.Error) {
        if (resource.error.code == HTTP_CODE_SERVER_FORCE_UPDATE) {
            val packageName: String = requireActivity().packageName
            AlertDialog.Builder(requireContext())
                .setTitle(getString(R.string.title_update_application_dialog))
                .setMessage(getString(R.string.message_update_application_dialog))
                .setPositiveButton(R.string.button_positive_update_application_dialog) { _, _ ->
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(
                                "https://play.google.com/store/apps/details?id=$packageName"
                            )
                        )
                    )
                }
                .setCancelable(false)
                .show()
        }
    }

    private fun initListeners() {
        toolbar?.setNavigationOnClickListener {
            val method = activity?.javaClass?.getDeclaredMethod("openDrawerMenu")
            method?.isAccessible = true
            method?.invoke(activity)
        }
    }

    private fun initToolbarTitleTextColor() {
        val titleTextViewField = Toolbar::class.java.getDeclaredField("mTitleTextView")
        titleTextViewField.isAccessible = true
        val titleTextView = titleTextViewField.get(toolbar) as TextView
        titleTextView.setTextColor(Color.WHITE)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(viewModel)
        recyclerViewShortcuts = null
        progressBar = null
        appBarLayout?.removeOnOffsetChangedListener(this)
        appBarLayout = null
        toolbar = null
    }

    private fun openSuccessPartnerApp() {
        val packageName = "com.classera.successpartner"
        if (isAppInstalled(requireActivity(), packageName))
            startActivity(activity?.packageManager?.getLaunchIntentForPackage(packageName))
        else {
            Toast.makeText(
                requireActivity(),
                "App not installed, Please download Inspire App from Google play.",
                Toast.LENGTH_LONG
            )
                .show()

            val url = "https://play.google.com/store/apps/details?id=com.classera.successpartner"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }

    private fun isAppInstalled(activity: Activity, packageName: String): Boolean {
        val pm = activity.packageManager
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
            Log.e("error", "error can't find")
        }
        return false
    }

    private fun hideScoreInfo() {
        if(viewModel.wasTeacherRole()){
            textViewScore?.visibility = View.GONE
        }
   }

    private companion object {

        private const val HTTP_CODE_SERVER_FORCE_UPDATE = 426
    }
}
