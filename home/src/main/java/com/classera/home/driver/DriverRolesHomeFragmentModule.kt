package com.classera.home.driver

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: 8/16/2021
 *
 * @author Jehad Abdalqader
 */
@Module
abstract class DriverRolesHomeFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: DriverRolesHomeViewModelFactory
        ): DriverRolesHomeViewModel {
            return ViewModelProvider(fragment, factory)[DriverRolesHomeViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: DriverRolesHomeFragment): Fragment
}
