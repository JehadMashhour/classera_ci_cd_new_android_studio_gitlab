package com.classera.home.teacher

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar

import androidx.recyclerview.widget.RecyclerView
import com.classera.core.Screen
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.models.vcr.vcrconfirm.VcrConfirmResponse
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.home.R
import com.classera.home.databinding.FragmentTeacherHomeBinding
import com.classera.home.student.StudentHomeVcrAdapter
import com.classera.utils.views.dialogs.messagebottomdialog.MessageBottomSheetFragment
import com.google.android.material.appbar.AppBarLayout
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Screen("Home")
open class TeacherHomeFragment : BaseBindingFragment(), AppBarLayout.OnOffsetChangedListener {

    @Inject
    lateinit var viewModel: TeacherHomeViewModel

    private var appBarLayout: AppBarLayout? = null
    private var recyclerViewShortcuts: RecyclerView? = null
    private var recyclerViewUpcomingVcr: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var toolbar: Toolbar? = null

    override val layoutId: Int = R.layout.fragment_teacher_home


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(viewModel)
        findViews()
        initAppBarLayoutOffsetListener()
        initViewModelListeners()
        initListeners()
        initToolbarTitleTextColor()
        getUpcomingVCRs()
    }

    open fun findViews() {
        recyclerViewShortcuts = view?.findViewById(R.id.recycler_view_fragment_home_shortcuts)
        recyclerViewUpcomingVcr = view?.findViewById(R.id.recycler_view_fragment_home_upcoming_vcr)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_home_dashboard)
        appBarLayout = view?.findViewById(R.id.app_bar_layout_fragment_home)
        toolbar = view?.findViewById(R.id.toolbar)
    }

    private fun initAppBarLayoutOffsetListener() {
        appBarLayout?.addOnOffsetChangedListener(this)
    }


    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        appBarLayout?.post {
            if (appBarLayout.totalScrollRange + verticalOffset == 0) {
                toolbar?.title = getString(R.string.title_fragment_home_toolbar)
            } else {
                toolbar?.title = " "
            }
        }
    }

    private fun initViewModelListeners() {
        viewModel.userLiveData.observe(this) {
            bind<FragmentTeacherHomeBinding> {
                this?.user = it
            }
        }
        viewModel.getShortcuts().observe(this, this::handleResource)

        viewModel.directionLiveData.observe(this) {
            val method = activity?.javaClass?.getDeclaredMethod(
                "goToFragment",
                Int::class.java,
                Bundle::class.java
            )
            method?.isAccessible = true
            val bundle = it.second
            method?.invoke(activity, it.first, bundle)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) progressBar?.visibility = View.VISIBLE else progressBar?.visibility =
            View.GONE
    }

    private fun handleSuccessResource() {
        val adapter = TeacherHomeAdapter(viewModel)
        adapter.setOnItemClickListener { _, position ->
            viewModel.onShortcutClicked(position)
            toolbar?.visibility = View.GONE
            appBarLayout?.visibility = View.GONE
            recyclerViewShortcuts?.visibility = View.GONE
        }
        recyclerViewShortcuts?.adapter = adapter
    }

    private fun handleErrorResource(resource: Resource.Error) {
        Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
        handleForceUpdate(resource)
    }

    private fun initListeners() {
        toolbar?.setNavigationOnClickListener {
            val method = activity?.javaClass?.getDeclaredMethod("openDrawerMenu")
            method?.isAccessible = true
            method?.invoke(activity)
        }
    }

    private fun initToolbarTitleTextColor() {
        val titleTextViewField = Toolbar::class.java.getDeclaredField("mTitleTextView")
        titleTextViewField.isAccessible = true
        val titleTextView = titleTextViewField.get(toolbar) as TextView
        titleTextView.setTextColor(Color.WHITE)
    }

    private fun handleForceUpdate(resource: Resource.Error) {
        if (resource.error.code == HTTP_CODE_SERVER_FORCE_UPDATE) {
            val packageName: String = requireActivity().packageName
            AlertDialog.Builder(requireContext())
                .setTitle(getString(R.string.title_update_application_dialog))
                .setMessage(getString(R.string.message_update_application_dialog))
                .setPositiveButton(R.string.button_positive_update_application_dialog)
                { _, _ ->
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(
                                "https://play.google.com/store/apps/details?id=$packageName"
                            )
                        )
                    )
                }
                .setCancelable(false)
                .show()
        }
    }

    private fun getUpcomingVCRs() {
        viewModel.getUpcomingVCRs().observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Loading -> {

                }
                is Resource.Success<*> -> {

                    val vcrList = it.element<BaseWrapper<List<VcrResponse>>>()?.data
                    if (vcrList.isNullOrEmpty()) {
                        recyclerViewUpcomingVcr?.visibility = View.GONE
                        return@observe
                    }

                    recyclerViewUpcomingVcr?.adapter = TeacherHomeVcrAdapter(viewModel)
                        .apply {
                            setOnItemClickListener(::onVcrItemClicked)
                        }

                }
                is Resource.Error -> {

                }
            }
        }
    }

    private fun onVcrItemClicked(view: View, position: Int) {
        val vcr = viewModel.getVcr(position)

        viewModel.getUpcomingVcrUrl(vcr?.id!!).observe(viewLifecycleOwner,
            {
                when (it) {
                    is Resource.Loading -> {

                    }

                    is Resource.Success<*> -> {
                        handleSuccessVcrUrl(it)
                    }

                    is Resource.Error -> {
                        MessageBottomSheetFragment.show(
                            fragment = this,
                            image = R.drawable.ic_info,

                            message = it.error.message ?: it.error.cause?.message,
                            buttonLeftText = getString(R.string.close)
                        )
                    }
                }
            })
    }

    private fun handleSuccessVcrUrl(resource: Resource) {
        val successResource = resource.element<BaseWrapper<VcrConfirmResponse>>()?.data
        when {
            successResource?.sessionUrl?.isNotEmpty() == true -> {
                showSuccessOrErrorVcrDialog(
                    successResource.sessionUrl,
                    getString(R.string.title_student_vcr_bottom_sheet),
                    getString(R.string.label_button_vcr_confirm_bottom_launch)
                )
            }
            else -> showSuccessOrErrorVcrDialog(
                successResource?.sessionUrl,
                getString(R.string.no_record_found),
                getString(R.string.close)
            )
        }

    }

    private fun showSuccessOrErrorVcrDialog(
        sessionUrl: String?,
        title: String?,
        buttonLeftText: String?
    ) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,
            title = title,
            buttonLeftText = buttonLeftText
        ) { t ->
            if (t == getString(R.string.label_button_vcr_confirm_bottom_launch)) {
                val openURL = Intent(Intent.ACTION_VIEW)
                openURL.data = Uri.parse(sessionUrl)
                startActivity(openURL)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(viewModel)
        recyclerViewShortcuts = null
        progressBar = null
        appBarLayout?.removeOnOffsetChangedListener(this)
        appBarLayout = null
        toolbar = null
    }

    private companion object {

        private const val HTTP_CODE_SERVER_FORCE_UPDATE = 426
    }
}
