package com.classera.home.other.models

import com.classera.data.models.user.UserRole
import com.classera.home.R

class TeacherSupervisorRoleItems : OtherRolesItem() {

    override val userRole: UserRole
        get() = UserRole.TEACHER_SUPERVISOR

    override val shortcutIcons: Array<Int>
        get() = arrayOf(
            R.drawable.ic_class_visit,
            R.drawable.ic_events
        )

    override val shortcutColors: Array<Int>
        get() = arrayOf(
            R.color.color_take_attendance,
            R.color.color_events
        )

    override val shortcutLabels: Array<Int>
        get() = arrayOf(
            R.string.title_row_dashboard_shortcut_class_visits,
            R.string.title_row_dashboard_shortcut_events
        )

    override val shortcutKeys: Array<String>
        get() = arrayOf(
            "class_visits",
            "events"
        )

    override val directions: Map<String, Int>
        get() = mapOf(
            "class_visits" to R.id.item_menu_activity_main_navigation_view_class_room_visits,
            "events" to R.id.item_menu_activity_main_navigation_view_calendar
        )
}
