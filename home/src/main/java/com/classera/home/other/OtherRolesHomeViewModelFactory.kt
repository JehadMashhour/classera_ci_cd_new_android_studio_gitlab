package com.classera.home.other

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.user.UserRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class OtherRolesHomeViewModelFactory @Inject constructor(
    private val application: Application,
    private val userRepository: UserRepository,
    private val prefs: Prefs
) : ViewModelProvider.AndroidViewModelFactory(application) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return OtherRolesHomeViewModel(application, userRepository, prefs) as T
    }
}
