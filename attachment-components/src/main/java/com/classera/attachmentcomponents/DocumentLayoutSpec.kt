package com.classera.attachmentcomponents

import com.classera.data.models.BackgroundColor
import com.classera.data.models.digitallibrary.Attachment
import com.facebook.litho.Column
import com.facebook.litho.Component
import com.facebook.litho.ComponentContext
import com.facebook.litho.annotations.LayoutSpec
import com.facebook.litho.annotations.OnCreateLayout
import com.facebook.litho.annotations.Prop
import com.facebook.litho.widget.Image
import com.facebook.yoga.YogaAlign
import com.facebook.yoga.YogaEdge
import com.facebook.yoga.YogaPositionType

@LayoutSpec
object DocumentLayoutSpec {

    @JvmStatic
    @OnCreateLayout
    fun onCreateLayout(
        componentContext: ComponentContext,
        @Prop attachment: Attachment?
    ): Component {
        return Column.create(componentContext)
            .alignSelf(YogaAlign.CENTER)
            .child(
                DocumentMount.create(componentContext).imageUrl(attachment?.attachmentUrl)
                    .build()
            )
            .background(
                componentContext.applicationContext.getDrawable(
                    attachment?.backgroundColor?.resId ?: BackgroundColor.GRAY.resId
                )
            ).child(
                Image.create(componentContext)
                    .widthDip(80f)
                    .heightDip(80f)
                    .alignSelf(YogaAlign.FLEX_END)
                    .positionType(YogaPositionType.ABSOLUTE)
                    .marginDip(YogaEdge.END, 25f)
                    .marginDip(YogaEdge.TOP, -12f)
                    .drawableRes(R.drawable.ic_bookmark_60)
                    .build()
            ).child(
                Image.create(componentContext)
                    .widthDip(30f)
                    .heightDip(30f)
                    .positionDip(YogaEdge.TOP, 8f)
                    .positionDip(YogaEdge.END, 20f)
                    .marginDip(YogaEdge.END, 5f)
                    .alignSelf(YogaAlign.FLEX_END)
                    .positionType(YogaPositionType.ABSOLUTE)
                    .drawableRes(R.drawable.ic_doc)
                    .build()
            )

            .build()
    }
}
