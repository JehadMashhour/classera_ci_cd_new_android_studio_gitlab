package com.classera.attachmentcomponents

import android.content.Intent
import android.net.Uri
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.Toast
import com.facebook.litho.ClickEvent
import com.facebook.litho.Column
import com.facebook.litho.Component
import com.facebook.litho.ComponentContext
import com.facebook.litho.annotations.FromEvent
import com.facebook.litho.annotations.LayoutSpec
import com.facebook.litho.annotations.OnCreateLayout
import com.facebook.litho.annotations.OnEvent
import com.facebook.litho.annotations.Prop
import com.facebook.litho.widget.Image
import com.facebook.yoga.YogaAlign
import com.facebook.yoga.YogaEdge
import com.facebook.yoga.YogaPositionType

/**
 * Project: Classera
 * Created: Jan 01, 2020
 *
 * @author Mohamed Hamdan
 */
@LayoutSpec
object PdfViewerLayoutSpec {

    @JvmStatic
    @OnCreateLayout
    fun onCreateLayout(componentContext: ComponentContext, @Prop url: String?): Component {
        return Column.create(componentContext)
            .child(PdfViewerMount.create(componentContext).pdfUrl(url).build())
            .child(
                Image.create(componentContext)
                    .widthDip(24f)
                    .heightDip(24f)
                    .alignSelf(YogaAlign.FLEX_END)
                    .drawableRes(R.drawable.ic_fullscreen)
                    .positionType(YogaPositionType.ABSOLUTE)
                    .positionPercent(YogaEdge.START, 90f)
                    .positionPercent(YogaEdge.TOP, 90f)
                    .clickHandler(PdfViewerLayout.onFullscreenClicked(componentContext))
            )
            .build()
    }

    @OnEvent(ClickEvent::class)
    fun onFullscreenClicked(c: ComponentContext?, @FromEvent view: View?, @Prop url: String?) {
        val intent = Intent(Intent.ACTION_VIEW)
        val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(getFileExtension(url))
        intent.setDataAndType(Uri.parse(url), mimeType)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if (intent.resolveActivity(view?.context?.packageManager!!) != null) {
            view.context?.startActivity(intent)
        } else {
            Toast.makeText(
                view.context
                , view.context?.getString(R.string.toast_attachment_messages_adapter_no_activity_found)
                , Toast.LENGTH_LONG
            )
                .show()
        }
    }

    private fun getFileExtension(url: String?): String? {
        var extension = url?.substring(url.lastIndexOf('.') + 1)
        val lastIndexOfFirstCharacterAfterExtension = extension?.indexOf('%') ?: -1
        if (lastIndexOfFirstCharacterAfterExtension != -1) {
            extension = extension?.substring(0, lastIndexOfFirstCharacterAfterExtension)
        }
        return extension
    }
}
