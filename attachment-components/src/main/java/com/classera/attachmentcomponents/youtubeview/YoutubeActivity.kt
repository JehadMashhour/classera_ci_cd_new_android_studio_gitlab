package com.classera.attachmentcomponents.youtubeview

import android.net.Uri
import android.os.Bundle
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.classera.attachmentcomponents.R
import com.classera.data.BuildConfig
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION
import com.google.android.youtube.player.YouTubePlayerView

/**
 * Created by Rawan Al-Theeb on 23/11/2020.
 * Classera
 * r.altheeb@classera.com
 */
class YoutubeActivity : YouTubeBaseActivity(), YouTubePlayer.OnInitializedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val layout = layoutInflater.inflate(R.layout.activity_youtube, null) as ConstraintLayout
        setContentView(layout)

        initYoutubePlayerView(layout)
    }

    private fun initYoutubePlayerView(layout: ConstraintLayout) {
        val youtubePlayerView = YouTubePlayerView(this)
        youtubePlayerView.layoutParams = ConstraintLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        )
        layout.addView(youtubePlayerView)

        youtubePlayerView.initialize(BuildConfig.flavorData.youtubeKey, this)
    }

    override fun onInitializationSuccess(
        provider: YouTubePlayer.Provider?, youTubePlayer: YouTubePlayer?,
        wasRestored: Boolean
    ) {
        youTubePlayer?.addFullscreenControlFlag(FULLSCREEN_FLAG_CONTROL_ORIENTATION)
        youTubePlayer?.setPlayerStateChangeListener(playerStateChangeListener)
        youTubePlayer?.setPlaybackEventListener(playbackEventListener)

        if (!wasRestored) {
            youTubePlayer?.cueVideo(getVideoIdFrom(getVideoUrl()).toString())
        }
    }

    override fun onInitializationFailure(
        provider: YouTubePlayer.Provider?,
        youTubeInitializationResult: YouTubeInitializationResult?
    ) {
        if (youTubeInitializationResult?.isUserRecoverableError == true) {
            youTubeInitializationResult.getErrorDialog(this, REQUEST_CODE).show()
        }
    }

    private val playbackEventListener =
        object : YouTubePlayer.PlaybackEventListener {
        override fun onSeekTo(p0: Int) {
            // No Impl
        }

        override fun onBuffering(p0: Boolean) {
            // No Impl
        }

        override fun onPlaying() {
            // No Impl
        }

        override fun onStopped() {
            // No Impl
        }

        override fun onPaused() {
            // No Impl
        }
    }

    private val playerStateChangeListener =
        object : YouTubePlayer.PlayerStateChangeListener {
        override fun onAdStarted() {
            // No Impl
        }

        override fun onLoading() {
            // No Impl
        }

        override fun onVideoStarted() {
            // No Impl
        }

        override fun onLoaded(p0: String?) {
            // No Impl
        }

        override fun onVideoEnded() {
            // No Impl
        }

        override fun onError(p0: YouTubePlayer.ErrorReason?) {
            // No Impl
        }
    }

    private fun getVideoUrl() = intent.getStringExtra(YOUTUBE_URL)

    private fun getVideoIdFrom(videoUrl: String?): String? {
        return Uri.parse(videoUrl).getQueryParameter("v")
    }

    companion object {

        private const val REQUEST_CODE = 0
        const val YOUTUBE_URL = "url"
    }
}
