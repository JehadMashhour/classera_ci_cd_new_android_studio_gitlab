package com.classera.attachmentcomponents

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.request.RequestOptions
import com.classera.data.glide.GlideApp
import com.facebook.litho.ComponentContext
import com.facebook.litho.ComponentLayout
import com.facebook.litho.Size
import com.facebook.litho.annotations.MountSpec
import com.facebook.litho.annotations.OnCreateMountContent
import com.facebook.litho.annotations.OnMeasure
import com.facebook.litho.annotations.OnMount
import com.facebook.litho.annotations.Prop
import com.facebook.litho.utils.MeasureUtils

/**
 * Project: Classera
 * Created: Nov 8, 2020
 *
 * @author rawan altheeb
 */
@MountSpec
object WebsiteMountSpec {

    @OnMeasure
    fun onMeasureLayout(c: ComponentContext, layout: ComponentLayout, widthSpec: Int, heightSpec: Int, size: Size) {
        MeasureUtils.measureWithEqualDimens(widthSpec, heightSpec, size)
    }

    @OnCreateMountContent
    fun onCreateMountContent(context: Context): ImageView {
        return ImageView(context)
    }

    @OnMount
    fun onMount(c: ComponentContext, attachmentCoverImageView: ImageView, @Prop imageUrl: String?) {
        GlideApp.with(c.applicationContext).load(imageUrl).apply(RequestOptions().centerCrop().placeholder(null).error(null)).into(attachmentCoverImageView)
    }
}
