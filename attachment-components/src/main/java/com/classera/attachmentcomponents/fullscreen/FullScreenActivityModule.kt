package com.classera.attachmentcomponents.fullscreen

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class FullScreenActivityModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: FullScreenActivityViewModelFactory
        ): FullScreenActivityViewModel {
            return ViewModelProvider(activity, factory)[FullScreenActivityViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: FullScreenActivity): AppCompatActivity
}
