package com.classera.attachmentcomponents

import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.digitallibrary.AttachmentTypes
import com.facebook.litho.Component
import com.facebook.litho.ComponentContext
import com.facebook.litho.annotations.LayoutSpec
import com.facebook.litho.annotations.OnCreateLayout
import com.facebook.litho.annotations.Prop

/**
 * Project: Classera
 * Created: Dec 28, 2019
 *
 * @author Mohamed Hamdan
 */
@LayoutSpec
object AttachmentLayoutSpec {

    private const val AMS_WORKAROUND = "(format=m3u8-cmaf)"

    @JvmStatic
    @OnCreateLayout
    fun onCreateLayout(componentContext: ComponentContext, @Prop attachment: Attachment?): Component {
        return when (attachment?.contentType) {
            AttachmentTypes.PDF -> {
                PdfViewerLayout.create(componentContext).url(attachment.attachmentUrl).build()
            }
            AttachmentTypes.IMAGE -> {
                ImageViewerLayout.create(componentContext).imageUrl(attachment.attachmentUrl).build()
            }
            AttachmentTypes.YOUTUBE -> {
                YoutubeLayout.create(componentContext).youtubeUrl(attachment.attachmentUrl).build()
            }
            AttachmentTypes.VIMEO -> {
                VimeoPlayerMount.create(componentContext).videoUrl(attachment.attachmentUrl).build()
            }
            AttachmentTypes.AUDIO, AttachmentTypes.VIDEO -> {
                MediaPlayerMount.create(componentContext).mediaUrl(attachment.attachmentUrl).attachmentType(attachment.contentType).build()
            }
            AttachmentTypes.STREAM -> {
                AMSMediaPlayerStreamMount.create(componentContext).mediaUrl(
                    attachment.streamUrl + AMS_WORKAROUND
                ).build()
            }
            AttachmentTypes.DOCUMENT -> {
                DocumentLayout.create(componentContext).attachment(attachment).build()
            }
            AttachmentTypes.WEBSITE -> {
                WebsiteLayout.create(componentContext).attachment(attachment).build()
            }
            AttachmentTypes.EXTERNAL_RESOURCES -> {
                MediaPlayerMount.create(componentContext).mediaUrl(attachment.attachmentUrl)
                    .attachmentType(attachment.contentType).build()
            }
            else -> {
                UnknownLayout.create(componentContext).attachment(attachment).build()
            }
        }
    }
}
