package com.calssera.vcr.admin

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.navigation.fragment.findNavController
import com.calssera.vcr.R
import com.calssera.vcr.VcrAdapter
import com.calssera.vcr.VcrFragment
import com.calssera.vcr.VcrViewModel
import com.calssera.vcr.confirmvcr.VCRConfirmViewModel
import com.calssera.vcr.lecturefragment.LectureFragment
import com.calssera.vcr.teacherlist.TeacherListFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.vcr.Lecture
import com.classera.data.models.vcr.teacher.Teacher
import com.classera.data.models.vcr.vcrconfirm.VcrConfirmResponse
import com.classera.data.network.errorhandling.Resource
import com.classera.utils.views.dialogs.messagebottomdialog.MessageBottomSheetFragment
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class VcrAdminFragment : VcrFragment() {

    @Inject
    lateinit var viewModel: VcrAdminViewModel

    @Inject
    lateinit var adapter: VcrAdminAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        initViewModelListeners()
        showAddVcrUI()
    }

    private fun initListeners() {
        floatingActionButtonAdd?.setOnClickListener {
            TeacherListFragment.show(
                fragment = this,
                title = getString(R.string.select_teacher)
            ) { _: String, teacher: Teacher ->
                val direction =
                    VcrAdminFragmentDirections.createVCRActionDirection().apply {
                        this.teacher = teacher
                    }
                findNavController().navigate(direction)
            }
        }

        startMaterialButton?.setOnClickListener {
            val direction = VcrAdminFragmentDirections.createVCRActionDirection()
            findNavController().navigate(direction)
        }

    }

    private fun showAddVcrUI() {
        constraintLayoutAddVCR?.visibility = View.VISIBLE
    }

    override fun getVcrViewModel(): VcrViewModel {
        return viewModel
    }

    override fun getVcrAdapter(): VcrAdapter<*> {
        return adapter
    }

    override fun handleItemClicked(view: View, position: Int) {
        when (view.id) {
            R.id.image_view_row_vcr_more -> {
                handleMoreClicked(view, position)
            }
            else -> {
                val vcr = viewModel.getVcr(position)

                vcr?.run {
                    if (this.isUpComing == true) {
                        getVCRUrl(this.id!!, VCRConfirmViewModel.UPCOMING)
                    } else {
                        getVCRUrl(this.sessionCode!!, VCRConfirmViewModel.PASSED)
                    }
                }
            }
        }
    }

    private fun handleMoreClicked(view: View, position: Int) {
        val menu = PopupMenu(requireContext(), view)
        menu.menuInflater.inflate(R.menu.menu_vcr_teacher_actions, menu.menu)
        menu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.item_menu_vcr_teacher_actions_edit -> {
                    handleEditItemClicked(position)
                }
                R.id.item_menu_vcr_teacher_actions_delete -> {
                    handleDeleteItemClicked(position)
                }
            }
            return@setOnMenuItemClickListener true
        }
        menu.show()
    }

    private fun getVCRUrl(vcrId: String, type: String) {
        viewModel.getVCRURL(vcrId, type).observe(viewLifecycleOwner,
            {
                when (it) {
                    is Resource.Loading -> {

                    }

                    is Resource.Success<*> -> {
                        when (type) {
                            VCRConfirmViewModel.UPCOMING -> {
                                handleUpComingSuccessVcrUrl(it)
                            }

                            VCRConfirmViewModel.PASSED -> {
                                handlePassedSuccessVcrUrl(it)
                            }
                        }

                    }

                    is Resource.Error -> {
                        MessageBottomSheetFragment.show(
                            fragment = this,
                            image = R.drawable.ic_info,

                            message = it.error.message ?: it.error.cause?.message,
                            buttonLeftText = getString(R.string.close)
                        )
                    }
                }
            })
    }

    private fun handleUpComingSuccessVcrUrl(resource: Resource) {
        val successResource = resource.element<BaseWrapper<VcrConfirmResponse>>()?.data
        when {
            successResource?.sessionUrl?.isNotEmpty() == true -> {
                showSuccessOrErrorVcrDialog(
                    successResource.sessionUrl,
                    getString(R.string.title_student_vcr_bottom_sheet),
                    getString(R.string.label_button_vcr_confirm_bottom_launch)
                )
            }
            else -> showSuccessOrErrorVcrDialog(
                successResource?.sessionUrl,
                getString(R.string.no_record_found),
                getString(R.string.close)
            )
        }
    }

    private fun handlePassedSuccessVcrUrl(resource: Resource) {
        val successResource = resource.element<BaseWrapper<VcrConfirmResponse>>()?.data
        when {
            successResource?.sessionUrl?.isNotEmpty() == true -> {
                showSuccessOrErrorVcrDialog(
                    successResource.sessionUrl,
                    getString(R.string.title_student_vcr_bottom_sheet_view),
                    getString(R.string.label_button_vcr_confirm_bottom_view)
                )
            }
            else -> showSuccessOrErrorVcrDialog(
                successResource?.sessionUrl,
                getString(R.string.no_record_found),
                getString(R.string.close)
            )
        }
    }


    private fun showSuccessOrErrorVcrDialog(
        sessionUrl: String?,
        title: String?,
        buttonLeftText: String?
    ) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,
            title = title,
            buttonLeftText = buttonLeftText
        ) { t ->
            if (t == getString(R.string.label_button_vcr_confirm_bottom_launch)
                || t == getString(R.string.label_button_vcr_confirm_bottom_view)
            ) {
                val openURL = Intent(Intent.ACTION_VIEW)
                openURL.data = Uri.parse(sessionUrl)
                startActivity(openURL)
            }
        }
    }

    private fun handleEditItemClicked(position: Int) {
        val vcr = viewModel.getVcr(position)?.copy()
        val title = vcr?.title
        val direction =
            VcrAdminFragmentDirections.addVcrDirection(title).apply {
                this.vendor = null
                this.vcr = vcr
            }
        findNavController().navigate(direction)
    }

    private fun handleDeleteItemClicked(position: Int) {

        AlertDialog.Builder(requireContext())
            .setTitle(R.string.title_delete_vcr_dialog)
            .setMessage(
                getString(
                    R.string.message_delete_vcr_dialog,
                    viewModel.getVcr(position)?.title
                )
            )
            .setPositiveButton(R.string.button_positive_delete_vcr_dialog) { _, _ ->
                viewModel.deleteVcr(
                    position
                )
            }
            .setNegativeButton(R.string.button_negative_delete_vcr_dialog, null)
            .show()
    }

    private fun initViewModelListeners() {
        viewModel.notifyItemRemovedLiveData.observe(this) { adapter.notifyItemRemoved(it) }
    }

}
