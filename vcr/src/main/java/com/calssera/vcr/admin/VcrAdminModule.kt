package com.calssera.vcr.admin

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.calssera.vcr.VcrViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class VcrAdminModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(fragment: Fragment, factory: VcrViewModelFactory): VcrAdminViewModel {
            return ViewModelProvider(fragment, factory)[VcrAdminViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideVcrAdapter(viewModel: VcrAdminViewModel): VcrAdminAdapter {
            return VcrAdminAdapter(viewModel)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: VcrAdminFragment): Fragment
}
