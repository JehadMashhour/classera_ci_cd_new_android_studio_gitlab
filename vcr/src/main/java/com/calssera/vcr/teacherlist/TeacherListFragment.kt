package com.calssera.vcr.teacherlist

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.calssera.vcr.R
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.selection.Selectable
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.models.vcr.teacher.Teacher
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject


class TeacherListFragment : BaseFragment() {

    override val layoutId: Int = R.layout.fragment_teacher_list

    @Inject
    lateinit var teacherListViewModel: TeacherListViewModel

    @Inject
    lateinit var teacherListAdapter: TeacherListAdapter

    @Inject
    lateinit var args: TeacherListFragmentArgs

    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var errorView: ErrorView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        setupTitle()
        setupRecyclerView()
        initToggleListener()
        getTeachers()
    }

    private fun findViews() {
        requireView().run {
            recyclerView = findViewById(R.id.fragment_teacher_list_recycler_view)
            progressBar = findViewById(R.id.fragment_teacher_list_progress_bar)
            errorView = findViewById(R.id.fragment_teacher_list_error_view)
        }
    }

    private fun setupTitle() {
        (activity as AppCompatActivity).supportActionBar?.title =
            args.title
    }

    private fun setupRecyclerView() {
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        recyclerView.adapter = teacherListAdapter
        teacherListAdapter.setOnItemClickListener { view, position ->
            handleItemClicked(view, position)
        }

        Handler(Looper.getMainLooper()).postDelayed({
            args.selectedId?.let { it ->
                teacherListViewModel.getSelectedItemPosition(it)
                    ?.let { recyclerView.smoothScrollToPosition(it + 1) }
            }
        }, DELAY)

    }

    private fun handleItemClicked(view: View, position: Int) {
        setFragmentResult(
            args.key,
            bundleOf(DATA to teacherListViewModel.getTeacher(position))
        )
        findNavController().navigateUp()
    }

    private fun initToggleListener() {
        teacherListViewModel.changedPosition.observe(this) {
            teacherListAdapter.notifyItemChanged(it)
        }
    }


    private fun getTeachers() {
        teacherListViewModel.getTeachers()
            .observe(viewLifecycleOwner, this::handleResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                teacherListAdapter.notifyDataSetChanged()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
        } else {
            progressBar.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView.visibility = View.VISIBLE
        errorView.setError(resource)
        errorView.setOnRetryClickListener { getTeachers() }
    }

    companion object {
        private const val DEFAULT_REQUEST_KEY = "default_request_key"

        const val DELAY = 500L
        const val DATA = "data"

        fun show(
            fragment: Fragment,
            requestKey: String = DEFAULT_REQUEST_KEY,
            title: String?,
            selectedId: String? = null,
            listener: ((requestKey: String, teacher: Teacher) -> Unit)
        ) {
            fragment.setFragmentResultListener(requestKey) { key, bundle ->
                listener(key, bundle.get(DATA) as Teacher)
            }

            fragment.findNavController().navigate(
                R.id.item_menu_activity_main_navigation_view_virtual_class_room_teachers_list, Bundle().apply {
                    putString("key", requestKey)
                    putString("title", title)
                    putString("selectedId", selectedId)
                }
            )
        }

    }
}
