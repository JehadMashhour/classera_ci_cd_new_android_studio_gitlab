package com.calssera.vcr.teacherlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.vcr.VcrRepository
import javax.inject.Inject


class TeacherViewModelFactory @Inject constructor(
    private val vcrRepository: VcrRepository,
    private val args: TeacherListFragmentArgs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TeacherListViewModel(vcrRepository, args) as T
    }
}
