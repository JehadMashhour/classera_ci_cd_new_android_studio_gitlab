package com.calssera.vcr.teacherlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.selection.Selectable
import com.classera.data.models.vcr.Lecture
import com.classera.data.models.vcr.teacher.Teacher
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.vcr.VcrRepository
import kotlinx.coroutines.Dispatchers


class TeacherListViewModel(
    private val vcrRepository: VcrRepository,
    private val args: TeacherListFragmentArgs
) :
    BaseViewModel() {

    private var teachers: MutableList<Teacher>? = mutableListOf()

    var lastSelectedPosition: Int? = null

    private val _changedPosition = MutableLiveData<Int>()
    val changedPosition: LiveData<Int> = _changedPosition

    private var currentPage: Int = DEFAULT_PAGE

    fun getTeachers() = liveData(Dispatchers.IO) {

        emit(Resource.Loading(show = true))

        val resource =
            tryResource { vcrRepository.getTeachers() }

        teachers?.addAll(
            getFilteredList(
                resource.element<BaseWrapper<List<Teacher>>>()?.data ?: mutableListOf()
            )
        )

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private fun getFilteredList(teachers: List<Teacher>): List<Teacher> {
        teachers.map {
            it.apply {
                selected = false
            }
        }.filter {
            it.id == args.selectedId && !args.selectedId.isNullOrEmpty()
        }.map {
            it.apply {
                it.selected = true
            }
        }
        return teachers
    }

    fun getItemsCount(): Int {
        return teachers?.size ?: 0
    }

    fun getTeacher(position: Int): Teacher? {
        return teachers?.get(position)
    }

    fun getTeacher(selectedId: String?): Teacher? {
        return teachers?.find {
            it.id == selectedId
        }
    }

    fun getSelectedItemPosition(selectedId: String?): Int? {
        return teachers?.indexOfFirst { it?.id == selectedId }
    }

    private fun toggleSelected(position: Int) {
        val selectable = teachers?.get(position)
        selectable?.apply {
            selected = selected.not()
        }
        _changedPosition.postValue(position)
    }

    fun toggleClickedSelectable(position: Int?) {
        if (lastSelectedPosition != position) {
            lastSelectedPosition?.let {
                toggleSelected(it)
            }

            position?.let {
                toggleSelected(it)
            }
            lastSelectedPosition = position
        }

    }

}
