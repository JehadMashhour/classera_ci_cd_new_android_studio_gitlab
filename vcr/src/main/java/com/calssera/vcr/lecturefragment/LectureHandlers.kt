package com.calssera.vcr.lecturefragment

import android.view.View

/**
 * Project: Classera
 * Created: 8/2/2021
 *
 * @author Jehad Abdalqader
 */
interface LectureHandlers {
    fun onSelectAllClicked(view: View)
    fun onApplyButtonClicked()
}
