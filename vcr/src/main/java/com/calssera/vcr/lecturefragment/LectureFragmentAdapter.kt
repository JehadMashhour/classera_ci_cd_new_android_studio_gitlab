package com.calssera.vcr.lecturefragment

import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.calssera.vcr.R
import com.calssera.vcr.databinding.RowLectureBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder

class LectureFragmentAdapter(private val lectureFragmentViewModel: LectureFragmentViewModel) :
    BaseAdapter<LectureFragmentAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowLectureBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int = lectureFragmentViewModel.getItemsCount()

    inner class ViewHolder(binding: RowLectureBinding) :
        BaseBindingViewHolder(binding) {

        private lateinit var parentCheckBoxFrameLayout: FrameLayout
        private lateinit var titleTextView: AppCompatTextView
        private lateinit var timeSlotTextView: AppCompatTextView
        private lateinit var preparationTextView: AppCompatTextView

        init {
            findViews()
            initMaterialCheckboxListener()
            initTitleListener()
            initTimeSlotListener()
            initPreparationListener()
        }

        private fun findViews() {
            parentCheckBoxFrameLayout =
                itemView.findViewById(R.id.row_lecture_frameLayout_parent_checkBox)
            titleTextView = itemView.findViewById(R.id.row_lecture_txt_view_title)
            timeSlotTextView = itemView.findViewById(R.id.row_lecture_txt_view_timeSlot)
            preparationTextView = itemView.findViewById(R.id.row_lecture_txt_view_preparation)
        }

        private fun initMaterialCheckboxListener() {
            parentCheckBoxFrameLayout.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    lectureFragmentViewModel.toggleSelected(clickedPosition)
                }
            }
        }

        private fun initTitleListener() {
            titleTextView.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    lectureFragmentViewModel.toggleExpanded(clickedPosition)
                }
            }
        }

        private fun initTimeSlotListener() {
            timeSlotTextView.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    if (lectureFragmentViewModel.getLecture(clickedPosition)?.selected == false) {
                        context?.let {
                            Toast.makeText(
                                it, it.getString(R.string.please_select_the_lecture),
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        return@setOnClickListener
                    }
                    lectureFragmentViewModel.onTimeSlotViewClicked(clickedPosition)
                }
            }
        }

        private fun initPreparationListener() {
            preparationTextView.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    if (lectureFragmentViewModel.getLecture(clickedPosition)?.selected == false) {
                        context?.let {
                            Toast.makeText(
                                it, it.getString(R.string.please_select_the_lecture),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        return@setOnClickListener
                    }
                    lectureFragmentViewModel.onPreparationViewClicked(clickedPosition)
                }
            }
        }

        override fun bind(position: Int) {
            bind<RowLectureBinding> {
                this.lecture = lectureFragmentViewModel.getLecture(position)
            }
        }
    }


}
