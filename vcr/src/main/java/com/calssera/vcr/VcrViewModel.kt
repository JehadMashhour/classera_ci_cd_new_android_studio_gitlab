package com.calssera.vcr

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.user.UserRole
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import kotlinx.coroutines.Dispatchers

/**
 * Project: Classera
 * Created: Jan 20, 2020
 *
 * @author Mohamed Hamdan
 */
abstract class VcrViewModel : BaseViewModel() {

    private var vcrList: MutableList<VcrResponse?> = mutableListOf()
    private var filterKey: String? = null
    open var currentPage: Int = DEFAULT_PAGE

    fun getVirtualClassRooms(
        filterKey: String?,
        pageNumber: Int,
        text: CharSequence?
    ): LiveData<Resource> {
        this.filterKey = filterKey
        return getVirtualClassRooms(filterKey, pageNumber, text, pageNumber == DEFAULT_PAGE)
    }

    private fun getVirtualClassRooms(
        filter: String?,
        pageNumber: Int,
        text: CharSequence?,
        showProgress: Boolean
    ) = liveData(Dispatchers.IO) {
        currentPage = pageNumber
        if (showProgress) {
            emit(Resource.Loading(show = true))
        }

        val resource = tryResource { getRooms(filter, pageNumber, text) }

        if (pageNumber == DEFAULT_PAGE) {
            vcrList.clear()
        }
        vcrList.addAll(
            resource.element<BaseWrapper<List<VcrResponse>>>()?.data ?: mutableListOf()
        )

        vcrList.map {
            it?.isUpComing = filterKey == "0"
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }


    abstract suspend fun getRooms(
        key: String?,
        pageNumber: Int,
        text: CharSequence? = null
    ): BaseWrapper<List<VcrResponse>>

    fun getVcrCount(): Int {
        return vcrList.size
    }

    fun getVcr(position: Int): VcrResponse? {
        return vcrList[position]
    }

    fun deleteItem(position: Int) {
        vcrList.removeAt(position)
    }

    abstract fun deleteVcr(position: Int)

    abstract fun getCurrentRole(): UserRole?
}
