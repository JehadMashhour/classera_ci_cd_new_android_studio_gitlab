package com.calssera.vcr.student

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import com.calssera.vcr.R
import com.calssera.vcr.VcrAdapter
import com.calssera.vcr.VcrFragment
import com.calssera.vcr.VcrViewModel
import com.calssera.vcr.confirmvcr.VCRConfirmViewModel
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.vcr.vcrconfirm.VcrConfirmResponse
import com.classera.data.network.errorhandling.Resource
import com.classera.utils.views.dialogs.messagebottomdialog.MessageBottomSheetFragment
import javax.inject.Inject


/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class VcrStudentFragment : VcrFragment() {

    @Inject
    lateinit var viewModel: VcrStudentViewModel

    @Inject
    lateinit var adapter: VcrStudentAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        floatingActionButtonAdd?.visibility = View.GONE
    }

    override fun getVcrViewModel(): VcrViewModel {
        return viewModel
    }

    override fun getVcrAdapter(): VcrAdapter<*> {
        return adapter
    }

    override fun handleItemClicked(view: View, position: Int) {
        val vcr = viewModel.getVcr(position)

        vcr?.run {
            if (this.isUpComing == true) {
                getVCRUrl(this.id!!, VCRConfirmViewModel.UPCOMING)
            } else {
                getVCRUrl(this.sessionCode!!, VCRConfirmViewModel.PASSED)
            }
        }
    }

    private fun getVCRUrl(vcrId: String, type: String) {
        viewModel.getVCRURL(vcrId, type).observe(viewLifecycleOwner,
            {
                when (it) {
                    is Resource.Loading -> {

                    }

                    is Resource.Success<*> -> {
                        when (type) {
                            VCRConfirmViewModel.UPCOMING -> {
                                handleUpComingSuccessVcrUrl(it)
                            }

                            VCRConfirmViewModel.PASSED -> {
                                handlePassedSuccessVcrUrl(it)
                            }
                        }

                    }

                    is Resource.Error -> {
                        MessageBottomSheetFragment.show(
                            fragment = this,
                            image = R.drawable.ic_info,

                            message = it.error.message ?: it.error.cause?.message,
                            buttonLeftText = getString(R.string.close)
                        )
                    }
                }
            })
    }

    private fun handleUpComingSuccessVcrUrl(resource: Resource) {
        val successResource = resource.element<BaseWrapper<VcrConfirmResponse>>()?.data
        when {
            successResource?.passcode?.isNotEmpty() == true -> {
                showPasscodeVcrDialog(successResource.passcode, successResource.sessionUrl)
            }
            successResource?.sessionUrl?.isNotEmpty() == true -> {
                showSuccessOrErrorVcrDialog(
                    successResource.sessionUrl,
                    getString(R.string.title_student_vcr_bottom_sheet),
                    getString(R.string.label_button_vcr_confirm_bottom_launch)
                )
            }
            else -> showSuccessOrErrorVcrDialog(
                successResource?.sessionUrl,
                getString(R.string.no_record_found),
                getString(R.string.close)
            )
        }
    }

    private fun handlePassedSuccessVcrUrl(resource: Resource) {
        val successResource = resource.element<BaseWrapper<VcrConfirmResponse>>()?.data
        when {
            successResource?.sessionUrl?.isNotEmpty() == true -> {
                showSuccessOrErrorVcrDialog(
                    successResource.sessionUrl,
                    getString(R.string.title_student_vcr_bottom_sheet_view),
                    getString(R.string.label_button_vcr_confirm_bottom_view)
                )
            }
            else -> showSuccessOrErrorVcrDialog(
                successResource?.sessionUrl,
                getString(R.string.no_record_found),
                getString(R.string.close)
            )
        }
    }

    private fun showPasscodeVcrDialog(
        passcode: String?,
        sessionUrl: String?
    ) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,
            title = getString(R.string.are_you_sure_you_want_to_start_vcr),
            message = getString(R.string.this_is_your_zoom_virtual_class_passcode) + "<br>"
                    + "<br>" + "<b>" + passcode + "</b>",
            buttonLeftText = getString(R.string.copy_and_continue),
            buttonRightText = getString(R.string.cancel),
        ) {
            when (it) {
                getString(R.string.copy_and_continue) -> {
                    val clipboard: ClipboardManager? =
                        context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
                    val clip =
                        ClipData.newPlainText("label", passcode)
                    clipboard?.setPrimaryClip(clip)

                    val openURL = Intent(Intent.ACTION_VIEW)
                    openURL.data = Uri.parse(sessionUrl)
                    startActivity(openURL)
                }
            }
        }
    }

    private fun showSuccessOrErrorVcrDialog(
        sessionUrl: String?,
        title: String?,
        buttonLeftText: String?
    ) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,
            title = title,
            buttonLeftText = buttonLeftText
        ) { t ->
            if (t == getString(R.string.label_button_vcr_confirm_bottom_launch)
                || t == getString(R.string.label_button_vcr_confirm_bottom_view)
            ) {
                val openURL = Intent(Intent.ACTION_VIEW)
                openURL.data = Uri.parse(sessionUrl)
                startActivity(openURL)
            }
        }
    }

}
