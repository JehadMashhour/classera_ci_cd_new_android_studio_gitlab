package com.calssera.vcr.teacher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.calssera.vcr.VcrViewModel
import com.calssera.vcr.student.VcrStudentViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.user.UserRole
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.vcr.VcrRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class VcrTeacherViewModel(private val vcrRepository: VcrRepository) : VcrViewModel() {

    override fun getCurrentRole(): UserRole? {
        return null
    }

    private val _notifyItemRemovedLiveData = MutableLiveData<Int>()
    val notifyItemRemovedLiveData: LiveData<Int> = _notifyItemRemovedLiveData

    override suspend fun getRooms(
        key: String?,
        pageNumber: Int,
        text: CharSequence?
    ): BaseWrapper<List<VcrResponse>> {
        return vcrRepository.getTeacherSmartClassrooms(key, pageNumber,text)
    }

    override fun deleteVcr(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val vcr = getVcr(position)
            vcr?.deleting = true

            val sessionCode = vcr?.sessionCode
            val id = vcr?.id
            val resource = tryNoContentResource {
                vcrRepository.deleteVcr(sessionCode, id)
            }
            if (resource.isSuccess()) {
                deleteItem(position)
                _notifyItemRemovedLiveData.postValue(position)
                return@launch
            }
            vcr?.deleting = false
        }
    }


    fun getVcrDetails(id: String?, vendorId: String?): LiveData<Resource> =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = true))
            val resource =
                tryResource { vcrRepository.getVcrDetails(id, vendorId) }
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getVCRURL(vcrId: String, type: String): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        when (type) {
            VcrStudentViewModel.UPCOMING -> {
                val resource = tryResource {
                    vcrRepository.getUpcomingVcrUrl(vcrId)
                }
                emit(resource)
            }
            VcrStudentViewModel.PASSED -> {
                val resource = tryResource {
                    vcrRepository.getPassedVcrUrl(vcrId)
                }
                emit(resource)
            }
        }
        emit(Resource.Loading(show = false))
    }

}
