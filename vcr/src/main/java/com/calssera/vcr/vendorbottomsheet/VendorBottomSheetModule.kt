package com.calssera.vcr.vendorbottomsheet

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.calssera.vcr.VcrViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class VendorBottomSheetModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(fragment: Fragment, factory: VcrViewModelFactory): VendorBottomSheetViewModel {
            return ViewModelProvider(fragment, factory)[VendorBottomSheetViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideVcrAdapter(
            viewModel: VendorBottomSheetViewModel
        ): VendorBottomSheetAdapter {
            return VendorBottomSheetAdapter(viewModel)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: VendorBottomSheetFragment): Fragment
}
