package com.calssera.vcr.confirmvcr


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import com.calssera.vcr.R
import com.classera.core.fragments.BaseBottomSheetDialogFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.vcr.vcrconfirm.VcrConfirmResponse
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject

/**
 * Created by Rawan Al-Theeb on 12/22/2019.
 * Classera
 * r.altheeb@classera.com
 */
class VCRConfirmBottomSheet : BaseBottomSheetDialogFragment() {

    @Inject
    lateinit var viewModel: VCRConfirmViewModel

    private var buttonSubmit: Button? = null
    private var progressBar: ProgressBar? = null
    private var type: String = ""
    private var vcrId: String = ""
    private var buttonText: String = ""
    private var sessionUrl: String = ""
    private var labelViewBottomSheet: TextView? = null

    override val layoutId: Int = R.layout.sheet_vcr_confirm

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.getString(KEY_VCR_TYPE)?.let {
            this.type = it
        }
        arguments?.getString(KEY_VCR_ID)?.let {
            this.vcrId = it
        }
        findViews()
        initListeners()
    }

    private fun findViews() {
        buttonSubmit = view?.findViewById(R.id.button_bottom_sheet_vcr_confirm_submit)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_submit)
        labelViewBottomSheet = view?.findViewById(R.id.text_view_bottom_sheet_vcr_confirm_title)

        when (type) {
            VCRConfirmViewModel.UPCOMING -> {
                buttonText = getString(R.string.label_button_vcr_confirm_bottom_launch)
                labelViewBottomSheet?.text = getString(R.string.title_student_vcr_bottom_sheet)
            }
            VCRConfirmViewModel.PASSED -> {
                buttonText = getString(R.string.label_button_vcr_confirm_bottom_view)
                labelViewBottomSheet?.text = getString(R.string.title_student_vcr_bottom_sheet_view)
            }
        }
    }

    private fun initListeners() {
        buttonSubmit?.setOnClickListener {
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse(sessionUrl)
            startActivity(openURL)
        }

        viewModel.getVCRURL(vcrId, type).observe(this, this::handleResource)
    }


    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<VcrConfirmResponse>>)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            buttonSubmit?.text = null
            buttonSubmit?.isEnabled = false
        } else {
            progressBar?.visibility = View.GONE
            buttonSubmit?.text = buttonText
        }
    }

    private fun handleSuccessResource(success: Resource.Success<BaseWrapper<VcrConfirmResponse>>) {
        progressBar?.visibility = View.GONE
        buttonSubmit?.setText(R.string.label_button_vcr_confirm_bottom_launch)
        buttonSubmit?.isEnabled = true

        success.data?.data?.sessionUrl?.let {
            sessionUrl = it
        }

        if (sessionUrl.isEmpty()) {
            buttonSubmit?.isEnabled = false
            Toast.makeText(context, getString(R.string.no_record_found) ?: "", Toast.LENGTH_LONG).show()
        } else {
            buttonSubmit?.isEnabled = true
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        buttonSubmit?.visibility = View.INVISIBLE
        labelViewBottomSheet?.text = resource.error.message
    }

    companion object {

        private const val KEY_VCR_TYPE = "vcrType"
        private const val KEY_VCR_ID = "vcrId"

        fun show(fragmentManager: FragmentManager, type: String?, id: String?) {
            val vcrBottomSheetDialogFragment = VCRConfirmBottomSheet()
            vcrBottomSheetDialogFragment.arguments = bundleOf(
                KEY_VCR_TYPE to type,
                KEY_VCR_ID to id
            )
            vcrBottomSheetDialogFragment.show(fragmentManager, "")
        }
    }
}
