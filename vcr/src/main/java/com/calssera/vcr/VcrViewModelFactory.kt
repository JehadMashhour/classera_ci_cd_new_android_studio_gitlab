package com.calssera.vcr

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.calssera.vcr.confirmvcr.VCRConfirmViewModel
import com.calssera.vcr.student.VcrStudentViewModel
import com.calssera.vcr.vendorbottomsheet.VendorBottomSheetViewModel
import com.calssera.vcr.admin.VcrAdminViewModel
import com.calssera.vcr.teacher.VcrTeacherViewModel

import com.classera.data.prefs.Prefs
import com.classera.data.repositories.lectures.LecturesRepository
import com.classera.data.repositories.vcr.VcrRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */
class VcrViewModelFactory @Inject constructor(
    private val vcrRepository: VcrRepository,
    private val lecturesRepository: LecturesRepository,
    private val prefs: Prefs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            VcrStudentViewModel::class.java -> {
                VcrStudentViewModel(vcrRepository,prefs) as T
            }
            VcrTeacherViewModel::class.java -> {
                VcrTeacherViewModel(vcrRepository) as T
            }
            VcrAdminViewModel::class.java -> {
                VcrAdminViewModel(vcrRepository) as T
            }
            VCRConfirmViewModel::class.java -> {
                VCRConfirmViewModel(vcrRepository) as T
            }
            VendorBottomSheetViewModel::class.java -> {
                VendorBottomSheetViewModel(vcrRepository) as T
            }
            else -> {
                throw IllegalAccessException("There is no view model called $modelClass")
            }
        }
    }
}
