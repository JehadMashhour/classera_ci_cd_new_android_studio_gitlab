package com.classera.digitallibrary

import android.app.Application
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseAndroidViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.courses.Course
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.filter.Filterable
import com.classera.data.models.rating.RatingResult
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.courses.CoursesRepository
import com.classera.data.repositories.digitallibrary.DigitalLibraryRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class DigitalLibraryViewModel(
    application: Application,
    private val digitalLibraryRepository: DigitalLibraryRepository,
    private val attachmentRepository: AttachmentRepository,
    private val coursesRepository: CoursesRepository,
    private val type: String?
) : BaseAndroidViewModel(application) {

    private var filters: MutableList<Course> = mutableListOf()
    private val _notifyAdapterItemLiveData = MutableLiveData<Int>()
    val notifyAdapterItemLiveData: LiveData<Int> = _notifyAdapterItemLiveData

    private val _toastLiveData = MutableLiveData<Any>()
    val toastLiveData: LiveData<Any> = _toastLiveData

    private var libraries: MutableList<Attachment> = mutableListOf()

    private var job = Job()

    var courseId: String? = null
    private var newLikeCount : Int? = null

    fun getDigitalLibrary(
        text: CharSequence?,
        filterKey: String?,
        courseId: String? = null,
        pageNumber: Int
    ): LiveData<Resource> {
        return getDigitalLibrary(text, filterKey, pageNumber, pageNumber == DEFAULT_PAGE, courseId)
    }

    fun refreshDigitalLibrary(text: CharSequence?, key: String?) =
        getDigitalLibrary(text, key, DEFAULT_PAGE, false, courseId)

    private fun getDigitalLibrary(
        text: CharSequence?,
        filter: String?,
        pageNumber: Int,
        showProgress: Boolean,
        courseId: String? = null
    ): LiveData<Resource> {

        job.cancel()
        job = Job()

        return liveData(CoroutineScope(job + Dispatchers.IO).coroutineContext) {

            if (showProgress) {
                emit(Resource.Loading(show = true))
            }

            val resource = tryResource {
                if (type == null) {
                    digitalLibraryRepository.getStudentLibrary(pageNumber, filter = filter, text = text)
                } else {
                    digitalLibraryRepository.getStudentAttachments(
                        pageNumber,
                        filter = type,
                        courseId = courseId,
                        text = text
                    )
                }
            }
            if (pageNumber == DEFAULT_PAGE) {
                libraries.clear()
            }
            libraries.addAll(resource.element<BaseWrapper<List<Attachment>>>()?.data ?: mutableListOf())
            emit(resource)
            emit(Resource.Loading(show = false))
        }
    }

    fun getLibrary(position: Int): Attachment? {
        return libraries[position]
    }

    fun getLibrariesCount(): Int {
        return libraries.size
    }

    fun onLikeClicked(position: Int) {
        var likedLibrary = libraries[position]
        newLikeCount = likedLibrary.likesCount
        likedLibrary.liked = !likedLibrary.liked
        handelLibraryLike(likedLibrary)

        _notifyAdapterItemLiveData.postValue(position)

        viewModelScope.launch {
            val resource = tryNoContentResource {
                attachmentRepository.addLike(likedLibrary.id)
            }

            if (resource is Resource.Error) {
                likedLibrary.liked = !likedLibrary.liked
                handelLibraryLike(likedLibrary)

                if (resource.error.resourceMessage != null) {
                    _toastLiveData.postValue(resource.error.message)
                } else {
                    _toastLiveData.postValue(resource.error.resourceMessage)
                }
            }
            sendLikeBroadcast(likedLibrary)
            _notifyAdapterItemLiveData.postValue(position)
        }
    }

    private fun handelLibraryLike(likedLibrary: Attachment) {
        newLikeCount = if (likedLibrary.liked){
            newLikeCount?.plus(1)
        } else {
            newLikeCount?.minus(1)
        }
    }

    private fun sendLikeBroadcast(likedLibrary: Attachment) {
        val intent = Intent("LIKE_CHANGED")
        intent.putExtra("new_count", newLikeCount)
        intent.putExtra("is_liked", likedLibrary.liked)
        application.sendBroadcast(intent)
    }

    fun onSubmitRatingClicked(position: Int, rating: Float) {
        val library = libraries[position]
        viewModelScope.launch {
            val resource = tryResource {
                attachmentRepository.addRate(library.id, rating.toString(), library.userId)
            }
            if (resource is Resource.Error) {
                if (resource.error.resourceMessage != null) {
                    _toastLiveData.postValue(resource.error.message)
                } else {
                    _toastLiveData.postValue(resource.error.resourceMessage)
                }
            }
            getLibrary(position)?.rate = resource.element<BaseWrapper<RatingResult>>()?.data?.newRating
            getLibrary(position)?.isRated = true
            getLibrary(position)?.isRate = true
            _notifyAdapterItemLiveData.value = position
        }
    }

    fun getCourses() = liveData(Dispatchers.IO) {

        emit(Resource.Loading(show = true))

        val resource =
            tryResource { coursesRepository.getCourses() }

        filters.clear()
        filters.addAll(resource.element<BaseWrapper<List<Course>>>()?.data ?: mutableListOf())
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getFilters(): Array<out Filterable> {
        return filters.toTypedArray()
    }

    fun increaseViewsCount(clickedPosition: Int) {
        if (clickedPosition == -1) {
            return
        }
        _notifyAdapterItemLiveData.value = clickedPosition
        libraries[clickedPosition].increaseViewsCount()
    }

    fun getSelectedFilter(): Filterable? {
        return filters.firstOrNull { it.filterId == courseId }
    }
}
