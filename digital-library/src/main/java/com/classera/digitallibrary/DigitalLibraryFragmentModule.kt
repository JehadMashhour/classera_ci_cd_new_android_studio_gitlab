package com.classera.digitallibrary

import android.app.Application
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.courses.CoursesRepository
import com.classera.data.repositories.digitallibrary.DigitalLibraryRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class DigitalLibraryFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: DigitalLibraryViewModelFactory
        ): DigitalLibraryViewModel {
            return ViewModelProvider(fragment, factory)[DigitalLibraryViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideDigitalLibraryViewModelFactory(
            application: Application,
            fragment: Fragment,
            digitalLibraryRepository: DigitalLibraryRepository,
            attachmentRepository: AttachmentRepository,
            coursesRepository: CoursesRepository
        ): DigitalLibraryViewModelFactory {
            val type = fragment.arguments?.getString("type")
            return DigitalLibraryViewModelFactory(
                application,
                digitalLibraryRepository,
                attachmentRepository,
                coursesRepository,
                type
            )
        }
    }

    @Binds
    abstract fun bindActivity(activity: DigitalLibraryFragment): Fragment
}
