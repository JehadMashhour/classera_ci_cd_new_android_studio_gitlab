package com.classera.digitallibrary

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.courses.CoursesRepository
import com.classera.data.repositories.digitallibrary.DigitalLibraryRepository

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class DigitalLibraryViewModelFactory(
    private val application: Application,
    private val digitalLibraryRepository: DigitalLibraryRepository,
    private val attachmentRepository: AttachmentRepository,
    private val coursesRepository: CoursesRepository,
    private val type: String?
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DigitalLibraryViewModel(
            application,
            digitalLibraryRepository,
            attachmentRepository,
            coursesRepository,
            type
        ) as T
    }
}
