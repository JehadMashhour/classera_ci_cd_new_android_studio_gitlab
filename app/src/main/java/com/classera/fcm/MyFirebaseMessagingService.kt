package com.classera.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import com.classera.R
import com.classera.core.Activities
import com.classera.data.models.notification.NotificationData
import com.classera.data.models.notification.NotificationMessage
import com.classera.data.prefs.Prefs
import com.classera.splash.SplashActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import dagger.android.AndroidInjection
import javax.inject.Inject


/**
 * Project: Classera
 * Created: Mar 04, 2020
 *
 * @author Mohamed Hamdan
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {


    @Inject
    lateinit var prefs: Prefs

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (prefs.notificationEnabled) {
            val jsonData = Gson().toJson(remoteMessage.data)
            val notificationData = Gson().fromJson(jsonData, NotificationData::class.java)
            val notificationMessage =
                Gson().fromJson(notificationData.message, NotificationMessage::class.java)
            val notificationBody = Gson().toJson(notificationMessage.body)
            val notificationUUID = Gson().toJson(notificationMessage.uuid)
            sendNotification(notificationMessage, notificationData.title, notificationBody, notificationUUID)
        }
    }

    private fun sendNotification(
        notificationMessage: NotificationMessage,
        title: String?,
        notificationBody: String?,
        notificationUUID: String?
    ) {
        val notificationBuilder =
            NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(title)
                .setAutoCancel(true)
                .setContentIntent(getPendingIntent(notificationMessage.event, notificationBody, notificationUUID))

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        initNotificationChannel(notificationManager)

        notificationManager.notify(
            (NOTIFICATION_ID_START_RANGE..NOTIFICATION_ID_END_RANGE).random(),
            notificationBuilder.build()
        )
    }

    private fun initNotificationChannel(notificationManager: NotificationManager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = getString(R.string.default_notification_channel_id)
            val channelName = getString(R.string.default_notification_channel_name)
            val channel = NotificationChannel(
                channelId,
                channelName,
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun getPendingIntent(event: String?, notificationBody: String?, notificationUUID: String?): PendingIntent {
        val intent = Intent(this, SplashActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            putExtra(Activities.Splash.NOTIFICATION_DATA_EVENT_EXTRA, event)
            putExtra(Activities.Splash.NOTIFICATION_DATA_BODY_EXTRA, notificationBody)
            putExtra(Activities.Splash.NOTIFICATION_DATA_UUID_EXTRA, notificationUUID)
        }

        return PendingIntent.getActivity(
            this,
            (NOTIFICATION_ID_START_RANGE..NOTIFICATION_ID_END_RANGE).random(),
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private companion object {

        private const val NOTIFICATION_ID_START_RANGE = 0
        private const val NOTIFICATION_ID_END_RANGE = 100
    }
}
