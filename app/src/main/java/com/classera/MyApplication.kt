package com.classera

import android.util.Log.VERBOSE
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import com.classera.data.binding.BindingAdapters
import com.classera.di.DaggerAppComponent
import com.facebook.soloader.SoLoader
import com.flurry.android.FlurryAgent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Inject


/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class MyApplication : DaggerApplication() {

    @Inject
    lateinit var bindingAdapters: BindingAdapters

    override fun onCreate() {
        super.onCreate()
        SoLoader.init(this, false)
        DataBindingUtil.setDefaultComponent(object : DataBindingComponent {

            override fun getBindingAdapters(): BindingAdapters {
                return this@MyApplication.bindingAdapters
            }
        })

        FlurryAgent.Builder()
            .withLogEnabled(true)
            .withCaptureUncaughtExceptions(true)
            .withLogLevel(VERBOSE)
            .build(this, FLURRY_KEY)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder()
            .application(this)
            .build()
            .apply {
                inject(this@MyApplication)
            }
    }

    companion object {
        private const val FLURRY_KEY = "CWMHWGC5R8WTPX57PXCW"
    }
}
