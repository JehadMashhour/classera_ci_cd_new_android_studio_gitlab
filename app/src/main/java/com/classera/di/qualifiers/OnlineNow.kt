package com.classera.di.qualifiers

import javax.inject.Qualifier

/**
 * Project: Classera
 * Created: Jan 11, 2021
 *
 * @author Saeed Halawani
 */
@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class OnlineNow
