package com.classera.di

import com.classera.data.add
import com.classera.data.models.assignments.AssignmentStatus
import com.classera.data.models.assignments.QuestionType
import com.classera.data.models.callchildren.CallStatusTypes
import com.classera.data.models.chat.MessageTypeEnum
import com.classera.data.models.digitallibrary.AttachmentTypes
import com.classera.data.models.reportcards.ReportCardType
import com.classera.data.models.user.UserRole
import com.classera.data.models.vcr.sharewith.ShareWithStatusTypes
import com.classera.data.moshi.enums.EnumsAdapter
import com.squareup.moshi.Moshi

/**
 * Project: Classera
 * Created: 8/18/2021
 *
 * @author Jehad Abdalqader
 */


fun Moshi.Builder.addMyEnumAdapters(): Moshi.Builder {
    return add(
        AssignmentStatus::class,
        EnumsAdapter.create(AssignmentStatus::class, AssignmentStatus.UNKNOWN)
    ).add(
        AttachmentTypes::class,
        EnumsAdapter.create(AttachmentTypes::class, AttachmentTypes.UNKNOWN)
    ).add(
        MessageTypeEnum::class,
        EnumsAdapter.create(MessageTypeEnum::class, MessageTypeEnum.UNKNOWN)
    ).add(
        UserRole::class, EnumsAdapter.create(UserRole::class, UserRole.UNKNOWN)
    ).add(
        ReportCardType::class,
        EnumsAdapter.create(ReportCardType::class, ReportCardType.UNKNOWN)
    ).add(
        QuestionType::class, EnumsAdapter.create(QuestionType::class, QuestionType.UNKNOWN)
    ).add(
        ShareWithStatusTypes::class,
        EnumsAdapter.create(ShareWithStatusTypes::class, ShareWithStatusTypes.UNKNOWN)
    ).add(
        ShareWithStatusTypes::class,
        EnumsAdapter.create(ShareWithStatusTypes::class, ShareWithStatusTypes.UNKNOWN)
    ).add(
        CallStatusTypes::class,
        EnumsAdapter.create(CallStatusTypes::class, CallStatusTypes.UNKNOWN)
    )
}
