package com.classera.mycard

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Rawan Altheeb
 */
@Module
abstract class MyCardFragmentModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: MyCardViewModelFactory
        ): MyCardViewModel {
            return ViewModelProvider(fragment, factory)[MyCardViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: MyCardFragment): Fragment
}
