package com.classera.mycard

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.observe
import com.classera.core.Screen
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.data.models.BaseWrapper
import com.classera.data.models.mycard.MyCardResponse
import com.classera.data.network.errorhandling.Resource
import com.classera.mycard.databinding.FragmentMyCardBinding
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Rawan Altheeb
 */
@Screen("My Card")
class MyCardFragment : BaseBindingFragment() {

    @Inject
    lateinit var viewModel: MyCardViewModel

    private var progressBar: ProgressBar? = null

    override val layoutId: Int = R.layout.fragment_my_card

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        viewModel.getUserCard().observe(this, this::handleResource)
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_my_card)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<MyCardResponse>>)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleSuccessResource(success: Resource.Success<BaseWrapper<MyCardResponse>>) {
        success.data?.data?.classeraCardsUsers?.let {
            bind<FragmentMyCardBinding> {
                this?.card = it
            }
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        val message =
            context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}
