package com.classera.mycard

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.mycard.MyCradRepository
import kotlinx.coroutines.Dispatchers

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Rawan Altheeb
 */
class MyCardViewModel(
    private val myCardRepository: MyCradRepository
) : BaseViewModel() {

    fun getUserCard(): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { myCardRepository.getUserCard() }
        emit(resource)
        emit(Resource.Loading(show = false))
    }
}
