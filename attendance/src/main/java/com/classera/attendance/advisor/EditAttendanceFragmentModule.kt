package com.classera.attendance.advisor

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Aug 15, 2020
 *
 * @author AhmeDroid
 */
@Module
abstract class EditAttendanceFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: EditAttendanceViewModelFactory
        ): EditAttendanceViewModel {
            return ViewModelProvider(fragment, factory)[EditAttendanceViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: EditAttendanceFragment): Fragment
}
