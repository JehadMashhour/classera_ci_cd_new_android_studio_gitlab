package com.classera.attendance

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.custom.views.QuickFilterView
import com.classera.core.fragments.BaseFragment
import com.classera.data.models.attendance.filter.AbsencesFilter
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.filter.FilterActivity
import javax.inject.Inject


@Screen("Take Attendance")
class AttendanceFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: AttendanceViewModel

    private var menuItemFilter: MenuItem? = null
    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var adapter: AttendanceAdapter? = null
    private var errorView: ErrorView? = null
    private var filterView: QuickFilterView? = null

    private var menuItem: Menu? = null

    override val layoutId: Int = R.layout.fragment_attendance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_attendance, menu)
        this.menuItem = menu
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        menuItemFilter = menu.findItem(R.id.item_menu_fragment_attendance_filter)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_fragment_attendance_filter -> {
                FilterActivity.start(this, initializeFilter()?.toTypedArray(), viewModel.selectedFilter)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initializeFilter(): List<AbsencesFilter?>? {
        val filters = mutableListOf<AbsencesFilter>()

        filters.add(0, AbsencesFilter("1",
            context?.getString(R.string.action_row_take_attendance_excused),
            false))

        filters.add(0, AbsencesFilter("0",
            context?.getString(R.string.label_attendance_fragment_unexcused),
            false)
        )
        return filters
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (FilterActivity.isDone(requestCode, resultCode, data)) {
            val filter = FilterActivity.getSelectedFilter(data)
            swipeRefreshLayout?.isRefreshing = true
            viewModel.getAbsencesListByFilter(filter)?.observe(this, ::handleResource)
            menuItem?.getItem(0)?.setIcon(R.drawable.menu_item_with_badge)
        } else if (FilterActivity.isAll(requestCode, resultCode, data)) {
            viewModel.selectedFilter = null
            adapter?.resetPaging()
            getAttendance()
            menuItem?.getItem(0)?.setIcon(R.drawable.ic_filter)
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initFilter()
        initListeners()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_assignments)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_assignments)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_assignments)
        errorView = view?.findViewById(R.id.error_view_fragment_assignments)
        filterView = view?.findViewById(R.id.filter_view_fragment_assignments)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            adapter?.resetPaging()
            getAttendance()
        }
        getAttendance()
    }

    private fun getAttendance(pageNumber: Int = DEFAULT_PAGE) {
        viewModel.getAbsencesList(filterView?.getSelectedFilterKey(), pageNumber)
            .observe(this, this::handleResource)


    }

    private fun initFilter() {
        filterView?.setAdapter(R.array.attendance_filter_entries, R.array.attendance_filter_entry_values)
        filterView?.setOnFilterSelectedListener {
            adapter?.resetPaging()
            recyclerView?.scrollToPosition(0)
            getAttendance(1)
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        menuItemFilter?.isEnabled = true
        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = AttendanceAdapter(viewModel)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreListener(::getAttendance)
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }

        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getAttendance() }
        adapter?.finishLoading()
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        filterView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }
}
