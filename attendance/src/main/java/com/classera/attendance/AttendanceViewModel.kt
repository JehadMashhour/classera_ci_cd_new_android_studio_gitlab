package com.classera.attendance

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.attendance.Absences
import com.classera.data.models.attendance.filter.AbsencesFilter
import com.classera.data.models.filter.Filterable
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.attendance.AttendanceRepository
import kotlinx.coroutines.Dispatchers

class AttendanceViewModel(private val attendanceRepository: AttendanceRepository) :
    BaseViewModel() {

    private var attendanceList: MutableList<Absences?> = mutableListOf()
    var selectedFilter: Filterable? = null

    private var filterKey: String? = null
    private var text: CharSequence? = null

    fun getAbsencesList(filterKey: String?, pageNumber: Int): LiveData<Resource> {
        this.filterKey = filterKey
        return getAbsencesList(
            filterKey,
            pageNumber,
            selectedFilter?.filterId ?: DEFAULT_FILTER_VALUE,
            showProgress = pageNumber == DEFAULT_PAGE
        )
    }

    fun getAbsencesListByFilter(filter: Filterable?): LiveData<Resource>? {
        if (filter == selectedFilter) {
            return null
        }
        this.selectedFilter = filter
        return getAbsencesList(filterKey, DEFAULT_PAGE, filter?.filterId, false)
    }

    private fun getAbsencesList(
        filter: String?,
        pageNumber: Int,
        excusedFilter: String?,
        showProgress: Boolean
    ) = liveData(Dispatchers.IO) {
        if (showProgress) {
            emit(Resource.Loading(show = true))
        }

        val resource = tryResource {
            attendanceRepository.getAbsences(filter, pageNumber, excusedFilter)
        }
        if (pageNumber == DEFAULT_PAGE) {
            attendanceList.clear()
        }
        attendanceList.addAll(
            resource.element<BaseWrapper<List<Absences>>>()?.data ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getAttendance(position: Int): Absences? {
        return attendanceList.get(position)
    }

    fun getAttendancesCount(): Int {
        return attendanceList.size
    }

    companion object {
        const val DEFAULT_FILTER_VALUE = "2"
    }
}

