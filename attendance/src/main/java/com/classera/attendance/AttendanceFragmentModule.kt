package com.classera.attendance

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class AttendanceFragmentModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AttendanceViewModelFactory
        ): AttendanceViewModel {
            return ViewModelProvider(fragment, factory)[AttendanceViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AttendanceFragment): Fragment
}

