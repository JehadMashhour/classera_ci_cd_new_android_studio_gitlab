package com.classera.callchildren.guardian

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.classera.data.models.callchildren.BaseStudent

/**
 * Project: Classera
 * Created: 8/19/2021
 *
 * @author Jehad Abdalqader
 */
class BaseStudentDiffCallback(
    private val oldList: List<BaseStudent>,
    private val newList: List<BaseStudent>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return oldList[oldPosition] == newList[newPosition]
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}
