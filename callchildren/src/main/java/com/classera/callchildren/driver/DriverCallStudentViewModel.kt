package com.classera.callchildren.driver
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.callchildren.base.BaseCallStudentViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.callchildren.ChildStudent
import com.classera.data.models.callchildren.DriverStudent
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.callchild.CallChildRepository
import kotlinx.coroutines.Dispatchers


class DriverCallStudentViewModel(
    private val callChildRepository: CallChildRepository,
    private val prefs: Prefs
) : BaseCallStudentViewModel(callChildRepository, prefs) {


    override fun getStudents(): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { callChildRepository.getPersonalDriverStudents() }

        setNewChildList(resource.element<BaseWrapper<List<DriverStudent>>>()?.data ?: mutableListOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }


}
