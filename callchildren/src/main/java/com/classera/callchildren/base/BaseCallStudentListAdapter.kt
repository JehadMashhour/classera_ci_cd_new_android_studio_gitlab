package com.classera.callchildren.base

import com.classera.core.adapter.BasePagingAdapter
import com.classera.core.adapter.BaseViewHolder

abstract class BaseCallStudentListAdapter<V : BaseViewHolder> : BasePagingAdapter<V>()
