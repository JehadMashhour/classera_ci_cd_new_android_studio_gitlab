package com.classera.callchildren.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.recyclerview.widget.DiffUtil
import com.classera.callchildren.guardian.BaseStudentDiffCallback
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.callchildren.BaseStudent
import com.classera.data.models.callchildren.CallStatus
import com.classera.data.models.callchildren.CallStatusTypes
import com.classera.data.models.callchildren.Request
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.callchild.CallChildRepository
import kotlinx.coroutines.Dispatchers

abstract class BaseCallStudentViewModel(
    private val callChildRepository: CallChildRepository,
    private val prefs: Prefs
) : BaseViewModel() {

    private var childList: MutableList<BaseStudent> = mutableListOf()

    private var baseStudentDiffResult: DiffUtil.DiffResult? = null

    abstract fun getStudents(): LiveData<Resource>

    open fun getChild(position: Int): BaseStudent {
        return childList[position]
    }

    open fun setChild(position: Int, baseStudent: BaseStudent) {
        childList[position] = baseStudent
    }

    open fun getChildListCount(): Int {
        return childList.size
    }

    protected fun setNewChildList(newChildList: List<BaseStudent>) {
        val baseStudentDiffCallback = BaseStudentDiffCallback(childList, newChildList)
        baseStudentDiffResult = DiffUtil.calculateDiff(baseStudentDiffCallback)

        childList.clear()
        childList.addAll(newChildList)
    }

    fun getBaseStudentDiffResult() = baseStudentDiffResult

    fun callChild(userId: String?): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { callChildRepository.callChild(userId) }
        emit(resource)
        emit(Resource.Loading(show = false))
    }


    fun getCallStatus(requestId: String?): LiveData<Resource> =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = true))
            val resource =
                tryResource { callChildRepository.getCallStatus(requestId) }
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun changeCallStatus(requestId: String?, status: String?): LiveData<Resource> =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = true))
            val resource =
                tryNoContentResource { callChildRepository.changeCallStatus(requestId, status) }
            emit(resource)
            emit(Resource.Loading(show = false))
        }
}
