package com.classera.settings

import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.settings.SettingsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SettingsViewModel(
    private val settingsRepository: SettingsRepository,
    private val prefs: Prefs
) : BaseViewModel() {

    fun changeUserLanguage(language: String) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { settingsRepository.changeLanguage(language) }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun notificationToggles() = liveData {
        emit(Resource.Loading(show = true))
        val resource = tryNoContentResource { settingsRepository.notificationToggle() }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun checkNotificationStatus() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { settingsRepository.getNotificationStatus() }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun isGlobalSchool(): Boolean {
        val schoolType = prefs.schoolType
        schoolType?.let {
            if (it == GLOBAL_SCHOOL_TYPE)
                return true
        }
        return false
    }

    private companion object {
        private const val GLOBAL_SCHOOL_TYPE = "1"
    }

}
