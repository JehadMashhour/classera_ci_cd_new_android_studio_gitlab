@file:Suppress("DEPRECATION")

package com.classera.settings

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.preference.ListPreference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import com.classera.core.Activities
import com.classera.core.Screen
import com.classera.core.intentTo
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.settings.NotificationStatus
import com.classera.data.network.errorhandling.Resource
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Created by Odai Nazzal on 1/11/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
@Screen("Settings")
class SettingsFragment : PreferenceFragmentCompat() {

    @Inject
    lateinit var viewModel: SettingsViewModel

    private var progressDialog: ProgressDialog? = null

    private var listPreferenceLanguage: ListPreference? = null
    private var switchPreferenceNotifications: SwitchPreference? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.fragment_settings, rootKey)
        initProgressDialog()
        findPreferences()
        initListeners()
        checkNotificationStatus()
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(requireContext())
        progressDialog?.setMessage(context?.getString(R.string.please_wait))
    }

    private fun findPreferences() {
        listPreferenceLanguage = findPreference("key_fragment_settings_language")
        switchPreferenceNotifications = findPreference("key_fragment_settings_notification")
        listPreferenceLanguage?.isVisible = !viewModel.isGlobalSchool()
    }

    private fun initListeners() {
        listPreferenceLanguage?.setOnPreferenceChangeListener { _, value ->
            val language = if (value == "ar") {
                "ara"
            } else {
                "eng"
            }
            viewModel.changeUserLanguage(language).observe(this, this::handleResource)
            return@setOnPreferenceChangeListener true
        }
        switchPreferenceNotifications?.setOnPreferenceChangeListener { _, newValue ->
            viewModel.notificationToggles().observe(this, ::handleNotificationResource)
            return@setOnPreferenceChangeListener true
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLanguageLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleLanguageSuccessResource()
            }
            is Resource.Error -> {
                handleLanguageErrorResource(resource)
            }
        }
    }

    private fun handleLanguageLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.hide()
        }
    }

    private fun handleLanguageSuccessResource() {
        val intent = intentTo(Activities.Splash)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun handleLanguageErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.resourceMessage, Toast.LENGTH_LONG).show()
    }

    private fun handleNotificationResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleNotificationLoadingResource(resource)
            }
            is Resource.Error -> {
                handleNotificationErrorResource(resource)
            }
        }
    }

    private fun handleNotificationLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.hide()
        }
    }

    private fun handleNotificationErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.resourceMessage, Toast.LENGTH_LONG).show()
        switchPreferenceNotifications?.isChecked = !(switchPreferenceNotifications?.isChecked ?: false)
    }

    private fun checkNotificationStatus() {
        viewModel.checkNotificationStatus().observe(this, this::handleNotificationStatusResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleNotificationStatusResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<NotificationStatus>>)
            }
        }
    }

    private fun handleSuccessResource(success: Resource.Success<BaseWrapper<NotificationStatus>>) {
        success.data?.data?.enabled?.let { enabled ->
            switchPreferenceNotifications?.isChecked = enabled
        }
    }
}
